# C4-Cassandra #

The Cassandra front-end currently uses a slightly outdated version of C4, which
is still called "ECChecker" (later renamed to C4).

### Additional Dependencies ###

(1) Install Apache Maven

- Follow the instructions on `https://maven.apache.org/install.html`.

(2) (optional) Install Grahphviz (for exporting graphs)

- Follow the instructions on `https://www.graphviz.org/download/`
- Set the environment variable `GRAPHVIZ_HOME` to the main directory of the GraphViz installation

### Building `ecchecker` ###

First, `ecchecker` needs to be exported to Maven. This requires maven to be installed.

Now run: `sbt publish-m2`

### Building `ecchecker-cassandra` ###

It is sufficient to run 

`mvn -DskipTests package`

in the main directory of `ecchecker-cassandra`. Note that the tests are expected to fail.

### Producing abstract histories ###

The subdirectory `examples/` contains all Cassandra benchmarks used in the
paper, as well as `simple-twitter`, which is a test application we wrote
ourselves.

The directory contains a script `runOnExamples.sh`, which, when run, produces
all abstract histories, and invokes ECChecker on them. The abstract histories,
in JSON format, the input to C4, are contained in directory
`output/*/ECGraph/*`.

### Manual inspection of produced warnings ###

`examples/classifications` contains the classification of all reported alarms, together with 
some explanation of why the bug is considered harmful/harmless. Other than the paper, it 
also lists "warnings", which we grouped together with other harmless violations in the paper.

### More information ###

How to run ECChecker-Cassandra on other examples, the folder structure of the
code, and all ways to tune the analysis are described in more detail in the
`README.md` file.

