#bin/bash

# Runs the evaluation on the examples.

# Build
mvn -f ../pom.xml -DskipTests clean package

# Execute the analysis
java -cp ../target/ecchecker-cassandra-0.0.1-SNAPSHOT-jar-with-dependencies.jar ch.ethz.inf.pm.ecchecker.cassandra.Evaluation output/evaluation.csv \
"cassandra-twitter;jars/cassandra-twitter.jar;schema/cassandra-twitter-schema.cql" \
"cassatwitter;jars/cassatwitter.jar;schema/cassatwitter-schema.cql" \
"currency-exchange;jars/currency-exchange.jar;schema/currency-exchange-schema.cql" \
"killrchat;jars/killrchat.jar;schema/killrchat-schema.cql" \
"playlist;jars/playlist.jar;schema/playlist-schema.cql" \
"roomstore;jars/roomstore.jar;schema/roomstore-schema.cql" \
"simple-twitter;jars/simple-twitter.jar;schema/simple-twitter-schema.cql" \
"twissandra;jars/twissandra.jar;schema/twissandra-schema.cql" \
"cassieq;jars/cassieq-core.jar;schema/cassieq-core-schema.cql;io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#ackMessage;io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#createQueue;io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#deleteQueue;io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#getAccountQueues;io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#getMessage;io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#getQueueDefinition;io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#getQueueStatistics;io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#putMessage;io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#updateMessage;io.paradoxical.cassieq.workers.QueueDeleter#delete"