package com.datastax.demo.killrchat.resource;

import com.datastax.demo.killrchat.model.UserModel;
import com.datastax.demo.killrchat.service.UserService;

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;

import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/users")
public class UserResource {

    @Inject
    private UserService service;

    @Inject
    private PasswordEncoder passwordEncoder;

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Transaction
    public void createUser(@NotNull @RequestBody @Valid UserModel model) {
        final String login = model.getLogin();
        ClientLocalValues.set("login", login);
        final String password = passwordEncoder.encode(model.getPassword());
        final UserModel fixedModel = new UserModel(login, password, model.getFirstname(), model.getLastname(), model.getEmail(), model.getBio());
        service.createUser(fixedModel);
    }

    @RequestMapping(value = "/{login:.+}", method = GET, produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    @Transaction
    public UserModel findByLogin(@PathVariable String login) {
    	ClientLocalValues.set("login", login);
        return service.findByLogin(login).toModel();
    }

}
