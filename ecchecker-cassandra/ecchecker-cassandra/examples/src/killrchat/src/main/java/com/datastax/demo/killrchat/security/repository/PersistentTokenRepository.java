package com.datastax.demo.killrchat.security.repository;

import com.datastax.demo.killrchat.entity.PersistentTokenEntity;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import info.archinnov.achilles.persistence.PersistenceManager;
import info.archinnov.achilles.type.OptionsBuilder;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;

import static info.archinnov.achilles.type.OptionsBuilder.withTtl;

import java.util.Iterator;

@Repository
public class PersistentTokenRepository {

    @Inject
    private Session session;

    public static final int TOKEN_VALIDITY_DAYS = 31;

    public static final int TOKEN_VALIDITY_SECONDS = 60 * 60 * 24 * TOKEN_VALIDITY_DAYS;

    public void insert(PersistentTokenEntity token) {
        session.execute("INSERT INTO security_tokens (series, token_date, authorities, user_agent, token_value, login, pass, ip_address) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
        		token.getSeries(), token.getTokenDate(), token.getAuthorities(), token.getUserAgent(), token.getTokenValue(), token.getLogin(), token.getPass(), token.getIpAddress(), token.getSeries());
    }

    public void deleteById(String series) {
    	session.execute("DELETE FROM security_tokens WHERE series = ?", series);
    }

    public PersistentTokenEntity findById(String presentedSeries) {
    	final Iterator<Row> rowIt = session.execute("SELECT series, token_date, authorities, user_agent, token_value, login, pass, ip_address FROM security_tokens WHERE series = ?", presentedSeries).iterator();
    	if (rowIt.hasNext()){
    		final Row row = rowIt.next();
    		final PersistentTokenEntity ret = new PersistentTokenEntity();
    		ret.setSeries(row.getString("series"));
    		//TODO: add all values
    		return ret;
    	} else {
    		return null;
    	}
    }

    public void update(PersistentTokenEntity token) {
    	session.execute("UPDATE security_tokens USING TTL = ? SET token_date = ?, authorities = ?, user_agent = ?, token_value = ?, login = ?, pass = ?, ip_address = ? WHERE series = ?", 
    			TOKEN_VALIDITY_SECONDS, token.getTokenDate(), token.getAuthorities(), token.getUserAgent(), token.getTokenValue(), token.getLogin(), token.getPass(), token.getIpAddress(), token.getSeries());
    }
}
