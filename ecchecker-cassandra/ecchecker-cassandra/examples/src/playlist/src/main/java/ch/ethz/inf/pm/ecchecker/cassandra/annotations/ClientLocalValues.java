package ch.ethz.inf.pm.ecchecker.cassandra.annotations;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ClientLocalValues {

	private static Map<String, Object> values = new ConcurrentHashMap<String, Object>();

	public static <T> void set(final String id, final T value) {
		values.put(id, value);
	}

	@SuppressWarnings("unchecked")
	public static <T> T get(final String id) {
		return (T) values.get(id);
	}
}
