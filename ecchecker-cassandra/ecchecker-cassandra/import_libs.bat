call mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file ^
  -Dfile=../../soot/soot-2016-12-06.jar ^
  -Dsources=../../soot/soot-2016-12-06-sources.jar ^
  -DgroupId=soot ^
  -DartifactId=soot ^
  -Dversion=20161206 ^
  -Dpackaging=jar ^
  -DlocalRepositoryPath=lib
  
call mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file ^
  -Dfile=../ecchecker/target/scala-2.11/ecchecker_2.11-1.0.jar ^
  -Dsources=../ecchecker/target/scala-2.11/ecchecker_2.11-1.0-sources.jar ^
  -Dpackaging=jar ^
  -DlocalRepositoryPath=lib ^
  -DgeneratePom=false ^
  -DpomFile=ecchecker-pom.xml