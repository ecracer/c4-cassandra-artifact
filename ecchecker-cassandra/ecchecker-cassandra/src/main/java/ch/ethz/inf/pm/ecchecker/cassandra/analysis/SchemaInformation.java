package ch.ethz.inf.pm.ecchecker.cassandra.analysis;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

/**
 * Represents the information that is known about the schema of the cassandra database
 */
public class SchemaInformation {

	/** contains entry table,column for all columns that are used somewhere explicitely */
	private final ImmutableSetMultimap<String, String> allWrittenColumns;
	/** contains a boolean for each table-column whether it is specified as a primary key */
	private final ImmutableMap<String, Boolean> primaryKeyColumns;
	/** Set of table-column that is never read in a statement */
	private final ImmutableSet<String> columnsNeverRead;
	/** columns that are updated in some statement */
	private final ImmutableSetMultimap<String, String> updatedColumns;
	/** tables where no rows are deleted */
	private final ImmutableSet<String> tablesWithoutDeletes;

	private SchemaInformation(final ImmutableSetMultimap<String, String> allWrittenColumns,
			final ImmutableMap<String, Boolean> primaryKeyColumns, final ImmutableSet<String> columnsNeverRead,
			final ImmutableSetMultimap<String, String> updatedColumns, final ImmutableSet<String> tablesWithoutDeletes) {
		this.allWrittenColumns = Objects.requireNonNull(allWrittenColumns);
		this.primaryKeyColumns = Objects.requireNonNull(primaryKeyColumns);
		this.columnsNeverRead = Objects.requireNonNull(columnsNeverRead);
		this.updatedColumns = Objects.requireNonNull(updatedColumns);
		this.tablesWithoutDeletes = Objects.requireNonNull(tablesWithoutDeletes);
	}

	public static SchemaInformation create() {
		return new SchemaInformation(ImmutableSetMultimap.of(), ImmutableMap.of(), ImmutableSet.of(), ImmutableSetMultimap.of(),
				ImmutableSet.of());
	}

	public static Builder builder() {
		return new Builder();
	}

	private static String createMapKey(final String table, final String column) {
		return Objects.requireNonNull(table) + "_" + Objects.requireNonNull(column);
	}

	public Set<String> getWrittenColumns(final String table) {
		return allWrittenColumns.get(table);
	}

	public Boolean isPartOfPrimaryKey(final String table, final String column) {
		return primaryKeyColumns.get(createMapKey(table, column));
	}

	public boolean isColumnNeverRead(final String table, final String column) {
		return columnsNeverRead.contains(createMapKey(table, column));
	}

	public boolean updatesAllColumnsEverUpdated(final String table, final Set<String> columns) {
		return updatedColumns.containsKey(table) && columns.containsAll(updatedColumns.get(table));
	}

	public boolean doDeletesHappenOnTable(final String table) {
		return !tablesWithoutDeletes.contains(table);
	}

	@Override
	public String toString() {
		return "SchemaInformation [allWrittenColumns=" + allWrittenColumns + ", primaryKeyColumns=" + primaryKeyColumns
				+ ", columnsNeverRead=" + columnsNeverRead + ", updatedColumns=" + updatedColumns + ", tablesWithoutDeletes="
				+ tablesWithoutDeletes + "]";
	}

	public static class Builder {

		private ImmutableSetMultimap.Builder<String, String> allWrittenColumns = ImmutableSetMultimap.builder();
		private Map<String, Boolean> primaryKeyColumns = new HashMap<>();
		private ImmutableSet.Builder<String> columnsNeverRead = ImmutableSet.builder();
		private ImmutableSetMultimap.Builder<String, String> updatedColumns = ImmutableSetMultimap.builder();
		private ImmutableSet.Builder<String> tablesWithoutDeletes = ImmutableSet.builder();

		private Builder() {
		}

		public Builder addColumnsWritten(final String table, final Set<String> columns) {
			allWrittenColumns.putAll(table, columns);
			return this;
		}

		public Builder addPrimaryKeyColumn(final String table, final String column) {
			addColumn(table, column, true);
			return this;
		}

		public Builder addNonPrimaryKeyColumn(final String table, final String column) {
			addColumn(table, column, false);
			return this;
		}

		private void addColumn(final String table, final String column, final boolean isPrimaryKey) {
			final String key = createMapKey(table, column);
			if (primaryKeyColumns.containsKey(key)) {
				if (isPrimaryKey != primaryKeyColumns.get(key)) {
					throw new IllegalArgumentException(table + "_" + column + " already set");
				}
			} else {
				primaryKeyColumns.put(key, isPrimaryKey);
			}
		}

		public Builder addNeverReadColumn(final String table, final String column) {
			columnsNeverRead.add(createMapKey(table, column));
			return this;
		}

		public Builder addUpdatedColumns(final String table, final Set<String> columns) {
			updatedColumns.putAll(table, columns);
			return this;
		}

		public Builder addTableWithoutDelete(final String table) {
			tablesWithoutDeletes.add(table);
			return this;
		}

		public SchemaInformation build() {
			return new SchemaInformation(allWrittenColumns.build(), ImmutableMap.copyOf(primaryKeyColumns),
					columnsNeverRead.build(), updatedColumns.build(), tablesWithoutDeletes.build());
		}
	}
}
