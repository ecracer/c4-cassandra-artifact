package ch.ethz.inf.pm.ecchecker.cassandra.analysis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.SetMultimap;

import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.ParsedSessionExecuteInvoke;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.CassandraParser;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ParsedCreateTableStatement;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.DeletePart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.QueryPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.UpdatePart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.UpsertPart;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.TransactionGraph;

/**
 * Collects the schema information that can be extracted from the transaction graphs
 * <ul>
 * <li>If a create schema file is specified, the file is parsed</li>
 * <li>All transactions are scanned for columns written, tables without deletes, ...</li>
 * </ul>
 */
public class SchemaInformationTransformer {

	private final static Logger LOG = LogManager.getLogger(SchemaInformationTransformer.class);

	private final Options options;

	public SchemaInformationTransformer(final Options options) {
		this.options = Objects.requireNonNull(options);
	}

	public SchemaInformation collect(final ImmutableMap<TransactionDescriptor, TransactionGraph> transactionGraphs) {

		final SchemaInformation.Builder builder = SchemaInformation.builder();

		collectAllWrittenColumns(transactionGraphs, builder);

		if (options.getCreateSchemaFile() != null) {
			readCreateSchemaFile(options.getCreateSchemaFile(), builder);
		}

		for (final Entry<TransactionDescriptor, TransactionGraph> entry : transactionGraphs.entrySet()) {
			for (final ParsedSessionExecuteInvoke parsedExecute : entry.getValue().nodes()) {
				for (final StatementPart part : parsedExecute.statement.getStatementParts()) {
					if (part instanceof UpdatePart) {
						// constraints in an UpdateStatement can only refer to PK-Columns
						final Set<Integer> constraintIdxs;
						if (part instanceof DeletePart) {
							constraintIdxs = new HashSet<>(((DeletePart) part).constraints.getConstraintIdxsPerColumn().values());
						} else {
							final UpsertPart upsPart = (UpsertPart) part;
							constraintIdxs = new HashSet<>(upsPart.components.getPossibleConstraintIdxs().values());
							constraintIdxs.removeAll(upsPart.components.getPossiblyUpdatedColumnIdxs().values());
						}
						for (final Integer constraintIdx : constraintIdxs) {
							builder.addPrimaryKeyColumn(part.table, part.getConstraint(constraintIdx).column);
						}
						// updates in an UpdateStatement can only refer to non-PK-Columns
						if (part instanceof UpsertPart) {
							final UpsertPart upsPart = (UpsertPart) part;
							final Set<Integer> columnIdxs = new HashSet<>(upsPart.components.getPossiblyUpdatedColumnIdxs().values());
							columnIdxs.removeAll(upsPart.components.getPossibleConstraintIdxs().values());
							for (final Integer columnIdx : columnIdxs) {
								builder.addNonPrimaryKeyColumn(part.table, upsPart.components.getColumnUpdate(columnIdx).column);
							}
						}
					}
				}
			}
		}

		addColumnsNeverReadInformation(transactionGraphs, builder);
		addTablesWithoutDeletesInformation(transactionGraphs, builder);

		return builder.build();
	}

	private void collectAllWrittenColumns(final ImmutableMap<TransactionDescriptor, TransactionGraph> transactionGraphs,
			final SchemaInformation.Builder builder) {

		for (final Entry<TransactionDescriptor, TransactionGraph> entry : transactionGraphs.entrySet()) {
			for (final ParsedSessionExecuteInvoke parsedExecute : entry.getValue().nodes()) {
				for (final StatementPart part : parsedExecute.statement.getStatementParts()) {
					if (part instanceof UpdatePart) {
						if (part instanceof DeletePart) {
							builder.addColumnsWritten(part.table, ((DeletePart) part).constraints.getConstraintIdxsPerColumn().keySet());
						} else {
							builder.addColumnsWritten(part.table, ((UpsertPart) part).components.getPossiblyUpdatedColumnIdxs().keySet());
							builder.addColumnsWritten(part.table, ((UpsertPart) part).components.getPossibleConstraintIdxs().keySet());
						}
					}
				}
			}
		}
	}

	private void readCreateSchemaFile(final String createSchemaFile, final SchemaInformation.Builder builder) {
		BufferedReader reader = null;
		try {
			final File schemaFile = new File(createSchemaFile);
			if (!schemaFile.exists()) {
				LOG.warn("Schema File is missing");
			}
			reader = new BufferedReader(new FileReader(schemaFile));
			final StringBuilder createSchemaContent = new StringBuilder();
			String nextLine;
			while ((nextLine = reader.readLine()) != null) {
				createSchemaContent.append(nextLine).append(" ");
			}
			for (final String createSchemaPart : createSchemaContent.toString().split(";")) {
				final ParsedCreateTableStatement createTable = CassandraParser.tryParseCreateTable(createSchemaPart);
				if (createTable != null) {
					for (final String pkCol : createTable.primaryKeyColumns) {
						builder.addPrimaryKeyColumn(createTable.table, pkCol);
					}
					for (final String nonPkCol : createTable.nonPrimaryKeyColumns) {
						builder.addNonPrimaryKeyColumn(createTable.table, nonPkCol);
					}
				}
			}
		} catch (IOException e) {
			LOG.error("Error reading schema file", e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
				}
			}
		}
	}

	private void addColumnsNeverReadInformation(final ImmutableMap<TransactionDescriptor, TransactionGraph> transactionGraphs,
			final SchemaInformation.Builder builder) {

		final SetMultimap<String, String> neverReadColumns = HashMultimap.create();

		// add all updated columns as never read
		for (final Entry<TransactionDescriptor, TransactionGraph> entry : transactionGraphs.entrySet()) {
			for (final ParsedSessionExecuteInvoke parsedExecute : entry.getValue().nodes()) {
				for (final StatementPart part : parsedExecute.statement.getStatementParts()) {
					if (part instanceof UpsertPart) {
						final UpsertPart upsPart = (UpsertPart) part;
						neverReadColumns.putAll(part.table, upsPart.components.getPossiblyUpdatedColumnIdxs().keys());
						builder.addUpdatedColumns(part.table, upsPart.components.getPossiblyUpdatedColumnIdxs().keySet());
					}
				}
			}
		}

		// remove queried columns from never read set
		for (final Entry<TransactionDescriptor, TransactionGraph> entry : transactionGraphs.entrySet()) {
			for (final ParsedSessionExecuteInvoke parsedExecute : entry.getValue().nodes()) {
				for (final StatementPart part : parsedExecute.statement.getStatementParts()) {
					if (part instanceof QueryPart) {
						final QueryPart queryPart = (QueryPart) part;
						neverReadColumns.get(queryPart.table).removeAll(queryPart.getPossibleConstraintIdxs().keySet());
						final Iterator<String> columnIt = neverReadColumns.get(queryPart.table).iterator();
						while (columnIt.hasNext()) {
							final String next = columnIt.next();
							if (!Boolean.FALSE.equals(queryPart.isSelectingColumn(next)) && !queryPart.isDisplayColumn(next)) {
								columnIt.remove();
							}
						}

					}
				}
			}
		}

		for (final Entry<String, String> neverReadColumn : neverReadColumns.entries()) {
			builder.addNeverReadColumn(neverReadColumn.getKey(), neverReadColumn.getValue());
		}
	}

	private void addTablesWithoutDeletesInformation(final ImmutableMap<TransactionDescriptor, TransactionGraph> transactionGraphs,
			final SchemaInformation.Builder builder) {

		final Set<String> tablesWithoutDeletes = new HashSet<>();

		// add all existing tables
		for (final Entry<TransactionDescriptor, TransactionGraph> entry : transactionGraphs.entrySet()) {
			for (final ParsedSessionExecuteInvoke parsedExecute : entry.getValue().nodes()) {
				for (final StatementPart part : parsedExecute.statement.getStatementParts()) {
					tablesWithoutDeletes.add(part.table);
				}
			}
		}

		// remove tables with deletes
		for (final Entry<TransactionDescriptor, TransactionGraph> entry : transactionGraphs.entrySet()) {
			for (final ParsedSessionExecuteInvoke parsedExecute : entry.getValue().nodes()) {
				for (final StatementPart part : parsedExecute.statement.getStatementParts()) {
					if (part instanceof DeletePart) {
						tablesWithoutDeletes.remove(part.table);
					}
				}
			}
		}

		for (final String tableWithoutDelete : tablesWithoutDeletes) {
			builder.addTableWithoutDelete(tableWithoutDelete);
		}
	}
}
