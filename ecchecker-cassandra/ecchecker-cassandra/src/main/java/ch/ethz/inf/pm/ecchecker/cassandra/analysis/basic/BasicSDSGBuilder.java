package ch.ethz.inf.pm.ecchecker.cassandra.analysis.basic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableMap;
import com.google.common.graph.ImmutableNetwork;
import com.google.common.graph.ImmutableValueGraph;
import com.google.common.graph.MutableNetwork;
import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.Network;
import com.google.common.graph.NetworkBuilder;
import com.google.common.graph.ValueGraphBuilder;

import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.SchemaInformation;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.basic.AbstractEdge.ArbitrationEdge;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.basic.AbstractEdge.DependencyEdge;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.CommutativityAbsorptionExprGenerator;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.ParsedSessionExecuteInvoke;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.QueryPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.UpdatePart;

/**
 * Builds an over-approximation of the depedency serialization graph
 */
public class BasicSDSGBuilder {

	private final CommutativityAbsorptionExprGenerator commutativityHelper;
	public final ImmutableNetwork<TransactionDescriptor, AbstractEdge> basicSDSG;

	public BasicSDSGBuilder(final Map<TransactionDescriptor, Set<ParsedSessionExecuteInvoke>> transactionExecutes,
			final SchemaInformation schemaInformation, final Options options) {

		this.commutativityHelper = CommutativityAbsorptionExprGenerator.create(schemaInformation, options);
		this.basicSDSG = ImmutableNetwork.copyOf(buildBasicSDSG(transactionExecutes));
	}

	private final Network<TransactionDescriptor, AbstractEdge> buildBasicSDSG(
			final Map<TransactionDescriptor, Set<ParsedSessionExecuteInvoke>> transactionExecutes) {

		final MutableNetwork<TransactionDescriptor, AbstractEdge> basicSDSG = NetworkBuilder.undirected().allowsParallelEdges(true)
				.allowsSelfLoops(true).build();
		final List<TransactionDescriptor> transactions = new ArrayList<>(transactionExecutes.keySet());
		for (final TransactionDescriptor transactionDescr : transactions) {
			basicSDSG.addNode(transactionDescr);
		}

		for (int i = 0; i < transactions.size(); i++) {
			final TransactionDescriptor t1 = transactions.get(i);
			final Set<ParsedSessionExecuteInvoke> t1e = transactionExecutes.get(t1);
			for (int j = i; j < transactions.size(); j++) {
				final TransactionDescriptor t2 = transactions.get(j);
				final Set<ParsedSessionExecuteInvoke> t2e = transactionExecutes.get(t2);
				for (final AbstractEdge edge : getEdgesBetweenTransactions(t1, t1e, t2, t2e)) {
					basicSDSG.addEdge(t1, t2, edge);
				}
			}
		}

		return basicSDSG;
	}

	private Set<AbstractEdge> getEdgesBetweenTransactions(final TransactionDescriptor t1, final Set<ParsedSessionExecuteInvoke> t1e,
			final TransactionDescriptor t2, final Set<ParsedSessionExecuteInvoke> t2e) {
		if (t1e.isEmpty() || t2e.isEmpty()) {
			return Collections.emptySet();
		}
		final Set<AbstractEdge> edges = new HashSet<>();
		for (final ParsedSessionExecuteInvoke e1 : t1e) {
			for (ParsedSessionExecuteInvoke e2 : t2e) {
				for (final StatementPart part1 : e1.statement.getStatementParts()) {
					for (final StatementPart part2 : e2.statement.getStatementParts()) {
						if (!commutativityHelper.isAlwaysCommuting(part1, part2)) {
							if (part1 instanceof QueryPart && part2 instanceof UpdatePart) {
								edges.add(DependencyEdge.create(t2, t1));
							} else if (part1 instanceof UpdatePart && part2 instanceof QueryPart) {
								edges.add(DependencyEdge.create(t1, t2));
							} else if (part1 instanceof UpdatePart && part2 instanceof UpdatePart) {
								edges.add(ArbitrationEdge.create(t1, t2));
							}
						}
					}
				}
			}
		}
		return edges;
	}

	public ImmutableMap<TransactionDescriptor, ImmutableValueGraph<TransactionExecution, EdgeValue>> findCriticalCycles() {
		final ImmutableMap.Builder<TransactionDescriptor, ImmutableValueGraph<TransactionExecution, EdgeValue>> resultBuilder = ImmutableMap
				.builder();

		for (final TransactionDescriptor transaction : basicSDSG.nodes()) {
			final MutableValueGraph<TransactionExecution, EdgeValue> graph = ValueGraphBuilder.directed().allowsSelfLoops(false).build();
			final TransactionExecution e1 = TransactionExecution.create(transaction, 1);
			graph.addNode(e1);
			final Set<TransactionDescriptor> usedDescriptors = new HashSet<>();
			usedDescriptors.add(transaction);
			final ImmutableValueGraph<TransactionExecution, EdgeValue> criticalCycle = findCriticalCycle(e1, e1, graph,
					Integer.MAX_VALUE, 2, usedDescriptors, 0, 0);
			if (criticalCycle != null) {
				resultBuilder.put(transaction, criticalCycle);
			}
		}

		return resultBuilder.build();
	}

	private ImmutableValueGraph<TransactionExecution, EdgeValue> findCriticalCycle(final TransactionExecution initial,
			final TransactionExecution next, final MutableValueGraph<TransactionExecution, EdgeValue> graph, final int minSize,
			final int nextId, final Set<TransactionDescriptor> usedDescriptors, final int antiDepCount, final int arbitrationCount) {

		ImmutableValueGraph<TransactionExecution, EdgeValue> cycle = null;
		int newMinSize = minSize;
		for (final AbstractEdge edge : basicSDSG.incidentEdges(next.transaction)) {
			final TransactionDescriptor otherTransaction = edge.getOtherTransaction(next.transaction);
			final int newAntiDepCount;
			final int newArbitrationCount;
			final EdgeValue edgeValue;
			if (edge instanceof ArbitrationEdge) {
				newAntiDepCount = antiDepCount;
				newArbitrationCount = arbitrationCount + 1;
				edgeValue = EdgeValue.ARBITRATION;
			} else {
				final DependencyEdge dep = (DependencyEdge) edge;
				newArbitrationCount = arbitrationCount;
				if (dep.isAntiDependency(next.transaction)) {
					newAntiDepCount = antiDepCount + 1;
					edgeValue = EdgeValue.ANTI_DEPENDENCY;
				} else {
					newAntiDepCount = antiDepCount;
					edgeValue = EdgeValue.DEPENDENCY;
				}
			}
			if (isCriticalCycle(newAntiDepCount, newArbitrationCount)) {
				// critical cycle
				if (otherTransaction.equals(initial.transaction)) {
					graph.putEdgeValue(next, initial, edgeValue);
					final ImmutableValueGraph<TransactionExecution, EdgeValue> ret = ImmutableValueGraph.copyOf(graph);
					graph.removeEdge(next, initial);
					return ret;
				}
			}
			if (newMinSize <= graph.nodes().size() + 1) {
				continue;
			}
			final Set<TransactionDescriptor> newUsedDescriptors;
			if (((edgeValue == EdgeValue.ANTI_DEPENDENCY && isCriticalCycle(antiDepCount, arbitrationCount))
					|| (edgeValue == EdgeValue.ARBITRATION && (arbitrationCount > 1 || isCriticalCycle(antiDepCount, arbitrationCount)))
					|| edgeValue == EdgeValue.DEPENDENCY)) {
				if (usedDescriptors.contains(otherTransaction)) {
					continue;
				} else {
					newUsedDescriptors = new HashSet<>(usedDescriptors);
					newUsedDescriptors.add(otherTransaction);
				}
			} else {
				newUsedDescriptors = new HashSet<>();
				newUsedDescriptors.add(otherTransaction);
			}
			final TransactionExecution newExec = TransactionExecution.create(otherTransaction, nextId);
			graph.addNode(newExec);
			graph.putEdgeValue(next, newExec, edgeValue);
			final ImmutableValueGraph<TransactionExecution, EdgeValue> possibleCycle = findCriticalCycle(initial, newExec, graph,
					newMinSize,
					nextId + 1, newUsedDescriptors, newAntiDepCount, newArbitrationCount);
			if (possibleCycle != null) {
				cycle = possibleCycle;
				newMinSize = possibleCycle.nodes().size();
			}
			graph.removeEdge(next, newExec);
			graph.removeNode(newExec);
		}

		return cycle;
	}

	private static boolean isCriticalCycle(final int antiDepCount, final int arbitrationCount) {
		return antiDepCount >= 2 || (antiDepCount == 1 && arbitrationCount >= 1);
	}
}
