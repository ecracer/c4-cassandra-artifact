package ch.ethz.inf.pm.ecchecker.cassandra.analysis.basic;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.graph.ImmutableValueGraph;
import com.google.common.graph.Network;

import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.AbstractSerializabilityAnalysis;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.SchemaInformation;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.ParsedSessionExecuteInvoke;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.GraphToDotUtil;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.TransactionGraph;
import ch.ethz.inf.pm.ecchecker.cassandra.util.FileUtils;

/**
 * Checks the transactions for serializability violations building an over-approximation of the DSG. Reports for each
 * transaction a minimal critical cycle
 */
public class BasicSerializabilityAnalysis extends AbstractSerializabilityAnalysis {

	private final static Logger LOG = LogManager.getLogger(BasicSerializabilityAnalysis.class);

	public BasicSerializabilityAnalysis(final Options options) {
		super(options);
	}

	public void check(final Map<TransactionDescriptor, TransactionGraph> transactionGraphs, final SchemaInformation schemaInformation) {

		final Map<TransactionDescriptor, Set<ParsedSessionExecuteInvoke>> transactionExecutes = new HashMap<>();
		transactionGraphs.forEach((key, val) -> transactionExecutes.put(key, val.nodes()));
		final BasicSDSGBuilder basicSDSGBuilder = new BasicSDSGBuilder(transactionExecutes, schemaInformation, options);
		final Map<TransactionDescriptor, ImmutableValueGraph<TransactionExecution, EdgeValue>> criticalCycles = basicSDSGBuilder
				.findCriticalCycles();
		if (criticalCycles.isEmpty()) {
			System.out.println("No critical cycles found. Program is serializable");
		} else {
			System.out.println(criticalCycles.keySet().size() + " critical cycles found for transactions " + criticalCycles.keySet());
		}

		if (outputDir != null) {
			writeBasicSDSGGraph(basicSDSGBuilder.basicSDSG);
			writeBasicCriticalCycleGraphs(criticalCycles);
		}
	}

	private void writeBasicSDSGGraph(final Network<TransactionDescriptor, AbstractEdge> basicSDSG) {
		final File basicSDSGDir = new File(outputDir, "basicSDSG");
		FileUtils.createEmptyDirectory(basicSDSGDir);
		try {
			GraphToDotUtil.printBasicSDSGGraph(new File(basicSDSGDir, "01_sdsg.dot"), "Basic SDSG", basicSDSG);
		} catch (IOException e) {
			LOG.error("Error writing dot file", e);
		}
	}

	private void writeBasicCriticalCycleGraphs(
			final Map<TransactionDescriptor, ImmutableValueGraph<TransactionExecution, EdgeValue>> criticalCycles) {
		final File basicCriticalCyclesDir = new File(outputDir, "basicCriticalCycles");
		FileUtils.createEmptyDirectory(basicCriticalCyclesDir);
		for (final Entry<TransactionDescriptor, ImmutableValueGraph<TransactionExecution, EdgeValue>> entry : criticalCycles
				.entrySet()) {
			try {
				GraphToDotUtil.printBasicCriticalCycleGraph(
						new File(basicCriticalCyclesDir, entry.getKey().getShortName().replaceAll("\\W+", "") + ".dot"),
						entry.getKey().getShortSignature(), entry.getValue());
			} catch (IOException e) {
				LOG.error("Error writing dot file", e);
			}
		}
	}
}
