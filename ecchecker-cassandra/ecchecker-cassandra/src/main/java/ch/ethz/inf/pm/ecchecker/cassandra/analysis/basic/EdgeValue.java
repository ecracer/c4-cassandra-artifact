package ch.ethz.inf.pm.ecchecker.cassandra.analysis.basic;

public enum EdgeValue {
	DEPENDENCY, ARBITRATION, ANTI_DEPENDENCY
}
