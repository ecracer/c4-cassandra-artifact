package ch.ethz.inf.pm.ecchecker.cassandra.analysis.basic;

import java.util.Objects;

import ch.ethz.inf.pm.ecchecker.cassandra.beans.TransactionDescriptor;

/**
 * Represents a unique transaction
 */
public class TransactionExecution {

	public final TransactionDescriptor transaction;
	public final int id;

	private TransactionExecution(final TransactionDescriptor transaction, final int id) {
		this.transaction = Objects.requireNonNull(transaction);
		this.id = id;
	}

	public static TransactionExecution create(final TransactionDescriptor transaction, final int id) {
		return new TransactionExecution(transaction, id);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((transaction == null) ? 0 : transaction.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransactionExecution other = (TransactionExecution) obj;
		if (id != other.id)
			return false;
		if (transaction == null) {
			if (other.transaction != null)
				return false;
		} else if (!transaction.equals(other.transaction))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TransactionExecution [transaction=" + transaction + ", id=" + id + "]";
	}
}
