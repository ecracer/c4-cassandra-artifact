package ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker;

public class ECCheckerResult {

	private int possibleViolationsSize2 = 0;
	private int possibleViolationsSize3 = 0;
	private int possibleViolationsSize4 = 0;

	private int verifiedViolationsSize2 = 0;
	private int verifiedViolationsSize3 = 0;
	private int verifiedViolationsSize4 = 0;

	private int totalPossibleViolationsSize4 = 0;
	private int totalVerifiedViolationsSize4 = 0;

	public int getPossibleViolationsSize2() {
		return possibleViolationsSize2;
	}

	public void setPossibleViolationsSize2(int possibleViolationsSize2) {
		this.possibleViolationsSize2 = possibleViolationsSize2;
	}

	public int getPossibleViolationsSize3() {
		return possibleViolationsSize3;
	}

	public void setPossibleViolationsSize3(int possibleViolationsSize3) {
		this.possibleViolationsSize3 = possibleViolationsSize3;
	}

	public int getPossibleViolationsSize4() {
		return possibleViolationsSize4;
	}

	public void setPossibleViolationsSize4(int possibleViolationsSize4) {
		this.possibleViolationsSize4 = possibleViolationsSize4;
	}

	public int getVerifiedViolationsSize2() {
		return verifiedViolationsSize2;
	}

	public void setVerifiedViolationsSize2(int verifiedViolationsSize2) {
		this.verifiedViolationsSize2 = verifiedViolationsSize2;
	}

	public int getVerifiedViolationsSize3() {
		return verifiedViolationsSize3;
	}

	public void setVerifiedViolationsSize3(int verifiedViolationsSize3) {
		this.verifiedViolationsSize3 = verifiedViolationsSize3;
	}

	public int getVerifiedViolationsSize4() {
		return verifiedViolationsSize4;
	}

	public void setVerifiedViolationsSize4(int verifiedViolationsSize4) {
		this.verifiedViolationsSize4 = verifiedViolationsSize4;
	}

	public int getTotalPossibleViolationsSize4() {
		return totalPossibleViolationsSize4;
	}

	public void setTotalPossibleViolationsSize4(int totalPossibleViolationsSize4) {
		this.totalPossibleViolationsSize4 = totalPossibleViolationsSize4;
	}

	public int getTotalVerifiedViolationsSize4() {
		return totalVerifiedViolationsSize4;
	}

	public void setTotalVerifiedViolationsSize4(int totalVerifiedViolationsSize4) {
		this.totalVerifiedViolationsSize4 = totalVerifiedViolationsSize4;
	}

	@Override
	public String toString() {
		return "ECCheckerResult [possibleViolationsSize2=" + possibleViolationsSize2 + ", possibleViolationsSize3="
				+ possibleViolationsSize3 + ", possibleViolationsSize4=" + possibleViolationsSize4 + ", verifiedViolationsSize2="
				+ verifiedViolationsSize2 + ", verifiedViolationsSize3=" + verifiedViolationsSize3 + ", verifiedViolationsSize4="
				+ verifiedViolationsSize4 + ", totalPossibleViolationsSize4=" + totalPossibleViolationsSize4
				+ ", totalVerifiedViolationsSize4=" + totalVerifiedViolationsSize4 + "]";
	}
}
