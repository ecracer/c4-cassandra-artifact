package ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import ch.ethz.inf.pm.ecchecker.And;
import ch.ethz.inf.pm.ecchecker.ArgLeft;
import ch.ethz.inf.pm.ecchecker.ArgRight;
import ch.ethz.inf.pm.ecchecker.BigAnd;
import ch.ethz.inf.pm.ecchecker.BigOr;
import ch.ethz.inf.pm.ecchecker.BoolSort$;
import ch.ethz.inf.pm.ecchecker.ClientLocalVar;
import ch.ethz.inf.pm.ecchecker.Equal;
import ch.ethz.inf.pm.ecchecker.Event;
import ch.ethz.inf.pm.ecchecker.EventArgVar;
import ch.ethz.inf.pm.ecchecker.Expr;
import ch.ethz.inf.pm.ecchecker.False$;
import ch.ethz.inf.pm.ecchecker.GlobalVar;
import ch.ethz.inf.pm.ecchecker.Graph;
import ch.ethz.inf.pm.ecchecker.Implies;
import ch.ethz.inf.pm.ecchecker.IntConst;
import ch.ethz.inf.pm.ecchecker.IntSort$;
import ch.ethz.inf.pm.ecchecker.Not;
import ch.ethz.inf.pm.ecchecker.Or;
import ch.ethz.inf.pm.ecchecker.OtherClientLocalVar;
import ch.ethz.inf.pm.ecchecker.PoEdge;
import ch.ethz.inf.pm.ecchecker.Skip$;
import ch.ethz.inf.pm.ecchecker.Sort;
import ch.ethz.inf.pm.ecchecker.StringConst;
import ch.ethz.inf.pm.ecchecker.StringSort$;
import ch.ethz.inf.pm.ecchecker.SystemSpecification;
import ch.ethz.inf.pm.ecchecker.ToEdge;
import ch.ethz.inf.pm.ecchecker.True$;
import ch.ethz.inf.pm.ecchecker.Unequal;

/**
 * Helpers for building Exprs
 */
public class ECFactory {

	// -------------------------------
	//               SORT
	// -------------------------------
	public static final BoolSort$ BOOL_SORT = BoolSort$.MODULE$;
	public static final StringSort$ STRING_SORT = StringSort$.MODULE$;
	public static final IntSort$ INT_SORT = IntSort$.MODULE$;

	// -------------------------------
	//               EXPR
	// -------------------------------
	public static final True$ TRUE = True$.MODULE$;
	public static final False$ FALSE = False$.MODULE$;

	public static StringConst strConst(final String str) {
		return StringConst.apply(str);
	}

	public static IntConst intConst(final int n) {
		return IntConst.apply(n);
	}

	private static final int CLIENT_ID_ARG_IDX = 0;
	private static final int INT_ARG_IDX_OFFSET = 1;
	private static final int BOOL_RETURNS_EMPTY_RESULT_IDX = 0;
	private static final int BOOL_INSERTS_NEW_ROWS_IDX = 1;
	private static final int BOOL_UPDATES_EXISTING_ROWS_IDX = 2;

	public static Expr argLeft(final int idx, final Sort sort) {
		return ArgLeft.apply(idx, sort);
	}

	public static Expr argRight(final int idx, final Sort sort) {
		return ArgRight.apply(idx, sort);
	}

	public static Expr strArgLeft(final int idx) {
		return argLeft(idx, STRING_SORT);
	}

	public static Expr strArgRight(final int idx) {
		return argRight(idx, STRING_SORT);
	}

	public static Expr intClientIdArgLeft() {
		return argLeft(CLIENT_ID_ARG_IDX, INT_SORT);
	}

	public static Expr intClientIdArgRight() {
		return argRight(CLIENT_ID_ARG_IDX, INT_SORT);
	}

	public static Expr intArgLeft(final int idx) {
		return argLeft(INT_ARG_IDX_OFFSET + idx, INT_SORT);
	}

	public static Expr intArgRight(final int idx) {
		return argRight(INT_ARG_IDX_OFFSET + idx, INT_SORT);
	}

	public static EventArgVar intEventArgVar(final int idx, final String eventId) {
		return EventArgVar.apply(INT_ARG_IDX_OFFSET + idx, eventId, INT_SORT);
	}

	public static Expr boolReturnsEmptyResultArgLeft() {
		return argLeft(BOOL_RETURNS_EMPTY_RESULT_IDX, BOOL_SORT);
	}

	public static Expr boolReturnsEmptyResultArgRight() {
		return argRight(BOOL_RETURNS_EMPTY_RESULT_IDX, BOOL_SORT);
	}

	public static Expr boolInsertsNewRowsArgLeft() {
		return argLeft(BOOL_INSERTS_NEW_ROWS_IDX, BOOL_SORT);
	}

	public static Expr boolInsertsNewRowsArgRight() {
		return argRight(BOOL_INSERTS_NEW_ROWS_IDX, BOOL_SORT);
	}

	public static Expr boolUpdatesExistingRowsArgLeft() {
		return argLeft(BOOL_UPDATES_EXISTING_ROWS_IDX, BOOL_SORT);
	}

	public static Expr boolUpdatesExistingRowsArgRight() {
		return argRight(BOOL_UPDATES_EXISTING_ROWS_IDX, BOOL_SORT);
	}

	public static Expr boolArgLeft(final int idx) {
		return argLeft(idx, BOOL_SORT);
	}

	public static Expr boolArgRight(final int idx) {
		return argRight(idx, BOOL_SORT);
	}

	public static Expr not(final Expr arg1) {
		if (TRUE.equals(arg1)) {
			return FALSE;
		} else if (FALSE.equals(arg1)) {
			return TRUE;
		} else {
			return Not.apply(arg1);
		}
	}

	public static Expr equal(final Expr arg1, final Expr arg2) {
		return Equal.apply(arg1, arg2);
	}

	public static Expr unequal(final Expr arg1, final Expr arg2) {
		return Unequal.apply(arg1, arg2);
	}

	public static Expr and(final Expr... args) {
		return and(Arrays.asList(args));
	}

	public static Expr and(final List<Expr> args) {
		if (args == null || args.isEmpty()) {
			throw new IllegalArgumentException();
		}
		final Iterator<Expr> argIt = args.iterator();
		List<Expr> res = new ArrayList<>();
		while (argIt.hasNext()) {
			final Expr next = argIt.next();
			if (next == FALSE) {
				return FALSE;
			} else if (next != TRUE) {
				res.add(next);
			}
		}
		if (res.isEmpty()) {
			return TRUE;
		} else if (res.size() == 1) {
			return res.get(0);
		} else if (res.size() == 2) {
			return And.apply(res.get(0), res.get(1));
		} else {
			return BigAnd.apply(ScalaUtils.from(res));
		}
	}

	public static Expr or(final Expr... args) {
		return or(Arrays.asList(args));
	}

	public static Expr or(final List<Expr> args) {
		if (args == null || args.isEmpty()) {
			throw new IllegalArgumentException();
		}
		final Iterator<Expr> argIt = args.iterator();
		List<Expr> res = new ArrayList<>();
		while (argIt.hasNext()) {
			final Expr next = argIt.next();
			if (next == TRUE) {
				return TRUE;
			} else if (next != FALSE) {
				res.add(next);
			}
		}
		if (res.isEmpty()) {
			return FALSE;
		} else if (res.size() == 1) {
			return res.get(0);
		} else if (res.size() == 2) {
			return Or.apply(res.get(0), res.get(1));
		} else {
			return BigOr.apply(ScalaUtils.from(res));
		}
	}

	public static Expr implies(final Expr left, final Expr right) {
		return Implies.apply(left, right);
	}

	// -------------------------------
	//               VARS
	// -------------------------------
	private static ClientLocalVar clientLocalVar(final String id, final Sort sort) {
		return ClientLocalVar.apply(id, sort);
	}

	public static OtherClientLocalVar asOtherClientLocalVar(final ClientLocalVar var) {
		return OtherClientLocalVar.apply(var.id(), var.sort());
	}

	public static ClientLocalVar strClientLocalVar(final String id) {
		return clientLocalVar(id, STRING_SORT);
	}

	public static ClientLocalVar intClientLocalVar(final String id) {
		return clientLocalVar(id, INT_SORT);
	}

	public static ClientLocalVar boolClientLocalVar(final String id) {
		return clientLocalVar(id, BOOL_SORT);
	}

	private static GlobalVar globalVar(final String id, final Sort sort) {
		return GlobalVar.apply(id, sort);
	}

	public static GlobalVar strGlobalVar(final String id) {
		return globalVar(id, STRING_SORT);
	}

	public static GlobalVar intGlobalVar(final String id) {
		return globalVar(id, INT_SORT);
	}

	public static GlobalVar boolGlobalVar(final String id) {
		return globalVar(id, BOOL_SORT);
	}

	// -------------------------------
	//               EVENTS
	// -------------------------------
	public static Event event(final String id, final String txn, final String operationID) {
		return new Event(id, txn, operationID, TRUE, false);
	}

	public static Event event(final String id, final String txn, final String operationID, final Expr constraint) {
		return new Event(id, txn, operationID, constraint, false);
	}

	public static Event skip(final String id, final String txn) {
		return event(id, txn, Skip$.MODULE$.id());
	}

	public static boolean isSkipEvent(final Event event) {
		return event.id().equals(Skip$.MODULE$.id());
	}

	// -------------------------------
	//               EDGES
	// -------------------------------
	public static PoEdge poEdge(final String sourceID, final String targetID) {
		return new PoEdge(sourceID, targetID, TRUE);
	}

	public static PoEdge poEdge(final String sourceID, final String targetID, final Expr constraint) {
		return new PoEdge(sourceID, targetID, constraint);
	}

	public static ToEdge toEdge(final String sourceID, final String targetID) {
		return new ToEdge(sourceID, targetID, false);
	}

	// ------------------------------- 
	//               GRAPH
	// -------------------------------
	public static Graph graph(final Collection<Event> events, final Collection<PoEdge> programOrder,
			final Collection<ToEdge> transactionOrder, final SystemSpecification systemSpec) {

		return new Graph(ScalaUtils.from(events), ScalaUtils.from(programOrder), ScalaUtils.from(transactionOrder), systemSpec, TRUE);
	}

	public static Graph graph(final List<Event> events, final List<PoEdge> programOrder, final List<ToEdge> transactionOrder,
			final SystemSpecification systemSpec, final Expr globalConstraint) {

		return new Graph(ScalaUtils.from(events), ScalaUtils.from(programOrder), ScalaUtils.from(transactionOrder), systemSpec,
				globalConstraint);
	}
}
