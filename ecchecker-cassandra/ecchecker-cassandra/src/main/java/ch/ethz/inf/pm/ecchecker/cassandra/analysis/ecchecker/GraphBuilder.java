package ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker;

import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.TRUE;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.and;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.asOtherClientLocalVar;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.boolInsertsNewRowsArgLeft;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.boolReturnsEmptyResultArgLeft;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.boolUpdatesExistingRowsArgLeft;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.equal;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.event;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.graph;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.intArgLeft;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.intArgRight;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.intClientIdArgLeft;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.intEventArgVar;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.not;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.or;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.poEdge;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.skip;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.toEdge;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.unequal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.SetMultimap;

import ch.ethz.inf.pm.ecchecker.ClientLocalVar;
import ch.ethz.inf.pm.ecchecker.Event;
import ch.ethz.inf.pm.ecchecker.EventArgVar;
import ch.ethz.inf.pm.ecchecker.Expr;
import ch.ethz.inf.pm.ecchecker.Graph;
import ch.ethz.inf.pm.ecchecker.Operation;
import ch.ethz.inf.pm.ecchecker.PoEdge;
import ch.ethz.inf.pm.ecchecker.Query;
import ch.ethz.inf.pm.ecchecker.SystemSpecification;
import ch.ethz.inf.pm.ecchecker.ToEdge;
import ch.ethz.inf.pm.ecchecker.Update;
import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.SchemaInformation;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.ParsedSessionExecuteInvoke;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.QueryPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.UpdatePart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.UpsertPart;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.AbstractTransactionGraph.EdgeConstraint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ClientImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.UUIDValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;

/**
 * Helper class for transforming the transaction graphs to a graph that can be used when calling ECChecker. System
 * Specification is built on construction. Afterwards, events and edges can be added using the methods.
 */
public class GraphBuilder {

	private static final Logger LOG = LogManager.getLogger(GraphBuilder.class);

	private final Options options;
	private final VariableProvider varProvider;
	private final ClientLocalVar clientIdVar;
	private final ImmutableList<Operation> operations;
	private final ImmutableBiMap<StatementPart, String> partToOperationId;
	private final ImmutableMap<String, Expr> commutativitySpecs;
	private final ImmutableMap<String, Expr> absorptionSpecs;
	private final ImmutableMap<String, Expr> asymmetricCommutativity;
	private final ImmutableMap<String, Expr> synchronizationSpec;
	private final ImmutableMap<String, Expr> legalitySpec;

	private final Map<String, Event> events = new TreeMap<>();
	private final List<PoEdge> programOrder = new ArrayList<>();
	private final List<ToEdge> transactionOrder = new ArrayList<>();
	private final Map<String, BiMap<Integer, ConcreteValue>> transactionLocalValues = new HashMap<>();
	private final SetMultimap<String, Integer> transactionLocalValuesUsedMultipleTimes = HashMultimap.create();
	private final Map<String, ClientLocalVar> clientLocalVariables = new HashMap<>();
	private final SetMultimap<UUIDValue, EventArgVar> randomUUIDVariables = HashMultimap.create();

	public GraphBuilder(final SchemaInformation schemaInfo, final StatementPart... parts) {
		this(new Options(), schemaInfo, parts);
	}

	public GraphBuilder(final Options options, final SchemaInformation schemaInfo, final StatementPart... parts) {
		this(options, schemaInfo, new HashSet<>(Arrays.asList(parts)), null);
	}

	private final Comparator<String> opIdComparator = new Comparator<String>() {

		@Override
		public int compare(String op1Id, String op2Id) {
			final StatementPart part1 = partToOperationId.inverse().get(op1Id);
			final StatementPart part2 = partToOperationId.inverse().get(op2Id);
			if (part1 instanceof UpdatePart && part2 instanceof QueryPart) {
				return -1;
			} else if (part2 instanceof UpdatePart && part1 instanceof QueryPart) {
				return 1;
			} else {
				return op1Id.compareTo(op2Id);
			}
		}
	};

	public GraphBuilder(final Options options, final SchemaInformation schemaInfo, final Map<StatementPart, String> parts) {
		this(options, schemaInfo, parts.keySet(), parts);
	}

	private GraphBuilder(final Options options, final SchemaInformation schemaInfo, final Set<StatementPart> parts,
			final Map<StatementPart, String> queries) {
		this.options = Objects.requireNonNull(options);
		varProvider = VariableProvider.create();
		clientIdVar = varProvider.newIntClientLocalVar("clientId");
		final CommutativityAbsorptionExprGenerator commutativityAbsorptionUtil = CommutativityAbsorptionExprGenerator.create(schemaInfo,
				options, varProvider);

		final ImmutableList.Builder<Operation> operationsBuilder = ImmutableList.builder();

		final String formatStr = "%0" + (int) Math.floor(Math.log10(parts.size() + 3) + 1) + "d_";

		int opCount = 0;
		final ImmutableBiMap.Builder<StatementPart, String> partToOperationIdBuilder = ImmutableBiMap.builder();
		// known parts
		for (final StatementPart part : parts) {
			final String operation = String.format(formatStr + part.getCQLCommand(), opCount++);
			operationsBuilder.add(part instanceof UpdatePart ? new Update(operation) : new Query(operation));
			partToOperationIdBuilder.put(part, operation);
			if (queries != null && LOG.isDebugEnabled()) {
				LOG.debug(operation + " was created from " + queries.get(part));
			}
		}
		operations = operationsBuilder.build();
		partToOperationId = partToOperationIdBuilder.build();

		final List<String> sortedOperationIds = new ArrayList<>(partToOperationId.values());
		Collections.sort(sortedOperationIds, opIdComparator);

		final ImmutableMap.Builder<String, Expr> commutativitySpecsBuilder = ImmutableMap.builder();
		final ImmutableMap.Builder<String, Expr> absorptionSpecsBuilder = ImmutableMap.builder();
		final ImmutableMap.Builder<String, Expr> asymmetricCommutativityBuilder = ImmutableMap.builder();
		final ImmutableMap.Builder<String, Expr> synchronizationSpecBuilder = ImmutableMap.builder();
		final ImmutableMap.Builder<String, Expr> legalitySpecBuilder = ImmutableMap.builder();
		for (int i = 0; i < sortedOperationIds.size(); i++) {
			final String op1Id = sortedOperationIds.get(i);
			final StatementPart part1 = partToOperationId.inverse().get(op1Id);
			for (int j = i; j < sortedOperationIds.size(); j++) {
				final String op2Id = sortedOperationIds.get(j);
				final StatementPart part2 = partToOperationId.inverse().get(op2Id);
				// commutativity: symmetric --> only one has to be specified
				commutativitySpecsBuilder.put(op1Id + "," + op2Id, commutativityAbsorptionUtil.getCommutativityExpr(part1, part2));
				// asymmetricCommutativity	
				final Expr asymmetric12 = commutativityAbsorptionUtil.getAsymetricCommutativityExpr(part1, part2);
				if (asymmetric12 != null) {
					asymmetricCommutativityBuilder.put(op1Id + "," + op2Id, asymmetric12);
				}
				if (!op1Id.equals(op2Id)) {
					final Expr asymmetric21 = commutativityAbsorptionUtil.getAsymetricCommutativityExpr(part2, part1);
					if (asymmetric21 != null) {
						asymmetricCommutativityBuilder.put(op2Id + "," + op1Id, asymmetric21);
					}
				}
				if (part1 instanceof UpdatePart && part2 instanceof UpdatePart) {
					// absorption: specify both ways
					absorptionSpecsBuilder.put(op1Id + "," + op2Id, commutativityAbsorptionUtil.getAbsorptionExpr(part1, part2));
					if (part1 != part2) {
						absorptionSpecsBuilder.put(op2Id + "," + op1Id, commutativityAbsorptionUtil.getAbsorptionExpr(part2, part1));
					}
				}
				// synchronization: specify both ways
				final Expr synchronizing12 = commutativityAbsorptionUtil.getSynchronizingOperationExpr(part1, part2);
				if (synchronizing12 != null) {
					synchronizationSpecBuilder.put(op1Id + "," + op2Id, synchronizing12);
				}
				final Expr synchronizing21 = commutativityAbsorptionUtil.getSynchronizingOperationExpr(part2, part1);
				if (synchronizing21 != null && part1 != part2) {
					synchronizationSpecBuilder.put(op2Id + "," + op1Id, synchronizing21);
				}
				// legality along CA
				final Expr legality12 = commutativityAbsorptionUtil.getLegalityExpr(part1, part2);
				if (legality12 != null) {
					legalitySpecBuilder.put(op1Id + "," + op2Id, legality12);
				}
				final Expr legality21 = commutativityAbsorptionUtil.getLegalityExpr(part2, part1);
				if (legality21 != null && part1 != part2) {
					legalitySpecBuilder.put(op2Id + "," + op1Id, legality21);
				}
			}
		}
		this.commutativitySpecs = commutativitySpecsBuilder.build();
		this.absorptionSpecs = absorptionSpecsBuilder.build();
		this.asymmetricCommutativity = asymmetricCommutativityBuilder.build();
		this.synchronizationSpec = synchronizationSpecBuilder.build();
		this.legalitySpec = legalitySpecBuilder.build();
	}

	public SystemSpecification getSpecification() {
		return new SystemSpecification(ScalaUtils.from(operations), ScalaUtils.from(commutativitySpecs),
				ScalaUtils.from(absorptionSpecs), ScalaUtils.from(asymmetricCommutativity), ScalaUtils.from(Collections.emptyMap()),
				ScalaUtils.from(synchronizationSpec), ScalaUtils.from(legalitySpec));
	}

	public Expr getCommutativityExpr(final StatementPart part1, final StatementPart part2) {
		final String id1 = partToOperationId.get(part1);
		final String id2 = partToOperationId.get(part2);
		if (opIdComparator.compare(id1, id2) < 0) {
			return commutativitySpecs.get(id1 + "," + id2);
		} else {
			return commutativitySpecs.get(id2 + "," + id1);
		}
	}

	public Expr getAbsorptionExpr(final StatementPart part1, final StatementPart part2) {
		final String id1 = partToOperationId.get(part1);
		final String id2 = partToOperationId.get(part2);
		return absorptionSpecs.get(id1 + "," + id2);
	}

	public String addEvent(final StatementPart part, final String transactionId) {
		return addEvent(part, transactionId, null);
	}

	public String addEvent(final StatementPart part, final String transactionId, final ParsedSessionExecuteInvoke sessionExecuteInvoke) {
		final String eventId = "e" + events.size() + "_" + part.getLabel();
		final List<Expr> eventConstraint = new ArrayList<>();
		eventConstraint.add(equal(intClientIdArgLeft(), clientIdVar));
		final Set<Integer> constraintIdxs = new HashSet<>(part.getPossibleConstraintIdxs().values());
		final Set<Integer> columnIdxs = part instanceof UpsertPart
				? new HashSet<>(((UpsertPart) part).components.getPossiblyUpdatedColumnIdxs().values())
				: Collections.emptySet();
		if (!constraintIdxs.isEmpty() || !columnIdxs.isEmpty()) {
			for (final Integer constraintIdx : constraintIdxs) {
				handleValue(eventId, transactionId, constraintIdx, part.getConstraint(constraintIdx).getValue(), part, eventConstraint);
			}
			if (part instanceof UpsertPart) {
				final UpsertPart upsPart = (UpsertPart) part;
				for (final Integer columnIdx : columnIdxs) {
					if (!constraintIdxs.contains(columnIdx)) {
						handleValue(eventId, transactionId, columnIdx, upsPart.components.getColumnUpdate(columnIdx).getValue(),
								part, eventConstraint);
					}
				}
			}
		}
		if (part instanceof UpsertPart) {
			final UpsertPart upsert = (UpsertPart) part;
			if (upsert.ifExists) {
				eventConstraint.add(not(boolInsertsNewRowsArgLeft()));
			} else if (upsert.ifNotExists) {
				eventConstraint.add(not(boolUpdatesExistingRowsArgLeft()));
			} else if (!upsert.canInsert) {
				// non-LWT that cannot insert must update rows
				eventConstraint.add(not(boolInsertsNewRowsArgLeft()));
				eventConstraint.add(boolUpdatesExistingRowsArgLeft());
			} else {
				// non-LWT has to insert a new row or update an existing one
				eventConstraint.add(or(boolInsertsNewRowsArgLeft(), boolUpdatesExistingRowsArgLeft()));
			}
		}
		events.put(eventId, event(eventId, transactionId, partToOperationId.get(part), and(eventConstraint)));
		return eventId;
	}

	public String addSkipEvent(final String label, final String transactionId) {
		final String eventId = "e" + events.size() + "_" + label;
		events.put(eventId, skip(eventId, transactionId));
		return eventId;
	}

	public void addTransactionOrderEdge(final String fromTransactionId, final String toTransactionId) {
		transactionOrder.add(toEdge(fromTransactionId, toTransactionId));
	}

	public void addProgramOrderEdge(final String fromEventId, final String toEventId) {
		addProgramOrderEdge(fromEventId, toEventId, null, EdgeConstraint.createEmpty());
	}

	public void addProgramOrderEdge(final String fromEventId, final String toEventId,
			final ParsedSessionExecuteInvoke fromEvent, final EdgeConstraint<ParsedSessionExecuteInvoke> sessionExecute) {

		final List<Expr> poConstraint = new ArrayList<>();
		poConstraint.add(TRUE);
		if (options.isPoConstraintsEnabled() && fromEvent != null && sessionExecute != null) {
			if (sessionExecute.emptyResultNodes().contains(fromEvent)) {
				poConstraint.add(boolReturnsEmptyResultArgLeft());
			} else if (sessionExecute.nonEmptyResultNodes().contains(fromEvent)) {
				poConstraint.add(not(boolReturnsEmptyResultArgLeft()));
			}
		}
		programOrder.add(poEdge(fromEventId, toEventId, and(poConstraint)));
	}

	private int getTransactionValueArgIdx(final StatementPart part, final int offset) {
		if (part == null) {
			// skip event
			return offset;
		} else {
			return (part.getMaxIdx() < 100 ? 100 : part.getMaxIdx() + 1) + offset;
		}
	}

	private void handleValue(final String eventId, final String transactionId, final int idx, final ConcreteValue value,
			final StatementPart part, final List<Expr> eventConstraint) {
		if (options.isValueAnalysisEnabled() && value != null) {
			if (value instanceof ClientImmutableValue) {
				// encode client equality
				final String id = ((ClientImmutableValue) value).id;
				if (!clientLocalVariables.containsKey(id)) {
					clientLocalVariables.put(id, varProvider.newIntClientLocalVar());
				}
				eventConstraint.add(equal(intArgLeft(idx), clientLocalVariables.get(id)));
			}
			if (value instanceof UUIDValue && ((UUIDValue) value).isRandom) {
				randomUUIDVariables.put((UUIDValue) value, intEventArgVar(idx, eventId));
			}
			if ((value instanceof UnknownImmutableValue && !((UnknownImmutableValue) value).isWidened) ||
					(value instanceof UUIDValue && !((UUIDValue) value).isWidened)) {
				// we have to encode equality of UnknownValues along the ProgramOrder in a single transaction
				if (!transactionLocalValues.containsKey(transactionId)) {
					transactionLocalValues.put(transactionId, HashBiMap.create());
				}
				final BiMap<Integer, ConcreteValue> transactionVals = transactionLocalValues.get(transactionId);
				final int offset;
				if (!transactionVals.containsValue(value)) {
					offset = transactionVals.size();
					transactionVals.put(offset, value);
				} else {
					offset = transactionVals.inverse().get(value);
					transactionLocalValuesUsedMultipleTimes.put(transactionId, offset);
				}
				eventConstraint.add(equal(intArgLeft(idx),
						intArgLeft(getTransactionValueArgIdx(part, offset))));
			}
		}
	}

	public Graph build() {
		return graph(new ArrayList<>(events.values()), transformProgramOrder(), transactionOrder, getSpecification(),
				buildGlobalConstraint());
	}

	public List<PoEdge> transformProgramOrder() {
		if (transactionLocalValuesUsedMultipleTimes.isEmpty()) {
			return programOrder;
		}
		final List<PoEdge> transformedPO = new ArrayList<>(programOrder.size());
		for (final PoEdge oldEdge : programOrder) {
			final Event sourceEvent = events.get(oldEdge.sourceID());
			if (transactionLocalValuesUsedMultipleTimes.containsKey(sourceEvent.txn())) {
				final Event targetEvent = events.get(oldEdge.targetID());
				final StatementPart sourcePart = partToOperationId.inverse().get(sourceEvent.operationID());
				final StatementPart targetPart = partToOperationId.inverse().get(targetEvent.operationID());
				final List<Expr> poConstraint = new ArrayList<>();
				poConstraint.add(oldEdge.constraint());
				for (int i : transactionLocalValuesUsedMultipleTimes.get(sourceEvent.txn())) {
					poConstraint.add(equal(intArgLeft(getTransactionValueArgIdx(sourcePart, i)),
							intArgRight(getTransactionValueArgIdx(targetPart, i))));
				}
				transformedPO.add(poEdge(oldEdge.sourceID(), oldEdge.targetID(), and(poConstraint)));
			} else {
				transformedPO.add(oldEdge);
			}
		}
		return transformedPO;
	}

	private Expr buildGlobalConstraint() {
		final List<Expr> globalConstraint = new ArrayList<>();
		globalConstraint.add(unequal(clientIdVar, asOtherClientLocalVar(clientIdVar)));
		if (options.isClientLocalAreGlobalUnique()) {
			for (final ClientLocalVar clv : clientLocalVariables.values()) {
				globalConstraint.add(unequal(clv, asOtherClientLocalVar(clv)));
			}
		}
		final List<UUIDValue> uuidValues = new ArrayList<>(randomUUIDVariables.keySet());
		for (int i = 0; i < uuidValues.size(); i++) {
			for (final EventArgVar argVar1 : randomUUIDVariables.get(uuidValues.get(i))) {
				for (int j = i + 1; j < uuidValues.size(); j++) {
					for (final EventArgVar argVar2 : randomUUIDVariables.get(uuidValues.get(j))) {
						globalConstraint.add(unequal(argVar1, argVar2));
					}
				}
			}
		}
		return and(globalConstraint);
	}

	@Override
	public String toString() {
		return build().toString();
	}
}
