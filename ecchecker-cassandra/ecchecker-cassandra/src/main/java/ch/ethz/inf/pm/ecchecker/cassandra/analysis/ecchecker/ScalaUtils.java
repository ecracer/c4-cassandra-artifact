package ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker;

import scala.Option;
import scala.Predef;
import scala.Tuple2;
import scala.collection.JavaConversions;

public class ScalaUtils {

	public static <T> scala.collection.immutable.List<T> from(final java.util.Collection<T> javaCollection) {
		return JavaConversions.collectionAsScalaIterable(javaCollection).toList();
	}

	public static <T> java.util.List<T> from(final scala.collection.immutable.List<T> scalaList) {
		return JavaConversions.seqAsJavaList(scalaList);
	}

	public static <T> java.util.Set<T> from(final scala.collection.immutable.Set<T> scalaSet) {
		return JavaConversions.setAsJavaSet(scalaSet);
	}

	public static <K, V> scala.collection.immutable.Map<K, V> from(final java.util.Map<K, V> javaMap) {
		return JavaConversions.mapAsScalaMap(javaMap).toMap(Predef.<Tuple2<K, V>> conforms());
	}

	public static <T> Option<T> emptyOption() {
		return Option.<T> empty();
	}
}
