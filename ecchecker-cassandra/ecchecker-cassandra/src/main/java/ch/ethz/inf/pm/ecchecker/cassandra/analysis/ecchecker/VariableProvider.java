package ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker;

import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.boolClientLocalVar;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.boolGlobalVar;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.intClientLocalVar;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.intGlobalVar;

import ch.ethz.inf.pm.ecchecker.ClientLocalVar;
import ch.ethz.inf.pm.ecchecker.GlobalVar;

public class VariableProvider {

	private int nextId = 0;

	private VariableProvider() {
	}

	public static VariableProvider create() {
		return new VariableProvider();
	}

	public GlobalVar newIntGlobalVar() {
		return newIntGlobalVar("u");
	}

	public GlobalVar newIntGlobalVar(final String prefix) {
		return intGlobalVar(prefix + "_g_int_" + nextId++);
	}

	public GlobalVar newBoolGlobalVar() {
		return newBoolGlobalVar("u");
	}

	public GlobalVar newBoolGlobalVar(final String prefix) {
		return boolGlobalVar(prefix + "_g_bool_" + nextId++);
	}

	public ClientLocalVar newIntClientLocalVar() {
		return newIntClientLocalVar("u");
	}

	public ClientLocalVar newIntClientLocalVar(final String prefix) {
		return intClientLocalVar(prefix + "_c_int_" + nextId++);
	}

	public ClientLocalVar newBoolClientLocalVar() {
		return newBoolClientLocalVar("u");
	}

	public ClientLocalVar newBoolClientLocalVar(final String prefix) {
		return boolClientLocalVar(prefix + "_c_bool_" + nextId++);
	}
}
