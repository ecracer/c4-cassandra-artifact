package ch.ethz.inf.pm.ecchecker.cassandra.annotations;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * If this map is used for storing values, the analysis assumes that the values set with the same key are everywhere the
 * same
 */
public class ClientLocalValues {

	private static Map<String, Object> values = new ConcurrentHashMap<>();

	public static <T> void set(final String id, final T value) {
		values.put(id, value);
	}

	@SuppressWarnings("unchecked")
	public static <T> T get(final String id) {
		return (T) values.get(id);
	}
}
