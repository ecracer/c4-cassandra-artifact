package ch.ethz.inf.pm.ecchecker.cassandra.annotations;

import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(METHOD)
@Retention(RetentionPolicy.RUNTIME)
/**
 * This annotation should be used to annotate methods that span a transaction. Set onlyForDisplaying = true if the
 * transaction is only used for displaying.
 */
public @interface Transaction {
	boolean onlyForDisplaying() default false;

	public static String SOOT_NAME = "Lch/ethz/inf/pm/ecchecker/cassandra/annotations/Transaction;";
}
