package ch.ethz.inf.pm.ecchecker.cassandra.beans;

import java.util.List;
import java.util.Objects;

import com.google.common.collect.ImmutableList;

/**
 * Represents a method uniquely
 */
public class Method {

	public final String className;
	public final String methodName;
	public final ImmutableList<String> argType;
	public final String returnType;
	public final String signature;

	private Method(final String className, final String methodName, final ImmutableList<String> argType, final String returnType,
			final String signature) {
		this.className = Objects.requireNonNull(className);
		this.methodName = Objects.requireNonNull(methodName);
		this.argType = Objects.requireNonNull(argType);
		this.returnType = Objects.requireNonNull(returnType);
		this.signature = Objects.requireNonNull(signature);
	}

	public static Method create(final String className, final String methodName, final List<String> argType,
			final String returnType, final String signature) {
		return new Method(className, methodName, ImmutableList.copyOf(argType), returnType, signature);
	}

	public String getShortName() {
		return className + "#" + methodName + "(...)";
	}

	public String getShortSignature() {
		return className + "#" + methodName + "(" + String.join(", ", argType) + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((signature == null) ? 0 : signature.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Method other = (Method) obj;
		if (signature == null) {
			if (other.signature != null)
				return false;
		} else if (!signature.equals(other.signature))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return getShortName();
	}
}
