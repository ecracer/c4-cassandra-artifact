package ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts;

import java.util.Objects;

/**
 * Base class for the selection of a column
 */
public class ColumnSelection {

	public final String column;

	protected ColumnSelection(final String column) {
		this.column = Objects.requireNonNull(column);
	}

	public static ColumnSelection create(final String column) {
		return new ColumnSelection(column);
	}

	public String getLabel() {
		return column;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((column == null) ? 0 : column.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColumnSelection other = (ColumnSelection) obj;
		if (column == null) {
			if (other.column != null)
				return false;
		} else if (!column.equals(other.column))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return getLabel();
	}
}
