package ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts;

import java.util.Set;

import com.google.common.collect.ImmutableSet;

/**
 * Represents the definition of a table
 */
public class ParsedCreateTableStatement {

	public final String table;
	public final ImmutableSet<String> primaryKeyColumns;
	public final ImmutableSet<String> nonPrimaryKeyColumns;

	private ParsedCreateTableStatement(final String table, final ImmutableSet<String> primaryKeyColumns,
			final ImmutableSet<String> nonPrimaryKeyColumns) {
		this.table = table;
		this.primaryKeyColumns = primaryKeyColumns;
		this.nonPrimaryKeyColumns = nonPrimaryKeyColumns;
	}

	public static ParsedCreateTableStatement create(final String table, final Set<String> primaryKeyColumns,
			final Set<String> nonPrimaryKeyColumns) {
		return new ParsedCreateTableStatement(table, ImmutableSet.copyOf(primaryKeyColumns), ImmutableSet.copyOf(nonPrimaryKeyColumns));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nonPrimaryKeyColumns == null) ? 0 : nonPrimaryKeyColumns.hashCode());
		result = prime * result + ((primaryKeyColumns == null) ? 0 : primaryKeyColumns.hashCode());
		result = prime * result + ((table == null) ? 0 : table.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParsedCreateTableStatement other = (ParsedCreateTableStatement) obj;
		if (nonPrimaryKeyColumns == null) {
			if (other.nonPrimaryKeyColumns != null)
				return false;
		} else if (!nonPrimaryKeyColumns.equals(other.nonPrimaryKeyColumns))
			return false;
		if (primaryKeyColumns == null) {
			if (other.primaryKeyColumns != null)
				return false;
		} else if (!primaryKeyColumns.equals(other.primaryKeyColumns))
			return false;
		if (table == null) {
			if (other.table != null)
				return false;
		} else if (!table.equals(other.table))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ParsedCreateTableStatement [table=" + table + ", primaryKeyColumns=" + primaryKeyColumns + ", nonPrimaryKeyColumns="
				+ nonPrimaryKeyColumns + "]";
	}
}
