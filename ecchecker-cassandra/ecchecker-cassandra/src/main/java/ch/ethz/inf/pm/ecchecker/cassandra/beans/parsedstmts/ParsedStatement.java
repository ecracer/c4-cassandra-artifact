package ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts;

import java.util.Objects;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.QueryPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.UpdatePart;

/**
 * Base class for the representation of a single parsed statement
 */
public abstract class ParsedStatement {

	public final String query;

	protected ParsedStatement(final String query) {
		this.query = Objects.requireNonNull(query);
	}

	public String getQuery() {
		return query;
	}

	public abstract ImmutableSet<? extends StatementPart> getStatementParts();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((query == null) ? 0 : query.hashCode());
		return result;
	}

	public abstract boolean equalsIgnoringQuery(final Object obj);

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParsedStatement other = (ParsedStatement) obj;
		if (query == null) {
			if (other.query != null)
				return false;
		} else if (!query.equals(other.query))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ParsedStatement [query=" + query + "]";
	}

	public static class ParsedUpdateStatement extends ParsedStatement {

		public final UpdatePart writePart;

		private ParsedUpdateStatement(String query, UpdatePart writePart) {
			super(query);
			this.writePart = Objects.requireNonNull(writePart);
		}

		public static ParsedUpdateStatement create(final String query, final UpdatePart writePart) {
			return new ParsedUpdateStatement(query, writePart);
		}

		@Override
		public ImmutableSet<UpdatePart> getStatementParts() {
			return ImmutableSet.of(writePart);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((writePart == null) ? 0 : writePart.hashCode());
			return result;
		}

		@Override
		public boolean equalsIgnoringQuery(final Object obj) {
			if (this == obj)
				return true;
			if (getClass() != obj.getClass())
				return false;
			ParsedUpdateStatement other = (ParsedUpdateStatement) obj;
			if (writePart == null) {
				if (other.writePart != null)
					return false;
			} else if (!writePart.equals(other.writePart))
				return false;
			return true;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			ParsedUpdateStatement other = (ParsedUpdateStatement) obj;
			if (writePart == null) {
				if (other.writePart != null)
					return false;
			} else if (!writePart.equals(other.writePart))
				return false;
			return true;
		}
	}

	public static class ParsedBatchUpdateStatement extends ParsedStatement {

		public final ImmutableSet<UpdatePart> updateParts;

		private ParsedBatchUpdateStatement(String query, ImmutableSet<UpdatePart> updateParts) {
			super(query);
			this.updateParts = Objects.requireNonNull(updateParts);
		}

		public static ParsedBatchUpdateStatement create(final String query, final Set<UpdatePart> updateParts) {
			return new ParsedBatchUpdateStatement(query, ImmutableSet.copyOf(updateParts));
		}

		@Override
		public ImmutableSet<UpdatePart> getStatementParts() {
			return updateParts;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((updateParts == null) ? 0 : updateParts.hashCode());
			return result;
		}

		@Override
		public boolean equalsIgnoringQuery(Object obj) {
			if (this == obj)
				return true;
			if (getClass() != obj.getClass())
				return false;
			ParsedBatchUpdateStatement other = (ParsedBatchUpdateStatement) obj;
			if (updateParts == null) {
				if (other.updateParts != null)
					return false;
			} else if (!updateParts.equals(other.updateParts))
				return false;
			return true;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			ParsedBatchUpdateStatement other = (ParsedBatchUpdateStatement) obj;
			if (updateParts == null) {
				if (other.updateParts != null)
					return false;
			} else if (!updateParts.equals(other.updateParts))
				return false;
			return true;
		}
	}

	public static class ParsedQueryStatement extends ParsedStatement {

		public final QueryPart queryPart;

		private ParsedQueryStatement(String query, QueryPart queryPart) {
			super(query);
			this.queryPart = Objects.requireNonNull(queryPart);
		}

		public static ParsedQueryStatement create(final String query, final QueryPart queryPart) {
			return new ParsedQueryStatement(query, queryPart);
		}

		@Override
		public ImmutableSet<QueryPart> getStatementParts() {
			return ImmutableSet.of(queryPart);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((queryPart == null) ? 0 : queryPart.hashCode());
			return result;
		}

		@Override
		public boolean equalsIgnoringQuery(Object obj) {
			if (this == obj)
				return true;
			if (getClass() != obj.getClass())
				return false;
			ParsedQueryStatement other = (ParsedQueryStatement) obj;
			if (queryPart == null) {
				if (other.queryPart != null)
					return false;
			} else if (!queryPart.equals(other.queryPart))
				return false;
			return true;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			ParsedQueryStatement other = (ParsedQueryStatement) obj;
			if (queryPart == null) {
				if (other.queryPart != null)
					return false;
			} else if (!queryPart.equals(other.queryPart))
				return false;
			return true;
		}
	}
}
