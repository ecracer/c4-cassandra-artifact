package ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetUpdate.SetOp;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnValueUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraint.EQConstraint;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraint.EQValConstraint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.NullValue;

/**
 * Base class for the different types of statements
 */
public abstract class StatementPart {

	public final String table;

	protected StatementPart(final String table) {
		this.table = table;
	}

	public abstract String getCQLCommand();

	public abstract String getLabel();

	public abstract ImmutableListMultimap<String, Integer> getPossibleConstraintIdxs();

	public abstract ImmutableListMultimap<String, Integer> getDefiniteConstraintIdxs();

	public abstract Constraint getConstraint(final int idx);

	public abstract int getMaxIdx();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((table == null) ? 0 : table.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatementPart other = (StatementPart) obj;
		if (table == null) {
			if (other.table != null)
				return false;
		} else if (!table.equals(other.table))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return getLabel();
	}

	public abstract static class QueryPart extends StatementPart {

		protected final Constraints constraints;
		/**
		 * Null --> all cols are display columns; Otherwise the set contains all display columns
		 */
		protected final ImmutableSet<String> displayColumns;

		public QueryPart(final String table, final Constraints constraints, final ImmutableSet<String> displayColumns) {
			super(table);
			this.constraints = Objects.requireNonNull(constraints);
			this.displayColumns = displayColumns;
		}

		public QueryPart setAllDisplayColumns() {
			return createNew(null);
		}

		public QueryPart setDisplayColumns(final Set<String> displayColumns) {
			return createNew(ImmutableSet.copyOf(displayColumns));
		}

		public boolean areAllDisplayColumns() {
			return displayColumns == null;
		}

		public boolean isDisplayColumn(final String column) {
			if (areAllDisplayColumns()) {
				return true;
			} else {
				return displayColumns.contains(column);
			}
		}

		/**
		 * Whether column is selected by this part
		 * 
		 * @param column
		 * @return true if it is selected, false if it is not selected
		 */
		public abstract boolean isSelectingColumn(final String column);

		protected abstract QueryPart createNew(final ImmutableSet<String> displayColumns);

		@Override
		public String getCQLCommand() {
			return "SELECT";
		}

		@Override
		public ImmutableListMultimap<String, Integer> getPossibleConstraintIdxs() {
			return constraints.getConstraintIdxsPerColumn();
		}

		@Override
		public ImmutableListMultimap<String, Integer> getDefiniteConstraintIdxs() {
			return constraints.getConstraintIdxsPerColumn();
		}

		@Override
		public Constraint getConstraint(final int idx) {
			return constraints.getConstraint(idx);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((constraints == null) ? 0 : constraints.hashCode());
			result = prime * result + ((displayColumns == null) ? 0 : displayColumns.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			QueryPart other = (QueryPart) obj;
			if (constraints == null) {
				if (other.constraints != null)
					return false;
			} else if (!constraints.equals(other.constraints))
				return false;
			if (displayColumns == null) {
				if (other.displayColumns != null)
					return false;
			} else if (!displayColumns.equals(other.displayColumns))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "SELECT ... FROM " + table;
		}
	}

	public static class SelectAllPart extends QueryPart {

		private SelectAllPart(final String table, final Constraints constraints, final ImmutableSet<String> displayColumns) {
			super(table, constraints, displayColumns);
		}

		public static SelectAllPart create(final String table, final Constraints constraints) {
			return new SelectAllPart(table, constraints, ImmutableSet.of());
		}

		@Override
		public boolean isSelectingColumn(String column) {
			return Boolean.TRUE;
		}

		@Override
		protected QueryPart createNew(final ImmutableSet<String> displayColumns) {
			return new SelectAllPart(table, constraints, displayColumns);
		}

		@Override
		public String getLabel() {
			return "SELECT_all_FROM_" + table + "_WHERE_" + constraints.getLabel();
		}

		@Override
		public int getMaxIdx() {
			return constraints.getConstraintSize();
		}
	}

	public static class SelectColsPart extends QueryPart {

		protected final ImmutableList<ColumnSelection> columns;

		private SelectColsPart(final String table, final ImmutableList<ColumnSelection> columns, final Constraints constraints,
				final ImmutableSet<String> displayColumns) {
			super(table, constraints, displayColumns);
			if (columns == null || columns.isEmpty()) {
				throw new IllegalArgumentException();
			}
			this.columns = columns;
		}

		public static SelectColsPart create(final String table, final List<ColumnSelection> columns, final Constraints constraints) {
			return new SelectColsPart(table, ImmutableList.copyOf(columns), constraints, ImmutableSet.of());
		}

		public static SelectColsPart create(final String table, final ColumnSelection column, Constraints constraints) {
			return new SelectColsPart(table, ImmutableList.of(column), constraints, ImmutableSet.of());
		}

		@Override
		public boolean isSelectingColumn(String column) {
			for (final ColumnSelection columnSel : columns) {
				if (column.equalsIgnoreCase(columnSel.column)) {
					return true;
				}
			}
			return false;
		}

		public ImmutableList<ColumnSelection> getSelectedColumns() {
			return columns;
		}

		@Override
		protected QueryPart createNew(final ImmutableSet<String> displayColumns) {
			return new SelectColsPart(table, columns, constraints, displayColumns);
		}
		@Override
		public boolean areAllDisplayColumns() {
			return displayColumns == null || displayColumns.containsAll(columns);
		}

		@Override
		public String getLabel() {
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT_");
			for (final ColumnSelection columnSel : columns) {
				sb.append(columnSel.getLabel()).append("_");
			}
			sb.append("FROM_");
			sb.append(table);
			sb.append("_WHERE_");
			sb.append(constraints.getLabel());
			return sb.toString();
		}

		@Override
		public int getMaxIdx() {
			return constraints.getConstraintSize();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((columns == null) ? 0 : columns.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			SelectColsPart other = (SelectColsPart) obj;
			if (columns == null) {
				if (other.columns != null)
					return false;
			} else if (!columns.equals(other.columns))
				return false;
			return true;
		}
	}

	public abstract static class UpdatePart extends StatementPart {

		public final boolean isLWT;

		protected UpdatePart(final String table, final boolean isLWT) {
			super(table);
			this.isLWT = isLWT;
		}
	}

	public static class DeletePart extends UpdatePart {

		public final Constraints constraints;
		public final boolean ifExists;

		private DeletePart(final String table, final Constraints constraints, final boolean isLWT, final boolean ifExists) {
			super(table, isLWT);
			this.constraints = Objects.requireNonNull(constraints);
			this.ifExists = ifExists;
		}

		public static DeletePart create(final String table, final Constraints constraints, final boolean isLWT, final boolean ifExists) {
			return new DeletePart(table, constraints, isLWT, ifExists);
		}

		@Override
		public String getCQLCommand() {
			return "DELETE";
		}

		@Override
		public ImmutableListMultimap<String, Integer> getPossibleConstraintIdxs() {
			return constraints.getConstraintIdxsPerColumn();
		}

		@Override
		public ImmutableListMultimap<String, Integer> getDefiniteConstraintIdxs() {
			return constraints.getConstraintIdxsPerColumn();
		}

		@Override
		public Constraint getConstraint(final int idx) {
			return constraints.getConstraint(idx);
		}

		@Override
		public String getLabel() {
			return "DELETE_FROM_" + table + "_WHERE_" + constraints.getLabel();
		}

		@Override
		public int getMaxIdx() {
			return constraints.getConstraintSize();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((constraints == null) ? 0 : constraints.hashCode());
			result = prime * result + (ifExists ? 1231 : 1237);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			DeletePart other = (DeletePart) obj;
			if (constraints == null) {
				if (other.constraints != null)
					return false;
			} else if (!constraints.equals(other.constraints))
				return false;
			if (ifExists != other.ifExists)
				return false;
			return true;
		}
	}

	public static class UpsertPart extends UpdatePart {

		private final String cqlCommand;
		public final UpsertComponents components;
		public final boolean canInsert;
		public final boolean ifNotExists;
		public final boolean ifExists;

		private UpsertPart(String cqlCommand, String table, final UpsertComponents components,
				boolean canInsert, boolean isLWT, boolean ifNotExists, boolean ifExists) {
			super(table, isLWT);
			this.components = Objects.requireNonNull(components);
			this.cqlCommand = Objects.requireNonNull(cqlCommand);
			this.canInsert = canInsert;
			this.ifNotExists = ifNotExists;
			this.ifExists = ifExists;
		}

		public static UpsertPart createInsert(final String table, final List<ColumnUpdate> columns, boolean ifNotExists) {
			final UpsertComponents.Builder builder = UpsertComponents.builder();
			for (final ColumnUpdate column : columns) {
				final Constraint correspondingConstraint;
				if (column instanceof ColumnValueUpdate) {
					correspondingConstraint = EQValConstraint.create(column.column, ((ColumnValueUpdate) column).value);
				} else {
					correspondingConstraint = EQConstraint.create(column.column);
				}
				builder.addColumnUpdateOrConstraint(column, correspondingConstraint);
			}
			return new UpsertPart("INSERT", table, builder.build(), true, ifNotExists, ifNotExists, false);
		}

		public static UpsertPart createUpdate(final String table, final List<ColumnUpdate> columns, final List<Constraint> constraints,
				boolean isLWT, boolean ifExists) {
			return createUpdateInternal("UPDATE", table, columns, constraints, isLWT, ifExists);
		}

		public static UpsertPart createDelete(final String table, final List<ColumnUpdate> columns, final List<Constraint> constraints,
				boolean isLWT, boolean ifExists) {
			return createUpdateInternal("DELETE", table, columns, constraints, isLWT, ifExists);
		}

		private static UpsertPart createUpdateInternal(final String cqlCommand, final String table, final List<ColumnUpdate> columns,
				final List<Constraint> constraints, final boolean isLWT, final boolean ifExists) {
			final UpsertComponents.Builder builder = UpsertComponents.builder();
			boolean addsValue = false;
			for (final ColumnUpdate column : columns) {
				// Updating to null or removing elements from a set does not insert a new row (only inserts new tombstones)
				if (!(column instanceof ColumnValueUpdate && ((ColumnValueUpdate) column).value instanceof NullValue) &&
						!(column instanceof ColumnSetUpdate && ((ColumnSetUpdate) column).operation == SetOp.REMOVE)) {

					addsValue = true;
				}
				builder.addColumnUpdate(column);
			}
			for (final Constraint constraint : constraints) {
				builder.addConstraint(constraint);
			}
			boolean canInsert = !ifExists && !"DELETE".equals(cqlCommand) && addsValue;
			return new UpsertPart(cqlCommand, table, builder.build(), canInsert, isLWT, false, ifExists);
		}

		public UpsertPart setCanInsert(final boolean canInsert) {
			if (this.canInsert == canInsert) {
				return this;
			} else {
				return new UpsertPart(cqlCommand, table, components, canInsert, isLWT, ifNotExists, ifExists);
			}
		}

		@Override
		public String getCQLCommand() {
			return cqlCommand;
		}

		@Override
		public ImmutableListMultimap<String, Integer> getPossibleConstraintIdxs() {
			return components.getPossibleConstraintIdxs();
		}

		@Override
		public ImmutableListMultimap<String, Integer> getDefiniteConstraintIdxs() {
			return components.getDefiniteConstraintIdxs();
		}

		@Override
		public Constraint getConstraint(int idx) {
			return components.getConstraint(idx);
		}

		@Override
		public String getLabel() {
			return cqlCommand + "_" + table + "_WITH_" + components.getLabel();
		}

		@Override
		public int getMaxIdx() {
			return components.getComponentSize();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + (canInsert ? 1231 : 1237);
			result = prime * result + ((components == null) ? 0 : components.hashCode());
			result = prime * result + ((cqlCommand == null) ? 0 : cqlCommand.hashCode());
			result = prime * result + (ifExists ? 1231 : 1237);
			result = prime * result + (ifNotExists ? 1231 : 1237);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			UpsertPart other = (UpsertPart) obj;
			if (canInsert != other.canInsert)
				return false;
			if (components == null) {
				if (other.components != null)
					return false;
			} else if (!components.equals(other.components))
				return false;
			if (cqlCommand == null) {
				if (other.cqlCommand != null)
					return false;
			} else if (!cqlCommand.equals(other.cqlCommand))
				return false;
			if (ifExists != other.ifExists)
				return false;
			if (ifNotExists != other.ifNotExists)
				return false;
			return true;
		}
	}
}