package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.beans.Method;
import soot.SootClass;
import soot.SootMethod;
import soot.Type;

/**
 * Helper that classifies all known methods
 */
public class SootMethodUtils {

	public static final MethodIdInterval UNKNOWN = new MethodIdInterval(Integer.MIN_VALUE, -1);

	public static final MethodIdInterval GENERAL = new MethodIdInterval(0, 299);
	public static final int OBJECT_INIT = 100;
	public static final int STRING_VALUE_OF = 110;
	public static final int INTEGER_VALUE_OF = 120;
	public static final int UUID_CREATE = 130;
	public static final int UUID_TO_STRING = 131;
	public static final int UUID_FROM_STRING = 132;
	public static final int SETS_NEW_HASH_SET = 140;

	public static final MethodIdInterval STRING_BUILDER = new MethodIdInterval(300, 399);
	public static final int STRING_BUILDER_INIT_NO_ARG = 300;
	public static final int STRING_BUILDER_INIT_STR_ARG = 301;
	public static final int STRING_BUILDER_INIT_INT_ARG = 302;
	public static final int STRING_BUILDER_APPEND = 303;
	public static final int STRING_BUILDER_TO_STRING = 304;

	public static final MethodIdInterval SESSION = new MethodIdInterval(400, 499);
	public static final int SESSION_EXEC_STRING_NO_BINDS = 400;
	public static final int SESSION_EXEC_STRING_WITH_BINDS = 401;
	public static final int SESSION_EXEC_STMT = 402;
	public static final int SESSION_EXEC_ASYNC_STRING_NO_BINDS = 403;
	public static final int SESSION_EXEC_ASYNC_STRING_WITH_BINDS = 404;
	public static final int SESSION_EXEC_ASYNC_STMT = 405;
	public static final int SESSION_PREPARE_STRING = 406;
	public static final int SESSION_PREPARE_STMT = 407;

	public static final MethodIdInterval STATEMENT = new MethodIdInterval(500, 599);
	public static final int STMT_SET_CONSISTENCY_LEVEL = 500;
	public static final int STMT_SET_SERIAL_CONSISTENCY_LEVEL = 501;
	public static final int STMT_GET_CONSISTENCY_LEVEL = 502;
	public static final int STMT_GET_SERIAL_CONSISTENCY_LEVEL = 503;
	public static final int STMT_GENERAL_SETTER_RETURNING_THIS = 504;
	public static final int STMT_GENERAL_GETTER = 505;
	public static final int PREP_STMT_BIND_NO_ARG = 510;
	public static final int PREP_STMT_BIND_1_ARG = 511;
	public static final int BOUND_STMT_BIND_1_ARG = 520;
	public static final int BOUND_STMT_SET_WITH_IDX = 521;
	public static final int BOUND_STMT_SET_WITH_NAME = 522;
	public static final int BOUND_STMT_SET_TO_NULL_WITH_IDX = 523;
	public static final int BOUND_STMT_SET_TO_NULL_WITH_NAME = 524;
	public static final int BOUND_STMT_SET_PARTITION_KEY_TOKEN = 525;
	public static final int BOUND_STMT_SET_ROUTING_KEY = 526;
	public static final int BOUND_STMT_GET_SOME_VALUE = 527;
	public static final int BOUND_STMT_INIT = 528;
	public static final int BATCH_STMT_INIT = 530;
	public static final int BATCH_STMT_ADD = 531;
	public static final int SIMPLE_STMT_INIT_NO_BINDS = 540;
	public static final int SIMPLE_STMT_INIT_WITH_BINDS = 541;

	public static final MethodIdInterval QB_INSERT = new MethodIdInterval(600, 699);
	public static final int QUERY_BUILDER_INSERT_INTO_KEYSPACE_TABLE = 600;
	public static final int QUERY_BUILDER_INSERT_INTO_TABLE = 601;
	public static final int INSERT_VALUE = 602;
	public static final int INSERT_IF_NOT_EXISTS = 603;

	public static final MethodIdInterval QB_SELECT = new MethodIdInterval(700, 799);
	public static final int QUERY_BUILDER_SELECT = 700;
	public static final int QUERY_BUILDER_SELECT_COLS = 701;
	public static final int SELECT_ALL = 702;
	public static final int SELECT_COLUMN = 703;
	public static final int SELECT_COUNT_ALL = 704;
	public static final int SELECT_DISTINCT = 705;
	public static final int SELECT_COLUMN_AS = 706;
	public static final int SELECT_FROM_TABLE = 707;
	public static final int SELECT_FROM_TABLE_KEYSPACE = 708;
	public static final int SELECT_ALLOW_FILTERING = 709;
	public static final int SELECT_LIMIT = 710;
	public static final int SELECT_ORDER_BY = 711;
	public static final int SELECT_WHERE = 712;
	public static final int SELECT_WHERE_CLAUSE = 713;
	public static final int SELECT_WHERE_AND_CLAUSE = 714;

	public static final MethodIdInterval QB_DELETE = new MethodIdInterval(800, 899);
	public static final int QUERY_BUILDER_DELETE = 800;
	public static final int QUERY_BUILDER_DELETE_COLS = 801;
	public static final int DELETE_ALL = 802;
	public static final int DELETE_COLUMN = 803;
	public static final int DELETE_LIST_ELT = 804;
	public static final int DELETE_MAP_ELT = 805;
	public static final int DELETE_SET_ELT = 806;
	public static final int DELETE_FROM_TABLE = 807;
	public static final int DELETE_FROM_TABLE_KEYSPACE = 808;
	public static final int DELETE_IF_EXISTS = 809;
	public static final int DELETE_ONLY_IF = 810;
	public static final int DELETE_ONLY_IF_CLAUSE = 811;
	public static final int DELETE_ONLY_IF_AND_CLAUSE = 812;
	public static final int DELETE_WHERE = 813;
	public static final int DELETE_WHERE_CLAUSE = 814;
	public static final int DELETE_WHERE_AND_CLAUSE = 815;

	public static final MethodIdInterval QB_UPDATE = new MethodIdInterval(900, 999);
	public static final int QUERY_BUILDER_UPDATE_KEYSPACE_TABLE = 900;
	public static final int QUERY_BUILDER_UPDATE_TABLE = 901;
	public static final int UPDATE_WITH = 902;
	public static final int UPDATE_WITH_ASSIGNMENT = 903;
	public static final int UPDATE_WITH_AND_ASSIGNMENT = 904;
	public static final int UPDATE_IF_EXISTS = 905;
	public static final int UPDATE_ONLY_IF = 906;
	public static final int UPDATE_ONLY_IF_CLAUSE = 907;
	public static final int UPDATE_ONLY_IF_AND_CLAUSE = 908;
	public static final int UPDATE_WHERE = 909;
	public static final int UPDATE_WHERE_CLAUSE = 910;
	public static final int UPDATE_WHERE_AND_CLAUSE = 911;

	public static final MethodIdInterval QB_BINDMARKER = new MethodIdInterval(1000, 1099);
	public static final int QUERY_BUILDER_BINDMARKER = 1000;
	public static final int QUERY_BUILDER_BINDMARKER_NAME = 1001;

	public static final MethodIdInterval QB_CLAUSE = new MethodIdInterval(1100, 1199);
	public static final int QUERY_BUILDER_CLAUSE_EQ = 1101;
	public static final int QUERY_BUILDER_CLAUSE_GT = 1102;
	public static final int QUERY_BUILDER_CLAUSE_GTE = 1103;
	public static final int QUERY_BUILDER_CLAUSE_IN = 1104;
	public static final int QUERY_BUILDER_CLAUSE_LT = 1105;
	public static final int QUERY_BUILDER_CLAUSE_LTE = 1106;
	public static final int QUERY_BUILDER_CLAUSE_CONTAINS = 1107;
	public static final int QUERY_BUILDER_CONTAINS_KEY = 1108;

	public static final MethodIdInterval QB_ASSIGNMENT = new MethodIdInterval(1200, 1299);
	public static final int QUERY_BUILDER_ASSIGNMENT_ADD = 1200;
	public static final int QUERY_BUILDER_ASSIGNMENT_ADD_ALL = 1201;
	public static final int QUERY_BUILDER_ASSIGNMENT_APPEND = 1202;
	public static final int QUERY_BUILDER_ASSIGNMENT_APPEND_ALL = 1203;
	public static final int QUERY_BUILDER_ASSIGNMENT_DECR = 1204;
	public static final int QUERY_BUILDER_ASSIGNMENT_DECR_VAL = 1205;
	public static final int QUERY_BUILDER_ASSIGNMENT_DISCARD = 1206;
	public static final int QUERY_BUILDER_ASSIGNMENT_DISCARD_ALL = 1207;
	public static final int QUERY_BUILDER_ASSIGNMENT_INCR = 1208;
	public static final int QUERY_BUILDER_ASSIGNMENT_INCR_VAL = 1209;
	public static final int QUERY_BUILDER_ASSIGNMENT_PREPREND = 1210;
	public static final int QUERY_BUILDER_ASSIGNMENT_PREPEND_ALL = 1211;
	public static final int QUERY_BUILDER_ASSIGNMENT_PUT = 1212;
	public static final int QUERY_BUILDER_ASSIGNMENT_PUT_ALL = 1213;
	public static final int QUERY_BUILDER_ASSIGNMENT_REMOVE = 1214;
	public static final int QUERY_BUILDER_ASSIGNMENT_REMOVE_ALL = 1215;
	public static final int QUERY_BUILDER_ASSIGNMENT_SET = 1216;
	public static final int QUERY_BUILDER_ASSIGNMENT_SET_IDX = 1217;

	public static final MethodIdInterval ANNOTATIONS = new MethodIdInterval(1300, 1399);
	public static final int AN_CLIENT_LOCAL_SET = 1300;
	public static final int AN_CLIENT_LOCAL_GET = 1301;

	public static final MethodIdInterval RESULT_SET = new MethodIdInterval(1400, 1499);
	public static final int RS_IS_EXHAUSTED = 1400;
	public static final int RS_ONE = 1401;
	public static final int ROW_GET_INT_BY_IDX = 1410;
	public static final int ROW_GET_INT_BY_COL = 1411;
	public static final int ROW_GET_STRING_BY_IDX = 1412;
	public static final int ROW_GET_STRING_BY_COL = 1413;

	/** Methods that bind a value and return itself */
	private static final Set<String> BOUND_STATEMENT_SETTERS = new HashSet<>(Arrays.asList(
			"setBool", "setBytes", "setBytesUnsafe", "setDate", "setDecimal", "setDouble", "setFloat", "setInet", "setInt", "setList",
			"setLong", "setMap", "setSet", "setString", "setToken", "setTupleValue", "setUDTValue", "setUUID", "setVarint"));
	/** Methods that return a value and do not modify the bound statement */
	private static final Set<String> BOUND_STATEMENT_GETTERS = new HashSet<>(Arrays.asList(
			"getBool", "getBytes", "getBytesUnsafe", "getDate", "getDecimal", "getDouble", "getFloat", "getInet", "getInt", "getKeyspace",
			"getList", "getLong", "getMap", "getObject", "getRoutingKey", "getSet", "getString", "getTupleValue", "getUDTValue", "getUUID",
			"getVarint", "isNull", "isSet", "preparedStatement"));

	/** Methods set a value and return itself. The value can be ignored in our analysis, so it is treated as a nop */
	private static final Set<String> STATEMENT_GENERAL_SETTERS = new HashSet<>(Arrays.asList(
			"enableTracing", "disableTracing", "setFetchSize", "setIdempotent", "setPagingState", "setPagingStateUnsafe",
			"setRetryPolicy", "setForceNoValues"));
	/** Methods that return a value and do not modify the statement */
	private static final Set<String> STATEMENT_GETTERS = new HashSet<>(Arrays.asList(
			"getFetchSize", "getKeyspace", "getRetryPolicy", "getRoutingKey", "isIdempotent", "isTracing", "getObject", "getQueryString",
			"getValues", "hasValues", "toString"));

	public static boolean is(final SootMethod method, final MethodIdInterval methodIds) {
		return methodIds.isInInterval(getMethodId(method));
	}

	public static SootMethod getObjectClassInitMethod() {
		final SootClass objectClass = SootTypeUtils.getObjectClass();
		return objectClass.getMethod(SootMethod.constructorName, Collections.emptyList());
	}

	public static int getMethodId(final SootMethod method) {
		Objects.requireNonNull(method);
		final Type declaredFrom = method.getDeclaringClass().getType();
		final String name = method.getName();
		final int nArgs = method.getParameterCount();
		final Type[] paramTypes = new Type[nArgs];
		for (int i = 0; i < nArgs; i++) {
			paramTypes[i] = method.getParameterType(i);
		}
		final boolean isStatic = method.isStatic();

		// java.lang.Object
		if (SootTypeUtils.isObject(declaredFrom)) {
			if (SootMethod.constructorName.equals(name)) {
				return OBJECT_INIT;
			}
		}

		// java.lang.String
		if (SootTypeUtils.isString(declaredFrom)) {
			if (isStatic && nArgs == 1 && "valueOf".equals(name)) {
				return STRING_VALUE_OF;
			}
		}

		// java.util.UUID or UUIDs Factory from datastax
		if (SootTypeUtils.isUUID(declaredFrom) || SootTypeUtils.isUUIDs(declaredFrom)) {
			if (isStatic && nArgs == 0 && ("randomUUID".equals(name) || "random".equals(name) || "timeBased".equals(name))) {
				return UUID_CREATE;
			} else if ("toString".equals(name) && nArgs == 0 && SootTypeUtils.isUUID(declaredFrom)) {
				return UUID_TO_STRING;
			} else if ("fromString".equals(name) && nArgs == 1 && SootTypeUtils.isString(paramTypes[0])
					&& SootTypeUtils.isUUID(declaredFrom)) {
				return UUID_FROM_STRING;
			}
		}

		// general methods
		if (SootTypeUtils.isSetsUtility(declaredFrom)) {
			if (isStatic && nArgs == 1 && "newHashSet".equals(name) && SootTypeUtils.isObjectArray(paramTypes[0])) {
				return SETS_NEW_HASH_SET;
			}
		}

		// java.lang.StringBuilder
		if (SootTypeUtils.isStringBuilder(declaredFrom)) {
			if (SootMethod.constructorName.equals(name)) {
				if (nArgs == 0)
					return STRING_BUILDER_INIT_NO_ARG;
				else if (nArgs == 1 && SootTypeUtils.isString(paramTypes[0]))
					return STRING_BUILDER_INIT_STR_ARG;
				else if (nArgs == 1 && SootTypeUtils.isInt(paramTypes[0]))
					return STRING_BUILDER_INIT_INT_ARG;
			} else if (nArgs == 1 && "append".equals(name)) {
				return STRING_BUILDER_APPEND;
			} else if (nArgs == 0 && "toString".equals(name)) {
				return STRING_BUILDER_TO_STRING;
			}
		}

		// com.datastax.driver.core.Session
		if (SootTypeUtils.isSession(declaredFrom) || SootTypeUtils.implementsSession(declaredFrom)) {
			if ("execute".equals(name)) {
				if (nArgs == 1 && SootTypeUtils.isString(paramTypes[0])) {
					return SESSION_EXEC_STRING_NO_BINDS;
				} else if (nArgs == 2 && SootTypeUtils.isString(paramTypes[0])) {
					return SESSION_EXEC_STRING_WITH_BINDS;
				} else if (nArgs == 1 && SootTypeUtils.isStatement(paramTypes[0])) {
					return SESSION_EXEC_STMT;
				}
			} else if ("executeAsync".equals(name)) {
				if (nArgs == 1 && SootTypeUtils.isString(paramTypes[0])) {
					return SESSION_EXEC_ASYNC_STRING_NO_BINDS;
				} else if (nArgs == 2 && SootTypeUtils.isString(paramTypes[0])) {
					return SESSION_EXEC_ASYNC_STRING_WITH_BINDS;
				} else if (nArgs == 1 && SootTypeUtils.isStatement(paramTypes[0])) {
					return SESSION_EXEC_ASYNC_STMT;
				}
			} else if ("prepare".equals(name) && nArgs == 1) {
				if (SootTypeUtils.isString(paramTypes[0])) {
					return SESSION_PREPARE_STRING;
				} else if (SootTypeUtils.isStatement(paramTypes[0])) {
					return SESSION_PREPARE_STMT;
				}
			}
		}

		// com.datastax.driver.core.Statement with all subclasses & subinterfaces
		if (SootTypeUtils.isStatement(declaredFrom)) {
			if (SootTypeUtils.isPreparedStatement(declaredFrom)) {
				if ("bind".equals(name) && nArgs == 0) {
					return PREP_STMT_BIND_NO_ARG;
				} else if ("bind".equals(name) && nArgs == 1 && SootTypeUtils.isObjectArray(paramTypes[0])) {
					return PREP_STMT_BIND_1_ARG;
				}
			}

			if (SootTypeUtils.isBoundStatement(declaredFrom)) {
				if (SootMethod.constructorName.equals(name) && nArgs == 1 && SootTypeUtils.isPreparedStatement(paramTypes[0])) {
					return BOUND_STMT_INIT;
				} else if ("bind".equals(name) && nArgs == 1 && SootTypeUtils.isObjectArray(paramTypes[0])) {
					return BOUND_STMT_BIND_1_ARG;
				} else if (nArgs == 1 && "setRoutingKey".equals(name)) {
					return BOUND_STMT_SET_ROUTING_KEY;
				} else if (nArgs == 1 && "setPartitionKey".equals(name)) {
					return BOUND_STMT_SET_PARTITION_KEY_TOKEN;
				} else if (nArgs == 1 && "setToNull".equals(name) && SootTypeUtils.isInt(paramTypes[0])) {
					return BOUND_STMT_SET_TO_NULL_WITH_IDX;
				} else if (nArgs == 1 && "setToNull".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
					return BOUND_STMT_SET_TO_NULL_WITH_NAME;
				} else if (nArgs == 2 && BOUND_STATEMENT_SETTERS.contains(name) && SootTypeUtils.isInt(paramTypes[0])) {
					return BOUND_STMT_SET_WITH_IDX;
				} else if (nArgs == 2 && BOUND_STATEMENT_SETTERS.contains(name) && SootTypeUtils.isString(paramTypes[0])) {
					return BOUND_STMT_SET_WITH_NAME;
				} else if (BOUND_STATEMENT_GETTERS.contains(name)) {
					return BOUND_STMT_GET_SOME_VALUE;
				}
			}

			if (SootTypeUtils.isBatchStatement(declaredFrom)) {
				if (SootMethod.constructorName.equals(name) && nArgs == 0) {
					return BATCH_STMT_INIT;
				} else if ("add".equals(name) && nArgs == 1 && SootTypeUtils.isStatement(paramTypes[0])) {
					return BATCH_STMT_ADD;
				}
			}

			if (SootTypeUtils.isSimpleStatement(declaredFrom)) {
				if (SootMethod.constructorName.equals(name) && nArgs == 1 && SootTypeUtils.isString(paramTypes[0])) {
					return SIMPLE_STMT_INIT_NO_BINDS;
				} else if (SootMethod.constructorName.equals(name) && nArgs == 2 && SootTypeUtils.isString(paramTypes[0])
						&& SootTypeUtils.isObjectArray(paramTypes[1])) {
					return SIMPLE_STMT_INIT_WITH_BINDS;
				}
			}

			if (nArgs == 1 && "setConsistencyLevel".equals(name) && SootTypeUtils.isConsistencyLevel(paramTypes[0])) {
				return STMT_SET_CONSISTENCY_LEVEL;
			} else if (nArgs == 1 && "setSerialConsistencyLevel".equals(name) && SootTypeUtils.isConsistencyLevel(paramTypes[0])) {
				return STMT_SET_SERIAL_CONSISTENCY_LEVEL;
			} else if (nArgs == 0 && "getConsistencyLevel".equals(name)) {
				return STMT_GET_CONSISTENCY_LEVEL;
			} else if (nArgs == 0 && "getSerialConsistencyLevel".equals(name)) {
				return STMT_GET_SERIAL_CONSISTENCY_LEVEL;
			} else if (STATEMENT_GENERAL_SETTERS.contains(name)) {
				return STMT_GENERAL_SETTER_RETURNING_THIS;
			} else if (STATEMENT_GETTERS.contains(name)) {
				return STMT_GENERAL_GETTER;
			}
		}

		// com.datastax.driver.core.querybuilder.QueryBuilder
		if (SootTypeUtils.isQueryBuilder(declaredFrom) && isStatic) {
			if (nArgs == 1 && "insertInto".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_INSERT_INTO_TABLE;
			} else if (nArgs == 2 && "insertInto".equals(name) && SootTypeUtils.isString(paramTypes[0])
					&& SootTypeUtils.isString(paramTypes[1])) {
				return QUERY_BUILDER_INSERT_INTO_KEYSPACE_TABLE;
			} else if (nArgs == 0 && "select".equals(name)) {
				return QUERY_BUILDER_SELECT;
			} else if (nArgs == 1 && "select".equals(name) && SootTypeUtils.isStringArray(paramTypes[0])) {
				return QUERY_BUILDER_SELECT_COLS;
			} else if (nArgs == 2 && "eq".equals(name) && SootTypeUtils.isString(paramTypes[0]) && SootTypeUtils.isObject(paramTypes[1])) {
				return QUERY_BUILDER_CLAUSE_EQ;
			} else if (nArgs == 2 && "gt".equals(name) && SootTypeUtils.isString(paramTypes[0]) && SootTypeUtils.isObject(paramTypes[1])) {
				return QUERY_BUILDER_CLAUSE_GT;
			} else if (nArgs == 2 && "gte".equals(name) && SootTypeUtils.isString(paramTypes[0]) && SootTypeUtils.isObject(paramTypes[1])) {
				return QUERY_BUILDER_CLAUSE_GTE;
			} else if (nArgs == 2 && "in".equals(name) && SootTypeUtils.isString(paramTypes[0])
					&& (SootTypeUtils.isObjectArray(paramTypes[1]) || SootTypeUtils.isList(paramTypes[1]))) {
				return QUERY_BUILDER_CLAUSE_IN;
			} else if (nArgs == 2 && "lt".equals(name) && SootTypeUtils.isString(paramTypes[0]) && SootTypeUtils.isObject(paramTypes[1])) {
				return QUERY_BUILDER_CLAUSE_LT;
			} else if (nArgs == 2 && "lte".equals(name) && SootTypeUtils.isString(paramTypes[0]) && SootTypeUtils.isObject(paramTypes[1])) {
				return QUERY_BUILDER_CLAUSE_LTE;
			} else if (nArgs == 2 && "contains".equals(name) && SootTypeUtils.isString(paramTypes[0])
					&& SootTypeUtils.isObject(paramTypes[1])) {
				return QUERY_BUILDER_CLAUSE_CONTAINS;
			} else if (nArgs == 2 && "containsKey".equals(name) && SootTypeUtils.isString(paramTypes[0])
					&& SootTypeUtils.isObject(paramTypes[1])) {
				return QUERY_BUILDER_CONTAINS_KEY;
			} else if (nArgs == 0 && "delete".equals(name)) {
				return QUERY_BUILDER_DELETE;
			} else if (nArgs == 1 && "delete".equals(name) && SootTypeUtils.isStringArray(paramTypes[0])) {
				return QUERY_BUILDER_DELETE_COLS;
			} else if (nArgs == 0 && "bindMarker".equals(name)) {
				return QUERY_BUILDER_BINDMARKER;
			} else if (nArgs == 1 && "bindMarker".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_BINDMARKER_NAME;
			} else if (nArgs == 2 && "add".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_ADD;
			} else if (nArgs == 2 && "addAll".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_ADD_ALL;
			} else if (nArgs == 2 && "append".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_APPEND;
			} else if (nArgs == 2 && "appendAll".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_APPEND_ALL;
			} else if (nArgs == 1 && "decr".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_DECR;
			} else if (nArgs == 2 && "decr".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_DECR_VAL;
			} else if (nArgs == 2 && "discard".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_DISCARD;
			} else if (nArgs == 2 && "discardAll".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_DISCARD_ALL;
			} else if (nArgs == 1 && "incr".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_INCR;
			} else if (nArgs == 2 && "incr".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_INCR_VAL;
			} else if (nArgs == 2 && "prepend".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_PREPREND;
			} else if (nArgs == 2 && "prependAll".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_PREPEND_ALL;
			} else if (nArgs == 3 && "put".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_PUT;
			} else if (nArgs == 2 && "putAll".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_PUT_ALL;
			} else if (nArgs == 2 && "remove".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_REMOVE;
			} else if (nArgs == 2 && "removeAll".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_REMOVE_ALL;
			} else if (nArgs == 2 && "set".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_SET;
			} else if (nArgs == 3 && "setIdx".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_ASSIGNMENT_SET_IDX;
			} else if (nArgs == 2 && "update".equals(name) && SootTypeUtils.isString(paramTypes[0])
					&& SootTypeUtils.isString(paramTypes[1])) {
				return QUERY_BUILDER_UPDATE_KEYSPACE_TABLE;
			} else if (nArgs == 1 && "update".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return QUERY_BUILDER_UPDATE_TABLE;
			}
		}

		// INSERT query builder
		if (SootTypeUtils.isInsert(declaredFrom)) {

			if (nArgs == 2 && "value".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return INSERT_VALUE;
			} else if (nArgs == 0 && "ifNotExists".equals(name)) {
				return INSERT_IF_NOT_EXISTS;
			}
		}

		// SELECT query builder
		if (SootTypeUtils.isSelect(declaredFrom)) {

			if (nArgs == 0 && "all".equals(name)) {
				return SELECT_ALL;
			} else if (nArgs == 1 && "column".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return SELECT_COLUMN;
			} else if (nArgs == 0 && "countAll".equals(name)) {
				return SELECT_COUNT_ALL;
			} else if (nArgs == 0 && "distinct".equals(name)) {
				return SELECT_DISTINCT;
			} else if (nArgs == 1 && "as".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return SELECT_COLUMN_AS;
			} else if (nArgs == 1 && "from".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return SELECT_FROM_TABLE;
			} else if (nArgs == 2 && "from".equals(name) && SootTypeUtils.isString(paramTypes[0])
					&& SootTypeUtils.isString(paramTypes[1])) {
				return SELECT_FROM_TABLE_KEYSPACE;
			} else if (nArgs == 0 && "allowFiltering".equals(name)) {
				return SELECT_ALLOW_FILTERING;
			} else if (nArgs == 1 && "limit".equals(name)
					&& (SootTypeUtils.isInt(paramTypes[0]) || SootTypeUtils.isBindMarker(paramTypes[0]))) {
				return SELECT_LIMIT;
			} else if (nArgs == 1 && "orderBy".equals(name) && SootTypeUtils.isOrderingArray(paramTypes[0])) {
				return SELECT_ORDER_BY;
			} else if (nArgs == 0 && "where".equals(name)) {
				return SELECT_WHERE;
			} else if (nArgs == 1 && "where".equals(name) && SootTypeUtils.isClause(paramTypes[0])) {
				return SELECT_WHERE_CLAUSE;
			} else if (nArgs == 1 && "and".equals(name) && SootTypeUtils.isClause(paramTypes[0])) {
				return SELECT_WHERE_AND_CLAUSE;
			}
		}

		// DELETE query builder
		if (SootTypeUtils.isDelete(declaredFrom)) {

			if (nArgs == 0 && "all".equals(name)) {
				return DELETE_ALL;
			} else if (nArgs == 1 && "column".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return DELETE_COLUMN;
			} else if (nArgs == 2 && "listElt".equals(name) && SootTypeUtils.isString(paramTypes[0])
					&& SootTypeUtils.isInt(paramTypes[1])) {
				return DELETE_LIST_ELT;
			} else if (nArgs == 2 && "mapElt".equals(name) && SootTypeUtils.isString(paramTypes[0])
					&& SootTypeUtils.isObject(paramTypes[1])) {
				return DELETE_MAP_ELT;
			} else if (nArgs == 2 && "setElt".equals(name) && SootTypeUtils.isString(paramTypes[0])
					&& SootTypeUtils.isObject(paramTypes[1])) {
				return DELETE_SET_ELT;
			} else if (nArgs == 1 && "from".equals(name) && SootTypeUtils.isString(paramTypes[0])) {
				return DELETE_FROM_TABLE;
			} else if (nArgs == 2 && "from".equals(name) && SootTypeUtils.isString(paramTypes[0])
					&& SootTypeUtils.isString(paramTypes[1])) {
				return DELETE_FROM_TABLE_KEYSPACE;
			} else if (nArgs == 0 && "ifExists".equals(name)) {
				return DELETE_IF_EXISTS;
			} else if (nArgs == 0 && "onlyIf".equals(name)) {
				return DELETE_ONLY_IF;
			} else if (nArgs == 1 && "onlyIf".equals(name) && SootTypeUtils.isClause(paramTypes[0])) {
				return DELETE_ONLY_IF_CLAUSE;
			} else if (SootTypeUtils.isDeleteConditions(declaredFrom) && nArgs == 1 && "and".equals(name)
					&& SootTypeUtils.isClause(paramTypes[0])) {
				return DELETE_ONLY_IF_AND_CLAUSE;
			} else if (nArgs == 0 && "where".equals(name)) {
				return DELETE_WHERE;
			} else if (nArgs == 1 && "where".equals(name) && SootTypeUtils.isClause(paramTypes[0])) {
				return DELETE_WHERE_CLAUSE;
			} else if (SootTypeUtils.isDeleteWhere(declaredFrom) && nArgs == 1 && "and".equals(name)
					&& SootTypeUtils.isClause(paramTypes[0])) {
				return DELETE_WHERE_AND_CLAUSE;
			}
		}

		// UPDATE query builder
		if (SootTypeUtils.isUpdate(declaredFrom)) {

			if (nArgs == 0 && "with".equals(name)) {
				return UPDATE_WITH;
			} else if (nArgs == 1 && "with".equals(name) && SootTypeUtils.isAssignment(paramTypes[0])) {
				return UPDATE_WITH_ASSIGNMENT;
			} else if (SootTypeUtils.isUpdateAssignment(declaredFrom) && nArgs == 1 && "and".equals(name)
					&& SootTypeUtils.isAssignment(paramTypes[0])) {
				return UPDATE_WITH_AND_ASSIGNMENT;
			} else if (nArgs == 0 && "ifExists".equals(name)) {
				return UPDATE_IF_EXISTS;
			} else if (nArgs == 0 && "onlyIf".equals(name)) {
				return UPDATE_ONLY_IF;
			} else if (nArgs == 1 && "onlyIf".equals(name) && SootTypeUtils.isClause(paramTypes[0])) {
				return UPDATE_ONLY_IF_CLAUSE;
			} else if (SootTypeUtils.isUpdateConditions(declaredFrom) && nArgs == 1 && "and".equals(name)
					&& SootTypeUtils.isClause(paramTypes[0])) {
				return UPDATE_ONLY_IF_AND_CLAUSE;
			} else if (nArgs == 0 && "where".equals(name)) {
				return UPDATE_WHERE;
			} else if (nArgs == 1 && "where".equals(name) && SootTypeUtils.isClause(paramTypes[0])) {
				return UPDATE_WHERE_CLAUSE;
			} else if (SootTypeUtils.isUpdateWhere(declaredFrom) && nArgs == 1 && "and".equals(name)
					&& SootTypeUtils.isClause(paramTypes[0])) {
				return UPDATE_WHERE_AND_CLAUSE;
			}
		}

		// Annotations
		if (SootTypeUtils.isClientLocalValuesAnnotation(declaredFrom)) {

			if (nArgs == 2 && "set".equals(name)) {
				return AN_CLIENT_LOCAL_SET;
			} else if (nArgs == 1 && "get".equals(name)) {
				return AN_CLIENT_LOCAL_GET;
			}
		}

		// ResultSet & Row
		if (SootTypeUtils.isResultSet(declaredFrom) || SootTypeUtils.isRow(declaredFrom)) {

			if (SootTypeUtils.isResultSet(declaredFrom)) {
				if (nArgs == 0 && "isExhausted".equals(name)) {
					return RS_IS_EXHAUSTED;
				} else if (nArgs == 0 && "one".equals(name)) {
					return RS_ONE;
				}
			} else if (SootTypeUtils.isRow(declaredFrom)) {
				if (nArgs == 1 && "getInt".equals(name)) {
					if (SootTypeUtils.isInt(paramTypes[0])) {
						return ROW_GET_INT_BY_IDX;
					} else if (SootTypeUtils.isString(paramTypes[0])) {
						return ROW_GET_INT_BY_COL;
					}
				} else if (nArgs == 1 && "getString".equals(name)) {
					if (SootTypeUtils.isInt(paramTypes[0])) {
						return ROW_GET_STRING_BY_IDX;
					} else if (SootTypeUtils.isString(paramTypes[0])) {
						return ROW_GET_STRING_BY_COL;
					}
				}
			}
		}

		return -1;
	}

	public static Method methodFrom(final SootMethod sootMethod) {
		final List<String> args = new ArrayList<>(sootMethod.getParameterCount());
		for (int i = 0; i < sootMethod.getParameterCount(); i++) {
			args.add(sootMethod.getParameterType(i).toString());
		}
		return Method.create(sootMethod.getDeclaringClass().getShortName(), sootMethod.getName(), args,
				sootMethod.getReturnType().toString(), sootMethod.getSignature());
	}

	public static class MethodIdInterval {
		public final int minId;
		public final int maxId;

		public MethodIdInterval(final int minId, final int maxId) {
			this.minId = minId;
			this.maxId = maxId;
		}

		public boolean isInInterval(final int methodId) {
			return minId <= methodId && methodId <= maxId;
		}
	}
}
