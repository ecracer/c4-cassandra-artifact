package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.Value;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ConsistencyLevelValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ConsistencyLevelValue.Level;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.IntValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.FieldDesc;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.SootField;
import soot.SootMethod;
import soot.jimple.Constant;
import soot.jimple.FieldRef;
import soot.jimple.IntConstant;
import soot.jimple.NullConstant;
import soot.jimple.StringConstant;
import soot.tagkit.ConstantValueTag;
import soot.tagkit.IntegerConstantValueTag;
import soot.tagkit.StringConstantValueTag;
import soot.tagkit.Tag;

/**
 * Helper for transforming {@link soot.Value} to {@link Value}
 */
public class SootValUtils {

	public static ImmutableValue transformImmutableValue(final Constant constant, final ProgramPointId calledFrom) {
		return transformImmutableValue(constant, calledFrom, 0);
	}

	public static ImmutableValue transformImmutableValue(final Constant constant, final ProgramPointId calledFrom, final int idx) {
		if (constant instanceof StringConstant) {
			return transformString((StringConstant) constant);
		} else if (constant instanceof NullConstant) {
			return transformNull((NullConstant) constant);
		} else if (constant instanceof IntConstant) {
			return transformInt((IntConstant) constant);
		} else {
			return UnknownImmutableValue.create(calledFrom);
		}
	}

	public static StringValue transformString(final StringConstant stringConstant) {
		return StringValue.create(stringConstant.value);
	}

	public static NullValue transformNull(final NullConstant nullConstant) {
		return NullValue.create();
	}

	public static IntValue transformInt(final IntConstant intConstant) {
		return IntValue.create(intConstant.value);
	}

	public static ConsistencyLevelValue transformConsistencyLevel(final FieldRef fieldRef) {
		final SootField field = fieldRef.getField();
		if (field.isStatic() && SootTypeUtils.isConsistencyLevel(field.getDeclaringClass().getType())) {
			return ConsistencyLevelValue.create(Level.valueOf(field.getName()));
		} else {
			throw new IllegalArgumentException();
		}
	}

	public static String getClassName(final SootMethod method) {
		return method.getDeclaringClass().getName();
	}

	public static String getMethodSignature(final SootMethod method) {
		return method.getSubSignature();
	}

	public static VarLocal transformLocal(final Local local, final CallStack callStack) {
		return VarLocal.create(callStack, local.getName(), local.getType().toString(), SootTypeUtils.resolveVarType(local.getType()));
	}

	public static VarLocalOrImmutableValue transformLocalOrImmutable(final soot.Value value, final ProgramPointId calledFrom) {
		if (value instanceof Local) {
			return transformLocal((Local) value, calledFrom.callStack);
		} else {
			return transformImmutableValue((Constant) value, calledFrom);
		}
	}

	public static boolean isStaticField(final FieldRef fieldRef) {
		return fieldRef.getField().isStatic();
	}

	public static FieldDesc transformField(final FieldRef fieldRef) {
		final SootField field = Objects.requireNonNull(fieldRef).getField();
		return transformField(field);
	}

	public static FieldDesc transformField(final SootField field) {
		ImmutableValue defaultValue = getDefaultValue(field.getType());
		for (final Tag tag : field.getTags()) {
			if (tag instanceof ConstantValueTag) {
				if (tag instanceof StringConstantValueTag) {
					defaultValue = StringValue.create(((StringConstantValueTag) tag).getStringValue());
				} else if (tag instanceof IntegerConstantValueTag) {
					defaultValue = IntValue.create(((IntegerConstantValueTag) tag).getIntValue());
				} else {
					defaultValue = UnknownImmutableValue.create(ProgramPointId.create(CallStack.EMPTY_STACK, new Object()));
				}
			}
		}
		return FieldDesc.create(field.getDeclaringClass().getName(), field.getName(), field.isStatic(),
				SootTypeUtils.resolveVarType(field.getType()), defaultValue, SootAnnotationUtils.hasAnnotation(field));
	}

	private final static Map<soot.Type, UnknownImmutableValue> defaultValues = new HashMap<>();

	public static ImmutableValue getDefaultValue(final soot.Type type) {
		if (SootTypeUtils.isInt(type)) {
			return IntValue.create(0);
		} else if (SootTypeUtils.isPrimitive(type)) {
			// all elements are initialized with the same element
			if (!defaultValues.containsKey(type)) {
				defaultValues.put(type, UnknownImmutableValue.create(ProgramPointId.create(CallStack.EMPTY_STACK, new Object())));
			}
			return defaultValues.get(type);
		} else {
			return NullValue.create();
		}
	}
}
