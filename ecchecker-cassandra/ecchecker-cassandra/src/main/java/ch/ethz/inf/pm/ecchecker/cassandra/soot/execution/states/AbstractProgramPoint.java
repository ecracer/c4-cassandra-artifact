package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.graphs.ExecuteGraph;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.AbstractReference;

/**
 * An abstract transformer for a given program point. The {@link ExecuteGraph} is built from all these
 * AbstractTransformers.
 *
 */
public abstract class AbstractProgramPoint {

	public final ProgramPointId id;

	protected AbstractProgramPoint(final ProgramPointId id) {
		this.id = Objects.requireNonNull(id);
	}

	public abstract void transform(final ExecutionState state, final ExecutionState oldState);

	public Set<AbstractReference> updatedReferences() {
		return Collections.emptySet();
	}

	public AbstractProgramPoint createWithCallstack(final CallStack callStack) {
		return createWithProgramPointId(ProgramPointId.create(callStack, id.idInMethod));
	}

	protected abstract AbstractProgramPoint createWithProgramPointId(final ProgramPointId id);

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractProgramPoint other = (AbstractProgramPoint) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
