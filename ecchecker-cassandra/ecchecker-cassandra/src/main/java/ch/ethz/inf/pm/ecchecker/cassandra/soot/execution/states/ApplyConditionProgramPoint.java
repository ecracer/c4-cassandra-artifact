package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states;

import java.util.Objects;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootValUtils;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.EmptyResultValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarType;
import soot.Local;
import soot.Value;
import soot.jimple.ConditionExpr;
import soot.jimple.EqExpr;
import soot.jimple.IntConstant;
import soot.jimple.NeExpr;

/**
 * Applies a condition from an if statement to the {@link ExecutionState}
 */
public class ApplyConditionProgramPoint extends AbstractProgramPoint {

	private final ConditionExpr conditionExpr;
	private final boolean negate;

	private ApplyConditionProgramPoint(final ProgramPointId id, final ConditionExpr conditionExpr, final boolean negate) {
		super(id);
		this.conditionExpr = Objects.requireNonNull(conditionExpr);
		this.negate = negate;
	}

	public static ApplyConditionProgramPoint create(final CallStack callStack, final ConditionExpr conditionExpr, final boolean negate) {
		return new ApplyConditionProgramPoint(ProgramPointId.create(callStack, conditionExpr), conditionExpr, negate);
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		if (conditionExpr.getOp1() instanceof Local) {
			handleConditionExpr(conditionExpr, (Local) conditionExpr.getOp1(), conditionExpr.getOp2(), state, oldState);
		} else if (conditionExpr.getOp2() instanceof Local) {
			handleConditionExpr(conditionExpr, (Local) conditionExpr.getOp2(), conditionExpr.getOp1(), state, oldState);
		}
	}

	private void handleConditionExpr(final ConditionExpr condition, final Local local0, final Value otherValue,
			final ExecutionState state, final ExecutionState oldState) {
		final VarLocal local = SootValUtils.transformLocal(local0, id.callStack);
		if (!(VarType.EXCLUDED.equals(local.varType))) {
			if (state.isLocalInitialized(local)) {
				final Set<AbstractReference> refs = state.getPointsTo(local);
				if (refs.size() == 1) {
					final AbstractReference ref = refs.iterator().next();
					if (!state.isNullReference(ref)) {
						final Set<ConcreteValue> vals = state.getConcreteValue(ref);
						if (vals.size() == 1) {
							final ConcreteValue singleVal = vals.iterator().next();
							if (singleVal instanceof EmptyResultValue && otherValue instanceof IntConstant &&
									(conditionExpr instanceof EqExpr || conditionExpr instanceof NeExpr)) {
								boolean returnedEmpty = ((EmptyResultValue) singleVal).isTrueWhenEmpty;
								if (((IntConstant) otherValue).value == 0) {
									returnedEmpty = !returnedEmpty;
								}
								if (negate) {
									returnedEmpty = !returnedEmpty;
								}
								if (conditionExpr instanceof NeExpr) {
									returnedEmpty = !returnedEmpty;
								}
								if (returnedEmpty) {
									state.addMustHaveReturnedEmptyResultConstraint(((EmptyResultValue) singleVal).resultFrom);
								} else {
									state.addMustHaveReturnedNonEmptyResultConstraint(((EmptyResultValue) singleVal).resultFrom);
								}
							}
						}
					}
				}
			}
		}
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new ApplyConditionProgramPoint(id, conditionExpr, negate);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((conditionExpr == null) ? 0 : conditionExpr.hashCode());
		result = prime * result + (negate ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplyConditionProgramPoint other = (ApplyConditionProgramPoint) obj;
		if (conditionExpr == null) {
			if (other.conditionExpr != null)
				return false;
		} else if (!conditionExpr.equals(other.conditionExpr))
			return false;
		if (negate != other.negate)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ApplyConditionProgramPoint [conditionExpr=" + conditionExpr + ", negate=" + negate + "]";
	}
}
