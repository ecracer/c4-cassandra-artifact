package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootTypeUtils;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootValUtils;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarType;
import soot.Local;
import soot.ValueBox;
import soot.jimple.DefinitionStmt;
import soot.jimple.Stmt;

/**
 * Sound transformer for a general statement. Sets everything to unknown.
 */
public class DefaultProgramPoint extends AbstractProgramPoint {

	public final Local definition;
	public final ImmutableSet<Local> mutableUses;

	private DefaultProgramPoint(final ProgramPointId programPointId, final Local definition, final ImmutableSet<Local> mutableUses) {
		super(programPointId);
		this.definition = definition;
		this.mutableUses = Objects.requireNonNull(mutableUses);
	}

	public static DefaultProgramPoint create(final CallStack callStack, final Stmt stmt) {
		final Local definition;
		if (stmt instanceof DefinitionStmt
				&& !SootTypeUtils.resolveVarType(((DefinitionStmt) stmt).getLeftOp().getType()).equals(VarType.EXCLUDED)) {
			// left must be a local (fields and arrays are handeled)
			definition = (Local) ((DefinitionStmt) stmt).getLeftOp();
		} else {
			definition = null;
		}
		final Set<Local> mutableUses = new HashSet<>();
		for (final ValueBox valueBox : stmt.getUseBoxes()) {
			if ((valueBox.getValue() instanceof Local) && !SootTypeUtils.isImmutable(valueBox.getValue().getType())
					&& !SootTypeUtils.resolveVarType(valueBox.getValue().getType()).equals(VarType.EXCLUDED)) {
				// uses are Immediates (fields and arrays are handeled), so a variable must be a local
				mutableUses.add((Local) valueBox.getValue());
			}
		}
		if (definition != null || !mutableUses.isEmpty()) {
			return new DefaultProgramPoint(ProgramPointId.create(callStack, stmt), definition, ImmutableSet.copyOf(mutableUses));
		} else {
			return null;
		}
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		if (definition != null) {
			final VarLocal def = SootValUtils.transformLocal(definition, id.callStack);
			if (!def.varType.equals(VarType.EXCLUDED)) {
				state.setUnknown(def, id);
			}
		}
		if (!mutableUses.isEmpty()) {
			final Set<VarLocal> setToUnknown = new HashSet<>();
			for (final Local mutableUse : mutableUses) {
				setToUnknown.add(SootValUtils.transformLocal(mutableUse, id.callStack));
			}
			state.setToUnknown(setToUnknown, id);
		}
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new DefaultProgramPoint(id, definition, mutableUses);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((definition == null) ? 0 : definition.hashCode());
		result = prime * result + ((mutableUses == null) ? 0 : mutableUses.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DefaultProgramPoint other = (DefaultProgramPoint) obj;
		if (definition == null) {
			if (other.definition != null)
				return false;
		} else if (!definition.equals(other.definition))
			return false;
		if (mutableUses == null) {
			if (other.mutableUses != null)
				return false;
		} else if (!mutableUses.equals(other.mutableUses))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DefaultProgramPoint [definition=" + definition + ", mutableUses=" + mutableUses + "]";
	}
}
