package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states;

import java.util.Objects;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootValUtils;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.ObjectReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarType;
import soot.Local;
import soot.jimple.IdentityStmt;
import soot.jimple.ParameterRef;
import soot.jimple.ThisRef;

/**
 * Transformer for setting up the this-pointer and the parameters
 */
public class IdentityStmtProgramPoint extends AbstractProgramPoint {

	private final IdentityStmt idStmt;

	private IdentityStmtProgramPoint(final ProgramPointId id, final IdentityStmt idStmt) {
		super(id);
		if (!(idStmt.getRightOp() instanceof ThisRef) && !(idStmt.getRightOp() instanceof ParameterRef)) {
			throw new IllegalArgumentException();
		}
		this.idStmt = Objects.requireNonNull(idStmt);
	}

	public static IdentityStmtProgramPoint create(final CallStack callStack, final IdentityStmt idStmt) {
		return new IdentityStmtProgramPoint(ProgramPointId.create(callStack, idStmt), idStmt);
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		if (idStmt.getRightOp() instanceof ThisRef) {
			final VarLocal leftLocal = SootValUtils.transformLocal((Local) idStmt.getLeftOp(), id.callStack);
			if (!leftLocal.varType.equals(VarType.INCLUDED)) {
				throw new RuntimeException("BaseLocal must be included");
			}
			final Set<VarLocal> baseLocals = state.getBaseLocal(id.callStack);
			boolean allUnknown = true;
			for (final VarLocal baseLocal : baseLocals) {
				for (final AbstractReference ref : state.getPointsTo(baseLocal)) {
					if (!(ref instanceof ObjectReference)) {
						allUnknown = false;
						continue;
					}
					if (!state.getObjectValue((ObjectReference) ref).isUnknownObject()) {
						allUnknown = false;
					}
				}
			}
			if (allUnknown) {
				state.setUnknown(leftLocal, id);
			} else {
				state.setLocalFromPhi(leftLocal, state.getBaseLocal(id.callStack), id);
			}
		} else {
			// ParameterRef
			final VarLocal leftLocal = SootValUtils.transformLocal((Local) idStmt.getLeftOp(), id.callStack);
			final Set<VarLocalOrImmutableValue> params = state.getParameter(id.callStack, ((ParameterRef) idStmt.getRightOp()).getIndex());
			state.setLocalFromPhi(leftLocal, params, id);
		}
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(final ProgramPointId id) {
		return new IdentityStmtProgramPoint(id, idStmt);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((idStmt == null) ? 0 : idStmt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdentityStmtProgramPoint other = (IdentityStmtProgramPoint) obj;
		if (idStmt == null) {
			if (other.idStmt != null)
				return false;
		} else if (!idStmt.equals(other.idStmt))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IdentityStmtProgramPoint [" + idStmt + "]";
	}
}
