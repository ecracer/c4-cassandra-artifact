package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states;

import java.util.Objects;

import soot.jimple.IfStmt;

/**
 * Marker for an ifStmt
 */
public class IfStmtProgramPoint extends AbstractProgramPoint {

	public final IfStmt ifStmt;

	private IfStmtProgramPoint(final ProgramPointId id, final IfStmt ifStmt) {
		super(id);
		this.ifStmt = Objects.requireNonNull(ifStmt);
	}

	public static IfStmtProgramPoint create(final CallStack callStack, final IfStmt ifStmt) {
		return new IfStmtProgramPoint(ProgramPointId.create(callStack, ifStmt), ifStmt);
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		// Nop
		// Only used as marker
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new IfStmtProgramPoint(id, ifStmt);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ifStmt == null) ? 0 : ifStmt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		IfStmtProgramPoint other = (IfStmtProgramPoint) obj;
		if (ifStmt == null) {
			if (other.ifStmt != null)
				return false;
		} else if (!ifStmt.equals(other.ifStmt))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IfStmtProgramPoint [" + ifStmt + "]";
	}
}
