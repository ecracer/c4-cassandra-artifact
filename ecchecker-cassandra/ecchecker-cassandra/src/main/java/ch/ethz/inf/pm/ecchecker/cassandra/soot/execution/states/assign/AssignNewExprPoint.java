package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.assign;

import java.util.Collections;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootTypeUtils;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootValUtils;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.BaseReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.ObjectReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.StringBuilderValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.RefObjectValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarType;
import soot.Local;
import soot.jimple.AssignStmt;
import soot.jimple.NewExpr;

/**
 * Transformer for the assignment of a new object
 */
public class AssignNewExprPoint extends AbstractAssignProgramPoint {

	private AssignNewExprPoint(final ProgramPointId id, final AssignStmt assign) {
		super(id, assign);
		if (!(assign.getLeftOp() instanceof Local) || !(assign.getRightOp() instanceof NewExpr)) {
			throw new IllegalArgumentException();
		}
	}

	public static AssignNewExprPoint create(final CallStack callStack, final AssignStmt assign) {
		return new AssignNewExprPoint(ProgramPointId.create(callStack, assign), assign);
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		final VarLocal leftLocal = SootValUtils.transformLocal((Local) assign.getLeftOp(), id.callStack);
		if (!leftLocal.varType.equals(VarType.EXCLUDED)) {
			final NewExpr newExpr = (NewExpr) assign.getRightOp();
			if (SootTypeUtils.isStringBuilder(newExpr.getBaseType())) {
				final BaseReference ref = BaseReference.create(id);
				state.setBaseValue(leftLocal, ref, Collections.singleton(StringBuilderValue.create()), id, oldState);
			} else if (!leftLocal.varType.equals(VarType.INCLUDED)) {
				state.setUnknown(leftLocal, id);
			} else {
				final ObjectReference ref = ObjectReference.create(id);
				final RefObjectValue val = RefObjectValue.create(newExpr.getBaseType().getClassName(),
						state.getIncludedFields().get(newExpr.getBaseType().getClassName()));
				state.setObjectValue(leftLocal, ref, val, id, oldState);
			}
		}
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new AssignNewExprPoint(id, assign);
	}
}
