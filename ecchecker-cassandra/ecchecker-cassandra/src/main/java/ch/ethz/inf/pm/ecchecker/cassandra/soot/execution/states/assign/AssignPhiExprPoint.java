package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.assign;

import java.util.HashSet;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootValUtils;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarType;
import soot.Local;
import soot.Value;
import soot.jimple.AssignStmt;
import soot.jimple.Constant;
import soot.shimple.PhiExpr;

/**
 * Transformer for a phiNode
 */
public class AssignPhiExprPoint extends AbstractAssignProgramPoint {

	private AssignPhiExprPoint(final ProgramPointId id, final AssignStmt assign) {
		super(id, assign);
		if (!(assign.getLeftOp() instanceof Local) || !(assign.getRightOp() instanceof PhiExpr)) {
			throw new IllegalArgumentException();
		}
	}

	public static AssignPhiExprPoint create(final CallStack callStack, final AssignStmt assign) {
		return new AssignPhiExprPoint(ProgramPointId.create(callStack, assign), assign);
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		final VarLocal leftLocal = SootValUtils.transformLocal((Local) assign.getLeftOp(), id.callStack);
		final PhiExpr phiExpr = (PhiExpr) assign.getRightOp();
		if (phiExpr.getArgCount() == 0) {
			return;
		}
		final Set<VarLocalOrImmutableValue> phiVals = new HashSet<>();
		boolean setUnknown = false;
		boolean hasExcluded = false;
		for (int i = 0; i < phiExpr.getArgCount(); i++) {
			final Value nextArg = phiExpr.getArgBox(i).getValue();
			if (nextArg instanceof Local) {
				final VarLocal nextLocal = SootValUtils.transformLocal((Local) nextArg, id.callStack);
				if (nextLocal.varType.equals(VarType.EXCLUDED)) {
					hasExcluded = true;
				}
				if (nextLocal.equals(leftLocal)) {
					setUnknown = true;
				} else {
					phiVals.add(nextLocal);
				}
			} else {
				phiVals.add(SootValUtils.transformImmutableValue((Constant) nextArg, id));
			}
		}

		if (setUnknown && !hasExcluded) {
			state.setToUnknown(phiVals, id);
			if (!leftLocal.varType.equals(VarType.EXCLUDED)) {
				state.setUnknown(leftLocal, id);
			}
		} else {
			state.setLocalFromPhi(leftLocal, phiVals, id);
		}
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new AssignPhiExprPoint(id, assign);
	}
}
