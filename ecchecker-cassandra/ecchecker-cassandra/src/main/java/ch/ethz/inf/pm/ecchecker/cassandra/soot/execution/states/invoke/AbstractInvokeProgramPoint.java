package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.invoke;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootValUtils;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.ArrayReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.BaseReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.Value;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.UnknownMutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.RefArrayValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarType;
import soot.Local;
import soot.jimple.Constant;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;

/**
 * Abstract Transformer for a method invoke. Resolves arguments and base values for the subclasses.
 */
public abstract class AbstractInvokeProgramPoint extends AbstractProgramPoint {

	protected final Local left;
	public final Stmt invokeStmt;
	protected final int methodId;

	protected AbstractInvokeProgramPoint(final ProgramPointId id, final Local left, final Stmt invokeStmt, final int methodId) {
		super(id);
		if (!invokeStmt.containsInvokeExpr()) {
			throw new IllegalArgumentException();
		}
		this.left = left; // may be null
		this.invokeStmt = Objects.requireNonNull(invokeStmt);
		this.methodId = methodId;
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		final InvokeExpr invoke = invokeStmt.getInvokeExpr();
		VarLocal leftLocal = left == null ? null : SootValUtils.transformLocal(left, id.callStack);
		if (leftLocal != null && leftLocal.varType.equals(VarType.EXCLUDED)) {
			leftLocal = null;
		}
		final List<VarLocalOrImmutableValue> argList = new ArrayList<>(invoke.getArgCount());
		for (int i = 0; i < invoke.getArgCount(); i++) {
			final soot.Value nextArg = invoke.getArg(i);
			if (nextArg instanceof Local) {
				final VarLocal nextArgLocal = SootValUtils.transformLocal((Local) nextArg, id.callStack);
				if (!nextArgLocal.varType.equals(VarType.EXCLUDED) && !state.isLocalInitialized(nextArgLocal)) {
					state.setToBottom();
					return;
				}
				argList.add(nextArgLocal);
			} else {
				argList.add(SootValUtils.transformImmutableValue((Constant) nextArg, id, i));
			}
		}
		if (invoke instanceof StaticInvokeExpr) {
			if (!transformStatic(leftLocal, argList, (StaticInvokeExpr) invoke, methodId, state, oldState)) {
				throw new RuntimeException("Unhandeled method " + methodId);
			}
		} else {
			final InstanceInvokeExpr instanceInvoke = (InstanceInvokeExpr) invoke;
			final Local base = (Local) instanceInvoke.getBase();
			final VarLocal baseLocal = SootValUtils.transformLocal(base, id.callStack);
			if (baseLocal.varType.equals(VarType.EXCLUDED)) {
				throw new RuntimeException("Base type of method is excluded");
			}
			if (!state.isLocalInitialized(baseLocal)) {
				state.setToBottom();
				return;
			}
			final Set<AbstractReference> baseRefs = state.ensureNonNullPointsTo(baseLocal, AbstractReference.class);
			if (baseRefs.isEmpty()) {
				state.setToBottom();
				return;
			}
			if (!transformInstance(leftLocal, baseLocal, baseRefs, argList, instanceInvoke, methodId, state, oldState)) {
				throw new RuntimeException("Unhandeled method " + methodId);
			}
		}
	}

	public boolean transformStatic(final VarLocal leftLocal, final List<VarLocalOrImmutableValue> args, final StaticInvokeExpr invokeExpr,
			final int methodId, final ExecutionState state, final ExecutionState oldState) {

		return false;
	}

	public boolean transformInstance(final VarLocal leftLocal, final VarLocal baseLocal, final Set<AbstractReference> baseRefs,
			final List<VarLocalOrImmutableValue> args, final InstanceInvokeExpr invokeExpr, final int methodId,
			final ExecutionState state, final ExecutionState oldState) {

		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((invokeStmt == null) ? 0 : invokeStmt.hashCode());
		result = prime * result + ((left == null) ? 0 : left.hashCode());
		result = prime * result + methodId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractInvokeProgramPoint other = (AbstractInvokeProgramPoint) obj;
		if (invokeStmt == null) {
			if (other.invokeStmt != null)
				return false;
		} else if (!invokeStmt.equals(other.invokeStmt))
			return false;
		if (left == null) {
			if (other.left != null)
				return false;
		} else if (!left.equals(other.left))
			return false;
		if (methodId != other.methodId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AbstractInvokeProgramPoint [" + invokeStmt + "]";
	}

	// Helpers
	protected static <T> Set<T> s(final T val) {
		return Collections.singleton(val);
	}

	protected <T extends Value> Set<T> resolveConcreteAndTransform(final VarLocalOrImmutableValue val,
			final ExecutionState state, final Function<ConcreteValue, Set<T>> transformer) {
		final Set<T> ret = new HashSet<>();
		if (val instanceof VarLocal) {
			final VarLocal local = (VarLocal) val;
			if (local.varType.equals(VarType.EXCLUDED)) {
				ret.addAll(transformer.apply(UnknownMutableValue.create()));
			} else {
				for (final AbstractReference ref : state.getPointsTo((VarLocal) val)) {
					if (state.isNullReference(ref)) {
						ret.addAll(transformer.apply(NullValue.create()));
					} else {
						final Set<ConcreteValue> cVals = state.getConcreteValue(ref);
						cVals.forEach(cVal -> ret.addAll(transformer.apply(cVal)));
					}
				}
			}
		} else {
			ret.addAll(transformer.apply((ImmutableValue) val));
		}
		return ret;
	}

	protected <T extends Value> Set<T> transformToImmutableArray(final VarLocalOrImmutableValue arrayLocal, final ExecutionState state,
			final Function<ImmutableValue[], Set<T>> transformer) {
		final Set<T> ret = new HashSet<>();
		if (arrayLocal instanceof VarLocal) {
			final VarLocal local = (VarLocal) arrayLocal;
			if (!local.varType.equals(VarType.ARRAY)) {
				ret.addAll(transformer.apply(null));
			} else {
				for (final AbstractReference ref : state.getPointsTo(local)) {
					if (ref instanceof ArrayReference) {
						final RefArrayValue array = state.getArrayValue((ArrayReference) ref);
						if (array.isUnknown() || array.isUnknownLength()) {
							ret.addAll(transformer.apply(null));
						} else {
							final ImmutableValue[] val = new ImmutableValue[array.getLength()];
							for (int i = 0; i < val.length; i++) {
								final ImmutableSet<VarLocalOrImmutableValue> values = array.getElement(i);
								final Set<ImmutableValue> immutables = new HashSet<>();
								boolean hasNonImmutables = false;
								for (final VarLocalOrImmutableValue value : values) {
									if (value instanceof ImmutableValue) {
										immutables.add((ImmutableValue) value);
									} else {
										final VarLocal valueLocal = (VarLocal) value;
										if (valueLocal.varType.equals(VarType.IMMUTABLE)) {
											immutables.addAll(state.getImmutableValues(valueLocal));
										} else {
											hasNonImmutables = true;
											break;
										}
									}
								}
								if (hasNonImmutables) {
									val[i] = UnknownImmutableValue.createWidened(id);
								} else {
									val[i] = ImmutableChoiceValue.create(id, immutables);
								}
							}
							ret.addAll(transformer.apply(val));
						}
					} else {
						ret.addAll(transformer.apply(null));
					}
				}
			}
		} else {
			ret.addAll(transformer.apply(null));
		}
		return ret;
	}

	protected <T extends Value> void transformAndPutValue(final Set<AbstractReference> refs, final ExecutionState state,
			final ExecutionState oldState, final Class<T> valueType, final Function<T, Set<T>> transformer) {

		if (refs.isEmpty()) {
			throw new IllegalArgumentException();
		}
		final boolean mustAlias = refs.size() == 1;
		for (final AbstractReference ref0 : refs) {
			final BaseReference ref = (BaseReference) ref0;
			final Set<Value> values = new HashSet<>();
			boolean setToUnknown = false;
			for (final Value oldVal : state.getBaseValue(ref)) {
				if (valueType.isAssignableFrom(oldVal.getClass())) {
					if (!mustAlias) {
						values.add(oldVal);
					}
					@SuppressWarnings("unchecked")
					final T oldValT = (T) oldVal;
					values.addAll(transformer.apply(oldValT));
				} else {
					setToUnknown = true;
					state.setToUnknown(ref, id);
					break;
				}
			}
			if (!setToUnknown) {
				state.updateBaseValue(ref, values, id, oldState);
			}
		}
	}
}
