package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.invoke;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootValUtils;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.SootMethod;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;

public class AnalyzedMethodInvokePoint extends AbstractInvokeProgramPoint {

	public final SootMethod calledMethod;
	public final AbstractProgramPoint methodReturnPoint;

	private AnalyzedMethodInvokePoint(final ProgramPointId id, final Local left, final Stmt invokeStmt, final int methodId,
			final SootMethod calledMethod) {
		super(id, left, invokeStmt, methodId);

		this.calledMethod = Objects.requireNonNull(calledMethod);
		this.methodReturnPoint = new MethodReturnPoint(this);
	}

	public static AnalyzedMethodInvokePoint create(final CallStack callStack, final Local left, final Stmt invokeStmt,
			final int methodId, final SootMethod calledMethod) {
		return new AnalyzedMethodInvokePoint(ProgramPointId.create(callStack, invokeStmt), calledMethod.isStaticInitializer() ? null : left,
				invokeStmt, methodId, calledMethod);
	}

	public CallStack getCallStackForCalledMethod() {
		return id.callStack.push(invokeStmt, SootMethodUtils.methodFrom(calledMethod));
	}

	@Override
	public boolean transformStatic(VarLocal leftLocal, List<VarLocalOrImmutableValue> args, StaticInvokeExpr invokeExpr, int methodId,
			ExecutionState state, final ExecutionState oldState) {

		state.setMethodCallState(getCallStackForCalledMethod(), null, args);
		return true;
	}

	@Override
	public boolean transformInstance(final VarLocal leftLocal, final VarLocal baseLocal, final Set<AbstractReference> baseRefs,
			final List<VarLocalOrImmutableValue> args, final InstanceInvokeExpr invokeExpr, final int methodId,
			final ExecutionState state, final ExecutionState oldState) {

		state.setMethodCallState(getCallStackForCalledMethod(), baseLocal, args);
		return true;
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new AnalyzedMethodInvokePoint(id, left, invokeStmt, methodId, calledMethod);

	}

	public static class MethodReturnPoint extends AbstractProgramPoint {

		public final AnalyzedMethodInvokePoint correspondingMethodInvoke;

		private MethodReturnPoint(final AnalyzedMethodInvokePoint correspondingMethodInvoke) {
			super(correspondingMethodInvoke.id);
			this.correspondingMethodInvoke = correspondingMethodInvoke;
		}

		@Override
		public void transform(final ExecutionState state, final ExecutionState oldState) {
			if (correspondingMethodInvoke.left != null) {
				final Set<VarLocalOrImmutableValue> returnVals = state
						.getReturnVals(correspondingMethodInvoke.getCallStackForCalledMethod());
				if (returnVals.isEmpty()) {
					state.setToBottom();
				} else {
					final VarLocal leftLocal = SootValUtils.transformLocal(correspondingMethodInvoke.left, id.callStack);
					state.setLocalFromPhi(leftLocal, returnVals, id);
				}
			}
		}

		@Override
		protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
			throw new RuntimeException(
					"Cannot recreate with different ProgramPointId. Use the public field of " + correspondingMethodInvoke);
		}

		@Override
		public int hashCode() {
			// use correspondingMethodInvokeHashCode
			int result = correspondingMethodInvoke.hashCode();
			return result + 1;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			MethodReturnPoint other = (MethodReturnPoint) obj;
			// equality is determined using corresponding method Invoke
			return correspondingMethodInvoke.equals(other.correspondingMethodInvoke);
		}

		@Override
		public String toString() {
			return "AnalyzedMethodReturnPoint[" + correspondingMethodInvoke.calledMethod.toString() + "]";
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((calledMethod == null) ? 0 : calledMethod.hashCode());
		// ignore methodReturn (recursion)
		// result = prime * result + ((methodReturnPoint == null) ? 0 : methodReturnPoint.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnalyzedMethodInvokePoint other = (AnalyzedMethodInvokePoint) obj;
		if (calledMethod == null) {
			if (other.calledMethod != null)
				return false;
		} else if (!calledMethod.equals(other.calledMethod))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AnalyzedMethodInvokePoint[" + calledMethod.toString() + "]";
	}
}
