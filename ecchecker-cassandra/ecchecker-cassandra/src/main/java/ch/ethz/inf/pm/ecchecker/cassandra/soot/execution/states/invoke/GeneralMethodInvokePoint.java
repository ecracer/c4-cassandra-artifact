package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.invoke;

import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.INTEGER_VALUE_OF;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.OBJECT_INIT;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.SETS_NEW_HASH_SET;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.STRING_VALUE_OF;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.UUID_CREATE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.UUID_FROM_STRING;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.UUID_TO_STRING;

import java.util.List;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.ImmutableReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.IntValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValueContainerValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.UUIDValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.ToStringTransformer;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;

public class GeneralMethodInvokePoint extends AbstractInvokeProgramPoint {

	private GeneralMethodInvokePoint(final ProgramPointId id, final Local left, final Stmt invokeStmt, final int methodId) {
		super(id, left, invokeStmt, methodId);
	}

	public static GeneralMethodInvokePoint create(final CallStack callStack, final Local left, final Stmt invokeStmt, final int methodId) {
		return new GeneralMethodInvokePoint(ProgramPointId.create(callStack, invokeStmt), left, invokeStmt, methodId);
	}

	@Override
	public boolean transformStatic(VarLocal leftLocal, List<VarLocalOrImmutableValue> args, StaticInvokeExpr invokeExpr, int methodId,
			ExecutionState state, ExecutionState oldState) {

		if (methodId == STRING_VALUE_OF) {
			final VarLocalOrImmutableValue arg0 = args.get(0);
			if (leftLocal != null) {
				final ImmutableValue val = ImmutableChoiceValue.create(id,
						resolveConcreteAndTransform(arg0, state, arg0val -> s(ToStringTransformer.exec(arg0val))));
				state.setImmutableValue(leftLocal, ImmutableReference.create(id), val, id, oldState);
			}
			return true;
		} else if (methodId == INTEGER_VALUE_OF) {
			final VarLocalOrImmutableValue arg0 = args.get(0);
			if (leftLocal != null) {
				if (arg0 instanceof IntValue) {
					state.setImmutableValue(leftLocal, ImmutableReference.create(id), (IntValue) arg0, id, oldState);
				} else {
					state.setUnknown(leftLocal, id);
				}
			}
			return true;
		} else if (methodId == UUID_CREATE) {
			if (leftLocal != null) {
				state.setImmutableValue(leftLocal, ImmutableReference.create(id), UUIDValue.createRandom(id), id, oldState);
			}
			return true;
		} else if (methodId == UUID_FROM_STRING) {
			if (leftLocal != null) {
				state.setImmutableValue(leftLocal, ImmutableReference.create(id), UUIDValue.createFromString(id), id, oldState);
			}
			return true;
		} else if (methodId == SETS_NEW_HASH_SET) {
			if (leftLocal != null) {
				state.setUnknown(leftLocal, id);
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean transformInstance(VarLocal leftLocal, VarLocal baseLocal, Set<AbstractReference> baseRefs,
			List<VarLocalOrImmutableValue> args, InstanceInvokeExpr invokeExpr, int methodId, ExecutionState state,
			final ExecutionState oldState) {

		if (methodId == OBJECT_INIT) {
			// nop
			return true;
		} else if (methodId == UUID_TO_STRING) {
			if (leftLocal != null) {
				state.setImmutableValue(leftLocal, ImmutableReference.create(id), ImmutableChoiceValue.create(id,
						resolveConcreteAndTransform(baseLocal, state, val -> s(StringValueContainerValue.create(val)))), id, null);
			}
			return true;
		}
		return false;
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new GeneralMethodInvokePoint(id, left, invokeStmt, methodId);
	}
}
