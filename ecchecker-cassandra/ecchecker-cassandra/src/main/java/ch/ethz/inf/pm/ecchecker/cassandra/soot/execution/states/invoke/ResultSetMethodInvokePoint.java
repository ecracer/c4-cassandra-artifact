package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.invoke;

import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.ROW_GET_INT_BY_COL;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.ROW_GET_INT_BY_IDX;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.ROW_GET_STRING_BY_COL;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.ROW_GET_STRING_BY_IDX;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.RS_IS_EXHAUSTED;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.RS_ONE;

import java.util.List;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.BaseReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.ImmutableReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.ResultSetValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.UnknownMutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.EmptyResultValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.IntValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StatementSingleResultValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.Stmt;

public class ResultSetMethodInvokePoint extends AbstractInvokeProgramPoint {

	private ResultSetMethodInvokePoint(final ProgramPointId id, final Local left, final Stmt invokeStmt, final int methodId) {
		super(id, left, invokeStmt, methodId);
	}

	public static ResultSetMethodInvokePoint create(final CallStack callStack, final Local left, final Stmt invokeStmt,
			final int methodId) {
		return new ResultSetMethodInvokePoint(ProgramPointId.create(callStack, invokeStmt), left, invokeStmt, methodId);
	}

	@Override
	public boolean transformInstance(final VarLocal leftLocal, final VarLocal baseLocal, final Set<AbstractReference> baseRefs,
			final List<VarLocalOrImmutableValue> args, final InstanceInvokeExpr invokeExpr, final int methodId,
			final ExecutionState state, final ExecutionState oldState) {

		if (methodId == RS_IS_EXHAUSTED) {
			if (leftLocal != null) {
				final Set<ImmutableValue> exhaustedValues = resolveConcreteAndTransform(baseLocal, state, rs -> {
					if (rs instanceof ResultSetValue && ((ResultSetValue) rs).isOnFirstRow) {
						return s(EmptyResultValue.create(((ResultSetValue) rs).resultFrom.executedFrom));
					} else {
						return s(UnknownImmutableValue.create(id));
					}
				});
				state.setImmutableValue(leftLocal, ImmutableReference.create(id), ImmutableChoiceValue.create(id, exhaustedValues), id,
						oldState);
			}
			return true;

		} else if (methodId == RS_ONE) {
			final Set<ConcreteValue> rs = resolveConcreteAndTransform(baseLocal, state, resultSet -> {
				if (resultSet instanceof ResultSetValue) {
					return s(resultSet);
				} else {
					return s(UnknownMutableValue.create());
				}
			});
			state.setBaseValueWithNull(leftLocal, BaseReference.create(id), rs, id, oldState);
			transformAndPutValue(baseRefs, state, oldState, ResultSetValue.class, resultSet -> s(resultSet.iteratorNext()));
			return true;

		} else if (methodId == ROW_GET_INT_BY_IDX || methodId == ROW_GET_INT_BY_COL || methodId == ROW_GET_STRING_BY_IDX
				|| methodId == ROW_GET_STRING_BY_COL) {

			if (leftLocal != null) {
				final Set<ImmutableValue> rowResults = resolveConcreteAndTransform(args.get(0), state,
						idx -> resolveConcreteAndTransform(baseLocal, state, rs -> {
							if ((idx instanceof IntValue || idx instanceof StringValue) && rs instanceof ResultSetValue) {
								return s(StatementSingleResultValue.create((ResultSetValue) rs, (ImmutableValue) idx));
							} else {
								return s(UnknownImmutableValue.create(id));
							}
						}));
				state.setImmutableValue(leftLocal, ImmutableReference.create(id), ImmutableChoiceValue.create(id, rowResults), id,
						oldState);
			}
			return true;
		}
		return false;
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new ResultSetMethodInvokePoint(id, left, invokeStmt, methodId);
	}
}
