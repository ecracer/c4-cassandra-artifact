package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.invoke;

import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.STRING_BUILDER_APPEND;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.STRING_BUILDER_INIT_INT_ARG;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.STRING_BUILDER_INIT_NO_ARG;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.STRING_BUILDER_INIT_STR_ARG;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.STRING_BUILDER_TO_STRING;

import java.util.List;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.ImmutableReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.StringBuilderValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValueContainerValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.ToStringTransformer;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.Stmt;

public class StringBuilderMethodInvokePoint extends AbstractInvokeProgramPoint {

	private StringBuilderMethodInvokePoint(final ProgramPointId id, final Local left, final Stmt invokeStmt, final int methodId) {
		super(id, left, invokeStmt, methodId);
	}

	public static StringBuilderMethodInvokePoint create(final CallStack callStack, final Local left, final Stmt invokeStmt,
			final int methodId) {
		return new StringBuilderMethodInvokePoint(ProgramPointId.create(callStack, invokeStmt), left, invokeStmt, methodId);
	}

	@Override
	public boolean transformInstance(final VarLocal leftLocal, final VarLocal baseLocal, final Set<AbstractReference> baseRefs,
			final List<VarLocalOrImmutableValue> args, final InstanceInvokeExpr invokeExpr, final int methodId,
			final ExecutionState state, final ExecutionState oldState) {

		if (methodId == STRING_BUILDER_TO_STRING) {
			if (leftLocal != null) {
				state.setImmutableValue(leftLocal, ImmutableReference.create(id), ImmutableChoiceValue.create(id,
						resolveConcreteAndTransform(baseLocal, state, val -> s(StringValueContainerValue.create(val)))), id, null);
			}
			return true;
		} else if (methodId == STRING_BUILDER_INIT_NO_ARG || methodId == STRING_BUILDER_INIT_INT_ARG) {
			return true;
		} else if (methodId == STRING_BUILDER_INIT_STR_ARG || methodId == STRING_BUILDER_APPEND) {
			if (leftLocal != null) {
				state.setLocalFromOther(leftLocal, baseLocal);
			}
			final VarLocalOrImmutableValue arg0 = args.get(0);
			transformAndPutValue(baseRefs, state, oldState, StringBuilderValue.class,
					stringBuilderValue -> resolveConcreteAndTransform(arg0, state, appendArg -> {
						final ImmutableValue visitedAppendArg = ToStringTransformer.exec(appendArg);
						return s(stringBuilderValue.addValue(visitedAppendArg));
					}));
			return true;
		}
		return false;
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new StringBuilderMethodInvokePoint(id, left, invokeStmt, methodId);
	}
}
