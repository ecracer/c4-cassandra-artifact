package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.invoke.qb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootTypeUtils;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.invoke.AbstractInvokeProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.BaseReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.Value;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.UnknownMutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.stmts.BuiltStatementAssignmentValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.stmts.BuiltStatementClauseValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.SootMethod;
import soot.jimple.InvokeExpr;
import soot.jimple.Stmt;

public abstract class AbstractQBInvokeProgramPoint extends AbstractInvokeProgramPoint {

	protected AbstractQBInvokeProgramPoint(ProgramPointId id, Local left, Stmt invokeStmt, int methodId) {
		super(id, left, invokeStmt, methodId);
	}

	private <T> Set<T> getArgOfValueType(final VarLocalOrImmutableValue arg, final Class<T> clazz, final ExecutionState state) {
		final Set<T> vals = new HashSet<>();
		if (arg instanceof VarLocal) {
			for (final AbstractReference ref : state.getPointsTo((VarLocal) arg)) {
				final Set<? extends Value> outVals;
				if (ConcreteValue.class.isAssignableFrom(clazz)) {
					outVals = state.getConcreteValue(ref);
				} else {
					outVals = state.getValue(ref);
				}
				final Set<Value> outValsT = new HashSet<>();
				for (final Value val : outVals) {
					if (val instanceof ImmutableChoiceValue) {
						outValsT.addAll(((ImmutableChoiceValue) val).choices);
					} else {
						outValsT.add(val);
					}
				}
				for (final Value val : outValsT) {

					if (val instanceof NullValue) {
						// skip
					} else if (clazz.isAssignableFrom(val.getClass())) {
						@SuppressWarnings("unchecked")
						T valAsT = (T) val;
						vals.add(valAsT);
					} else {
						return null;
					}
				}
			}
		} else if (clazz.isAssignableFrom(arg.getClass())) {
			@SuppressWarnings("unchecked")
			T valAsT = (T) arg;
			vals.add(valAsT);
		} else {
			return null;
		}
		return vals;
	}

	/**
	 * Transforms all arguments. If the argument is declared as String, it is transformed to StringValue. If it is
	 * declared as Clause, it is transformed to BuiltStatementClauseValue. If it is declared as Assignment, it is
	 * transformed to BuiltStatementAssignmentValue. If it is declared as a String[], it is transformed to a
	 * StringValue[], otherwise it is added as an VarLocalOrImmutableValue. For all transformations, null values are
	 * ignored (as these yield errors in the QueryBuilder)
	 * 
	 * @param invokeExpr
	 * @param flow
	 * @return EmptyList if an arg is bottom, null if an arg is Unknown, list of args otherwise
	 */
	protected List<Object> getArgs(final List<VarLocalOrImmutableValue> args, final InvokeExpr invokeExpr, final ExecutionState state) {
		final SootMethod method = invokeExpr.getMethod();
		final List<Object> targs = new ArrayList<>(invokeExpr.getArgCount());
		for (int i = 0; i < invokeExpr.getArgCount(); i++) {
			final VarLocalOrImmutableValue arg = args.get(i);
			if (SootTypeUtils.isString(method.getParameterType(i))) {
				final Set<StringValue> strs = getArgOfValueType(arg, StringValue.class, state);
				if (strs == null) {
					// set to unknown
					return null;
				} else {
					targs.add(strs);
				}
			} else if (SootTypeUtils.isClause(method.getParameterType(i))) {
				final Set<BuiltStatementClauseValue> clauses = getArgOfValueType(arg, BuiltStatementClauseValue.class, state);
				if (clauses == null) {
					// set to unknown
					return null;
				} else {
					targs.add(clauses);
				}
			} else if (SootTypeUtils.isAssignment(method.getParameterType(i))) {
				final Set<BuiltStatementAssignmentValue> assignments = getArgOfValueType(arg, BuiltStatementAssignmentValue.class, state);
				if (assignments == null) {
					// set to unknown
					return null;
				} else {
					targs.add(assignments);
				}
			} else if (SootTypeUtils.isStringArray(method.getParameterType(i))) {
				final Set<ImmutableValue[]> arrays = new HashSet<>();
				final Set<Value> ret = transformToImmutableArray(arg, state, array -> {
					if (array == null) {
						return s(UnknownMutableValue.create());
					} else {
						arrays.add(array);
						return Collections.emptySet();
					}
				});
				if (!ret.isEmpty() || arrays.size() != 1) {
					// set to unknown
					return null;
				} else {
					final ImmutableValue[] array = arrays.iterator().next();
					final StringValue[] argAsStrings = new StringValue[array.length];
					for (int j = 0; j < argAsStrings.length; j++) {
						if (array[j] instanceof StringValue) {
							argAsStrings[j] = (StringValue) array[j];
						} else {
							return null;
						}
					}
					targs.add(argAsStrings);
				}
			} else {
				targs.add(arg);
			}
		}
		return targs;
	}

	protected void transformQueryBuilderStaticInvoke(final VarLocal leftLocal, final List<VarLocalOrImmutableValue> args,
			final Stmt invokeStmt, final ExecutionState state, final ExecutionState oldState,
			final Function<List<Object>, Set<Value>> transformer) {

		if (leftLocal == null) {
			return;
		}
		final List<Object> targs = getArgs(args, invokeStmt.getInvokeExpr(), state);
		if (state.isBottom()) {
			// nop
		} else if (targs == null) {
			// one arg is unknown
			state.setUnknown(leftLocal, id);
		} else {
			state.setBaseValue(leftLocal, BaseReference.create(id), transformer.apply(targs), id, oldState);
		}
	}

	protected <T> void transformQueryBuilderInstanceInvoke(final VarLocal leftLocal, final VarLocal baseLocal,
			final Set<AbstractReference> baseRefs, final List<VarLocalOrImmutableValue> args, final Stmt invokeStmt,
			final ExecutionState state, final ExecutionState oldState, final Class<T> clazz,
			final BiFunction<T, List<Object>, Set<Value>> transformer) {

		if (leftLocal != null) {
			state.setLocalFromOther(leftLocal, baseLocal);
		}
		final List<Object> targs = getArgs(args, invokeStmt.getInvokeExpr(), state);
		if (state.isBottom()) {
			return;
		} else if (targs == null) {
			// one arg is unknown
			state.setToUnknown(baseLocal, id);
			return;
		}

		boolean mustAlias = baseRefs.size() == 1;
		for (final AbstractReference baseRef0 : baseRefs) {
			final BaseReference baseRef = (BaseReference) baseRef0;
			final Set<Value> results = new HashSet<>();
			boolean setToUnknown = false;
			for (final Value val : state.getBaseValue(baseRef)) {
				if (clazz.isAssignableFrom(val.getClass())) {
					if (!mustAlias) {
						results.add(val);
					}
					@SuppressWarnings("unchecked")
					final T tVal = (T) val;
					results.addAll(transformer.apply(tVal, targs));
				} else {
					setToUnknown = true;
					state.setToUnknown(baseRef, id);
					break;
				}
			}
			if (!setToUnknown) {
				state.updateBaseValue(baseRef, results, id, oldState);
			}
		}
	}

	// helpers
	protected <T, V> Set<T> transformSet(final Object values, final Class<V> valueType, final Function<V, T> transformer) {
		final Set<T> ret = new HashSet<>();
		@SuppressWarnings("rawtypes")
		final Set valuesSet = (Set) values;
		for (final Object val : valuesSet) {
			if (!valueType.isAssignableFrom(val.getClass())) {
				throw new RuntimeException("Illegal transformation");
			}
			@SuppressWarnings("unchecked")
			final V valV = (V) val;
			ret.add(transformer.apply(valV));
		}
		return ret;
	}

	protected <T, V> Set<T> transformSetS(final Object values, final Class<V> valueType,
			final Function<V, Set<T>> transformer) {
		final Set<T> ret = new HashSet<>();
		@SuppressWarnings("rawtypes")
		final Set valuesSet = (Set) values;
		for (final Object val : valuesSet) {
			if (!valueType.isAssignableFrom(val.getClass())) {
				throw new RuntimeException("Illegal transformation");
			}
			@SuppressWarnings("unchecked")
			final V valV = (V) val;
			ret.addAll(transformer.apply(valV));
		}
		return ret;
	}
}
