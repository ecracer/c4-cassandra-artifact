package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.invoke.qb;

import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_ADD;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_ADD_ALL;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_APPEND;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_APPEND_ALL;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_DECR;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_DECR_VAL;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_DISCARD;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_DISCARD_ALL;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_INCR;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_INCR_VAL;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_PREPEND_ALL;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_PREPREND;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_PUT;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_PUT_ALL;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_REMOVE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_REMOVE_ALL;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_SET;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_ASSIGNMENT_SET_IDX;

import java.util.List;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.stmts.BuiltStatementAssignmentValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;

public class QBAssignmentInvokePoint extends AbstractQBInvokeProgramPoint {

	private QBAssignmentInvokePoint(final ProgramPointId id, final Local left, final Stmt invokeStmt, final int methodId) {
		super(id, left, invokeStmt, methodId);
	}

	public static QBAssignmentInvokePoint create(final CallStack callStack, final Local left, final Stmt invokeStmt,
			final int methodId) {
		return new QBAssignmentInvokePoint(ProgramPointId.create(callStack, invokeStmt), left, invokeStmt, methodId);
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new QBAssignmentInvokePoint(id, left, invokeStmt, methodId);
	}

	@Override
	public boolean transformStatic(final VarLocal leftLocal, final List<VarLocalOrImmutableValue> args, final StaticInvokeExpr invokeExpr,
			final int methodId, final ExecutionState state, final ExecutionState oldState) {

		if (methodId == QUERY_BUILDER_ASSIGNMENT_ADD || methodId == QUERY_BUILDER_ASSIGNMENT_ADD_ALL
				|| methodId == QUERY_BUILDER_ASSIGNMENT_APPEND || methodId == QUERY_BUILDER_ASSIGNMENT_APPEND_ALL
				|| methodId == QUERY_BUILDER_ASSIGNMENT_PUT_ALL) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementAssignmentValue.create(
									first.string + " = " + first.string + " + %s", (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == QUERY_BUILDER_ASSIGNMENT_DECR) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementAssignmentValue.create(first.string + " = " + first.string + " - 1")));
			return true;
		} else if (methodId == QUERY_BUILDER_ASSIGNMENT_DECR_VAL) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementAssignmentValue.create(
									first.string + " = " + first.string + " - %s", (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == QUERY_BUILDER_ASSIGNMENT_DISCARD || methodId == QUERY_BUILDER_ASSIGNMENT_DISCARD_ALL
				|| methodId == QUERY_BUILDER_ASSIGNMENT_REMOVE || methodId == QUERY_BUILDER_ASSIGNMENT_REMOVE_ALL) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementAssignmentValue.create(
									first.string + " = " + first.string + " - %s", (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == QUERY_BUILDER_ASSIGNMENT_INCR) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementAssignmentValue.create(first.string + " = " + first.string + " + 1")));
			return true;
		} else if (methodId == QUERY_BUILDER_ASSIGNMENT_INCR_VAL) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementAssignmentValue.create(
									first.string + " = " + first.string + " + %s", (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == QUERY_BUILDER_ASSIGNMENT_PREPREND || methodId == QUERY_BUILDER_ASSIGNMENT_PREPEND_ALL) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementAssignmentValue.create(
									first.string + " = %s + " + first.string, (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == QUERY_BUILDER_ASSIGNMENT_PUT || methodId == QUERY_BUILDER_ASSIGNMENT_SET_IDX) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementAssignmentValue.create(first.string + "[%s] = %s",
									(VarLocalOrImmutableValue) targs.get(1), (VarLocalOrImmutableValue) targs.get(2))));
			return true;
		} else if (methodId == QUERY_BUILDER_ASSIGNMENT_SET) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementAssignmentValue.create(
									first.string + " = %s", (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		}
		return false;
	}
}
