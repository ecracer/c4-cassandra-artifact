package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.invoke.qb;

import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_CLAUSE_CONTAINS;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_CLAUSE_EQ;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_CLAUSE_GT;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_CLAUSE_GTE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_CLAUSE_IN;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_CLAUSE_LT;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_CLAUSE_LTE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_CONTAINS_KEY;

import java.util.List;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.stmts.BuiltStatementClauseValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;

public class QBClauseInvokePoint extends AbstractQBInvokeProgramPoint {

	private QBClauseInvokePoint(final ProgramPointId id, final Local left, final Stmt invokeStmt, final int methodId) {
		super(id, left, invokeStmt, methodId);
	}

	public static QBClauseInvokePoint create(final CallStack callStack, final Local left, final Stmt invokeStmt,
			final int methodId) {
		return new QBClauseInvokePoint(ProgramPointId.create(callStack, invokeStmt), left, invokeStmt, methodId);
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new QBClauseInvokePoint(id, left, invokeStmt, methodId);
	}

	@Override
	public boolean transformStatic(final VarLocal leftLocal, final List<VarLocalOrImmutableValue> args, final StaticInvokeExpr invokeExpr,
			final int methodId, final ExecutionState state, final ExecutionState oldState) {

		if (methodId == QUERY_BUILDER_CLAUSE_EQ) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementClauseValue.create(first.string + " = %s", (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == QUERY_BUILDER_CLAUSE_GT) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementClauseValue.create(first.string + " > %s", (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == QUERY_BUILDER_CLAUSE_GTE) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementClauseValue.create(first.string + " >= %s", (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == QUERY_BUILDER_CLAUSE_IN) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementClauseValue.create(first.string + " IN (%s)", (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == QUERY_BUILDER_CLAUSE_LT) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementClauseValue.create(first.string + " < %s", (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == QUERY_BUILDER_CLAUSE_LTE) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementClauseValue.create(first.string + " <= %s", (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == QUERY_BUILDER_CLAUSE_CONTAINS) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementClauseValue.create(first.string + " CONTAINS %s",
									(VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == QUERY_BUILDER_CONTAINS_KEY) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class,
							first -> BuiltStatementClauseValue.create(first.string + " CONTAINS KEY %s",
									(VarLocalOrImmutableValue) targs.get(1))));
			return true;
		}
		return false;
	}
}
