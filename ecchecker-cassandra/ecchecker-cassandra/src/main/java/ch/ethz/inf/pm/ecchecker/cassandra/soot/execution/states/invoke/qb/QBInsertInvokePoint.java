package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.invoke.qb;

import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.INSERT_IF_NOT_EXISTS;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.INSERT_VALUE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_INSERT_INTO_KEYSPACE_TABLE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_INSERT_INTO_TABLE;

import java.util.List;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.Value;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.stmts.BuiltInsertStatementValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;

public class QBInsertInvokePoint extends AbstractQBInvokeProgramPoint {

	private QBInsertInvokePoint(final ProgramPointId id, final Local left, final Stmt invokeStmt, final int methodId) {
		super(id, left, invokeStmt, methodId);
	}

	public static QBInsertInvokePoint create(final CallStack callStack, final Local left, final Stmt invokeStmt,
			final int methodId) {
		return new QBInsertInvokePoint(ProgramPointId.create(callStack, invokeStmt), left, invokeStmt, methodId);
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new QBInsertInvokePoint(id, left, invokeStmt, methodId);
	}

	@Override
	public boolean transformStatic(final VarLocal leftLocal, final List<VarLocalOrImmutableValue> args, final StaticInvokeExpr invokeExpr,
			final int methodId, final ExecutionState state, final ExecutionState oldState) {

		if (methodId == QUERY_BUILDER_INSERT_INTO_TABLE) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class, first -> BuiltInsertStatementValue.create(null, first)));
			return true;
		} else if (methodId == QUERY_BUILDER_INSERT_INTO_KEYSPACE_TABLE) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> this.<Value, StringValue> transformSetS(targs.get(0), StringValue.class,
							first -> transformSet(targs.get(1), StringValue.class,
									second -> (BuiltInsertStatementValue.create(first, second)))));
			return true;
		}
		return false;
	}

	@Override
	public boolean transformInstance(final VarLocal leftLocal, final VarLocal baseLocal, final Set<AbstractReference> baseRefs,
			final List<VarLocalOrImmutableValue> args, final InstanceInvokeExpr invokeExpr, final int methodId,
			final ExecutionState state, final ExecutionState oldState) {

		if (methodId == INSERT_VALUE) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltInsertStatementValue.class,
					(val, targs) -> transformSet(targs.get(0), StringValue.class,
							first -> val.addColumnValue(first, (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == INSERT_IF_NOT_EXISTS) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltInsertStatementValue.class,
					(val, targs) -> s(val.setIfNotExists(true)));
			return true;
		}
		return false;
	}
}
