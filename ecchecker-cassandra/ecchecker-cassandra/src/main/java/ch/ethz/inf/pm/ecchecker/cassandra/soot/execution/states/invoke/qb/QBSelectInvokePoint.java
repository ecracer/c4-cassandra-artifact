package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.invoke.qb;

import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_SELECT;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_SELECT_COLS;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.SELECT_ALL;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.SELECT_ALLOW_FILTERING;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.SELECT_COLUMN;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.SELECT_COLUMN_AS;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.SELECT_COUNT_ALL;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.SELECT_DISTINCT;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.SELECT_FROM_TABLE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.SELECT_FROM_TABLE_KEYSPACE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.SELECT_LIMIT;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.SELECT_ORDER_BY;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.SELECT_WHERE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.SELECT_WHERE_AND_CLAUSE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.SELECT_WHERE_CLAUSE;

import java.util.List;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.Value;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.stmts.BuiltSelectStatementValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.stmts.BuiltStatementClauseValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;

public class QBSelectInvokePoint extends AbstractQBInvokeProgramPoint {

	private QBSelectInvokePoint(final ProgramPointId id, final Local left, final Stmt invokeStmt, final int methodId) {
		super(id, left, invokeStmt, methodId);
	}

	public static QBSelectInvokePoint create(final CallStack callStack, final Local left, final Stmt invokeStmt,
			final int methodId) {
		return new QBSelectInvokePoint(ProgramPointId.create(callStack, invokeStmt), left, invokeStmt, methodId);
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new QBSelectInvokePoint(id, left, invokeStmt, methodId);
	}

	@Override
	public boolean transformStatic(final VarLocal leftLocal, final List<VarLocalOrImmutableValue> args, final StaticInvokeExpr invokeExpr,
			final int methodId, final ExecutionState state, final ExecutionState oldState) {

		if (methodId == QUERY_BUILDER_SELECT) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> s(BuiltSelectStatementValue.create(null)));
			return true;
		} else if (methodId == QUERY_BUILDER_SELECT_COLS) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> s(BuiltSelectStatementValue.create((StringValue[]) targs.get(0))));
			return true;
		}
		return false;
	}

	@Override
	public boolean transformInstance(final VarLocal leftLocal, final VarLocal baseLocal, final Set<AbstractReference> baseRefs,
			final List<VarLocalOrImmutableValue> args, final InstanceInvokeExpr invokeExpr, final int methodId,
			final ExecutionState state, final ExecutionState oldState) {

		if (methodId == SELECT_ALL) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltSelectStatementValue.class,
					(val, targs) -> s(val.addColumn(StringValue.create("*"))));
			return true;
		} else if (methodId == SELECT_COLUMN) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltSelectStatementValue.class,
					(val, targs) -> transformSet(targs.get(0), StringValue.class, first -> val.addColumn(first)));
			return true;
		} else if (methodId == SELECT_COUNT_ALL) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltSelectStatementValue.class,
					(val, targs) -> s(val.addColumn(StringValue.create("count(*)"))));
			return true;
		} else if (methodId == SELECT_DISTINCT) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltSelectStatementValue.class,
					(val, targs) -> s(val.setDistinct(true)));
			return true;
		} else if (methodId == SELECT_COLUMN_AS) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltSelectStatementValue.class,
					(val, targs) -> transformSet(targs.get(0), StringValue.class, first -> val.addAs(first)));
			return true;
		} else if (methodId == SELECT_FROM_TABLE) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltSelectStatementValue.class,
					(val, targs) -> transformSet(targs.get(0), StringValue.class,
							first -> val.from(null, first)));
			return true;
		} else if (methodId == SELECT_FROM_TABLE_KEYSPACE) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltSelectStatementValue.class,
					(val, targs) -> this.<Value, StringValue> transformSetS(targs.get(0), StringValue.class,
							first -> transformSet(targs.get(1), StringValue.class, second -> val.from(first, second))));
			return true;
		} else if (methodId == SELECT_ALLOW_FILTERING) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltSelectStatementValue.class,
					(val, targs) -> s(val));
			return true;
		} else if (methodId == SELECT_LIMIT) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltSelectStatementValue.class,
					(val, targs) -> s(val));
			return true;
		} else if (methodId == SELECT_ORDER_BY) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltSelectStatementValue.class,
					(val, targs) -> s(val));
			return true;
		} else if (methodId == SELECT_WHERE) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltSelectStatementValue.class,
					(val, targs) -> s(val));
			return true;
		} else if (methodId == SELECT_WHERE_CLAUSE || methodId == SELECT_WHERE_AND_CLAUSE) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltSelectStatementValue.class,
					(val, targs) -> transformSet(targs.get(0), BuiltStatementClauseValue.class, first -> val.addWhere(first)));
			return true;
		}
		return false;
	}
}
