package ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.invoke.qb;

import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_UPDATE_KEYSPACE_TABLE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.QUERY_BUILDER_UPDATE_TABLE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.UPDATE_IF_EXISTS;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.UPDATE_ONLY_IF;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.UPDATE_ONLY_IF_AND_CLAUSE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.UPDATE_ONLY_IF_CLAUSE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.UPDATE_WHERE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.UPDATE_WHERE_AND_CLAUSE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.UPDATE_WHERE_CLAUSE;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.UPDATE_WITH;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.UPDATE_WITH_AND_ASSIGNMENT;
import static ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils.UPDATE_WITH_ASSIGNMENT;

import java.util.List;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.Value;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.stmts.BuiltStatementAssignmentValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.stmts.BuiltStatementClauseValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.stmts.BuiltUpdateStatementValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;

public class QBUpdateInvokePoint extends AbstractQBInvokeProgramPoint {

	private QBUpdateInvokePoint(final ProgramPointId id, final Local left, final Stmt invokeStmt, final int methodId) {
		super(id, left, invokeStmt, methodId);
	}

	public static QBUpdateInvokePoint create(final CallStack callStack, final Local left, final Stmt invokeStmt,
			final int methodId) {
		return new QBUpdateInvokePoint(ProgramPointId.create(callStack, invokeStmt), left, invokeStmt, methodId);
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new QBUpdateInvokePoint(id, left, invokeStmt, methodId);
	}

	@Override
	public boolean transformStatic(final VarLocal leftLocal, final List<VarLocalOrImmutableValue> args, final StaticInvokeExpr invokeExpr,
			final int methodId, final ExecutionState state, final ExecutionState oldState) {

		if (methodId == QUERY_BUILDER_UPDATE_TABLE) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> transformSet(targs.get(0), StringValue.class, first -> BuiltUpdateStatementValue.create(null, first)));
			return true;
		} else if (methodId == QUERY_BUILDER_UPDATE_KEYSPACE_TABLE) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> this.<Value, StringValue> transformSetS(targs.get(0), StringValue.class,
							first -> transformSet(targs.get(1), StringValue.class,
									second -> (BuiltUpdateStatementValue.create(first, second)))));
			return true;
		}
		return false;
	}

	@Override
	public boolean transformInstance(final VarLocal leftLocal, final VarLocal baseLocal, final Set<AbstractReference> baseRefs,
			final List<VarLocalOrImmutableValue> args, final InstanceInvokeExpr invokeExpr, final int methodId,
			final ExecutionState state, final ExecutionState oldState) {

		if (methodId == UPDATE_WITH) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltUpdateStatementValue.class,
					(val, targs) -> s(val));
			return true;
		} else if (methodId == UPDATE_WITH_ASSIGNMENT || methodId == UPDATE_WITH_AND_ASSIGNMENT) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltUpdateStatementValue.class,
					(val, targs) -> transformSet(targs.get(0), BuiltStatementAssignmentValue.class, first -> val.addAssignment(first)));
			return true;
		} else if (methodId == UPDATE_IF_EXISTS) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltUpdateStatementValue.class,
					(val, targs) -> s(val.setIfExists(true)));
			return true;
		} else if (methodId == UPDATE_ONLY_IF) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltUpdateStatementValue.class,
					(val, targs) -> s(val));
			return true;
		} else if (methodId == UPDATE_ONLY_IF_CLAUSE || methodId == UPDATE_ONLY_IF_AND_CLAUSE) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltUpdateStatementValue.class,
					(val, targs) -> transformSet(targs.get(0), BuiltStatementClauseValue.class, first -> val.addCondition(first)));
			return true;
		} else if (methodId == UPDATE_WHERE) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltUpdateStatementValue.class,
					(val, targs) -> s(val));
			return true;
		} else if (methodId == UPDATE_WHERE_CLAUSE || methodId == UPDATE_WHERE_AND_CLAUSE) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltUpdateStatementValue.class,
					(val, targs) -> transformSet(targs.get(0), BuiltStatementClauseValue.class, first -> val.addWhere(first)));
			return true;
		}
		return false;
	}
}
