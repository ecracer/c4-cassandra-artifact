package ch.ethz.inf.pm.ecchecker.cassandra.soot.fieldvalues;

import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.SetMultimap;

import ch.ethz.inf.pm.ecchecker.cassandra.beans.Method;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.ExecuteGraph;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.ArrayReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.BaseReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.ImmutableReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.ObjectReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.visitor.ReferenceVisitor;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.Value;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.UnknownMutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.RefArrayValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.RefObjectValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.RefValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.FieldDesc;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.vars.VarLocal;
import soot.SootMethod;

/**
 * Loops over all methods and collects possible field values. For static fields, only ImmutableValues are collected. For
 * members, VarLocalOrImmutableValues are collected. Note that global references and global locals are used to ensure
 * that they do not interfere with an actual flow state. Also, only fields that are not modified are included.
 */
public class FieldValueCollector {

	private final static Logger LOG = LogManager.getLogger(FieldValueCollector.class);

	private final BiMap<AbstractReference, AbstractReference> globalReferences = HashBiMap.create();
	private final BiMap<VarLocal, VarLocal> globalLocals = HashBiMap.create();
	private final ExecutionState initialState;

	public FieldValueCollector(final Map<SootMethod, ExecuteGraph> executeGraphs,
			final Set<FieldDesc> includedStaticFields, final Set<FieldDesc> includedFields,
			final SetMultimap<String, FieldDesc> fieldsPerClass) {

		this.initialState = calculateInitialState(executeGraphs, includedStaticFields, includedFields, fieldsPerClass);
	}

	public ExecutionState getInitialState() {
		return initialState.clone();
	}

	private ExecutionState calculateInitialState(final Map<SootMethod, ExecuteGraph> executeGraphs,
			final Set<FieldDesc> includedStaticFields, final Set<FieldDesc> includedFields,
			final SetMultimap<String, FieldDesc> fieldsPerClass) {

		final ImmutableSetMultimap.Builder<FieldDesc, VarLocalOrImmutableValue> possibleFieldValues = ImmutableSetMultimap.builder();
		for (final FieldDesc includedField : includedFields) {
			possibleFieldValues.put(includedField, includedField.defaultValue);
		}

		final ImmutableSetMultimap<FieldDesc, ImmutableValue> initialStaticFieldMap = getStaticInitialState(executeGraphs,
				includedStaticFields);

		ExecutionState oldState = new ExecutionState();
		ExecutionState newState = new ExecutionState(ImmutableSetMultimap.copyOf(fieldsPerClass), possibleFieldValues.build(),
				ImmutableSetMultimap.copyOf(initialStaticFieldMap), ImmutableSetMultimap.of(), ImmutableMap.of(), ImmutableSetMultimap.of(),
				ImmutableMap.of(), ImmutableMap.of());

		int iteration = 0;
		while (!oldState.equals(newState)) {
			iteration++;
			if (LOG.isDebugEnabled()) {
				LOG.debug("Iteration " + iteration);
			}
			oldState = newState.clone();
			for (final Entry<SootMethod, ExecuteGraph> entry : executeGraphs.entrySet()) {

				final ExecutionState nextState = newState.clone();
				final Method method = SootMethodUtils.methodFrom(entry.getKey());
				final CallStack callStack = CallStack.EMPTY_STACK.push(method, method);
				nextState.addInitialMethodCallState(callStack, entry.getKey());

				final FieldValueAnalysis nextAnalysis = new FieldValueAnalysis(callStack, entry.getValue(), nextState, false);

				final ImmutableSetMultimap<FieldDesc, ImmutableValue> nextStaticFieldMap = mergeStaticValuesMap(newState,
						nextAnalysis.getStaticFieldValues());
				newState = getNextExecutionState(nextAnalysis.getFinalFlow(), nextAnalysis.getChangedRefs(), nextStaticFieldMap);
			}
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("Field Collection finished in " + iteration + " iterations");
			LOG.debug("Static Field Values: " + newState.getStaticFieldMap());
			LOG.debug("Field Values: " + newState.getPossibleFieldValues());
		}
		return newState;
	}

	private ImmutableSetMultimap<FieldDesc, ImmutableValue> mergeStaticValuesMap(final ExecutionState oldState,
			final SetMultimap<FieldDesc, ImmutableValue> newMap) {
		final ImmutableSetMultimap.Builder<FieldDesc, ImmutableValue> ret = ImmutableSetMultimap.builder();
		final ImmutableSetMultimap<FieldDesc, VarLocalOrImmutableValue> oldStaticFields = oldState.getStaticFieldMap();
		for (final FieldDesc newKey : newMap.keySet()) {
			if (oldStaticFields.containsKey(newKey)) {
				for (final VarLocalOrImmutableValue oldVal : oldStaticFields.get(newKey)) {
					ret.put(newKey, (ImmutableValue) oldVal);
				}
				ret.putAll(newKey, newMap.get(newKey));
			}
		}
		return ret.build();
	}

	private ExecutionState getNextExecutionState(final ExecutionState newState, final Set<AbstractReference> changedRefs,
			final ImmutableSetMultimap<FieldDesc, ImmutableValue> newStaticFieldMap) {

		final SetMultimap<FieldDesc, VarLocalOrImmutableValue> newPossibleFieldValues = getNewPossibleFieldValues(newState,
				changedRefs);

		final ImmutableSetMultimap<String, FieldDesc> newIncludedFields = getNewIncludedFields(newState.getIncludedFields(),
				newPossibleFieldValues.keySet());

		// store transformed references only
		final SetMultimap<VarLocal, AbstractReference> newPointsToMap = HashMultimap.create();
		final Map<ImmutableReference, ImmutableValue> newImmutableValueMap = new HashMap<>();
		final SetMultimap<BaseReference, Value> newBaseValueMap = HashMultimap.create();
		final Map<ArrayReference, RefArrayValue> newArrayValueMap = new HashMap<>();
		final Map<ObjectReference, RefObjectValue> newObjectValueMap = new HashMap<>();

		final Deque<VarLocalOrImmutableValue> workList = new LinkedList<>(newPossibleFieldValues.values());

		final ReferenceVisitor<Void> refVisitor = new ReferenceVisitor<Void>() {

			@Override
			public Void visit(AbstractReference reference) {
				return reference.apply(this);
			}

			@Override
			public Void visitImmutableReference(final ImmutableReference immutableReference) {
				// if a value is already present, we have to widen (otherwise we would override the value)
				final ImmutableReference globalRef = (ImmutableReference) getGlobalReference(immutableReference);
				if (newImmutableValueMap.containsKey(globalRef)) {
					newImmutableValueMap.put(globalRef, newImmutableValueMap.get(globalRef).widenWith(
							newState.getImmutableValue(immutableReference), ProgramPointId.create(CallStack.EMPTY_STACK, globalRef)));
				} else {
					newImmutableValueMap.put(globalRef, newState.getImmutableValue(immutableReference));
				}
				return null;
			}

			@Override
			public Void visitBaseReference(BaseReference baseReference) {
				if (baseReference != BaseReference.NULL_REF) {
					for (final Value val : newState.getBaseValue(baseReference)) {
						if (val instanceof RefValue) {
							workList.addAll(((RefValue) val).getUsedVarLocalOrImmutableValues());
						}
					}
					newBaseValueMap.putAll((BaseReference) getGlobalReference(baseReference), newState.getBaseValue(baseReference));
				}
				return null;
			}

			@Override
			public Void visitArrayReference(ArrayReference arrayReference) {
				if (arrayReference != ArrayReference.NULL_REF) {
					final RefArrayValue array = newState.getArrayValue(arrayReference);
					workList.addAll(array.getAllElements());
					newArrayValueMap.put((ArrayReference) getGlobalReference(arrayReference), array);
				}
				return null;
			}

			@Override
			public Void visitObjectReference(ObjectReference objectReference) {
				if (objectReference != ObjectReference.NULL_REF) {
					final RefObjectValue object = newState.getObjectValue(objectReference);
					workList.addAll(object.getAllFieldValues());
					newObjectValueMap.put((ObjectReference) getGlobalReference(objectReference), object);
				}
				return null;
			}
		};
		while (!workList.isEmpty()) {
			final VarLocalOrImmutableValue next = workList.removeFirst();
			if (next instanceof VarLocal) {
				final VarLocal nextLocal = (VarLocal) next;
				for (final AbstractReference ref : newState.getPointsTo(nextLocal)) {
					refVisitor.visit(ref);
					newPointsToMap.put(getGlobalLocal(nextLocal), getGlobalReference(ref));
				}
			}
		}

		// transform locals
		final Function<VarLocalOrImmutableValue, VarLocalOrImmutableValue> localTransformer = localOrImmutable -> localOrImmutable instanceof VarLocal
				? getGlobalLocal((VarLocal) localOrImmutable)
				: localOrImmutable;
		final ImmutableSetMultimap.Builder<FieldDesc, VarLocalOrImmutableValue> newPossibleFieldValuesBuilder = ImmutableSetMultimap
				.builder();
		for (final Entry<FieldDesc, VarLocalOrImmutableValue> entry : newPossibleFieldValues.entries()) {
			newPossibleFieldValuesBuilder.put(entry.getKey(), localTransformer.apply(entry.getValue()));
		}
		final ImmutableSetMultimap.Builder<BaseReference, Value> newBaseValueMapBuilder = ImmutableSetMultimap.builder();
		for (final Entry<BaseReference, Value> entry : newBaseValueMap.entries()) {
			if (entry.getValue() instanceof RefValue) {
				newBaseValueMapBuilder.put(entry.getKey(),
						((RefValue) entry.getValue()).transformVarLocalOrImmutableValues(localTransformer));
			} else {
				newBaseValueMapBuilder.put(entry);
			}
		}
		final ImmutableMap.Builder<ArrayReference, RefArrayValue> newArrayValueMapBuilder = ImmutableMap.builder();
		for (final Entry<ArrayReference, RefArrayValue> entry : newArrayValueMap.entrySet()) {
			newArrayValueMapBuilder.put(entry.getKey(), entry.getValue().transformVarLocalOrImmutableValues(localTransformer));
		}
		final ImmutableMap.Builder<ObjectReference, RefObjectValue> newObjectValueMapBuilder = ImmutableMap.builder();
		for (final Entry<ObjectReference, RefObjectValue> entry : newObjectValueMap.entrySet()) {
			newObjectValueMapBuilder.put(entry.getKey(), entry.getValue().transformVarLocalOrImmutableValues(localTransformer));
		}

		return new ExecutionState(newIncludedFields, newPossibleFieldValuesBuilder.build(),
				ImmutableSetMultimap.copyOf(newStaticFieldMap), ImmutableSetMultimap.copyOf(newPointsToMap),
				ImmutableMap.copyOf(newImmutableValueMap), newBaseValueMapBuilder.build(),
				newArrayValueMapBuilder.build(), newObjectValueMapBuilder.build());
	}

	private SetMultimap<FieldDesc, VarLocalOrImmutableValue> getNewPossibleFieldValues(final ExecutionState newState,
			final Set<AbstractReference> changedRefs) {

		final SetMultimap<String, FieldDesc> oldIncludedFields = newState.getIncludedFields();

		final SetMultimap<FieldDesc, VarLocalOrImmutableValue> newPossibleFieldValues = HashMultimap
				.create(newState.getPossibleFieldValues());
		final Set<FieldDesc> newExcluded = new HashSet<>();

		for (final RefObjectValue refObject : newState.getObjectValueMap().values()) {
			for (final FieldDesc includedField : oldIncludedFields.get(refObject.getClassName())) {
				if (refObject.isUnknown(includedField)) {
					newExcluded.add(includedField);
				} else if (refObject.isFieldSet(includedField)) {
					newPossibleFieldValues.putAll(includedField, refObject.getField(includedField));
				} else {
					newPossibleFieldValues.put(includedField, includedField.defaultValue);
				}
			}
		}

		final Iterator<FieldDesc> it = newPossibleFieldValues.keySet().iterator();
		while (it.hasNext()) {
			final FieldDesc next = it.next();
			if (newExcluded.contains(next) || hasUnknown(newPossibleFieldValues.get(next), newState, changedRefs, new HashSet<>())) {
				it.remove();
			}
		}
		return newPossibleFieldValues;
	}

	private boolean hasUnknown(final Set<VarLocalOrImmutableValue> values, final ExecutionState newState,
			final Set<AbstractReference> changedRefs, final Set<VarLocal> checkedLocals) {

		for (final VarLocalOrImmutableValue value : values) {
			if (value instanceof UnknownImmutableValue) {
				return true;
			} else if (value instanceof VarLocal) {
				if (checkedLocals.contains(value)) {
					continue;
				} else {
					checkedLocals.add((VarLocal) value);
				}
				for (final AbstractReference ref : newState.getPointsTo((VarLocal) value)) {
					if (globalReferences.containsValue(ref)) {
						if (changedRefs.contains(ref)) {
							return true;
						}
					}
					if (ref instanceof ImmutableReference) {
						final ImmutableValue immu = newState.getImmutableValue((ImmutableReference) ref);
						if (immu instanceof UnknownImmutableValue
								|| (immu instanceof ImmutableChoiceValue && ((ImmutableChoiceValue) immu).hasUnknownValue())) {
							return true;
						}
					} else if (ref instanceof BaseReference && ref != BaseReference.NULL_REF) {
						for (final Value val : newState.getBaseValue((BaseReference) ref)) {
							if (val instanceof UnknownMutableValue) {
								return true;
							}
							if (val instanceof RefValue && hasUnknown(((RefValue) val).getUsedVarLocalOrImmutableValues(), newState,
									changedRefs, checkedLocals)) {
								return true;
							}
						}
					} else if (ref instanceof ArrayReference && ref != ArrayReference.NULL_REF) {
						if (newState.getArrayValue((ArrayReference) ref).isUnknown()
								|| hasUnknown(newState.getArrayValue((ArrayReference) ref).getAllElements(), newState, changedRefs,
										checkedLocals)) {
							return true;
						}
					} else if (ref instanceof ObjectReference && ref != ObjectReference.NULL_REF) {
						if (newState.getObjectValue((ObjectReference) ref).isUnknownObject()
								|| hasUnknown(newState.getObjectValue((ObjectReference) ref).getAllFieldValues(), newState, changedRefs,
										checkedLocals)) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	private AbstractReference getGlobalReference(final AbstractReference ref) {
		if (ref == ObjectReference.NULL_REF || ref == ArrayReference.NULL_REF || ref == BaseReference.NULL_REF) {
			return ref;
		} else if (globalReferences.containsValue(ref)) {
			return ref;
		} else if (globalReferences.containsKey(ref)) {
			return globalReferences.get(ref);
		} else {
			final AbstractReference globalRef = globalRefCreator.visit(ref);
			globalReferences.put(ref, globalRef);
			return globalRef;
		}
	}

	private VarLocal getGlobalLocal(final VarLocal local) {
		if (globalLocals.containsValue(local)) {
			return local;
		} else if (globalLocals.containsKey(local)) {
			return globalLocals.get(local);
		} else {
			final VarLocal globalLocal = VarLocal.create(local.callstack.push(new Object(), local.callstack.method), local.variableName,
					local.typeName, local.varType);
			globalLocals.put(local, globalLocal);
			return globalLocal;
		}
	}

	private final ReferenceVisitor<AbstractReference> globalRefCreator = new ReferenceVisitor<AbstractReference>() {

		@Override
		public AbstractReference visit(AbstractReference reference) {
			return reference.apply(this);
		}

		@Override
		public AbstractReference visitImmutableReference(final ImmutableReference immutableReference) {
			final ProgramPointId newId = ProgramPointId.create(immutableReference.createdIn.callStack, new Object());
			return ImmutableReference.create(newId);
		}

		@Override
		public AbstractReference visitBaseReference(BaseReference baseReference) {
			final ProgramPointId newId = ProgramPointId.create(baseReference.createdIn.callStack, new Object());
			return BaseReference.create(newId);
		}

		@Override
		public AbstractReference visitArrayReference(ArrayReference arrayReference) {
			final ProgramPointId newId = ProgramPointId.create(arrayReference.createdIn.callStack, new Object());
			return ArrayReference.create(newId);
		}

		@Override
		public AbstractReference visitObjectReference(ObjectReference objectReference) {
			final ProgramPointId newId = ProgramPointId.create(objectReference.createdIn.callStack, new Object());
			return ObjectReference.create(newId);
		}
	};

	private ImmutableSetMultimap<String, FieldDesc> getNewIncludedFields(final SetMultimap<String, FieldDesc> oldIncludedFields,
			final Set<FieldDesc> fields) {
		final ImmutableSetMultimap.Builder<String, FieldDesc> retBuilder = ImmutableSetMultimap.builder();
		for (final Entry<String, FieldDesc> entry : oldIncludedFields.entries()) {
			if (fields.contains(entry.getValue())) {
				retBuilder.put(entry);
			}
		}
		return retBuilder.build();
	}

	private ImmutableSetMultimap<FieldDesc, ImmutableValue> getStaticInitialState(
			final Map<SootMethod, ExecuteGraph> executeGraphs,
			final Set<FieldDesc> includedStaticFields) {
		SetMultimap<FieldDesc, ImmutableValue> oldStaticFieldMap = HashMultimap.create();
		SetMultimap<FieldDesc, ImmutableValue> newStaticFieldMap = HashMultimap.create();
		for (final FieldDesc includedField : includedStaticFields) {
			if (!includedField.hasAnnotation) {
				newStaticFieldMap.put(includedField, includedField.defaultValue);
			}
		}
		while (!oldStaticFieldMap.equals(newStaticFieldMap)) {
			oldStaticFieldMap = HashMultimap.create(newStaticFieldMap);
			for (final Entry<SootMethod, ExecuteGraph> entry : executeGraphs.entrySet()) {
				if (entry.getKey().isStaticInitializer()) {
					final ExecutionState initialState = new ExecutionState(ImmutableSetMultimap.of(), ImmutableSetMultimap.of(),
							ImmutableSetMultimap.copyOf(newStaticFieldMap), ImmutableSetMultimap.of(), ImmutableMap.of(),
							ImmutableSetMultimap.of(), ImmutableMap.of(), ImmutableMap.of());
					newStaticFieldMap = new FieldValueAnalysis(CallStack.create(SootMethodUtils.methodFrom(entry.getKey())),
							entry.getValue(), initialState, true).getStaticFieldValues();
				}
			}
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("Static Field Values From Initializer: " + newStaticFieldMap);
		}
		return ImmutableSetMultimap.copyOf(newStaticFieldMap);
	}
}
