package ch.ethz.inf.pm.ecchecker.cassandra.soot.reference;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.visitor.ReferenceVisitor;

public class ArrayReference extends AbstractReference {

	public static final ArrayReference NULL_REF = new ArrayReference(ProgramPointId.create(CallStack.EMPTY_STACK, new Object()));

	private ArrayReference(final ProgramPointId createdIn) {
		super(createdIn);
	}

	public static ArrayReference create(final ProgramPointId createdIn) {
		return new ArrayReference(createdIn);
	}

	@Override
	public <R> R apply(ReferenceVisitor<R> visitor) {
		return visitor.visitArrayReference(this);
	}
}
