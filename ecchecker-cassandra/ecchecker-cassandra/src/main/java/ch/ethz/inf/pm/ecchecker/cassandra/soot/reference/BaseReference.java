package ch.ethz.inf.pm.ecchecker.cassandra.soot.reference;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.visitor.ReferenceVisitor;

public class BaseReference extends AbstractReference {

	public static final BaseReference NULL_REF = new BaseReference(ProgramPointId.create(CallStack.EMPTY_STACK, new Object()));

	private BaseReference(final ProgramPointId createdIn) {
		super(createdIn);
	}

	public static BaseReference create(final ProgramPointId createdIn) {
		return new BaseReference(createdIn);
	}

	@Override
	public <R> R apply(ReferenceVisitor<R> visitor) {
		return visitor.visitBaseReference(this);
	}
}
