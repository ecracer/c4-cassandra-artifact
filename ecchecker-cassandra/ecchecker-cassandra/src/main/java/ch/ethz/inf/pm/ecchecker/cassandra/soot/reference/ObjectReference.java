package ch.ethz.inf.pm.ecchecker.cassandra.soot.reference;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.visitor.ReferenceVisitor;

public class ObjectReference extends AbstractReference {

	public static final ObjectReference NULL_REF = new ObjectReference(ProgramPointId.create(CallStack.EMPTY_STACK, new Object()));

	private ObjectReference(final ProgramPointId createdIn) {
		super(createdIn);
	}

	public static ObjectReference create(final ProgramPointId createdIn) {
		return new ObjectReference(createdIn);
	}

	@Override
	public <R> R apply(ReferenceVisitor<R> visitor) {
		return visitor.visitObjectReference(this);
	}
}
