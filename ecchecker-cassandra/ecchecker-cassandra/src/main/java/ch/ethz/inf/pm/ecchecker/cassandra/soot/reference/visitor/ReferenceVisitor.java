package ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.visitor;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.ArrayReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.BaseReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.ImmutableReference;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.reference.ObjectReference;

public interface ReferenceVisitor<R> {

	R visit(final AbstractReference reference);

	R visitImmutableReference(final ImmutableReference immutableReference);

	R visitBaseReference(final BaseReference baseReference);

	R visitArrayReference(final ArrayReference arrayReference);

	R visitObjectReference(final ObjectReference objectReference);
}