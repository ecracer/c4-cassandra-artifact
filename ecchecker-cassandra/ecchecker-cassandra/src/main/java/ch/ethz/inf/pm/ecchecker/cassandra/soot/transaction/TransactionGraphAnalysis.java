package ch.ethz.inf.pm.ecchecker.cassandra.soot.transaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.google.common.graph.Graph;
import com.google.common.graph.Graphs;

import ch.ethz.inf.pm.ecchecker.cassandra.graphs.UnparsedTransactionGraph;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import soot.toolkits.graph.DirectedGraph;
import soot.toolkits.scalar.ForwardFlowAnalysis;

class TransactionGraphAnalysis extends ForwardFlowAnalysis<AbstractProgramPoint, ExecutionState> {

	private final AbstractProgramPoint exitNode;
	private final ExecutionState entryFlow;

	public TransactionGraphAnalysis(final Graph<AbstractProgramPoint> executionGraph, final AbstractProgramPoint entryNode,
			final AbstractProgramPoint exitNode, final ExecutionState entryFlow) {
		super(new SootGraph(executionGraph, entryNode, exitNode));
		this.entryFlow = Objects.requireNonNull(entryFlow);
		this.exitNode = Objects.requireNonNull(exitNode);
		doAnalysis();
	}

	public UnparsedTransactionGraph getTransactionGraph() {
		if (getFlowAfter(exitNode).isBottom()) {
			throw new RuntimeException("Analysis reported bottom");
		} else {
			return getFlowAfter(exitNode).getFinalTransactionGraph();
		}
	}

	@Override
	protected ExecutionState entryInitialFlow() {
		return entryFlow.clone();
	}

	@Override
	protected ExecutionState newInitialFlow() {
		return new ExecutionState();
	}

	@Override
	protected void merge(ExecutionState in1, ExecutionState in2, ExecutionState out) {
		in1.mergeWith(in2, out);
	}

	@Override
	protected void copy(ExecutionState source, ExecutionState dest) {
		source.copyTo(dest);
	}

	@Override
	protected void flowThrough(ExecutionState in, AbstractProgramPoint d, ExecutionState out) {
		final ExecutionState oldOut = getFlowAfter(d).clone();
		in.copyTo(out);
		if (!out.isBottom()) {
			d.transform(out, oldOut);
		}
	}

	/**
	 * Directed Graph that can be used by the fixed point analysis provided by soot. Additional NopProgramPoints are
	 * inserted as head and tail. Also, a methodInvokePoints is connected with its methodReturnPoint
	 * 
	 * @author Arthur Kurath
	 */
	private static class SootGraph implements DirectedGraph<AbstractProgramPoint> {

		private final Graph<AbstractProgramPoint> executionGraph;
		private final AbstractProgramPoint entryNode;
		private final AbstractProgramPoint exitNode;

		public SootGraph(Graph<AbstractProgramPoint> executionGraph, AbstractProgramPoint entryNode, AbstractProgramPoint exitNode) {
			this.executionGraph = Objects.requireNonNull(executionGraph);
			this.entryNode = Objects.requireNonNull(entryNode);
			this.exitNode = Objects.requireNonNull(exitNode);
		}

		@Override
		public List<AbstractProgramPoint> getHeads() {
			return Collections.singletonList(entryNode);
		}

		@Override
		public List<AbstractProgramPoint> getTails() {
			return Collections.singletonList(exitNode);
		}

		@Override
		public List<AbstractProgramPoint> getPredsOf(AbstractProgramPoint s) {
			return new ArrayList<>(executionGraph.predecessors(s));
		}

		@Override
		public List<AbstractProgramPoint> getSuccsOf(AbstractProgramPoint s) {
			// Back edges from loops should be added at the end of the list. In this way, soot takes them earlier into account when 
			// doing the flow analysis
			final Set<AbstractProgramPoint> succs = executionGraph.successors(s);
			if (succs.size() > 1) {
				final LinkedList<AbstractProgramPoint> ret = new LinkedList<>();
				for (final AbstractProgramPoint succ : succs) {
					if (Graphs.reachableNodes(executionGraph, succ).contains(s)) {
						ret.addLast(succ);
					} else {
						ret.addFirst(succ);
					}
				}
				// soot requires instance of RandomAccess
				return new ArrayList<>(ret);
			} else {
				return new ArrayList<>(succs);
			}
		}

		@Override
		public int size() {
			return executionGraph.nodes().size();
		}

		@Override
		public Iterator<AbstractProgramPoint> iterator() {
			return executionGraph.nodes().iterator();
		}
	}
}
