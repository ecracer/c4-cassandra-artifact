package ch.ethz.inf.pm.ecchecker.cassandra.soot.transaction;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.ImmutableMap;
import com.google.common.graph.EndpointPair;
import com.google.common.graph.GraphBuilder;
import com.google.common.graph.MutableGraph;

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.ExecuteGraph;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.GraphToDotUtil;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.UnparsedTransactionGraph;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootAnnotationUtils;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootMethodUtils;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootOptions;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CaughtExceptionProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.NopProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ThrowExceptionProgramPoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.invoke.AnalyzedMethodInvokePoint;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.invoke.AnalyzedMethodInvokePoint.MethodReturnPoint;
import soot.SootMethod;

/**
 * The unparsed transaction graphs are collected in this step. For each declared transaction, a super graph is built
 * that includes all methods that are called from the transaction method. Each transformer is instantiated in the graph
 * with a CallStack that contains all invokeStatmenets that lead to the execution of the method. This ensures, that a
 * method can be included multiple times if it is called from different locations, but that recursion works non the
 * less.
 */
public class TransactionGraphCollector {
	private final static Logger LOG = LogManager.getLogger(TransactionGraphCollector.class);

	private final SootOptions options;
	public final ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> transactionGraphs;

	public TransactionGraphCollector(final SootOptions options, final Map<SootMethod, ExecuteGraph> executeGraphs,
			final Set<SootMethod> transactionMethods, final ExecutionState initialState) {

		this.options = Objects.requireNonNull(options);
		transactionGraphs = calculateTransactionGraphs(executeGraphs, transactionMethods, initialState);
	}

	public ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> calculateTransactionGraphs(
			final Map<SootMethod, ExecuteGraph> executeGraphs, final Set<SootMethod> transactionMethods,
			final ExecutionState initialState) {

		final ImmutableMap.Builder<TransactionDescriptor, UnparsedTransactionGraph> transactionGraphsBuilder = ImmutableMap
				.builder();
		for (final SootMethod transactionMethod : transactionMethods) {
			final Transaction annotation = SootAnnotationUtils.getTransactionAnnotation(transactionMethod);
			final TransactionDescriptor transaction = TransactionDescriptor.create(SootMethodUtils.methodFrom(transactionMethod),
					annotation != null ? annotation.onlyForDisplaying() : false);
			if (executeGraphs.containsKey(transactionMethod)) {
				final UnparsedTransactionGraph transactionGraph = calculateTransactionGraph(transactionMethod,
						executeGraphs, initialState);

				transactionGraphsBuilder.put(transaction, transactionGraph);
			} else {
				transactionGraphsBuilder.put(transaction, UnparsedTransactionGraph.createEmpty());
			}
		}
		return transactionGraphsBuilder.build();
	}

	private UnparsedTransactionGraph calculateTransactionGraph(final SootMethod method, final Map<SootMethod, ExecuteGraph> executeGraphs,
			final ExecutionState initialState) {

		final CallStack bootstrapCallStack = CallStack.create(SootMethodUtils.methodFrom(method));
		final NopProgramPoint entry = NopProgramPoint.create(bootstrapCallStack);
		final NopProgramPoint exit = NopProgramPoint.create(bootstrapCallStack);

		final MutableGraph<AbstractProgramPoint> fullGraph = getFullGraph(method, entry, exit, executeGraphs);

		if (LOG.isTraceEnabled()) {
			try {
				final File tempDir = Files.createTempDirectory("sootAnalysisGraph").toFile();
				GraphToDotUtil.logGraphToTemp(tempDir, method, fullGraph);
				LOG.trace("Full graph for " + bootstrapCallStack.method.getShortName() + " was written to " + tempDir.getAbsolutePath());
			} catch (IOException e) {
				LOG.error("Error logging graph", e);
			}
		}

		// create initial state
		final ExecutionState entryFlow = initialState.clone();
		entryFlow.addInitialMethodCallState(bootstrapCallStack, method);

		return new TransactionGraphAnalysis(fullGraph, entry, exit, entryFlow).getTransactionGraph();
	}

	private MutableGraph<AbstractProgramPoint> getFullGraph(final SootMethod method, final AbstractProgramPoint entry,
			final AbstractProgramPoint exit, final Map<SootMethod, ExecuteGraph> executeGraphs) {

		final MutableGraph<AbstractProgramPoint> fullGraph = GraphBuilder.directed().allowsSelfLoops(true).build();

		// setup initial graph
		final ExecuteGraph firstMethodGraph = executeGraphs.get(method);
		firstMethodGraph.nodes().forEach(node -> fullGraph.addNode(node));
		firstMethodGraph.edges().forEach(edge -> fullGraph.putEdge(edge.nodeU(), edge.nodeV()));
		fullGraph.addNode(entry);
		firstMethodGraph.sourceNodes().forEach(source -> fullGraph.putEdge(entry, source));
		fullGraph.addNode(exit);
		firstMethodGraph.sinkNodes().forEach(sink -> fullGraph.putEdge(sink, exit));
		if (firstMethodGraph.isBypassPossible()) {
			fullGraph.putEdge(entry, exit);
		}

		// initial worklist with methods that are missing in the full graph
		final Deque<AnalyzedMethodInvokePoint> worklist = new LinkedList<>();
		for (final AbstractProgramPoint node : firstMethodGraph.nodes()) {
			if (node instanceof AnalyzedMethodInvokePoint) {
				worklist.addLast((AnalyzedMethodInvokePoint) node);
			}
		}

		// add all missing methods (and adding new ones)
		final Set<AnalyzedMethodInvokePoint> processedInvokes = new HashSet<>();
		while (!worklist.isEmpty()) {
			final AnalyzedMethodInvokePoint methodInvoke = worklist.removeFirst();
			if (!processedInvokes.contains(methodInvoke)) {
				processedInvokes.add(methodInvoke);
				final SootMethod calledMethod = methodInvoke.calledMethod;
				if (!executeGraphs.containsKey(calledMethod)) {
					throw new RuntimeException("Method " + calledMethod.getSignature() + " is not analyzed");
				}
				final ExecuteGraph graph = executeGraphs.get(calledMethod);
				final CallStack newStack = methodInvoke.getCallStackForCalledMethod();

				// transform nodes (so that the new callstack is used) and add them to the graph
				final Map<AbstractProgramPoint, AbstractProgramPoint> transformedNodes = new HashMap<>();
				for (final AbstractProgramPoint node : graph.nodes()) {
					if (!(node instanceof MethodReturnPoint)) {
						final AbstractProgramPoint transformedNode = node.createWithCallstack(newStack);
						transformedNodes.put(node, transformedNode);
						fullGraph.addNode(transformedNode);
						if (transformedNode instanceof AnalyzedMethodInvokePoint) {
							final AnalyzedMethodInvokePoint methodInvokeNode = (AnalyzedMethodInvokePoint) node;
							final AnalyzedMethodInvokePoint transformedMethodInvokeNode = (AnalyzedMethodInvokePoint) transformedNode;
							transformedNodes.put(methodInvokeNode.methodReturnPoint, transformedMethodInvokeNode.methodReturnPoint);
							fullGraph.addNode(transformedMethodInvokeNode.methodReturnPoint);

							worklist.add(transformedMethodInvokeNode);
						}
					}
				}
				// add all missing edges
				for (final EndpointPair<AbstractProgramPoint> edge : graph.edges()) {
					if (!(edge.nodeU() instanceof ThrowExceptionProgramPoint) || edge.nodeV() instanceof CaughtExceptionProgramPoint) {
						fullGraph.putEdge(transformedNodes.get(edge.nodeU()), transformedNodes.get(edge.nodeV()));
					}
				}

				final Set<CaughtExceptionProgramPoint> caughtExceptionSuccs = new HashSet<>();
				for (final AbstractProgramPoint succ : fullGraph.successors(methodInvoke.methodReturnPoint)) {
					if (succ instanceof CaughtExceptionProgramPoint) {
						caughtExceptionSuccs.add((CaughtExceptionProgramPoint) succ);
					}
				}

				for (final CaughtExceptionProgramPoint caugthExceptionSucc : caughtExceptionSuccs) {
					for (final AbstractProgramPoint node : graph.nodes()) {
						fullGraph.putEdge(transformedNodes.get(node), caugthExceptionSucc);
					}
				}

				// add source and sink
				graph.sourceNodes().forEach(source -> fullGraph.putEdge(methodInvoke, transformedNodes.get(source)));
				graph.sinkNodes().forEach(sink -> {
					if (!(sink instanceof ThrowExceptionProgramPoint)) {
						fullGraph.putEdge(transformedNodes.get(sink), methodInvoke.methodReturnPoint);
					}
				});

			}
		}

		if (!options.isSoundThrowAnalysisEnabled()) {
			// remove the edges from a normal transformer to a catch statement
			// this edges are needed during construction, but are not needed for our analysis
			for (final EndpointPair<AbstractProgramPoint> edge : new ArrayList<>(fullGraph.edges())) {
				if (edge.nodeV() instanceof CaughtExceptionProgramPoint && !(edge.nodeU() instanceof ThrowExceptionProgramPoint)) {
					fullGraph.removeEdge(edge.nodeU(), edge.nodeV());
				}
			}
		}

		// connect each throw transformer with the exit
		for (final AbstractProgramPoint node : new ArrayList<>(fullGraph.nodes())) {
			if (node instanceof ThrowExceptionProgramPoint) {
				fullGraph.putEdge(node, exit);
			}
		}
		return fullGraph;
	}
}
