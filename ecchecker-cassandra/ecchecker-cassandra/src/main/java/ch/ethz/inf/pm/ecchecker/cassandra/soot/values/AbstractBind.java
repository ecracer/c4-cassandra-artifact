package ch.ethz.inf.pm.ecchecker.cassandra.soot.values;

import java.util.Objects;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.ConcreteValue;

public abstract class AbstractBind<T> {
	public final T value;

	protected AbstractBind(final T value) {
		this.value = Objects.requireNonNull(value);
	}

	public abstract <newT> AbstractBind<newT> createNew(final newT value);

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractBind<?> other = (AbstractBind<?>) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	/**
	 * Bind created by the analysis (e.g. for abstracting queries built using the StringBuilder)
	 */
	public static class SystemBind<T> extends AbstractBind<T> {
		public final String name;

		private SystemBind(final String name, final T value) {
			super(value);
			this.name = name;
		}

		public static <T> SystemBind<T> create(final String name, final T value) {
			return new SystemBind<>(name, value);
		}

		@Override
		public <newT> AbstractBind<newT> createNew(newT value) {
			return new SystemBind<>(name, value);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			SystemBind<?> other = (SystemBind<?>) obj;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}
	}

	/**
	 * Bind provided by the user. Idx must be an integer or a string
	 */
	public static class UserBind<T> extends AbstractBind<T> {
		public final ConcreteValue idx;

		private UserBind(final ConcreteValue idx, final T value) {
			super(value);
			this.idx = idx;
		}

		public static <T> UserBind<T> create(final ConcreteValue idx, final T value) {
			return new UserBind<>(idx, value);
		}

		@Override
		public <newT> AbstractBind<newT> createNew(newT value) {
			return new UserBind<>(idx, value);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((idx == null) ? 0 : idx.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			UserBind<?> other = (UserBind<?>) obj;
			if (idx == null) {
				if (other.idx != null)
					return false;
			} else if (!idx.equals(other.idx))
				return false;
			return true;
		}
	}

	public static class PartitionKeyTokenBind<T> extends AbstractBind<T> {

		private PartitionKeyTokenBind(final T value) {
			super(value);
		}

		public static <T> PartitionKeyTokenBind<T> create(final T value) {
			return new PartitionKeyTokenBind<>(value);
		}

		@Override
		public <newT> AbstractBind<newT> createNew(newT value) {
			return new PartitionKeyTokenBind<>(value);
		}
	}
}
