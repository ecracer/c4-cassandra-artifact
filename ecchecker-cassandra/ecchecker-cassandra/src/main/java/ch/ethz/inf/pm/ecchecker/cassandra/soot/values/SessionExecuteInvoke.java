package ch.ethz.inf.pm.ecchecker.cassandra.soot.values;

import java.util.Objects;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.StatementValue;

/**
 * Specific invocation of a SessionExecuteCall, e.g. in a TransactionGraph
 */
public class SessionExecuteInvoke {

	/** From where the call is invoked */
	public final ProgramPointId executedFrom;
	/** Whether this execute is called async */
	public final boolean async;
	/** List of statements that may (must if size == 1) be called here */
	public final ImmutableSet<StatementValue> statementArg;

	private SessionExecuteInvoke(final ProgramPointId executedFrom, final boolean async, final ImmutableSet<StatementValue> statementArg) {
		this.executedFrom = Objects.requireNonNull(executedFrom);
		this.async = async;
		this.statementArg = Objects.requireNonNull(statementArg);
	}

	public static SessionExecuteInvoke create(final ProgramPointId executedFrom, final boolean async,
			final Set<StatementValue> statementArg) {
		return new SessionExecuteInvoke(executedFrom, async, ImmutableSet.copyOf(statementArg));
	}

	public SessionExecuteInvoke mergeWith(final SessionExecuteInvoke other) {
		if (!executedFrom.equals(other.executedFrom) || async != other.async) {
			throw new RuntimeException("Cannot merge different SessionExecuteInvoke");
		} else {
			return new SessionExecuteInvoke(executedFrom, async,
					ImmutableSet.<StatementValue> builder().addAll(statementArg).addAll(other.statementArg).build());
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (async ? 1231 : 1237);
		result = prime * result + ((executedFrom == null) ? 0 : executedFrom.hashCode());
		result = prime * result + ((statementArg == null) ? 0 : statementArg.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SessionExecuteInvoke other = (SessionExecuteInvoke) obj;
		if (async != other.async)
			return false;
		if (executedFrom == null) {
			if (other.executedFrom != null)
				return false;
		} else if (!executedFrom.equals(other.executedFrom))
			return false;
		if (statementArg == null) {
			if (other.statementArg != null)
				return false;
		} else if (!statementArg.equals(other.statementArg))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SessionExecuteInvoke [executedFrom=" + executedFrom + ", async=" + async + ", statementArg="
				+ statementArg + "]";
	}
}
