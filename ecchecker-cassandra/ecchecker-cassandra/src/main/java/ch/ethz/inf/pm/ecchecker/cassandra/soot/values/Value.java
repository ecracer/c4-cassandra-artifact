package ch.ethz.inf.pm.ecchecker.cassandra.soot.values;

/**
 * General interface for the abstractions of the different values. Subclasses of this interface must be immutable.
 */
public interface Value {

}
