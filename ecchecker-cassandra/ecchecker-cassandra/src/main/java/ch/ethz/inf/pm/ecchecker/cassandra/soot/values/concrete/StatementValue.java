package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.google.common.collect.ImmutableList;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractBind;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractBind.SystemBind;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractStatementValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.Value;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.ConcreteValueVisitor;

public class StatementValue extends AbstractStatementValue<ConcreteValue, StatementValue> implements ConcreteValue {

	public final String query;

	private StatementValue(final String query, final ImmutableList<AbstractBind<ConcreteValue>> binds) {
		this(query, binds, NullValue.create(), NullValue.create());
	}

	protected StatementValue(final String query, final ImmutableList<AbstractBind<ConcreteValue>> binds,
			final ImmutableValue consistencyLevel, final ImmutableValue serialConsistencyLevel) {
		super(binds, consistencyLevel, serialConsistencyLevel);
		this.query = Objects.requireNonNull(query);
	}

	public static StatementValue create(final String query, final List<AbstractBind<ConcreteValue>> binds) {
		return new StatementValue(query, ImmutableList.copyOf(binds));
	}

	public static StatementValue create(final String query, final List<AbstractBind<ConcreteValue>> binds,
			final ImmutableValue consistencyLevel, final ImmutableValue serialConsistencyLevel) {
		return new StatementValue(query, ImmutableList.copyOf(binds), consistencyLevel, serialConsistencyLevel);
	}

	public static StatementValue createUnknown() {
		return create("", Collections.emptyList());
	}

	public String getQueryWithUnnamedBindMarkers() {
		String ret = query;
		for (final AbstractBind<?> bind : binds) {
			if (bind instanceof SystemBind<?>) {
				ret = ret.replaceAll(":" + ((SystemBind<?>) bind).name, "?");
			}
		}
		return ret;
	}

	@Override
	public <R> R apply(ConcreteValueVisitor<R> visitor) {
		return visitor.visitStatementValue(this);
	}

	@Override
	protected StatementValue createNewInternal(final ImmutableList<AbstractBind<ConcreteValue>> binds, ImmutableValue consistencyLevel,
			ImmutableValue serialConsistencyLevel) {
		return new StatementValue(query, binds, consistencyLevel, serialConsistencyLevel);
	}

	@Override
	public ConcreteValue widenWith(Value newValue, ProgramPointId updatedFrom) {
		if (newValue instanceof StatementValue && query.equals(((StatementValue) newValue).query)) {
			return (StatementValue) newValue;
		} else {
			return UnknownMutableValue.create();
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((query == null) ? 0 : query.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatementValue other = (StatementValue) obj;
		if (query == null) {
			if (other.query != null)
				return false;
		} else if (!query.equals(other.query))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "StatementValue [query=" + query + "]";
	}
}
