package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable;

import java.util.Objects;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.ImmutableValueVisitor;

public class BindMarkerValue extends AbstractImmutableValue implements ImmutableValue {

	public final ImmutableValue name;

	private BindMarkerValue(final ImmutableValue name) {
		this.name = name;
	}

	public static BindMarkerValue createNamed(final ImmutableValue name) {
		return new BindMarkerValue(Objects.requireNonNull(name));
	}

	public static BindMarkerValue createAnonymous() {
		return new BindMarkerValue(null);
	}

	@Override
	public <R> R apply(ImmutableValueVisitor<R> visitor) {
		return visitor.visitBindMarkerValue(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BindMarkerValue other = (BindMarkerValue) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BindMarker [" + (name == null ? "?" : ":" + name) + "]";
	}
}
