package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable;

import java.util.Objects;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.Value;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.ImmutableValueVisitor;

public class ClientImmutableValue extends AbstractImmutableValue {

	public final String id;

	private ClientImmutableValue(final String id) {
		this.id = Objects.requireNonNull(id);
	}

	public static ClientImmutableValue create(final String id) {
		return new ClientImmutableValue(id);
	}

	@Override
	public ImmutableValue widenWith(Value newValue, ProgramPointId updatedFrom) {
		if (equals(newValue)) {
			return this;
		} else {
			return super.widenWith(newValue, updatedFrom);
		}
	}

	@Override
	public <R> R apply(ImmutableValueVisitor<R> visitor) {
		return visitor.visitClientImmutableValue(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientImmutableValue other = (ClientImmutableValue) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "ClientImmutableValue [id=" + id + "]";
	}
}
