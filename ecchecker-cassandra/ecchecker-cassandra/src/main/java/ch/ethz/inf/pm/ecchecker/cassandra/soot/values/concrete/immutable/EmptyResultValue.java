package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.ImmutableValueVisitor;

public class EmptyResultValue extends AbstractImmutableValue implements ImmutableValue {

	public final ProgramPointId resultFrom;
	public final boolean isTrueWhenEmpty;

	private EmptyResultValue(final ProgramPointId resultFrom, final boolean isTrueWhenEmpty) {
		this.resultFrom = resultFrom;
		this.isTrueWhenEmpty = isTrueWhenEmpty;
	}

	public static EmptyResultValue create(final ProgramPointId resultFrom) {
		return new EmptyResultValue(resultFrom, true);
	}

	public EmptyResultValue setIsTrueWhenEmpty(final boolean isTrueWhenEmpty) {
		if (this.isTrueWhenEmpty == isTrueWhenEmpty) {
			return this;
		} else {
			return new EmptyResultValue(resultFrom, isTrueWhenEmpty);
		}
	}

	@Override
	public <R> R apply(ImmutableValueVisitor<R> visitor) {
		return visitor.visitEmptyResultValue(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isTrueWhenEmpty ? 1231 : 1237);
		result = prime * result + ((resultFrom == null) ? 0 : resultFrom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmptyResultValue other = (EmptyResultValue) obj;
		if (isTrueWhenEmpty != other.isTrueWhenEmpty)
			return false;
		if (resultFrom == null) {
			if (other.resultFrom != null)
				return false;
		} else if (!resultFrom.equals(other.resultFrom))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "EmptyResultValue [resultFrom=" + resultFrom + ", isTrueWhenEmpty=" + isTrueWhenEmpty + "]";
	}
}
