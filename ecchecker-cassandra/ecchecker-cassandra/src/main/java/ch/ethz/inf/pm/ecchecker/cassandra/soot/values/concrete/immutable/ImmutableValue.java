package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.Value;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.ImmutableValueVisitor;

public interface ImmutableValue extends ConcreteValue, VarLocalOrImmutableValue {

	<R> R apply(final ImmutableValueVisitor<R> visitor);

	@Override
	ImmutableValue widenWith(final Value newValue, final ProgramPointId updatedFrom);
}
