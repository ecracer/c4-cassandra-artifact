package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.ImmutableValueVisitor;

public class IntValue extends AbstractImmutableValue implements ImmutableValue {

	public final int number;

	private IntValue(final int number) {
		this.number = number;
	}

	public static IntValue create(final int number) {
		return new IntValue(number);
	}

	@Override
	public <R> R apply(ImmutableValueVisitor<R> visitor) {
		return visitor.visitIntValue(this);
	}

	@Override
	public int hashCode() {
		return number;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntValue other = (IntValue) obj;
		if (number != other.number)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IntValue [" + number + "]";
	}
}
