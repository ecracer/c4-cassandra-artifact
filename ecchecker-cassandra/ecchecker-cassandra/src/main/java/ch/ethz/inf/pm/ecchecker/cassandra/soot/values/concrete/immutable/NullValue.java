package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.ImmutableValueVisitor;

public class NullValue extends AbstractImmutableValue {

	public final static NullValue INSTANCE = new NullValue();

	private NullValue() {
	}

	public static NullValue create() {
		return INSTANCE;
	}

	@Override
	public <R> R apply(ImmutableValueVisitor<R> visitor) {
		return visitor.visitNullValue(this);
	}

	@Override
	public int hashCode() {
		return 85451;
	}

	@Override
	public boolean equals(Object obj) {
		// singleton
		if (this == obj)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return "NullValue";
	}
}
