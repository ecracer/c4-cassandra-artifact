package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable;

import java.util.Objects;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.ResultSetValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.ImmutableValueVisitor;

public class StatementSingleResultValue extends AbstractImmutableValue implements ImmutableValue {

	public final ResultSetValue resultSet;
	public final ImmutableValue idx;

	private StatementSingleResultValue(final ResultSetValue resultSet, final ImmutableValue idx) {
		this.resultSet = Objects.requireNonNull(resultSet);
		this.idx = Objects.requireNonNull(idx);
	}

	public static StatementSingleResultValue create(final ResultSetValue resultSet, final ImmutableValue idx) {
		return new StatementSingleResultValue(resultSet, idx);
	}

	@Override
	public <R> R apply(ImmutableValueVisitor<R> visitor) {
		return visitor.visitStatementSingleResultValue(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idx == null) ? 0 : idx.hashCode());
		result = prime * result + ((resultSet == null) ? 0 : resultSet.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatementSingleResultValue other = (StatementSingleResultValue) obj;
		if (idx == null) {
			if (other.idx != null)
				return false;
		} else if (!idx.equals(other.idx))
			return false;
		if (resultSet == null) {
			if (other.resultSet != null)
				return false;
		} else if (!resultSet.equals(other.resultSet))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "StatementSingleResultValue [resultSet=" + resultSet + ", idx=" + idx + "]";
	}
}
