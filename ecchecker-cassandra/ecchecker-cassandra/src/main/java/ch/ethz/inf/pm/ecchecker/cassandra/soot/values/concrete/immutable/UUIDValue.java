package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable;

import java.util.Objects;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.Value;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.ImmutableValueVisitor;

/**
 * UUID value. If isRandom is true,
 */
public class UUIDValue extends AbstractImmutableValue {

	public final ProgramPointId createdIn;
	public final boolean isRandom;
	public final boolean isWidened;

	private UUIDValue(final ProgramPointId createdIn, final boolean isRandom, final boolean isWidened) {
		this.createdIn = Objects.requireNonNull(createdIn);
		this.isRandom = isRandom;
		this.isWidened = isWidened;
	}

	public static UUIDValue createRandom(final ProgramPointId createdIn) {
		return new UUIDValue(createdIn, true, false);
	}

	public static UUIDValue createFromString(final ProgramPointId createdIn) {
		return new UUIDValue(createdIn, false, false);
	}

	@Override
	public <R> R apply(ImmutableValueVisitor<R> visitor) {
		return visitor.visitUUIDValue(this);
	}

	@Override
	public ImmutableValue widenWith(Value newValue, ProgramPointId updatedFrom) {
		if (newValue instanceof UUIDValue && createdIn.equals(((UUIDValue) newValue).createdIn)
				&& isRandom == ((UUIDValue) newValue).isRandom) {
			if (isWidened) {
				return this;
			} else {
				return new UUIDValue(createdIn, isRandom, true);
			}
		} else {
			return UnknownImmutableValue.create(createdIn);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createdIn == null) ? 0 : createdIn.hashCode());
		result = prime * result + (isRandom ? 1231 : 1237);
		result = prime * result + (isWidened ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UUIDValue other = (UUIDValue) obj;
		if (createdIn == null) {
			if (other.createdIn != null)
				return false;
		} else if (!createdIn.equals(other.createdIn))
			return false;
		if (isRandom != other.isRandom)
			return false;
		if (isWidened != other.isWidened)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return (isRandom ? "" : "str_") + "UUID " + (isWidened ? "w " : "") + Integer.toHexString(createdIn.hashCode());
	}
}
