package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable;

import java.util.Objects;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.Value;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.ImmutableValueVisitor;

/**
 * Abstraction of a value that is unknown at runtime. Values with the same createdInLocation are equal, if they have not
 * been widened.
 */
public class UnknownImmutableValue extends AbstractImmutableValue {

	public final ProgramPointId createdIn;
	public final int createdInIdx;
	public final boolean isWidened;

	private UnknownImmutableValue(final ProgramPointId createdIn, final int createdInIdx, final boolean isWidened) {
		this.createdIn = Objects.requireNonNull(createdIn);
		this.createdInIdx = createdInIdx;
		this.isWidened = isWidened;
	}

	public static UnknownImmutableValue create(final ProgramPointId createdIn) {
		return new UnknownImmutableValue(createdIn, 0, false);
	}

	public static UnknownImmutableValue createWithIdx(final ProgramPointId createdIn, final int idx) {
		return new UnknownImmutableValue(createdIn, idx, false);
	}

	public static UnknownImmutableValue createWidened(final ProgramPointId createdIn) {
		return new UnknownImmutableValue(createdIn, 0, true);
	}

	@Override
	public <R> R apply(ImmutableValueVisitor<R> visitor) {
		return visitor.visitUnknownImmutableValue(this);
	}

	@Override
	public ImmutableValue widenWith(Value newValue, ProgramPointId updatedFrom) {
		if (isWidened) {
			return this;
		}
		return new UnknownImmutableValue(createdIn, createdInIdx, true);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createdIn == null) ? 0 : createdIn.hashCode());
		result = prime * result + createdInIdx;
		result = prime * result + (isWidened ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnknownImmutableValue other = (UnknownImmutableValue) obj;
		if (createdIn == null) {
			if (other.createdIn != null)
				return false;
		} else if (!createdIn.equals(other.createdIn))
			return false;
		if (createdInIdx != other.createdInIdx)
			return false;
		if (isWidened != other.isWidened)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UnknownImmutableValue " + (isWidened ? "W " : "") + Integer.toHexString(createdIn.hashCode() + createdInIdx);
	}
}
