package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs;

import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.RefValueVisitor;
import ch.ethz.inf.pm.ecchecker.cassandra.util.Utils;

public class RefArrayValue implements RefValue {

	protected final static int UNKNOWN_LENGTH = -1;
	protected final int length;
	protected final boolean isUnknown;
	protected final ImmutableValue defaultElement;
	protected final ImmutableSetMultimap<Integer, VarLocalOrImmutableValue> elements;

	private RefArrayValue(final int length, final boolean isUnknown, final ImmutableValue defaultElement,
			final ImmutableSetMultimap<Integer, VarLocalOrImmutableValue> elements) {
		if (length < 0 && length != UNKNOWN_LENGTH) {
			throw new IllegalArgumentException();
		}
		this.length = length;
		this.isUnknown = isUnknown;
		this.defaultElement = Objects.requireNonNull(defaultElement);
		this.elements = Objects.requireNonNull(elements);
	}

	public static RefArrayValue create(final int length, final ImmutableValue defaultElement) {
		if (length < 0) {
			throw new IllegalArgumentException();
		}
		return new RefArrayValue(length, false, defaultElement, ImmutableSetMultimap.of());
	}

	public static RefArrayValue createWithUnknownLength(final ImmutableValue defaultElement) {
		return new RefArrayValue(UNKNOWN_LENGTH, false, defaultElement, ImmutableSetMultimap.of());
	}

	public static RefArrayValue createUnknown() {
		return new RefArrayValue(UNKNOWN_LENGTH, true, NullValue.create(), ImmutableSetMultimap.of());
	}

	@Override
	public <R> R apply(RefValueVisitor<R> visitor) {
		return visitor.visitRefArrayValue(this);
	}

	public boolean isUnknownLength() {
		return length == UNKNOWN_LENGTH;
	}

	public int getLength() {
		if (length == UNKNOWN_LENGTH) {
			throw new IllegalStateException();
		}
		return length;
	}

	public boolean isUnknown() {
		return isUnknown;
	}

	public ImmutableValue getDefaultElement() {
		return defaultElement;
	}

	public ImmutableSet<VarLocalOrImmutableValue> getElement(final int idx) {
		if (isUnknown) {
			throw new IllegalStateException();
		}
		checkIdx(idx);
		if (elements.containsKey(idx)) {
			return elements.get(idx);
		} else {
			return ImmutableSet.of(defaultElement);
		}
	}

	public RefArrayValue setElement(final int idx, final Set<VarLocalOrImmutableValue> element) {
		if (isUnknown) {
			throw new IllegalStateException();
		}
		checkIdx(idx);
		return new RefArrayValue(length, isUnknown, defaultElement, Utils.multimapWithValues(elements, idx, element));
	}

	public ImmutableSet<VarLocalOrImmutableValue> getAllElements() {
		return ImmutableSet.copyOf(elements.values());
	}

	@Override
	public Set<VarLocalOrImmutableValue> getUsedVarLocalOrImmutableValues() {
		return getAllElements();
	}

	@Override
	public RefArrayValue transformVarLocalOrImmutableValues(Function<VarLocalOrImmutableValue, VarLocalOrImmutableValue> transformer) {
		final ImmutableSetMultimap.Builder<Integer, VarLocalOrImmutableValue> elementsBuilder = ImmutableSetMultimap.builder();
		for (final Entry<Integer, VarLocalOrImmutableValue> element : elements.entries()) {
			elementsBuilder.put(element.getKey(), transformer.apply(element.getValue()));
		}
		return new RefArrayValue(length, isUnknown, defaultElement, elementsBuilder.build());
	}

	public RefArrayValue toUnknown() {
		return new RefArrayValue(length, true, defaultElement, ImmutableSetMultimap.of());
	}

	private void checkIdx(final int idx) {
		if (idx < 0 || (length != UNKNOWN_LENGTH && idx >= length)) {
			throw new IllegalArgumentException();
		}
	}

	public RefArrayValue mergeWith(final RefArrayValue other) {
		if (length != other.length || !defaultElement.equals(other.defaultElement)) {
			throw new IllegalArgumentException("Cannot merge different arrays");
		}
		if (isUnknown) {
			return this;
		} else if (other.isUnknown) {
			return other;
		}
		final ImmutableSet<Integer> differentIdx = Utils.getMissingKeys(elements, other.elements);
		ImmutableSetMultimap<Integer, VarLocalOrImmutableValue> newElements = Utils.mergeMultimap(elements, other.elements);
		if (!differentIdx.isEmpty()) {
			for (final Integer idx : differentIdx) {
				newElements = Utils.multimapWithAdditionalValue(newElements, idx, defaultElement);
			}
		}
		return new RefArrayValue(length, false, defaultElement, newElements);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((defaultElement == null) ? 0 : defaultElement.hashCode());
		result = prime * result + ((elements == null) ? 0 : elements.hashCode());
		result = prime * result + (isUnknown ? 1231 : 1237);
		result = prime * result + length;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RefArrayValue other = (RefArrayValue) obj;
		if (defaultElement == null) {
			if (other.defaultElement != null)
				return false;
		} else if (!defaultElement.equals(other.defaultElement))
			return false;
		if (elements == null) {
			if (other.elements != null)
				return false;
		} else if (!elements.equals(other.elements))
			return false;
		if (isUnknown != other.isUnknown)
			return false;
		if (length != other.length)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RefArrayValue [length=" + length + ", isUnknown=" + isUnknown + ", defaultElement=" + defaultElement + ", elements="
				+ elements + "]";
	}
}