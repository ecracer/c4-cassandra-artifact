package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs;

import java.util.Set;
import java.util.function.Function;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.Value;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.RefValueVisitor;

/**
 * General interface for a value that refers to other references. We use {@link VarLocalOrImmutableValue} as we have SSA
 * form.
 */
public interface RefValue extends Value {

	<R> R apply(final RefValueVisitor<R> visitor);

	Set<VarLocalOrImmutableValue> getUsedVarLocalOrImmutableValues();

	RefValue transformVarLocalOrImmutableValues(Function<VarLocalOrImmutableValue, VarLocalOrImmutableValue> transformer);
}
