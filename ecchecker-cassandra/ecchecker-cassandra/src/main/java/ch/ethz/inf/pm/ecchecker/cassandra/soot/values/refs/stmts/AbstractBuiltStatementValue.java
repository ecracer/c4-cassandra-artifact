package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.stmts;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import com.google.common.collect.ImmutableList;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractBind;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractStatementValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.RefValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.RefValueVisitor;
import ch.ethz.inf.pm.ecchecker.cassandra.util.Utils;

/**
 * Abstract statement value for all types of statements that are created using the QueryBuilder API from the datastax
 * driver
 */
public abstract class AbstractBuiltStatementValue extends AbstractStatementValue<VarLocalOrImmutableValue, AbstractBuiltStatementValue>
		implements
			RefValue {

	protected AbstractBuiltStatementValue(final ImmutableList<AbstractBind<VarLocalOrImmutableValue>> binds) {
		this(binds, NullValue.create(), NullValue.create());
	}

	protected AbstractBuiltStatementValue(final ImmutableList<AbstractBind<VarLocalOrImmutableValue>> binds,
			final ImmutableValue consistencyLevel, final ImmutableValue serialConsistencyLevel) {
		super(binds, consistencyLevel, serialConsistencyLevel);
	}

	public AbstractBuiltStatementValue createNew(final ImmutableList<AbstractBind<VarLocalOrImmutableValue>> binds,
			final ImmutableValue consistencyLevel, final ImmutableValue serialConsistencyLevel) {
		return createNewInternal(binds, consistencyLevel, serialConsistencyLevel);
	}

	@Override
	public <R> R apply(RefValueVisitor<R> visitor) {
		return visitor.visitAbstractBuiltStatementValue(this);
	}

	protected String getNewBindName() {
		return getNewBindName(0);
	}

	protected String getNewBindName(final int offset) {
		return Utils.getRandomLowercaseString10(binds.size() + offset);
	}

	@Override
	public Set<VarLocalOrImmutableValue> getUsedVarLocalOrImmutableValues() {
		final Set<VarLocalOrImmutableValue> ret = new HashSet<>();
		for (final AbstractBind<VarLocalOrImmutableValue> bind : binds) {
			ret.add(bind.value);
		}
		return ret;
	}

	@Override
	public AbstractBuiltStatementValue transformVarLocalOrImmutableValues(
			Function<VarLocalOrImmutableValue, VarLocalOrImmutableValue> transformer) {
		final ImmutableList.Builder<AbstractBind<VarLocalOrImmutableValue>> bindsBuilder = ImmutableList.builder();
		for (final AbstractBind<VarLocalOrImmutableValue> bind : binds) {
			bindsBuilder.add(bind.createNew(transformer.apply(bind.value)));
		}
		return createNewInternal(bindsBuilder.build(), consistencyLevel, serialConsistencyLevel);
	}

	public abstract String getStatement();

	@Override
	public int hashCode() {
		int result = super.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}
}
