package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.stmts;

import java.util.Objects;

import com.google.common.collect.ImmutableList;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractBind;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractBind.SystemBind;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;

public class BuiltDeleteStatementValue extends AbstractBuiltStatementValue {

	public final ImmutableList<String> columns;
	public final String table;
	public final ImmutableList<String> clauses;
	public final ImmutableList<String> conditions;
	public final boolean ifExists;

	private BuiltDeleteStatementValue(final ImmutableList<String> columns) {
		super(ImmutableList.of());
		this.columns = Objects.requireNonNull(columns);
		this.table = "";
		this.clauses = ImmutableList.of();
		this.conditions = ImmutableList.of();
		this.ifExists = false;
	}

	public static BuiltDeleteStatementValue create(final StringValue[] columns) {
		if (columns == null) {
			return new BuiltDeleteStatementValue(ImmutableList.of());
		} else {
			final ImmutableList.Builder<String> colBuilder = ImmutableList.builder();
			for (int i = 0; i < columns.length; i++) {
				colBuilder.add(columns[i].string);
			}
			return new BuiltDeleteStatementValue(colBuilder.build());
		}
	}

	private BuiltDeleteStatementValue(final ImmutableList<AbstractBind<VarLocalOrImmutableValue>> binds,
			final ImmutableValue consistencyLevel, final ImmutableValue serialConsistencyLevel, final ImmutableList<String> columns,
			final String table, final ImmutableList<String> clauses, final ImmutableList<String> conditions, final boolean ifExists) {
		super(binds, consistencyLevel, serialConsistencyLevel);
		this.columns = Objects.requireNonNull(columns);
		this.table = Objects.requireNonNull(table);
		this.clauses = Objects.requireNonNull(clauses);
		this.conditions = Objects.requireNonNull(conditions);
		this.ifExists = ifExists;
	}

	public BuiltDeleteStatementValue from(final StringValue keyspace, final StringValue table) {
		return new BuiltDeleteStatementValue(
				binds,
				consistencyLevel,
				serialConsistencyLevel,
				columns,
				keyspace == null ? table.string : keyspace.string + "." + table.string,
				clauses,
				conditions,
				ifExists);
	}

	public BuiltDeleteStatementValue addColumn(final StringValue column) {
		return new BuiltDeleteStatementValue(
				binds,
				consistencyLevel,
				serialConsistencyLevel,
				ImmutableList.<String> builder().addAll(columns).add(column.string).build(),
				table,
				clauses,
				conditions,
				ifExists);
	}

	public BuiltDeleteStatementValue addCollectionElementColumn(final StringValue column, final VarLocalOrImmutableValue value) {
		final String bindMarker = getNewBindName();
		return new BuiltDeleteStatementValue(
				ImmutableList.<AbstractBind<VarLocalOrImmutableValue>> builder().addAll(binds).add(SystemBind.create(bindMarker, value))
						.build(),
				consistencyLevel,
				serialConsistencyLevel,
				ImmutableList.<String> builder().addAll(columns).add(String.format(column.string + "[%s]", ":" + bindMarker)).build(),
				table,
				clauses,
				conditions,
				ifExists);
	}

	public BuiltDeleteStatementValue addWhere(final BuiltStatementClauseValue clause) {
		final String bindMarker = getNewBindName();
		return new BuiltDeleteStatementValue(
				ImmutableList.<AbstractBind<VarLocalOrImmutableValue>> builder().addAll(binds)
						.add(SystemBind.create(bindMarker, clause.value))
						.build(),
				consistencyLevel,
				serialConsistencyLevel,
				columns,
				table,
				ImmutableList.<String> builder().addAll(clauses).add(clause.getClauseWithNamedBind(":" + bindMarker)).build(),
				conditions,
				ifExists);
	}

	public BuiltDeleteStatementValue addCondition(final BuiltStatementClauseValue clause) {
		final String bindMarker = getNewBindName();
		return new BuiltDeleteStatementValue(
				ImmutableList.<AbstractBind<VarLocalOrImmutableValue>> builder().addAll(binds)
						.add(SystemBind.create(bindMarker, clause.value)).build(),
				consistencyLevel,
				serialConsistencyLevel,
				columns,
				table,
				clauses,
				ImmutableList.<String> builder().addAll(conditions).add(clause.getClauseWithNamedBind(":" + bindMarker)).build(),
				ifExists);
	}

	public BuiltDeleteStatementValue setIfExists(final boolean ifExists) {
		return new BuiltDeleteStatementValue(binds, consistencyLevel, serialConsistencyLevel, columns, table, clauses, conditions,
				ifExists);
	}

	@Override
	protected AbstractBuiltStatementValue createNewInternal(final ImmutableList<AbstractBind<VarLocalOrImmutableValue>> binds,
			final ImmutableValue consistencyLevel, final ImmutableValue serialConsistencyLevel) {
		return new BuiltDeleteStatementValue(binds, consistencyLevel, serialConsistencyLevel, columns, table, clauses, conditions,
				ifExists);
	}

	@Override
	public String getStatement() {
		final StringBuilder sb = new StringBuilder("DELETE");
		if (!columns.isEmpty()) {
			sb.append(" ");
			sb.append(String.join(", ", columns));
		}
		sb.append(" FROM ").append(table);
		if (!clauses.isEmpty()) {
			sb.append(" WHERE ");
			sb.append(String.join(" AND ", clauses));
		}
		if (!conditions.isEmpty()) {
			sb.append(" IF ");
			sb.append(String.join(" AND ", conditions));
		}
		if (ifExists) {
			sb.append(" IF EXISTS");
		}
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((clauses == null) ? 0 : clauses.hashCode());
		result = prime * result + ((columns == null) ? 0 : columns.hashCode());
		result = prime * result + ((conditions == null) ? 0 : conditions.hashCode());
		result = prime * result + (ifExists ? 1231 : 1237);
		result = prime * result + ((table == null) ? 0 : table.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BuiltDeleteStatementValue other = (BuiltDeleteStatementValue) obj;
		if (clauses == null) {
			if (other.clauses != null)
				return false;
		} else if (!clauses.equals(other.clauses))
			return false;
		if (columns == null) {
			if (other.columns != null)
				return false;
		} else if (!columns.equals(other.columns))
			return false;
		if (conditions == null) {
			if (other.conditions != null)
				return false;
		} else if (!conditions.equals(other.conditions))
			return false;
		if (ifExists != other.ifExists)
			return false;
		if (table == null) {
			if (other.table != null)
				return false;
		} else if (!table.equals(other.table))
			return false;
		return true;
	}
}
