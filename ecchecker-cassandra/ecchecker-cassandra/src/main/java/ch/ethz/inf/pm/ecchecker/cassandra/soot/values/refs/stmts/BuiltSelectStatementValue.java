package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.stmts;

import java.util.Objects;

import com.google.common.collect.ImmutableList;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractBind;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractBind.SystemBind;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;

public class BuiltSelectStatementValue extends AbstractBuiltStatementValue {

	public final ImmutableList<String> columns;
	public final String table;
	public final ImmutableList<String> clauses;
	public final boolean isDistinct;

	private BuiltSelectStatementValue(final ImmutableList<String> columns) {
		super(ImmutableList.of());
		this.columns = Objects.requireNonNull(columns);
		this.table = "";
		this.clauses = ImmutableList.of();
		this.isDistinct = false;
	}

	public static BuiltSelectStatementValue create(final StringValue[] columns) {
		if (columns == null) {
			return new BuiltSelectStatementValue(ImmutableList.of());
		} else {
			final ImmutableList.Builder<String> colBuilder = ImmutableList.builder();
			for (int i = 0; i < columns.length; i++) {
				colBuilder.add(columns[i].string);
			}
			return new BuiltSelectStatementValue(colBuilder.build());
		}
	}

	private BuiltSelectStatementValue(final ImmutableList<AbstractBind<VarLocalOrImmutableValue>> binds,
			final ImmutableValue consistencyLevel, final ImmutableValue serialConsistencyLevel, final ImmutableList<String> columns,
			final String table, final ImmutableList<String> clauses, final boolean isDistinct) {
		super(binds, consistencyLevel, serialConsistencyLevel);
		this.columns = Objects.requireNonNull(columns);
		this.table = Objects.requireNonNull(table);
		this.clauses = Objects.requireNonNull(clauses);
		this.isDistinct = isDistinct;
	}

	public BuiltSelectStatementValue from(final StringValue keyspace, final StringValue table) {
		return new BuiltSelectStatementValue(
				binds,
				consistencyLevel,
				serialConsistencyLevel,
				columns,
				keyspace == null ? table.string : keyspace.string + "." + table.string,
				clauses,
				isDistinct);
	}

	public BuiltSelectStatementValue addColumn(final StringValue column) {
		return new BuiltSelectStatementValue(
				binds,
				consistencyLevel,
				serialConsistencyLevel,
				ImmutableList.<String> builder().addAll(columns).add(column.string).build(),
				table,
				clauses,
				isDistinct);
	}

	public BuiltSelectStatementValue addAs(final StringValue as) {
		final ImmutableList.Builder<String> colBuilder = ImmutableList.builder();
		for (int i = 0; i < columns.size() - 1; i++) {
			colBuilder.add(columns.get(i));
		}
		colBuilder.add(columns.get(columns.size() - 1) + " AS " + as.string);
		return new BuiltSelectStatementValue(
				binds,
				consistencyLevel,
				serialConsistencyLevel,
				colBuilder.build(),
				table,
				clauses,
				isDistinct);
	}

	public BuiltSelectStatementValue addWhere(final BuiltStatementClauseValue clause) {
		final String bindMarker = getNewBindName();
		return new BuiltSelectStatementValue(
				ImmutableList.<AbstractBind<VarLocalOrImmutableValue>> builder().addAll(binds)
						.add(SystemBind.create(bindMarker, clause.value)).build(),
				consistencyLevel,
				serialConsistencyLevel,
				columns,
				table,
				ImmutableList.<String> builder().addAll(clauses).add(clause.getClauseWithNamedBind(":" + bindMarker)).build(),
				isDistinct);
	}

	public BuiltSelectStatementValue setDistinct(final boolean isDistinct) {
		return new BuiltSelectStatementValue(
				binds,
				consistencyLevel,
				serialConsistencyLevel,
				columns,
				table,
				clauses,
				isDistinct);
	}

	@Override
	public String getStatement() {
		final StringBuilder sb = new StringBuilder("SELECT ");
		if (isDistinct) {
			sb.append("DISTINCT ");
		}
		sb.append(String.join(", ", columns));
		sb.append(" FROM ").append(table);
		if (!clauses.isEmpty()) {
			sb.append(" WHERE ");
			sb.append(String.join(" AND ", clauses));
		}
		return sb.toString();
	}

	@Override
	protected AbstractBuiltStatementValue createNewInternal(final ImmutableList<AbstractBind<VarLocalOrImmutableValue>> binds,
			ImmutableValue consistencyLevel, ImmutableValue serialConsistencyLevel) {
		return new BuiltSelectStatementValue(binds, consistencyLevel, serialConsistencyLevel, columns, table, clauses, isDistinct);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((clauses == null) ? 0 : clauses.hashCode());
		result = prime * result + ((columns == null) ? 0 : columns.hashCode());
		result = prime * result + (isDistinct ? 1231 : 1237);
		result = prime * result + ((table == null) ? 0 : table.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BuiltSelectStatementValue other = (BuiltSelectStatementValue) obj;
		if (clauses == null) {
			if (other.clauses != null)
				return false;
		} else if (!clauses.equals(other.clauses))
			return false;
		if (columns == null) {
			if (other.columns != null)
				return false;
		} else if (!columns.equals(other.columns))
			return false;
		if (isDistinct != other.isDistinct)
			return false;
		if (table == null) {
			if (other.table != null)
				return false;
		} else if (!table.equals(other.table))
			return false;
		return true;
	}
}
