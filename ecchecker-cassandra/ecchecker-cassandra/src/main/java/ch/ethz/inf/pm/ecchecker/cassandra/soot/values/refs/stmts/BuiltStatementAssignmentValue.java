package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.stmts;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.RefValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.RefValueVisitor;

public class BuiltStatementAssignmentValue implements RefValue {

	public final String assignment;
	public final ImmutableList<VarLocalOrImmutableValue> values;

	private BuiltStatementAssignmentValue(final String assignment, final ImmutableList<VarLocalOrImmutableValue> values) {
		this.assignment = Objects.requireNonNull(assignment);
		this.values = Objects.requireNonNull(values);
	}

	/**
	 * @param clause
	 *            Final clause without bind markers
	 * @param value
	 * @return
	 */
	public static BuiltStatementAssignmentValue create(final String assignment) {
		return new BuiltStatementAssignmentValue(assignment, ImmutableList.of());
	}

	/**
	 * @param clause
	 *            Final clause containing %s for the bind marker
	 * @param value
	 * @return
	 */
	public static BuiltStatementAssignmentValue create(final String assignment, final VarLocalOrImmutableValue value) {
		return new BuiltStatementAssignmentValue(assignment, ImmutableList.of(value));
	}

	/**
	 * @param clause
	 *            Final clause containing %s for the bind marker
	 * @param value
	 * @return
	 */
	public static BuiltStatementAssignmentValue create(final String assignment, final VarLocalOrImmutableValue value,
			final VarLocalOrImmutableValue value2) {
		return new BuiltStatementAssignmentValue(assignment, ImmutableList.of(value, value2));
	}

	public static BuiltStatementAssignmentValue create(final String assignment, final List<VarLocalOrImmutableValue> values) {
		final ImmutableList.Builder<VarLocalOrImmutableValue> valuesBuilder = ImmutableList.builder();
		for (final VarLocalOrImmutableValue value : values) {
			valuesBuilder.add(value);
		}
		return new BuiltStatementAssignmentValue(assignment, valuesBuilder.build());
	}

	@Override
	public <R> R apply(RefValueVisitor<R> visitor) {
		return visitor.visitBuiltStatmentAssignmentValue(this);
	}

	public String getAssignmentWithNamedBinds(final String[] bindMarkers) {
		if (values.size() != bindMarkers.length) {
			throw new IllegalArgumentException();
		}
		return String.format(assignment, (Object[]) bindMarkers);
	}

	@Override
	public Set<VarLocalOrImmutableValue> getUsedVarLocalOrImmutableValues() {
		return ImmutableSet.copyOf(values);
	}

	@Override
	public BuiltStatementAssignmentValue transformVarLocalOrImmutableValues(
			Function<VarLocalOrImmutableValue, VarLocalOrImmutableValue> transformer) {
		final ImmutableList.Builder<VarLocalOrImmutableValue> valuesBuilder = ImmutableList.builder();
		for (final VarLocalOrImmutableValue value : values) {
			valuesBuilder.add(transformer.apply(value));
		}
		return new BuiltStatementAssignmentValue(assignment, valuesBuilder.build());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((assignment == null) ? 0 : assignment.hashCode());
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BuiltStatementAssignmentValue other = (BuiltStatementAssignmentValue) obj;
		if (assignment == null) {
			if (other.assignment != null)
				return false;
		} else if (!assignment.equals(other.assignment))
			return false;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BuiltStatementAssignmentValue [assignment=" + assignment + ", values=" + values + "]";
	}
}
