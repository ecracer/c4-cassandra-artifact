package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.stmts;

import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.refs.RefValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors.RefValueVisitor;

public class BuiltStatementClauseValue implements RefValue {

	public final String clause;
	public final VarLocalOrImmutableValue value;

	private BuiltStatementClauseValue(final String clause, final VarLocalOrImmutableValue value) {
		this.clause = Objects.requireNonNull(clause);
		this.value = Objects.requireNonNull(value);
	}

	/**
	 * @param clause
	 *            Final clause containing %s for the bind marker
	 * @param value
	 * @return
	 */
	public static BuiltStatementClauseValue create(final String clause, final VarLocalOrImmutableValue value) {
		return new BuiltStatementClauseValue(clause, value);
	}

	@Override
	public <R> R apply(RefValueVisitor<R> visitor) {
		return visitor.visitBuiltStatmentClauseValue(this);
	}

	public String getClauseWithNamedBind(final String bindMarker) {
		return String.format(clause, bindMarker);
	}

	@Override
	public Set<VarLocalOrImmutableValue> getUsedVarLocalOrImmutableValues() {
		return ImmutableSet.of(value);
	}

	@Override
	public BuiltStatementClauseValue transformVarLocalOrImmutableValues(
			Function<VarLocalOrImmutableValue, VarLocalOrImmutableValue> transformer) {
		return new BuiltStatementClauseValue(clause, transformer.apply(value));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clause == null) ? 0 : clause.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BuiltStatementClauseValue other = (BuiltStatementClauseValue) obj;
		if (clause == null) {
			if (other.clause != null)
				return false;
		} else if (!clause.equals(other.clause))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BuiltStatementClauseValue [clause=" + clause + ", value=" + value + "]";
	}
}
