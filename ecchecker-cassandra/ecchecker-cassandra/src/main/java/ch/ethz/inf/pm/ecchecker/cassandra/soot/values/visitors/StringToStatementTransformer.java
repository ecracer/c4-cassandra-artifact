package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractBind;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractBind.SystemBind;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractBind.UserBind;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.StringBuilderValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.IntValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValueContainerValue;
import ch.ethz.inf.pm.ecchecker.cassandra.util.Utils;

public class StringToStatementTransformer {

	private final ConcreteValue statement;
	private final ImmutableValue[] userBinds;

	public StringToStatementTransformer(final ConcreteValue statement, final ImmutableValue[] binds) {
		this.statement = Objects.requireNonNull(statement);
		this.userBinds = binds;
	}

	public Set<StatementValue> transform() {
		final Set<StatementValue> statements = new HashSet<>();
		if (statement instanceof ImmutableChoiceValue) {
			for (ImmutableValue choice : ((ImmutableChoiceValue) statement).choices) {
				for (final ConcreteValue stmt : STRING_EXTRACTOR.visit(choice)) {
					statements.add(transformStatement(stmt));
				}
			}
		} else {
			for (final ConcreteValue stmt : STRING_EXTRACTOR.visit(statement)) {
				statements.add(transformStatement(stmt));
			}
		}
		return statements;
	}

	private final static AbstractConcreteValueVisitor<Set<ConcreteValue>> STRING_EXTRACTOR = new AbstractConcreteValueVisitor<Set<ConcreteValue>>() {

		@Override
		public Set<ConcreteValue> visitDefault(ConcreteValue value) {
			return Collections.singleton(value);
		}

		@Override
		public Set<ConcreteValue> visitImmutableValue(ImmutableValue immutableValue) {
			return new HashSet<>(STRING_EXTRACTOR_HELPER.visit(immutableValue));
		}

		@Override
		public Set<ConcreteValue> visitStringBuilderValue(StringBuilderValue stringBuilderValue) {
			List<List<ImmutableValue>> newStringBuilderValues = new ArrayList<>();
			newStringBuilderValues.add(new ArrayList<>());

			for (final ImmutableValue val : stringBuilderValue.values) {
				final Set<ImmutableValue> transformed = STRING_EXTRACTOR_HELPER.visit(val);
				List<List<ImmutableValue>> oldNewStringBuilderValues = newStringBuilderValues;
				newStringBuilderValues = new ArrayList<>();
				for (final ImmutableValue t : transformed) {
					for (List<ImmutableValue> o : oldNewStringBuilderValues) {
						final List<ImmutableValue> newStringBuilderValue = new ArrayList<>(o);
						newStringBuilderValue.add(t);
						newStringBuilderValues.add(newStringBuilderValue);
					}
				}
			}

			final Set<ConcreteValue> ret = new HashSet<>();
			for (final List<ImmutableValue> newStringBuilderValue : newStringBuilderValues) {
				ret.add(StringBuilderValue.create(newStringBuilderValue));
			}
			return ret;
		}
	};

	private final static AbstractImmutableValueVisitor<Set<ImmutableValue>> STRING_EXTRACTOR_HELPER = new AbstractImmutableValueVisitor<Set<ImmutableValue>>() {

		@Override
		public Set<ImmutableValue> visitDefault(ImmutableValue value) {
			return Collections.singleton(value);
		}

		@Override
		public Set<ImmutableValue> visitStringValueContainer(StringValueContainerValue stringValueContainerValue) {
			final Set<ImmutableValue> ret = new HashSet<>();
			final Set<ConcreteValue> transformed = STRING_EXTRACTOR.visit(stringValueContainerValue.value);
			for (final ConcreteValue t : transformed) {
				ret.add(StringValueContainerValue.create(t));
			}
			return ret;
		}

		@Override
		public Set<ImmutableValue> visitImmutableChoiceValue(ImmutableChoiceValue immutableChoiceValue) {
			final Set<ImmutableValue> ret = new HashSet<>();
			for (final ImmutableValue choice : immutableChoiceValue.choices) {
				ret.addAll(visit(choice));
			}
			return ret;
		};
	};

	private StatementValue transformStatement(final ConcreteValue statement) {
		if (!(statement instanceof StringValue) && !(statement instanceof StringValueContainerValue)) {
			return StatementValue.createUnknown();
		}
		final List<AbstractBind<ConcreteValue>> binds = new ArrayList<>();
		final List<Object> queryElements = new ArrayList<>();
		new AbstractConcreteValueVisitor<Void>() {

			@Override
			public Void visitDefault(ConcreteValue value) {
				final String bindName = Utils.getRandomLowercaseString10(binds.size());
				final SystemBind<ConcreteValue> bind = SystemBind.create(bindName, value);
				binds.add(bind);
				queryElements.add(bind);
				return null;
			}

			@Override
			public Void visitImmutableValue(ImmutableValue immutableValue) {
				if (immutableValue instanceof IntValue) {
					queryElements.add(String.valueOf(((IntValue) immutableValue).number));
					return null;
				} else if (immutableValue instanceof StringValue) {
					queryElements.add(((StringValue) immutableValue).string);
					return null;
				} else if (immutableValue instanceof StringValueContainerValue) {
					return visit(((StringValueContainerValue) immutableValue).value);
				}
				return visitDefault(immutableValue);
			}

			@Override
			public Void visitStringBuilderValue(StringBuilderValue stringBuilderValue) {
				visit(stringBuilderValue.values);
				return null;
			}
		}.visit(statement);

		if (!(queryElements.get(0) instanceof String)) {
			return StatementValue.createUnknown();
		}

		final String[] query = new String[queryElements.size()];
		query[0] = (String) queryElements.get(0);

		for (int i = 1; i < query.length; i++) {
			if (queryElements.get(i) instanceof String) {
				query[i] = (String) queryElements.get(i);
			} else {
				final SystemBind<?> bind = (SystemBind<?>) queryElements.get(i);
				query[i] = ":" + bind.name;
				// resolve ':bindname' generated when string concatenation was used in the project
				if (i + 1 < query.length) {
					if (queryElements.get(i + 1) instanceof SystemBind<?>) {
						continue;
					} else {
						final String nextElement = (String) queryElements.get(i + 1);
						if (query[i - 1].endsWith("'") && nextElement.startsWith("'")) {
							query[i - 1] = query[i - 1].substring(0, query[i - 1].length() - 1);
							query[i + 1] = nextElement.substring(1);
						} else {
							query[i + 1] = nextElement;
						}
						i++;
					}
				}
			}
		}

		if (userBinds != null) {
			for (int i = 0; i < userBinds.length; i++) {
				binds.add(UserBind.<ConcreteValue> create(IntValue.create(i), userBinds[i]));
			}
		}
		return StatementValue.create(String.join("", query), binds);
	}
}
