package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.StringBuilderValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.UnknownMutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ConsistencyLevelValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.IntValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValueContainerValue;

public class ToStringTransformer {

	public static ImmutableValue exec(final ConcreteValue val) {
		return INSTANCE.visit(val);
	}

	private final static AbstractConcreteValueVisitor<ImmutableValue> INSTANCE = new AbstractConcreteValueVisitor<ImmutableValue>() {

		@Override
		public ImmutableValue visitDefault(ConcreteValue value) {
			return StringValueContainerValue.create(UnknownMutableValue.create());
		}

		@Override
		public ImmutableValue visitImmutableValue(ImmutableValue immutableValue) {
			return IMMU_INSTANCE.visit(immutableValue);
		}

		@Override
		public ImmutableValue visitStringBuilderValue(StringBuilderValue stringBuilderValue) {
			return StringValueContainerValue.create(stringBuilderValue);
		}

	};

	private final static AbstractImmutableValueVisitor<ImmutableValue> IMMU_INSTANCE = new AbstractImmutableValueVisitor<ImmutableValue>() {

		@Override
		public ImmutableValue visitDefault(final ImmutableValue value) {
			return StringValueContainerValue.create(value);
		}

		@Override
		public ImmutableValue visitStringValue(StringValue stringValue) {
			return stringValue;
		}

		@Override
		public ImmutableValue visitNullValue(NullValue nullValue) {
			// according to https://docs.oracle.com/javase/7/docs/api/java/lang/String.html#valueOf(java.lang.Object)
			return StringValue.create("null");
		}

		@Override
		public ImmutableValue visitIntValue(IntValue intValue) {
			return StringValue.create(String.valueOf(intValue.number));
		}

		@Override
		public ImmutableValue visitConsistencyLevelValue(ConsistencyLevelValue consistencyLevel) {
			return StringValue.create(consistencyLevel.level.toString());
		};

		@Override
		public ImmutableValue visitStringValueContainer(StringValueContainerValue stringValueContainer) {
			return stringValueContainer;
		}
	};
}
