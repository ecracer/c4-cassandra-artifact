package ch.ethz.inf.pm.ecchecker.cassandra.soot.values.visitors;

public interface ValueVisitor<R> extends ConcreteValueVisitor<R>, RefValueVisitor<R> {

}
