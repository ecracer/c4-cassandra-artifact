package ch.ethz.inf.pm.ecchecker.cassandra.soot.vars;

import java.util.Objects;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;

public class VarLocal implements VarLocalOrImmutableValue {

	public final CallStack callstack;
	public final String variableName;
	public final String typeName;
	public final VarType varType;

	private VarLocal(final CallStack callstack, final String variableName, final String typeName, final VarType varType) {
		this.callstack = Objects.requireNonNull(callstack);
		this.variableName = Objects.requireNonNull(variableName);
		this.typeName = Objects.requireNonNull(typeName);
		this.varType = Objects.requireNonNull(varType);
	}

	public static VarLocal create(final CallStack callstack, final String variableName, final String typeName, final VarType varType) {
		return new VarLocal(callstack, variableName, typeName, varType);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((callstack == null) ? 0 : callstack.hashCode());
		result = prime * result + ((typeName == null) ? 0 : typeName.hashCode());
		result = prime * result + ((varType == null) ? 0 : varType.hashCode());
		result = prime * result + ((variableName == null) ? 0 : variableName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VarLocal other = (VarLocal) obj;
		if (callstack == null) {
			if (other.callstack != null)
				return false;
		} else if (!callstack.equals(other.callstack))
			return false;
		if (typeName == null) {
			if (other.typeName != null)
				return false;
		} else if (!typeName.equals(other.typeName))
			return false;
		if (varType != other.varType)
			return false;
		if (variableName == null) {
			if (other.variableName != null)
				return false;
		} else if (!variableName.equals(other.variableName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return callstack + " " + variableName;
	}
}
