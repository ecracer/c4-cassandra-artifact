package ch.ethz.inf.pm.ecchecker.cassandra.soot.vars;

import java.util.Objects;

/**
 * Abstraction of the types
 * <ul>
 * <li>Object: general java object</li>
 * <li>Immutable: immutable java object or primitive</li>
 * <li>Array: array</li>
 * <li>Included: java object that has a type from a class that is analyzed</li>
 * <li>Excluded: some java object we do not know</li>
 * </ul>
 */
public enum VarType {

	OBJECT, IMMUTABLE, ARRAY, INCLUDED, EXCLUDED;

	public static boolean isAssignableTo(final VarType targetType, final VarType sourceType) {
		Objects.requireNonNull(targetType);
		Objects.requireNonNull(sourceType);
		if (targetType == VarType.EXCLUDED || sourceType == VarType.EXCLUDED) {
			return false;
		} else if (targetType.equals(sourceType)) {
			return true;
		} else if (targetType == VarType.OBJECT) {
			return true;
		}
		return false;
	}
}
