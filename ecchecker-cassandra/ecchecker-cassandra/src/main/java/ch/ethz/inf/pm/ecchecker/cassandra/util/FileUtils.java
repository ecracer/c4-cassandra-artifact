package ch.ethz.inf.pm.ecchecker.cassandra.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FileUtils {

	private static final Logger LOG = LogManager.getLogger(FileUtils.class);

	public static void createEmptyDirectory(final File dir) {
		try {
			if (dir.exists()) {
				Files.walkFileTree(dir.toPath(), new SimpleFileVisitor<Path>() {
					@Override
					public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
						Files.delete(file);
						return FileVisitResult.CONTINUE;
					}

					@Override
					public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
						Files.delete(dir);
						return FileVisitResult.CONTINUE;
					}
				});
			}
			dir.mkdirs();
		} catch (IOException e) {
			LOG.error("Error deleting directory", dir);
		}
	}

}
