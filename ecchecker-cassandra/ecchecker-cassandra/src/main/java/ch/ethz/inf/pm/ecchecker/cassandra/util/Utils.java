package ch.ethz.inf.pm.ecchecker.cassandra.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.SetMultimap;

public class Utils {

	private final static Random RANDOM = new Random();
	private final static List<String> GENERATED_STRINGS_10 = new ArrayList<>();

	public static String getRandomLowercaseString10(final int index) {
		if (index < 0) {
			throw new IllegalArgumentException();
		}
		if (index < GENERATED_STRINGS_10.size()) {
			return GENERATED_STRINGS_10.get(index);
		}
		String newVal;
		do {
			final char[] value = new char[10];
			for (int i = 0; i < 10; i++) {
				value[i] = (char) (97 + RANDOM.nextInt(26));
			}
			newVal = new String(value);
		} while (GENERATED_STRINGS_10.contains(newVal));
		GENERATED_STRINGS_10.add(newVal);
		return newVal;
	}

	public static <T> void consumeAllPermutations(final Collection<? extends T> c, final Consumer<List<T>> consumer) {
		if (c.size() == 1) {
			consumer.accept(Collections.singletonList(c.iterator().next()));
		} else {
			final List<T> cAsList = new ArrayList<>(c);
			permutationHelper(cAsList, new ArrayList<>(c.size()), consumer);
		}
	}

	private static <T> void permutationHelper(final List<T> c, final List<Integer> indexList, final Consumer<List<T>> consumer) {
		if (indexList.size() == c.size()) {
			final List<T> finalList = new ArrayList<>(c.size());
			for (final Integer idx : indexList) {
				finalList.add(c.get(idx));
			}
			consumer.accept(finalList);
		} else {
			final int idx = indexList.size();
			for (int i = 0; i < c.size(); i++) {
				if (!indexList.contains(i)) {
					indexList.add(i);
					permutationHelper(c, indexList, consumer);
					indexList.remove(idx);
				}
			}
		}
	}

	public static <K1, K2, V> ImmutableMap<K1, ImmutableSetMultimap<K2, V>> copyOfMapOfMaps(
			final Map<? extends K1, ? extends SetMultimap<? extends K2, ? extends V>> map) {
		final ImmutableMap.Builder<K1, ImmutableSetMultimap<K2, V>> resultBuilder = ImmutableMap.builder();
		for (final Entry<? extends K1, ? extends SetMultimap<? extends K2, ? extends V>> entry : map.entrySet()) {
			resultBuilder.put(entry.getKey(), ImmutableSetMultimap.copyOf(entry.getValue()));
		}
		return resultBuilder.build();
	}

	public static <T> ImmutableSet<T> setWithElement(final ImmutableSet<T> set, final T element) {
		if (set.contains(element)) {
			return set;
		} else {
			return ImmutableSet.<T> builder().addAll(set).add(element).build();
		}
	}

	public static <K> ImmutableSet<K> setWithValue(final ImmutableSet<K> set, final K newValue) {
		if (set.contains(newValue)) {
			return set;
		} else {
			return ImmutableSet.<K> builder().addAll(set).add(newValue).build();
		}
	}

	public static <K> ImmutableSet<K> setWithoutValue(final ImmutableSet<K> set, final K oldValue) {
		if (!set.contains(oldValue)) {
			return set;
		} else {
			final ImmutableSet.Builder<K> builder = ImmutableSet.builder();
			for (final K value : set) {
				if (!oldValue.equals(value)) {
					builder.add(value);
				}
			}
			return builder.build();
		}
	}

	public static <K, V> ImmutableMap<K, V> mapWithValue(final ImmutableMap<K, V> map, final K newKey, final V newValue) {
		final V oldVal = map.get(newKey);
		if ((oldVal == null && newValue == null) || (newValue != null && newValue.equals(oldVal))) {
			return map;
		}
		if (oldVal != null) {
			final ImmutableMap.Builder<K, V> builder = ImmutableMap.builder();
			for (final Entry<K, V> entry : map.entrySet()) {
				if (newKey.equals(entry.getKey())) {
					builder.put(newKey, newValue);
				} else {
					builder.put(entry);
				}
			}
			return builder.build();
		} else {
			return ImmutableMap.<K, V> builder().putAll(map).put(newKey, newValue).build();
		}
	}

	public static <K, V> ImmutableSetMultimap<K, V> multimapWithValue(final ImmutableSetMultimap<K, V> multimap, final K newKey,
			final V newValue) {
		final Set<V> oldVals = multimap.get(newKey);
		if (oldVals.size() == 1 && newValue.equals(oldVals.iterator().next())) {
			return multimap;
		}
		if (!oldVals.isEmpty()) {
			final ImmutableSetMultimap.Builder<K, V> builder = ImmutableSetMultimap.builder();
			for (final K key : multimap.keySet()) {
				if (newKey.equals(key)) {
					builder.put(newKey, newValue);
				} else {
					builder.putAll(key, multimap.get(key));
				}
			}
			return builder.build();
		} else {
			return ImmutableSetMultimap.<K, V> builder().putAll(multimap).put(newKey, newValue).build();
		}
	}

	public static <K, V> ImmutableSetMultimap<K, V> multimapWithAdditionalValue(final ImmutableSetMultimap<K, V> multimap, final K newKey,
			final V newValue) {
		if (multimap.containsEntry(newKey, newValue)) {
			return multimap;
		}
		return ImmutableSetMultimap.<K, V> builder().putAll(multimap).put(newKey, newValue).build();
	}

	public static <K, V> ImmutableSetMultimap<K, V> multimapWithValues(final ImmutableSetMultimap<K, V> multimap, final K newKey,
			final Set<? extends V> newValues) {
		final Set<V> oldVals = multimap.get(newKey);
		if (newValues.equals(oldVals)) {
			return multimap;
		}
		if (!oldVals.isEmpty()) {
			final ImmutableSetMultimap.Builder<K, V> builder = ImmutableSetMultimap.builder();
			for (final K key : multimap.keySet()) {
				if (newKey.equals(key)) {
					if (!newValues.isEmpty()) {
						builder.putAll(newKey, newValues);
					}
				} else {
					builder.putAll(key, multimap.get(key));
				}
			}
			return builder.build();
		} else {
			return ImmutableSetMultimap.<K, V> builder().putAll(multimap).putAll(newKey, newValues).build();
		}
	}

	public static <K, V> ImmutableSetMultimap<K, V> multimapWithoutKey(final ImmutableSetMultimap<K, V> multimap, final K oldKey) {
		if (!multimap.containsKey(oldKey)) {
			return multimap;
		} else {
			final ImmutableSetMultimap.Builder<K, V> builder = ImmutableSetMultimap.builder();
			for (final K key : multimap.keySet()) {
				if (!oldKey.equals(key)) {
					builder.putAll(key, multimap.get(key));
				}
			}
			return builder.build();
		}
	}

	public static <K> ImmutableSet<K> mergeSet(final ImmutableSet<K> set1, final ImmutableSet<K> set2) {
		if (set1 == set2 || set2.isEmpty()) {
			return set1;
		} else if (set1.isEmpty()) {
			return set2;
		}
		return ImmutableSet.<K> builder().addAll(set1).addAll(set2).build();
	}

	public static <K> ImmutableSet<K> intersectSet(final ImmutableSet<K> set1, final ImmutableSet<K> set2) {
		if (set1 == set2) {
			return set1;
		} else if (set1.isEmpty() || set2.isEmpty()) {
			return ImmutableSet.of();
		} else {
			final ImmutableSet.Builder<K> ret = ImmutableSet.builder();
			for (final K k : set1) {
				if (set2.contains(k)) {
					ret.add(k);
				}
			}
			return ret.build();
		}
	}

	public static <K, V> ImmutableSetMultimap<K, V> mergeMultimap(final ImmutableSetMultimap<K, V> multimap1,
			final ImmutableSetMultimap<K, V> multimap2) {
		if (multimap1 == multimap2 || multimap2.isEmpty()) {
			return multimap1;
		} else if (multimap1.isEmpty()) {
			return multimap2;
		}
		return ImmutableSetMultimap.<K, V> builder().putAll(multimap1).putAll(multimap2).build();
	}

	public static <K> ImmutableSet<K> getMissingKeys(final ImmutableSetMultimap<K, ?> multimap1,
			final ImmutableSetMultimap<K, ?> multimap2) {
		if (multimap1 == multimap2) {
			return ImmutableSet.of();
		} else if (multimap1.isEmpty()) {
			return multimap2.keySet();
		} else if (multimap2.isEmpty()) {
			return multimap1.keySet();
		}

		final ImmutableSet.Builder<K> resultBuilder = ImmutableSet.builder();
		for (final K k : multimap1.keySet()) {
			if (!multimap2.containsKey(k)) {
				resultBuilder.add(k);
			}
		}
		for (final K k : multimap2.keySet()) {
			if (!multimap1.containsKey(k)) {
				resultBuilder.add(k);
			}
		}
		return resultBuilder.build();
	}

	public static <K, V> ImmutableMap<K, V> mergeMap(final ImmutableMap<K, V> map1, final ImmutableMap<K, V> map2,
			final Function<K, V> mergeFunction) {
		if (map1 == map2 || map2.isEmpty()) {
			return map1;
		} else if (map1.isEmpty()) {
			return map2;
		}

		final ImmutableMap.Builder<K, V> retBuilder = ImmutableMap.builder();
		for (final Entry<K, V> entry : map1.entrySet()) {
			if (map2.containsKey(entry.getKey())) {
				final V v2 = map2.get(entry.getKey());
				if (v2.equals(entry.getValue())) {
					retBuilder.put(entry);
				} else {
					retBuilder.put(entry.getKey(), mergeFunction.apply(entry.getKey()));
				}
			} else {
				retBuilder.put(entry);
			}
		}
		for (final Entry<K, V> entry : map2.entrySet()) {
			if (!map1.containsKey(entry.getKey())) {
				retBuilder.put(entry);
			}
		}
		return retBuilder.build();
	}

	public static <K, V> ImmutableMap<K, V> intersectMap(final ImmutableMap<K, V> map1, final ImmutableMap<K, V> map2) {
		if (map1 == map2) {
			return map1;
		}

		final ImmutableMap.Builder<K, V> retBuilder = ImmutableMap.builder();
		for (final Entry<K, V> entry : map1.entrySet()) {
			if (map2.containsKey(entry.getKey())) {
				final V v2 = map2.get(entry.getKey());
				if (v2.equals(entry.getValue())) {
					retBuilder.put(entry);
				}
			}
		}
		return retBuilder.build();
	}
}
