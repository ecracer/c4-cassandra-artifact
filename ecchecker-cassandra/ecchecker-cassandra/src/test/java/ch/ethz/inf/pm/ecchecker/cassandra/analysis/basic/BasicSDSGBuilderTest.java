package ch.ethz.inf.pm.ecchecker.cassandra.analysis.basic;

import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.SchemaInformation;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.basic.AbstractEdge.ArbitrationEdge;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.basic.AbstractEdge.DependencyEdge;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.Method;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.ParsedSessionExecuteInvoke;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.ParsedSessionExecuteInvoke.ConsistencyLevel;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnSelection;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnUnknownValueUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraint.EQValConstraint;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraints;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ParsedStatement.ParsedQueryStatement;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ParsedStatement.ParsedUpdateStatement;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.DeletePart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.SelectAllPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.SelectColsPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.UpsertPart;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;

public class BasicSDSGBuilderTest {

	private final SchemaInformation schemaInfo = SchemaInformation.builder().addPrimaryKeyColumn("table1", "key")
			.addNonPrimaryKeyColumn("table1", "val1").build();
	private final Options options = new Options();

	private final TransactionDescriptor t1 = TransactionDescriptor
			.create(Method.create("className", "method1", Collections.emptyList(), "void", "method1()"), false);
	private final TransactionDescriptor t2 = TransactionDescriptor
			.create(Method.create("className", "method2", Collections.emptyList(), "void", "method2()"), false);

	private final Method testMethod = Method.create("className", "methodName", Collections.emptyList(), "void", "test()");

	private final ParsedSessionExecuteInvoke s1 = ParsedSessionExecuteInvoke.create(testMethod, 2, false,
			ParsedQueryStatement.create("SELECT * FROM table1",
					SelectAllPart.create("table1", Constraints.create())),
			ConsistencyLevel.ALL, ConsistencyLevel.ALL);

	private final ParsedSessionExecuteInvoke s2 = ParsedSessionExecuteInvoke.create(testMethod, 3, false,
			ParsedQueryStatement.create("SELECT key2 FROM table1",
					SelectColsPart.create("table1", ImmutableList.of(ColumnSelection.create("key2")), Constraints.create())),
			ConsistencyLevel.ALL, ConsistencyLevel.ALL);
	private final ParsedSessionExecuteInvoke s3 = ParsedSessionExecuteInvoke.create(testMethod, 4, false,
			ParsedQueryStatement.create("SELECT /*!DISPLAY * !*/ * FROM table1",
					SelectAllPart.create("table1", Constraints.create()).setAllDisplayColumns()),
			ConsistencyLevel.ALL, ConsistencyLevel.ALL);
	private final ParsedSessionExecuteInvoke s4 = ParsedSessionExecuteInvoke.create(testMethod, 5, false,
			ParsedQueryStatement.create("SELECT /*!DISPLAY val1, val2 !*/ * FROM table1",
					SelectAllPart.create("table1", Constraints.create()).setDisplayColumns(ImmutableSet.of("val1", "val2"))),
			ConsistencyLevel.ALL, ConsistencyLevel.ALL);
	private final ParsedSessionExecuteInvoke i1 = ParsedSessionExecuteInvoke.create(testMethod, 6, false,
			ParsedUpdateStatement.create("INSERT INTO table1 (key, val1) VALUES (?, ?)",
					UpsertPart.createInsert("table1",
							ImmutableList.of(ColumnUnknownValueUpdate.create("key"), ColumnUnknownValueUpdate.create("val1")), false)),
			ConsistencyLevel.ALL, ConsistencyLevel.ALL);
	private final ParsedSessionExecuteInvoke up1 = ParsedSessionExecuteInvoke.create(testMethod, 7, false,
			ParsedUpdateStatement.create("UPDATE table1 SET val2 = ? WHERE key = 'val'",
					UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("val2")),
							Collections.emptyList(), false, false).setCanInsert(false)),
			ConsistencyLevel.ALL, ConsistencyLevel.ALL);
	private final ParsedSessionExecuteInvoke d1 = ParsedSessionExecuteInvoke.create(testMethod, 8, false,
			ParsedUpdateStatement.create("DELETE FROM table1 WHERE key = 'val'",
					DeletePart.create("table1", Constraints.create(EQValConstraint.create("key", StringValue.create("val"))), false,
							false)),
			ConsistencyLevel.ALL, ConsistencyLevel.ALL);

	@Test
	public void testSelect() {
		final Map<TransactionDescriptor, Set<ParsedSessionExecuteInvoke>> transactionMap = new HashMap<>();
		transactionMap.put(t1, Collections.singleton(s1));
		transactionMap.put(t2, Collections.singleton(s1));
		final BasicSDSGBuilder sdsg = new BasicSDSGBuilder(transactionMap, schemaInfo, options);

		assertEquals(0, sdsg.basicSDSG.edgesConnecting(t1, t1).size());
		assertEquals(0, sdsg.basicSDSG.edgesConnecting(t1, t2).size());
		assertEquals(0, sdsg.basicSDSG.edgesConnecting(t2, t2).size());

		assertEquals(0, sdsg.findCriticalCycles().size());
	}

	@Test
	public void testInsert() {
		final Map<TransactionDescriptor, Set<ParsedSessionExecuteInvoke>> transactionMap = new HashMap<>();
		transactionMap.put(t1, Collections.singleton(i1));
		transactionMap.put(t2, Collections.singleton(i1));
		final BasicSDSGBuilder sdsg = new BasicSDSGBuilder(transactionMap, schemaInfo, options);

		assertEquals(Collections.singleton(ArbitrationEdge.create(t1, t1)), sdsg.basicSDSG.edgesConnecting(t1, t1));
		assertEquals(Collections.singleton(ArbitrationEdge.create(t1, t2)), sdsg.basicSDSG.edgesConnecting(t1, t2));
		assertEquals(Collections.singleton(ArbitrationEdge.create(t2, t2)), sdsg.basicSDSG.edgesConnecting(t2, t2));

		assertEquals(0, sdsg.findCriticalCycles().size());
	}

	@Test
	public void testInsertSelect() {
		final Map<TransactionDescriptor, Set<ParsedSessionExecuteInvoke>> transactionMap = new HashMap<>();
		transactionMap.put(t1, Collections.singleton(i1));
		transactionMap.put(t2, Collections.singleton(s1));
		final BasicSDSGBuilder sdsg = new BasicSDSGBuilder(transactionMap, schemaInfo, options);

		assertEquals(Collections.singleton(ArbitrationEdge.create(t1, t1)), sdsg.basicSDSG.edgesConnecting(t1, t1));
		assertEquals(Collections.singleton(DependencyEdge.create(t1, t2)), sdsg.basicSDSG.edgesConnecting(t1, t2));
		assertEquals(0, sdsg.basicSDSG.edgesConnecting(t2, t2).size());

		assertEquals(2, sdsg.findCriticalCycles().size());
	}

	@Test
	public void testDisjointInsertSelect() {
		// SELECT can depend on UPDATE even if disjoint columns are used. An UPDATE may insert a new value that is queried by a SELECT afterwards
		final Map<TransactionDescriptor, Set<ParsedSessionExecuteInvoke>> transactionMap = new HashMap<>();
		transactionMap.put(t1, Collections.singleton(i1));
		transactionMap.put(t2, Collections.singleton(s2));
		final BasicSDSGBuilder sdsg = new BasicSDSGBuilder(transactionMap, schemaInfo, options);

		assertEquals(Collections.singleton(ArbitrationEdge.create(t1, t1)), sdsg.basicSDSG.edgesConnecting(t1, t1));
		assertEquals(Collections.singleton(DependencyEdge.create(t1, t2)), sdsg.basicSDSG.edgesConnecting(t1, t2));
		assertEquals(0, sdsg.basicSDSG.edgesConnecting(t2, t2).size());

		assertEquals(2, sdsg.findCriticalCycles().size());
	}

	@Test
	public void testDisjointUpdates() {
		final Map<TransactionDescriptor, Set<ParsedSessionExecuteInvoke>> transactionMap = new HashMap<>();
		transactionMap.put(t1, Collections.singleton(i1));
		transactionMap.put(t2, Collections.singleton(up1));
		final BasicSDSGBuilder sdsg = new BasicSDSGBuilder(transactionMap, schemaInfo, options);

		assertEquals(Collections.singleton(ArbitrationEdge.create(t1, t1)), sdsg.basicSDSG.edgesConnecting(t1, t1));
		assertEquals(0, sdsg.basicSDSG.edgesConnecting(t1, t2).size());
		assertEquals(Collections.singleton(ArbitrationEdge.create(t2, t2)), sdsg.basicSDSG.edgesConnecting(t2, t2));

		assertEquals(0, sdsg.findCriticalCycles().size());
	}

	@Test
	public void testDisjointSelectUpdate1() {
		final Map<TransactionDescriptor, Set<ParsedSessionExecuteInvoke>> transactionMap = new HashMap<>();
		transactionMap.put(t1, Collections.singleton(s2));
		transactionMap.put(t2, Collections.singleton(up1));
		final BasicSDSGBuilder sdsg = new BasicSDSGBuilder(transactionMap, schemaInfo, options);

		assertEquals(0, sdsg.basicSDSG.edgesConnecting(t1, t1).size());
		assertEquals(0, sdsg.basicSDSG.edgesConnecting(t1, t2).size());
		assertEquals(Collections.singleton(ArbitrationEdge.create(t2, t2)), sdsg.basicSDSG.edgesConnecting(t2, t2));

		assertEquals(0, sdsg.findCriticalCycles().size());
	}

	@Test
	public void testDisjointSelectUpdate2() {
		final Map<TransactionDescriptor, Set<ParsedSessionExecuteInvoke>> transactionMap = new HashMap<>();
		transactionMap.put(t1, Collections.singleton(s3));
		transactionMap.put(t2, Collections.singleton(up1));
		final BasicSDSGBuilder sdsg = new BasicSDSGBuilder(transactionMap, schemaInfo, options);

		assertEquals(0, sdsg.basicSDSG.edgesConnecting(t1, t1).size());
		assertEquals(0, sdsg.basicSDSG.edgesConnecting(t1, t2).size());
		assertEquals(Collections.singleton(ArbitrationEdge.create(t2, t2)), sdsg.basicSDSG.edgesConnecting(t2, t2));

		assertEquals(0, sdsg.findCriticalCycles().size());
	}

	@Test
	public void testDisjointSelectUpdate3() {
		final Map<TransactionDescriptor, Set<ParsedSessionExecuteInvoke>> transactionMap = new HashMap<>();
		transactionMap.put(t1, Collections.singleton(s4));
		transactionMap.put(t2, Collections.singleton(up1));
		final BasicSDSGBuilder sdsg = new BasicSDSGBuilder(transactionMap, schemaInfo, options);

		assertEquals(0, sdsg.basicSDSG.edgesConnecting(t1, t1).size());
		assertEquals(0, sdsg.basicSDSG.edgesConnecting(t1, t2).size());
		assertEquals(Collections.singleton(ArbitrationEdge.create(t2, t2)), sdsg.basicSDSG.edgesConnecting(t2, t2));

		assertEquals(0, sdsg.findCriticalCycles().size());
	}

	@Test
	public void testDeleteDelete() {
		final Map<TransactionDescriptor, Set<ParsedSessionExecuteInvoke>> transactionMap = new HashMap<>();
		transactionMap.put(t1, Collections.singleton(d1));
		transactionMap.put(t2, Collections.singleton(d1));
		final BasicSDSGBuilder sdsg = new BasicSDSGBuilder(transactionMap, schemaInfo, options);

		assertEquals(0, sdsg.basicSDSG.edgesConnecting(t1, t1).size());
		assertEquals(0, sdsg.basicSDSG.edgesConnecting(t1, t2).size());
		assertEquals(0, sdsg.basicSDSG.edgesConnecting(t2, t2).size());

		assertEquals(0, sdsg.findCriticalCycles().size());
	}

	@Test
	public void testInsertInsert() {
		final Map<TransactionDescriptor, Set<ParsedSessionExecuteInvoke>> transactionMap = new HashMap<>();
		transactionMap.put(t1, Collections.singleton(i1));
		final BasicSDSGBuilder sdsg = new BasicSDSGBuilder(transactionMap, schemaInfo, options);

		assertEquals(Collections.singleton(ArbitrationEdge.create(t1, t1)), sdsg.basicSDSG.edgesConnecting(t1, t1));

		assertEquals(0, sdsg.findCriticalCycles().size());
	}
}
