package ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.SchemaInformation;

public class ECCheckerAbsorptionTest extends AbstractECCheckerTest {

	public ECCheckerAbsorptionTest() throws IOException {
		super("ECCheckerAbsorption");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("users", "username")
				.addNonPrimaryKeyColumn("users", "password").build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setAbsorptionEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setAbsorptionEnabled(false);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, ECCheckerResult result) {
		if (options.isAbsorptionEnabled()) {
			assertEquals(0, result.getTotalVerifiedViolationsSize4());
		} else {
			assertEquals(2, result.getTotalVerifiedViolationsSize4());
		}
	}
}
