package ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.SchemaInformation;

public class ECCheckerAsymmetricCommutativityTest extends AbstractECCheckerTest {

	public ECCheckerAsymmetricCommutativityTest() throws IOException {
		super("ECCheckerAsymCommu");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("users", "username")
				.addNonPrimaryKeyColumn("users", "password").addPrimaryKeyColumn("playlist_tracks", "username")
				.addPrimaryKeyColumn("playlist_tracks", "playlist_name").build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setLegalitySpecEnabled(false);
		options.setAsymmetricCommutativityEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setLegalitySpecEnabled(false);
		options.setAsymmetricCommutativityEnabled(false);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, ECCheckerResult result) {
		if (options.isAsymmetricCommutativityEnabled()) {
			assertEquals(2, result.getTotalVerifiedViolationsSize4());
		} else {
			assertEquals(3, result.getTotalVerifiedViolationsSize4());
		}
	}
}
