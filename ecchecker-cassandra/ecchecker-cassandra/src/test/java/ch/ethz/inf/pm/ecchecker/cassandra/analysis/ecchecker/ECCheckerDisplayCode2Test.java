package ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.SchemaInformation;

public class ECCheckerDisplayCode2Test extends AbstractECCheckerTest {

	public ECCheckerDisplayCode2Test() throws IOException {
		super("ECCheckerDisplayCode2");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("users", "username").addNonPrimaryKeyColumn("users", "password")
				.addNonPrimaryKeyColumn("users", "playlists").addPrimaryKeyColumn("playlist_tracks", "username")
				.addPrimaryKeyColumn("playlist_tracks", "playlist").addPrimaryKeyColumn("playlist_tracks", "track_id").build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setDisplayCodeEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setDisplayCodeEnabled(false);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, ECCheckerResult result) {
		if (options.isDisplayCodeEnabled()) {
			assertEquals(0, result.getTotalVerifiedViolationsSize4());
		} else {
			assertEquals(4, result.getTotalVerifiedViolationsSize4());
		}
	}
}
