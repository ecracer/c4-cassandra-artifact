package ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.SchemaInformation;

public class ECCheckerLegalityTest extends AbstractECCheckerTest {

	public ECCheckerLegalityTest() throws IOException {
		super("ECCheckerLegalitySpec");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("users", "username").addNonPrimaryKeyColumn("users", "playlist_names")
				.addTableWithoutDelete("users").addColumnsWritten("users", ImmutableSet.of("username", "playlist_names")).build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setLegalitySpecEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setLegalitySpecEnabled(false);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, ECCheckerResult result) {
		if (options.isLegalitySpecEnabled()) {
			assertEquals(0, result.getTotalVerifiedViolationsSize4());
		} else {
			assertEquals(3, result.getTotalVerifiedViolationsSize4());
		}
	}
}
