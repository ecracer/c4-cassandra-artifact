package ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.SchemaInformation;

public class ECCheckerLegalityTest2 extends AbstractECCheckerTest {

	public ECCheckerLegalityTest2() throws IOException {
		super("ECCheckerLegalitySpec2");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("users", "login").addNonPrimaryKeyColumn("users", "password")
				.addNonPrimaryKeyColumn("users", "room_names").addTableWithoutDelete("users")
				.addColumnsWritten("users", ImmutableSet.of("login", "password", "room_names")).build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(false);
		options.setLegalitySpecEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(false);
		options.setLegalitySpecEnabled(false);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, ECCheckerResult result) {
		if (options.isLegalitySpecEnabled()) {
			assertEquals(0, result.getTotalVerifiedViolationsSize4());
		} else {
			assertEquals(6, result.getTotalVerifiedViolationsSize4());
		}
	}
}
