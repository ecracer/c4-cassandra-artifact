package ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.SchemaInformation;

public class ECCheckerProcessTest extends AbstractECCheckerTest {

	public ECCheckerProcessTest() throws IOException {
		super("ECCheckerProcess");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("users", "username")
				.addNonPrimaryKeyColumn("users", "password").build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(false);
		options.setSinglePOPathEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(false);
		options.setSinglePOPathEnabled(false);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, ECCheckerResult result) {
		if (options.isSinglePOPathEnabled()) {
			assertEquals(0, result.getVerifiedViolationsSize2());
			assertEquals(0, result.getVerifiedViolationsSize3());
			assertEquals(1, result.getVerifiedViolationsSize4());
		} else {
			assertEquals(1, result.getVerifiedViolationsSize2());
			assertEquals(0, result.getVerifiedViolationsSize3());
			assertEquals(0, result.getVerifiedViolationsSize4());
		}
	}
}
