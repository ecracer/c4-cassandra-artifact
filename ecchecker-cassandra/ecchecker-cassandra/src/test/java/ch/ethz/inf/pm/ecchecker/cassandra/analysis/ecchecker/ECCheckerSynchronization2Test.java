package ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.SchemaInformation;

public class ECCheckerSynchronization2Test extends AbstractECCheckerTest {

	public ECCheckerSynchronization2Test() throws IOException {
		super("ECCheckerSynchronization2");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("chat_rooms", "room_name")
				.addNonPrimaryKeyColumn("chat_rooms", "participants").build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(false);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, ECCheckerResult result) {
		if (options.isSynchronizingOperationsEnabled()) {
			assertEquals(3, result.getTotalVerifiedViolationsSize4());
		} else {
			assertEquals(9, result.getTotalVerifiedViolationsSize4());
		}
	}
}
