package ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.SchemaInformation;

public class ECCheckerValueAnalysisTest extends AbstractECCheckerTest {

	public ECCheckerValueAnalysisTest() throws IOException {
		super("ECCheckerValueAnalysis");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("users", "username").addPrimaryKeyColumn("track_by_id", "id")
				.addNonPrimaryKeyColumn("track_by_id", "artist").addNonPrimaryKeyColumn("track_by_id", "track").build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setValueAnalysisEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setValueAnalysisEnabled(false);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, ECCheckerResult result) {
		// addUser und getUser gibt immer eine Violation (da keine ClientLocalValues usw. drin sind)
		if (options.isValueAnalysisEnabled()) {
			assertEquals(1, result.getTotalVerifiedViolationsSize4());
		} else {
			assertEquals(2, result.getTotalVerifiedViolationsSize4());
		}
	}
}
