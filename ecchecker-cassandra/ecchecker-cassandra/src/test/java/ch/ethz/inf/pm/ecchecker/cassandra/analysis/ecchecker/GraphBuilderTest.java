package ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker;

import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.FALSE;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.TRUE;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.and;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.boolInsertsNewRowsArgLeft;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.boolUpdatesExistingRowsArgLeft;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.intArgLeft;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.intArgRight;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.not;
import static ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECFactory.unequal;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collections;

import org.junit.Test;

import com.google.common.collect.ImmutableList;

import ch.ethz.inf.pm.ecchecker.Encoder;
import ch.ethz.inf.pm.ecchecker.Main;
import ch.ethz.inf.pm.ecchecker.Unfolder;
import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.SchemaInformation;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnSelection;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnCounterUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetUpdate.SetOp;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnUnknownValueUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnValueUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraint.EQConstraint;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraint.EQValConstraint;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraint.INConstraint;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraint.INValsConstraint;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraint.UnknownConstraint;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraints;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.QueryPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.SelectAllPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.SelectColsPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.UpdatePart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.UpsertPart;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ClientImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.UUIDValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;
import ch.ethz.inf.pm.ecchecker.output.Statistics;

public class GraphBuilderTest {

	@Test
	public void testSelectSelect() {
		final SelectAllPart select = SelectAllPart.create("table1", Constraints.create());
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), select);
		assertEquals(graphBuilder.getCommutativityExpr(select, select), TRUE);

		graphBuilder.addEvent(select, "q1");
		graphBuilder.addEvent(select, "q2");
		graphBuilder.addTransactionOrderEdge("q1", "q2");

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption()).isEmpty());
	}

	@Test
	public void testSelectUpdate() {
		final UnknownImmutableValue key = UnknownImmutableValue.create(ProgramPointId.create(CallStack.EMPTY_STACK, "1"));
		final SelectAllPart select = SelectAllPart.create("table1", Constraints.create(EQValConstraint.create("key", key)));
		final UpsertPart insert = UpsertPart.createInsert("table1",
				ImmutableList.of(ColumnValueUpdate.create("key", key), ColumnUnknownValueUpdate.create("val")), false);
		final GraphBuilder graphBuilder = new GraphBuilder(
				SchemaInformation.builder().addPrimaryKeyColumn("table1", "key").addNonPrimaryKeyColumn("table1", "val").build(), select,
				insert);

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(select, "t1"),
				graphBuilder.addEvent(insert, "t1"));

		assertEquals(1, Unfolder.unfoldAndCheck(graphBuilder.build()).size());
	}

	@Test
	public void testSelectUpdateDisplayCols() {
		final UnknownImmutableValue key = UnknownImmutableValue.create(ProgramPointId.create(CallStack.EMPTY_STACK, "1"));
		final QueryPart select = SelectAllPart.create("table1", Constraints.create(EQValConstraint.create("key", key)))
				.setDisplayColumns(Collections.singleton("set_value"));
		final UpsertPart insert = UpsertPart.createUpdate("table1",
				ImmutableList.of(ColumnSetUpdate.create("set_value", SetOp.ADD)), ImmutableList.of(EQValConstraint.create("key", key)),
				true, true);
		final GraphBuilder graphBuilder = new GraphBuilder(
				SchemaInformation.builder().addPrimaryKeyColumn("table1", "key").addNonPrimaryKeyColumn("table1", "set_value").build(),
				select,
				insert);

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(select, "t1"),
				graphBuilder.addEvent(insert, "t1"));

		assertEquals(0, Unfolder.unfoldAndCheck(graphBuilder.build()).size());
	}

	@Test
	public void testSelectUpdateUsingUUIDs() {
		for (int i = 0; i < 2; i++) {
			final UUIDValue key = i == 0
					? UUIDValue.createRandom(ProgramPointId.unique())
					: UUIDValue.createFromString(ProgramPointId.unique());
			final SelectAllPart select = SelectAllPart.create("table1", Constraints.create(EQValConstraint.create("key", key)));
			final UpsertPart insert = UpsertPart.createInsert("table1",
					ImmutableList.of(ColumnValueUpdate.create("key", key), ColumnUnknownValueUpdate.create("val")), false);
			final GraphBuilder graphBuilder = new GraphBuilder(
					SchemaInformation.builder().addPrimaryKeyColumn("table1", "key").addNonPrimaryKeyColumn("table1", "val").build(),
					select,
					insert);

			graphBuilder.addProgramOrderEdge(
					graphBuilder.addEvent(select, "t1"),
					graphBuilder.addEvent(insert, "t1"));

			assertEquals(i, Unfolder.unfoldAndCheck(graphBuilder.build()).size());
		}
	}

	@Test
	public void testUpdateSelect() {
		final SelectAllPart select = SelectAllPart.create("table1", Constraints.create());
		final UpsertPart update = UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(UnknownConstraint.create("key")), false, false);
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), select, update);
		assertEquals(graphBuilder.getCommutativityExpr(select, select), TRUE);
		assertNotEquals(graphBuilder.getCommutativityExpr(select, update), TRUE);
		assertNotEquals(graphBuilder.getAbsorptionExpr(update, update), TRUE);

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update, "t1"),
				graphBuilder.addEvent(select, "t1"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update, "t2"),
				graphBuilder.addEvent(select, "t2"));

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption()).isDefined());
	}

	@Test
	public void testUpdateSelectUsingEmptyIN() {
		final SelectAllPart select = SelectAllPart.create("table1", Constraints.create(INValsConstraint.create("key", ImmutableList.of())));
		final UpsertPart update = UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(UnknownConstraint.create("key")), false, false);
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), select, update);
		assertEquals(graphBuilder.getCommutativityExpr(select, select), TRUE);
		assertEquals(graphBuilder.getCommutativityExpr(select, update), TRUE);

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update, "t1"),
				graphBuilder.addEvent(select, "t1"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update, "t2"),
				graphBuilder.addEvent(select, "t2"));

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption()).isEmpty());
	}

	@Test
	public void testSelectUpdateDifferentTransaction() {
		final SelectAllPart select = SelectAllPart.create("table1", Constraints.create());
		final UpsertPart update = UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(UnknownConstraint.create("key")), false, false);
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), select, update);

		graphBuilder.addEvent(update, "tu1");
		graphBuilder.addEvent(update, "tu2");
		graphBuilder.addEvent(select, "tq1");
		graphBuilder.addEvent(select, "tq2");

		graphBuilder.addTransactionOrderEdge("tu1", "tq1");
		graphBuilder.addTransactionOrderEdge("tu2", "tq2");

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption()).isDefined());
	}

	@Test
	public void testSelectUpdateDifferentTables() {
		final SelectAllPart select1 = SelectAllPart.create("table1", Constraints.create());
		final UpsertPart update1 = UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(UnknownConstraint.create("key")), false, false);
		final SelectAllPart select2 = SelectAllPart.create("table2", Constraints.create());
		final UpdatePart update2 = UpsertPart.createUpdate("table2", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(UnknownConstraint.create("key")), false, false);
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), select1, update1, select2, update2);

		assertEquals(graphBuilder.getCommutativityExpr(select1, select2), TRUE);
		assertNotEquals(graphBuilder.getCommutativityExpr(select1, update1), TRUE);
		assertEquals(graphBuilder.getCommutativityExpr(select1, update2), TRUE);
		assertNotEquals(graphBuilder.getAbsorptionExpr(update1, update1), TRUE);
		assertNotEquals(graphBuilder.getAbsorptionExpr(update2, update2), TRUE);
		assertEquals(graphBuilder.getAbsorptionExpr(update1, update2), FALSE);
		assertEquals(graphBuilder.getAbsorptionExpr(update2, update1), FALSE);

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update1, "t1"),
				graphBuilder.addEvent(select1, "t1"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update2, "t2"),
				graphBuilder.addEvent(select2, "t2"));

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption()).isEmpty());
	}

	@Test
	public void testSelectUpdateDifferentRows() {
		final SelectAllPart select1 = SelectAllPart.create("table1",
				Constraints.create(EQValConstraint.create("key", StringValue.create("str1"))));
		final UpsertPart update1 = UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(EQValConstraint.create("key", StringValue.create("str1"))), false, false);
		final SelectAllPart select2 = SelectAllPart.create("table1",
				Constraints.create(EQValConstraint.create("key", StringValue.create("str2"))));
		final UpsertPart update2 = UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(EQValConstraint.create("key", StringValue.create("str2"))), false, false);
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.builder().addPrimaryKeyColumn("table1", "key").build(),
				select1, update1, select2, update2);

		assertEquals(graphBuilder.getCommutativityExpr(select1, select2), TRUE);
		assertNotEquals(graphBuilder.getCommutativityExpr(select1, update1), TRUE);
		assertNotEquals(graphBuilder.getCommutativityExpr(select1, update2), unequal(intArgLeft(1), intArgRight(1)));
		assertEquals(graphBuilder.getAbsorptionExpr(update1, update2),
				and(not(boolInsertsNewRowsArgLeft()), not(boolUpdatesExistingRowsArgLeft())));
		assertEquals(graphBuilder.getAbsorptionExpr(update2, update1),
				and(not(boolInsertsNewRowsArgLeft()), not(boolUpdatesExistingRowsArgLeft())));

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update1, "t1"),
				graphBuilder.addEvent(select1, "t1"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update2, "t2"),
				graphBuilder.addEvent(select2, "t2"));

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption()).isEmpty());
	}

	@Test
	public void testInsertUpdate() {
		final UpsertPart insert = UpsertPart.createInsert("table1",
				ImmutableList.of(ColumnUnknownValueUpdate.create("key"), ColumnUnknownValueUpdate.create("col1")), false);
		final UpsertPart update = UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(UnknownConstraint.create("key")), false, false);
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.builder().addPrimaryKeyColumn("table1", "key").build(), insert,
				update);

		assertNotEquals(graphBuilder.getCommutativityExpr(insert, insert), TRUE);
		assertNotEquals(graphBuilder.getCommutativityExpr(insert, update), TRUE);
		assertNotEquals(graphBuilder.getCommutativityExpr(update, update), TRUE);

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(insert, "t1"),
				graphBuilder.addEvent(update, "t1"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(insert, "t2"),
				graphBuilder.addEvent(update, "t2"));

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption()).isEmpty());
	}

	@Test
	public void testCircularDependencies() {
		final UpsertPart update1 = UpsertPart.createUpdate("table1",
				ImmutableList.of(ColumnUnknownValueUpdate.create("val")), ImmutableList.of(UnknownConstraint.create("key")), false, false);
		final UpsertPart update2 = UpsertPart.createUpdate("table2",
				ImmutableList.of(ColumnUnknownValueUpdate.create("val")), ImmutableList.of(UnknownConstraint.create("key")), false, false);
		final UpsertPart update3 = UpsertPart.createUpdate("table3",
				ImmutableList.of(ColumnUnknownValueUpdate.create("val")), ImmutableList.of(UnknownConstraint.create("key")), false, false);

		final SelectAllPart select1 = SelectAllPart.create("table1", Constraints.create(UnknownConstraint.create("key")));
		final SelectAllPart select2 = SelectAllPart.create("table2", Constraints.create(UnknownConstraint.create("key")));

		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), update1, update2, update3, select1, select2);

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update1, "t1"),
				graphBuilder.addEvent(select2, "t1"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update2, "t2"),
				graphBuilder.addEvent(update3, "t2"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update3, "t3"),
				graphBuilder.addEvent(select1, "t3"));

		graphBuilder.addTransactionOrderEdge("t1", "t2");

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption()).isDefined());
	}

	@Test
	public void testCircularDependenciesUsingCounter() {
		final UpsertPart update1 = UpsertPart.createUpdate("table1",
				ImmutableList.of(ColumnUnknownValueUpdate.create("val")), ImmutableList.of(UnknownConstraint.create("key")), false, false);
		final UpsertPart update2 = UpsertPart.createUpdate("table2",
				ImmutableList.of(ColumnUnknownValueUpdate.create("val")), ImmutableList.of(UnknownConstraint.create("key")), false, false);
		final UpsertPart update3 = UpsertPart.createUpdate("table3",
				ImmutableList.of(ColumnCounterUpdate.create("val")), ImmutableList.of(UnknownConstraint.create("key")), false, false);

		final SelectAllPart select1 = SelectAllPart.create("table1", Constraints.create(UnknownConstraint.create("key")));
		final SelectAllPart select2 = SelectAllPart.create("table2", Constraints.create(UnknownConstraint.create("key")));

		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), update1, update2, update3, select1, select2);

		assertEquals(TRUE, graphBuilder.getCommutativityExpr(update3, update3));

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update1, "t1"),
				graphBuilder.addEvent(select2, "t1"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update2, "t2"),
				graphBuilder.addEvent(update3, "t2"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update3, "t3"),
				graphBuilder.addEvent(select1, "t3"));

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption()).isEmpty());
	}

	@Test
	public void testUsingUnfolder() {
		final SelectAllPart select = SelectAllPart.create("table1", Constraints.create());
		final UpsertPart update = UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(UnknownConstraint.create("key")), false, false);
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), select, update);

		graphBuilder.addEvent(update, "t1");
		graphBuilder.addEvent(select, "t2");

		graphBuilder.addTransactionOrderEdge("t1", "t1");
		graphBuilder.addTransactionOrderEdge("t1", "t2");
		graphBuilder.addTransactionOrderEdge("t2", "t1");
		graphBuilder.addTransactionOrderEdge("t2", "t2");

		assertEquals(1, Unfolder.unfoldAndCheck(graphBuilder.build()).size());
	}

	@Test
	public void testAsymetric() {
		final SelectAllPart select = SelectAllPart.create("users", Constraints.create(EQConstraint.create("username")));
		final UpsertPart insert = UpsertPart.createInsert("users",
				ImmutableList.of(ColumnUnknownValueUpdate.create("username"), ColumnUnknownValueUpdate.create("rooms")),
				true);
		final UpsertPart update = UpsertPart.createUpdate("users", Collections.singletonList(ColumnUnknownValueUpdate.create("rooms")),
				Collections.singletonList(EQConstraint.create("username")), false, false).setCanInsert(false);

		for (int i = 0; i <= 1; i++) {
			final Options options = new Options();
			final GraphBuilder graphBuilder = new GraphBuilder(options,
					SchemaInformation.builder().addPrimaryKeyColumn("users", "username").build(), select, insert, update);

			graphBuilder.addEvent(update, "t1");
			graphBuilder.addProgramOrderEdge(graphBuilder.addEvent(select, "t2"), graphBuilder.addEvent(insert, "t2"));

			Main.encodeAsymmetricCommutativity_$eq(i == 0);
			Main.encodeSynchronizingOperations_$eq(false);
			assertEquals(1 + i, Unfolder.unfoldAndCheck(graphBuilder.build()).size());
		}
	}

	@Test
	public void testAsymetricUUIDs() {
		// Select cannot know random UUID created in the insert
		final SelectAllPart select = SelectAllPart.create("tracks", Constraints.create(INConstraint.create("id")));
		final UpsertPart insert = UpsertPart.createInsert("tracks",
				ImmutableList.of(ColumnValueUpdate.create("id", UUIDValue.createRandom(ProgramPointId.unique())),
						ColumnUnknownValueUpdate.create("genre")),
				false);

		for (int i = 0; i <= 1; i++) {
			final Options options = new Options();
			options.setSideChannelsEnabled(i != 0);
			final GraphBuilder graphBuilder = new GraphBuilder(options,
					SchemaInformation.builder().addPrimaryKeyColumn("tracks", "id").addNonPrimaryKeyColumn("tracks", "genre").build(),
					insert, select);

			graphBuilder.addEvent(insert, "t1");
			graphBuilder.addEvent(select, "t2");

			graphBuilder.addTransactionOrderEdge("t1", "t1");
			graphBuilder.addTransactionOrderEdge("t1", "t2");
			graphBuilder.addTransactionOrderEdge("t2", "t1");
			graphBuilder.addTransactionOrderEdge("t2", "t2");

			assertEquals(i, Unfolder.unfoldAndCheck(graphBuilder.build()).size());
		}
	}

	@Test
	public void testAsymetricUUIDs2() {
		for (int i = 0; i <= 1; i++) {
			final SelectAllPart select;
			if (i == 0) {
				//Could be range query which may include new track
				select = SelectAllPart.create("tracks", Constraints.create(UnknownConstraint.create("id")));
			} else {
				//Could also include new track
				select = SelectAllPart.create("tracks", Constraints.create(EQConstraint.create("genre")));
			}
			final UpsertPart insert = UpsertPart.createInsert("tracks",
					ImmutableList.of(ColumnValueUpdate.create("id", UUIDValue.createRandom(ProgramPointId.unique())),
							ColumnUnknownValueUpdate.create("genre")),
					false);

			final Options options = new Options();
			options.setSideChannelsEnabled(false);
			final GraphBuilder graphBuilder = new GraphBuilder(options,
					SchemaInformation.builder().addPrimaryKeyColumn("tracks", "id").addNonPrimaryKeyColumn("tracks", "genre").build(),
					insert, select);

			graphBuilder.addEvent(insert, "t1");
			graphBuilder.addEvent(select, "t2");

			graphBuilder.addTransactionOrderEdge("t1", "t1");
			graphBuilder.addTransactionOrderEdge("t1", "t2");
			graphBuilder.addTransactionOrderEdge("t2", "t1");
			graphBuilder.addTransactionOrderEdge("t2", "t2");

			assertEquals(1, Unfolder.unfoldAndCheck(graphBuilder.build()).size());
		}
	}

	@Test
	public void testSynchronizingOperations() {
		final SelectAllPart select = SelectAllPart.create("users",
				Constraints.create(EQValConstraint.create("username", ClientImmutableValue.create("username"))));
		final UpsertPart insert = UpsertPart.createInsert("users",
				ImmutableList.of(ColumnValueUpdate.create("username", ClientImmutableValue.create("username")),
						ColumnUnknownValueUpdate.create("rooms")),
				true);
		final UpsertPart update = UpsertPart.createUpdate("users", ImmutableList.of(ColumnUnknownValueUpdate.create("rooms")),
				ImmutableList.of(EQConstraint.create("username")),
				true, true);

		for (int i = 0; i <= 1; i++) {
			final Options options = new Options();
			final GraphBuilder graphBuilder = new GraphBuilder(options,
					SchemaInformation.builder().addPrimaryKeyColumn("users", "username").addNonPrimaryKeyColumn("users", "rooms")
							.addTableWithoutDelete("users").build(),
					insert, select, update);

			graphBuilder.addEvent(update, "t1");
			graphBuilder.addProgramOrderEdge(
					graphBuilder.addEvent(select, "t2"),
					graphBuilder.addEvent(insert, "t2"));

			graphBuilder.addTransactionOrderEdge("t1", "t1");
			graphBuilder.addTransactionOrderEdge("t1", "t2");
			graphBuilder.addTransactionOrderEdge("t2", "t1");
			graphBuilder.addTransactionOrderEdge("t2", "t2");

			Main.encodeSynchronizingOperations_$eq(i == 0);

			assertEquals(i * 2, Unfolder.unfoldAndCheck(graphBuilder.build()).size());
		}
	}

	@Test
	public void testInsertIfNotExists() {
		final SelectColsPart select = SelectColsPart.create("users", ImmutableList.of(ColumnSelection.create("username")),
				Constraints.create(EQValConstraint.create("username", ClientImmutableValue.create("username"))));
		final UpsertPart insert = UpsertPart.createInsert("users",
				ImmutableList.of(ColumnValueUpdate.create("username", ClientImmutableValue.create("username")),
						ColumnUnknownValueUpdate.create("password")),
				true);
		final UpsertPart updateDifferent = UpsertPart.createUpdate("users", ImmutableList.of(ColumnUnknownValueUpdate.create("room_names")),
				ImmutableList.of(EQValConstraint.create("username", ClientImmutableValue.create("username"))),
				false, false);

		for (int i = 0; i < 2; i++) {
			final Options options = new Options();
			final GraphBuilder graphBuilder = new GraphBuilder(options,
					SchemaInformation.builder().addPrimaryKeyColumn("users", "username").addNonPrimaryKeyColumn("users", "password")
							.addNonPrimaryKeyColumn("users", "room_names").addTableWithoutDelete("users").build(),
					insert, select, updateDifferent);

			graphBuilder.addEvent(updateDifferent, "t1");
			graphBuilder.addEvent(select, "t2");
			graphBuilder.addEvent(insert, "t3");

			graphBuilder.addTransactionOrderEdge("t1", "t1");
			graphBuilder.addTransactionOrderEdge("t1", "t2");
			graphBuilder.addTransactionOrderEdge("t1", "t3");
			graphBuilder.addTransactionOrderEdge("t2", "t1");
			graphBuilder.addTransactionOrderEdge("t2", "t2");
			graphBuilder.addTransactionOrderEdge("t2", "t3");
			graphBuilder.addTransactionOrderEdge("t3", "t1");
			graphBuilder.addTransactionOrderEdge("t3", "t2");
			graphBuilder.addTransactionOrderEdge("t3", "t3");

			Main.encodeAsymmetricCommutativity_$eq(i == 0);
			Main.encodeSynchronizingOperations_$eq(false);

			Unfolder.unfoldAndCheck(graphBuilder.build());
			assertEquals(3 * i, Statistics.totalVerifiedViolationsSize4());
		}
	}
}
