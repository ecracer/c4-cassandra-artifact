package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import com.google.common.collect.ImmutableMap;

import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.SchemaInformation;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.TransactionGraphTransformer;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECCheckerSerializabilityAnalysis;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.TransactionGraph;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.UnparsedTransactionGraph;

public class AnalysisClientLocalValueQueryResultTest extends AbstractSootTest {

	public AnalysisClientLocalValueQueryResultTest() throws IOException {
		super("ClientLocalValueQueryResult", new String[0]);
	}

	@Override
	protected void checkTransactionExecuteGraph(ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> executeGraphs) {

		final ImmutableMap<TransactionDescriptor, TransactionGraph> transactionExecuteGraphs = new TransactionGraphTransformer(
				new Options()).transform(executeGraphs);

		assertEquals(3, executeGraphs.size());

		final SchemaInformation schema = SchemaInformation.builder()
				.addPrimaryKeyColumn("users", "username").addNonPrimaryKeyColumn("users", "tracks")
				.addPrimaryKeyColumn("users2", "username").addNonPrimaryKeyColumn("users2", "tracks")
				.addPrimaryKeyColumn("users3", "username").addNonPrimaryKeyColumn("users3", "tracks")
				.build();

		final Options options = new Options();
		options.setClientLocalAreGlobalUnique(true);

		int violations = new ECCheckerSerializabilityAnalysis(options).check(transactionExecuteGraphs, schema).getVerifiedViolationsSize2();
		assertEquals(1, violations);
	}
}
