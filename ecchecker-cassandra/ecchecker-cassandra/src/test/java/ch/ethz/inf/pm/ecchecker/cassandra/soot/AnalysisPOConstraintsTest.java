package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import com.google.common.collect.ImmutableMap;

import ch.ethz.inf.pm.ecchecker.cassandra.Options;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.SchemaInformation;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.TransactionGraphTransformer;
import ch.ethz.inf.pm.ecchecker.cassandra.analysis.ecchecker.ECCheckerSerializabilityAnalysis;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.TransactionGraph;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.UnparsedTransactionGraph;

public class AnalysisPOConstraintsTest extends AbstractSootTest {

	public AnalysisPOConstraintsTest() throws IOException {
		super("POConstraints", new String[0]);
	}

	@Override
	protected void checkTransactionExecuteGraph(ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> executeGraphs) {

		final Options options = new Options();

		final ImmutableMap<TransactionDescriptor, TransactionGraph> transactionExecuteGraphs = new TransactionGraphTransformer(options)
				.transform(executeGraphs);

		assertEquals(4, executeGraphs.size());

		final SchemaInformation schema = SchemaInformation.builder()
				.addPrimaryKeyColumn("users", "username").addPrimaryKeyColumn("followers", "username")
				.addPrimaryKeyColumn("followers", "follower").addTableWithoutDelete("users").addTableWithoutDelete("followers")
				.build();

		int violations = new ECCheckerSerializabilityAnalysis(options).check(transactionExecuteGraphs, schema)
				.getTotalVerifiedViolationsSize4();
		assertEquals(1, violations);
	}
}
