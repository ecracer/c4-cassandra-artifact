package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.ecchecker.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.UnparsedTransactionGraph;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.SessionExecuteInvoke;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.StatementValue;

public class AnalysisRecursionTest extends AbstractSootTest {

	public AnalysisRecursionTest() throws IOException {
		super("Recursion", "test");
	}

	@Override
	protected void checkTransactionExecuteGraph(ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> executeGraph) {
		assertEquals(1, executeGraph.size());
		final UnparsedTransactionGraph graph = executeGraph.values().iterator().next();
		assertEquals(4, graph.nodes().size());
		SessionExecuteInvoke node0 = null;
		SessionExecuteInvoke nodeRec0 = null;
		SessionExecuteInvoke node1 = null;
		SessionExecuteInvoke nodeRec1 = null;
		for (final SessionExecuteInvoke node : graph.nodes()) {
			assertEquals(1, node.statementArg.size());
			final StatementValue stmt = node.statementArg.iterator().next();
			if ("SELECT * FROM test_table_0 WHERE i = 0".equals(stmt.query)) {
				if (node.executedFrom.callStack.callStack.size() == 1) {
					node0 = node;
				} else {
					nodeRec0 = node;
				}
			} else if ("SELECT * FROM test_table_1 WHERE i = 1".equals(stmt.query)) {
				if (node.executedFrom.callStack.callStack.size() == 1) {
					node1 = node;
				} else {
					nodeRec1 = node;
				}
			} else {
				assertTrue(false);
			}
		}
		assertEquals(ImmutableSet.of(node0), graph.sourceNodes());
		assertEquals(ImmutableSet.of(node1), graph.sinkNodes());

		assertEquals(ImmutableSet.of(nodeRec0, node1), graph.successors(node0));
		assertEquals(ImmutableSet.of(nodeRec0, nodeRec1), graph.successors(nodeRec0));
		assertEquals(ImmutableSet.of(nodeRec1, node1), graph.successors(nodeRec1));
		assertEquals(ImmutableSet.of(), graph.successors(node1));
	}
}