package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Map.Entry;

import com.google.common.collect.ImmutableMap;

import ch.ethz.inf.pm.ecchecker.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.UnparsedTransactionGraph;

public class AnalysisTransactionsTest extends AbstractSootTest {

	public AnalysisTransactionsTest() throws IOException {
		super("Transactions", new String[0]);
	}

	@Override
	protected void checkTransactionExecuteGraph(ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> executeGraphs) {
		assertEquals(5, executeGraphs.size());
		for (final Entry<TransactionDescriptor, UnparsedTransactionGraph> entry : executeGraphs.entrySet()) {
			if ("method1".equals(entry.getKey().entryMethod.methodName)) {
				checkMethod1(entry.getValue());
			} else if ("method2".equals(entry.getKey().entryMethod.methodName)) {
				checkMethod2(entry.getValue());
			} else if ("method3".equals(entry.getKey().entryMethod.methodName)) {
				checkMethod3(entry.getValue());
			} else if ("method4".equals(entry.getKey().entryMethod.methodName)) {
				checkMethod4(entry.getValue());
			} else if ("method5".equals(entry.getKey().entryMethod.methodName)) {
				checkMethod5(entry.getValue());
			} else {
				assertTrue(false);
			}
		}
	}

	private void checkMethod1(final UnparsedTransactionGraph graph1) {
		assertFalse(graph1.isBypassPossible());
		assertEquals(5, graph1.nodes().size());
		assertEquals(6, graph1.edges().size());
		assertEquals(1, graph1.sourceNodes().size());
		assertEquals(2, graph1.sinkNodes().size());
	}

	private void checkMethod2(final UnparsedTransactionGraph graph2) {
		assertFalse(graph2.isBypassPossible());
		assertEquals(3, graph2.nodes().size());
		assertEquals(4, graph2.edges().size());
		assertEquals(1, graph2.sourceNodes().size());
		assertEquals(1, graph2.sinkNodes().size());
	}

	private void checkMethod3(final UnparsedTransactionGraph graph3) {
		assertTrue(graph3.isBypassPossible());
		assertEquals(2, graph3.nodes().size());
		assertEquals(2, graph3.edges().size());
		assertEquals(1, graph3.sourceNodes().size());
		assertEquals(2, graph3.sinkNodes().size());
	}

	private void checkMethod4(final UnparsedTransactionGraph graph4) {
		assertFalse(graph4.isBypassPossible());
		assertEquals(3, graph4.nodes().size());
		assertEquals(2, graph4.edges().size());
		assertEquals(1, graph4.sourceNodes().size());
		assertEquals(1, graph4.sinkNodes().size());
	}

	private void checkMethod5(final UnparsedTransactionGraph graph5) {
		assertTrue(graph5.isBypassPossible());
		assertEquals(0, graph5.nodes().size());
		assertEquals(0, graph5.edges().size());
		assertEquals(0, graph5.sourceNodes().size());
		assertEquals(0, graph5.sinkNodes().size());
	}
}
