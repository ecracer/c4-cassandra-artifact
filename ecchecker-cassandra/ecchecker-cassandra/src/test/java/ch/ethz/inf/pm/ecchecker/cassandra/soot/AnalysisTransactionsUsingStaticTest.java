package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Map.Entry;

import com.google.common.collect.ImmutableMap;

import ch.ethz.inf.pm.ecchecker.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.UnparsedTransactionGraph;

public class AnalysisTransactionsUsingStaticTest extends AbstractSootTest {

	public AnalysisTransactionsUsingStaticTest() throws IOException {
		super("TransactionsUsingStatic", new String[0]);
	}

	@Override
	protected void checkTransactionExecuteGraph(ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> executeGraphs) {
		assertEquals(1, executeGraphs.size());
		for (final Entry<TransactionDescriptor, UnparsedTransactionGraph> entry : executeGraphs.entrySet()) {
			if ("method1".equals(entry.getKey().entryMethod.methodName)) {
				checkMethod1(entry.getValue());
			} else {
				assertTrue(false);
			}
		}
	}

	private void checkMethod1(final UnparsedTransactionGraph graph1) {
		assertFalse(graph1.isBypassPossible());
		assertEquals(1, graph1.nodes().size());
		assertEquals(0, graph1.edges().size());
		assertEquals(1, graph1.sourceNodes().size());
		assertEquals(1, graph1.sinkNodes().size());
	}
}
