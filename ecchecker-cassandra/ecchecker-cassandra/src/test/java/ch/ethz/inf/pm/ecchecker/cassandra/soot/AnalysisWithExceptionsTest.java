package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Map.Entry;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.ecchecker.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.ecchecker.cassandra.graphs.UnparsedTransactionGraph;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.SessionExecuteInvoke;

public class AnalysisWithExceptionsTest extends AbstractSootTest {

	public AnalysisWithExceptionsTest() throws IOException {
		super("WithExceptions", new String[0]);
	}

	@Override
	protected void checkTransactionExecuteGraph(ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> executeGraphs) {
		assertEquals(2, executeGraphs.size());
		for (final Entry<TransactionDescriptor, UnparsedTransactionGraph> entry : executeGraphs.entrySet()) {
			if ("method1".equals(entry.getKey().entryMethod.methodName)) {
				checkMethod1(entry.getValue());
			} else if ("method2".equals(entry.getKey().entryMethod.methodName)) {
				checkMethod2(entry.getValue());
			} else {
				assertTrue(false);
			}
		}
	}

	private void checkMethod1(final UnparsedTransactionGraph graph1) {
		assertEquals(3, graph1.nodes().size());
		SessionExecuteInvoke invoke1 = null;
		SessionExecuteInvoke invoke2 = null;
		SessionExecuteInvoke invoke3 = null;
		for (SessionExecuteInvoke invoke : graph1.nodes()) {
			assertEquals(1, invoke.statementArg.size());
			final String cql = invoke.statementArg.iterator().next().query;
			if (cql.contains("method1_1")) {
				invoke1 = invoke;
			} else if (cql.contains("method1_2")) {
				invoke2 = invoke;
			} else if (cql.contains("method1_3")) {
				invoke3 = invoke;
			} else {
				assertTrue(false);
			}
		}
		assertNotNull(invoke1);
		assertNotNull(invoke2);
		assertNotNull(invoke3);

		assertFalse(graph1.isBypassPossible());
		assertEquals(ImmutableSet.of(invoke1), graph1.sourceNodes());
		assertEquals(ImmutableSet.of(invoke2, invoke3), graph1.successors(invoke1));
		assertEquals(ImmutableSet.of(invoke3), graph1.successors(invoke2));
		assertEquals(ImmutableSet.of(), graph1.successors(invoke3));
		assertEquals(ImmutableSet.of(invoke1, invoke3), graph1.sinkNodes());
	}

	private void checkMethod2(final UnparsedTransactionGraph graph2) {
		assertEquals(3, graph2.nodes().size());
		SessionExecuteInvoke invoke1 = null;
		SessionExecuteInvoke invoke2 = null;
		SessionExecuteInvoke invoke3 = null;
		for (SessionExecuteInvoke invoke : graph2.nodes()) {
			assertEquals(1, invoke.statementArg.size());
			final String cql = invoke.statementArg.iterator().next().query;
			if (cql.contains("method2_1")) {
				invoke1 = invoke;
			} else if (cql.contains("helper")) {
				invoke2 = invoke;
			} else if (cql.contains("method2_3")) {
				invoke3 = invoke;
			} else {
				assertTrue(false);
			}
		}
		assertNotNull(invoke1);
		assertNotNull(invoke2);
		assertNotNull(invoke3);

		assertFalse(graph2.isBypassPossible());
		assertEquals(ImmutableSet.of(invoke1), graph2.sourceNodes());
		assertEquals(ImmutableSet.of(invoke2), graph2.successors(invoke1));
		assertEquals(ImmutableSet.of(invoke3), graph2.successors(invoke2));
		assertEquals(ImmutableSet.of(), graph2.successors(invoke3));
		assertEquals(ImmutableSet.of(invoke1, invoke3), graph2.sinkNodes());
	}
}
