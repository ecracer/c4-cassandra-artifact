package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootOptions;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.SootOptions.TransactionMethod;

public class SootOptionsTest {

	@Test
	public void testTransactionMethods() {
		final SootOptions options = new SootOptions();
		options.addTransactionMethod(TransactionMethod.parse("ch.ethz.Main#main"));
		options.addTransactionMethod(TransactionMethod.parse("ch.ethz.Main#main()"));
		options.addTransactionMethod(TransactionMethod.parse("ch.ethz.Main#main(int, Object)"));
		assertEquals(options.getTransactionMethods().get(0), new SootOptions.TransactionMethod("ch.ethz.Main", "main", null));
		assertEquals(options.getTransactionMethods().get(1),
				new SootOptions.TransactionMethod("ch.ethz.Main", "main", new String[0]));
		assertEquals(options.getTransactionMethods().get(2),
				new SootOptions.TransactionMethod("ch.ethz.Main", "main", new String[]{"int", "Object"}));
	}
}
