package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorArrayEscapeTest extends AbstractSootTest {

	public StmtExtractorArrayEscapeTest() throws IOException {
		super("ArrayEscape", "test1", "test2", "test3", "test4");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(4, result.size());

		for (final SessionExecuteInvoke nextCall : result) {
			final Set<String> cqls = getCQLs(nextCall);
			switch (nextCall.executedFrom.callStack.method.methodName) {
				case "test1" :
					assertEquals(1, cqls.size());
					assertTrue(cqls.contains("SELECT * FROM test_1"));
					break;
				case "test3" :
					assertEquals(1, cqls.size());
					assertTrue(cqls.contains("SELECT * FROM test_3"));
					break;
				default :
					assertEquals(1, cqls.size());
					assertTrue(cqls.contains(""));
					break;
			}
		}
	}
}
