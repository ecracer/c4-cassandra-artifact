package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorArraysPhiTest extends AbstractSootTest {

	public StmtExtractorArraysPhiTest() throws IOException {
		super("ArraysPhi", "test1", "test2");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(2, result.size());

		final Set<String> cqls = new HashSet<>();
		result.forEach(res -> cqls.addAll(getCQLs(res)));

		assertEquals(6, cqls.size());
		assertTrue(cqls.contains("SELECT * FROM test_1"));
		assertTrue(cqls.contains("SELECT * FROM test_1 WHERE k = ?"));
		assertTrue(cqls.contains("SELECT * FROM test_2"));
		assertTrue(cqls.contains("SELECT * FROM test_2 WHERE k = ?"));
		assertTrue(cqls.contains("SELECT * FROM test_2_1"));
		assertTrue(cqls.contains("SELECT * FROM test_2_2"));
	}
}
