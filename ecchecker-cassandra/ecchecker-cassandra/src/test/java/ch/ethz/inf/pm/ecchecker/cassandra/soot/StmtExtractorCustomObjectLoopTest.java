package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.SessionExecuteInvoke;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;

public class StmtExtractorCustomObjectLoopTest extends AbstractSootTest {

	public StmtExtractorCustomObjectLoopTest() throws IOException {
		super("CustomObjectLoop", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(2, result.size());

		int testValueCount = 0;
		int unknownCount = 0;

		for (final SessionExecuteInvoke invoke : result) {
			for (final StatementValue nextStatement : invoke.statementArg) {
				assertEquals("SELECT * FROM test_table WHERE i = ?", nextStatement.query);
				assertEquals(1, nextStatement.binds.size());
				final ConcreteValue nextBindVal = nextStatement.binds.iterator().next().value;
				if (StringValue.create("test").equals(nextBindVal)) {
					testValueCount++;
				} else if (nextBindVal instanceof UnknownImmutableValue && !((UnknownImmutableValue) nextBindVal).isWidened) {
					unknownCount++;
				} else {
					assertEquals(1, 0);
				}
			}
		}

		assertEquals(1, testValueCount);
		assertEquals(1, unknownCount);
	}
}