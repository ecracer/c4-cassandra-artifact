package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.SessionExecuteInvoke;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.UUIDValue;

public class StmtExtractorFieldUUIDTest extends AbstractSootTest {

	public StmtExtractorFieldUUIDTest() throws IOException {
		super("FieldUUID", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(1, result.size());
		final SessionExecuteInvoke nextCall = result.iterator().next();
		assertEquals(1, nextCall.statementArg.size());
		final StatementValue stmt = nextCall.statementArg.iterator().next();
		assertEquals("SELECT val FROM values WHERE id = ?", stmt.query);
		assertEquals(1, stmt.binds.size());
		final ImmutableChoiceValue choices = (ImmutableChoiceValue) stmt.binds.iterator().next().value;
		assertEquals(3, choices.choices.size());
		assertTrue(choices.choices.contains(NullValue.create()));
		for (final ImmutableValue choice : choices.choices) {
			if (!(choice instanceof NullValue)) {
				assertTrue(((UUIDValue) choice).isWidened);
			}
		}
	}
}
