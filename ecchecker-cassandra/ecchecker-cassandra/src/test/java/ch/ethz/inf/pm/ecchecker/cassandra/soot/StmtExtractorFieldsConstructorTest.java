package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorFieldsConstructorTest extends AbstractSootTest {

	public StmtExtractorFieldsConstructorTest() throws IOException {
		super("FieldsConstructor", "execute");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(1, result.size());

		final SessionExecuteInvoke nextCall = result.iterator().next();
		assertEquals(1, nextCall.statementArg.size());
		assertEquals("SELECT * FROM test", nextCall.statementArg.iterator().next().getQueryWithUnnamedBindMarkers());
	}
}
