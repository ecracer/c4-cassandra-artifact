package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorFieldsSeveralInitTest extends AbstractSootTest {

	public StmtExtractorFieldsSeveralInitTest() throws IOException {
		super("FieldsSeveralInit", "test1", "test2", "test3", "test4");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(4, result.size());

		for (final SessionExecuteInvoke nextCall : result) {
			final Set<String> cqls = getCQLs(nextCall);
			assertEquals(4, cqls.size());
			assertTrue(cqls.contains(""));
			assertTrue(cqls.contains("SELECT * FROM test1"));
			assertTrue(cqls.contains("SELECT * FROM test2"));
			assertTrue(cqls.contains("SELECT * FROM test3"));
		}
	}
}
