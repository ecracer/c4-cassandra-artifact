package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorFieldsSimpleTest extends AbstractSootTest {

	public StmtExtractorFieldsSimpleTest() throws IOException {
		super("FieldsSimple", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(1, result.size());
		final SessionExecuteInvoke nextCall = result.iterator().next();
		final Set<String> cqls = getCQLs(nextCall);
		assertEquals(3, cqls.size());
		assertTrue(cqls.contains(""));
		assertTrue(cqls.contains("SELECT * FROM test_table"));
		assertTrue(cqls.contains("SELECT * FROM test_table WHERE k = ?"));
	}
}
