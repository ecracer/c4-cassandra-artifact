package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.SessionExecuteInvoke;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.ConsistencyLevelValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.NullValue;

public class StmtExtractorFieldsWithArraysTest extends AbstractSootTest {

	public StmtExtractorFieldsWithArraysTest() throws IOException {
		super("FieldsWithArrays", "execute");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(4, result.size());

		for (final SessionExecuteInvoke nextCall : result) {
			checkQuorumDefaultConsistency(nextCall.statementArg);
			final Set<String> cqls = getCQLs(nextCall);
			switch (nextCall.executedFrom.callStack.method.methodName) {
				case "create" :
					assertEquals(1, cqls.size());
					assertTrue(cqls.contains("INSERT INTO test (key, val) VALUES (?, ?)"));
					break;
				case "read" :
					assertEquals(1, cqls.size());
					assertTrue(cqls.contains("SELECT * FROM test WHERE key = ?"));
					break;
				case "update" :
					assertEquals(1, cqls.size());
					assertTrue(cqls.contains("UPDATE test SET val = ? WHERE key = ?"));
					break;
				case "delete" :
					assertEquals(1, cqls.size());
					assertTrue(cqls.contains("DELETE FROM test WHERE key = ?"));
					break;
				default :
					assertTrue(false);
			}
		}
	}

	private void checkQuorumDefaultConsistency(final Set<StatementValue> stmts) {
		assertEquals(2, stmts.size());
		boolean defaultConsistency = false;
		boolean quorumConsistency = false;
		for (final StatementValue stmt : stmts) {
			if (stmt.consistencyLevel instanceof ConsistencyLevelValue
					&& ((ConsistencyLevelValue) stmt.consistencyLevel).level == ConsistencyLevelValue.Level.QUORUM) {
				quorumConsistency = true;
			} else if (stmt.consistencyLevel instanceof NullValue) {
				defaultConsistency = true;
			}
		}
		assertTrue(defaultConsistency);
		assertTrue(quorumConsistency);
	}
}
