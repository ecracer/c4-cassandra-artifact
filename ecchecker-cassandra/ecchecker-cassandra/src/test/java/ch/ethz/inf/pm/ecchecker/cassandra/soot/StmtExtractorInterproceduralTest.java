package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.SessionExecuteInvoke;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;

public class StmtExtractorInterproceduralTest extends AbstractSootTest {

	public StmtExtractorInterproceduralTest() throws IOException {
		super("Interprocedural", "test1");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(1, result.size());
		final SessionExecuteInvoke nextCall = result.iterator().next();

		assertEquals(4, nextCall.statementArg.size());
		boolean hasTest1k1 = false;
		boolean hasTest1 = false;
		boolean hasTest2k1 = false;
		boolean hasTest2 = false;

		for (final StatementValue statement : nextCall.statementArg) {
			if ("SELECT * FROM test1 WHERE k = 1".equals(statement.getQueryWithUnnamedBindMarkers())) {
				hasTest1k1 = true;
				continue;
			} else if ("SELECT * FROM test2 WHERE k = 1".equals(statement.getQueryWithUnnamedBindMarkers())) {
				hasTest2k1 = true;
				continue;
			} else if ("SELECT * FROM test1 WHERE k = ?".equals(statement.getQueryWithUnnamedBindMarkers())) {
				hasTest1 = true;
			} else if ("SELECT * FROM test2 WHERE k = ?".equals(statement.getQueryWithUnnamedBindMarkers())) {
				hasTest2 = true;
			} else {
				assertTrue(false);
			}

			boolean hasWidenedUnknown = false;
			assertEquals(1, statement.binds.size());
			if (statement.binds.get(0).value instanceof UnknownImmutableValue
					&& ((UnknownImmutableValue) statement.binds.get(0).value).isWidened) {
				hasWidenedUnknown = true;
			} else {
				assertTrue(false);
			}

			assertTrue(hasWidenedUnknown);
		}

		assertTrue(hasTest1);
		assertTrue(hasTest1k1);
		assertTrue(hasTest2);
		assertTrue(hasTest2k1);
	}
}
