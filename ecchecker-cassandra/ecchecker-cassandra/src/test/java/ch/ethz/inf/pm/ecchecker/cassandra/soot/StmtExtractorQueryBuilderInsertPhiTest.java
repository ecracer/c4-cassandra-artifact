package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorQueryBuilderInsertPhiTest extends AbstractSootTest {

	public StmtExtractorQueryBuilderInsertPhiTest() throws IOException {
		super("QueryBuilderInsertPhi", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(1, result.size());

		final Set<String> cqls = new HashSet<>();
		result.forEach(res -> cqls.addAll(getCQLs(res)));

		assertEquals(4, cqls.size());
		assertTrue(cqls.contains("INSERT INTO user (username, password) VALUES (?, ?)"));
		assertTrue(cqls.contains("INSERT INTO user (username, password) VALUES (?, ?) IF NOT EXISTS"));
		assertTrue(cqls.contains("INSERT INTO blah (dummy) VALUES (?)"));
		assertTrue(cqls.contains("INSERT INTO blah (dummy) VALUES (?) IF NOT EXISTS"));
	}
}