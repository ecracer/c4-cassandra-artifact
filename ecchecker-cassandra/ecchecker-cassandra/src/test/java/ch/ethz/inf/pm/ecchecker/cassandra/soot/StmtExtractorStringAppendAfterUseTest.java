package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorStringAppendAfterUseTest extends AbstractSootTest {

	public StmtExtractorStringAppendAfterUseTest() throws IOException {
		super("StringAppendAfterUse", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(2, result.size());
		final Set<String> cqls = new HashSet<>();
		result.forEach(res -> cqls.addAll(getCQLs(res)));

		assertEquals(2, cqls.size());
		assertTrue(cqls.contains("SELECT * FROM test_table"));
		assertTrue(cqls.contains("SELECT * FROM test_table WHERE j = ?"));
	}
}