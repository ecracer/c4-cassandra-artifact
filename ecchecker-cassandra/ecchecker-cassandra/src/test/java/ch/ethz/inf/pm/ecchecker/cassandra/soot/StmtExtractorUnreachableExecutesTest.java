package ch.ethz.inf.pm.ecchecker.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorUnreachableExecutesTest extends AbstractSootTest {

	public StmtExtractorUnreachableExecutesTest() throws IOException {
		super("UnreachableExecutes", "testNullInvoke", "testIfElse");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(1, result.size());

		final Set<String> cqls = new HashSet<>();
		result.forEach(call -> cqls.addAll(getCQLs(call)));

		assertEquals(1, cqls.size());
		assertTrue(cqls.contains("SELECT * FROM testIfElse WHERE k = 0"));
	}
}