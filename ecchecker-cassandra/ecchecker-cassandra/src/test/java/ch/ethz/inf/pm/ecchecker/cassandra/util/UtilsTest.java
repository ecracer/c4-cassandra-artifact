package ch.ethz.inf.pm.ecchecker.cassandra.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.junit.Test;

import ch.ethz.inf.pm.ecchecker.cassandra.util.Utils;
import scala.collection.mutable.StringBuilder;

public class UtilsTest {

	@Test
	public void testGetRandomLowercaseString() {
		final String[] randomStrings = new String[1000];
		for (int i = 0; i < randomStrings.length; i++) {
			randomStrings[i] = Utils.getRandomLowercaseString10(i);
		}
		for (int i = 0; i < randomStrings.length; i++) {
			assertTrue(randomStrings[i].matches("^[a-z]{10}$"));
			assertEquals(randomStrings[i], Utils.getRandomLowercaseString10(i));
		}
		assertEquals(randomStrings.length, new HashSet<>(Arrays.asList(randomStrings)).size());
	}

	@Test
	public void testConsumeAllPermutations() {
		final List<String> onePerm = new ArrayList<>();
		Utils.consumeAllPermutations(Collections.singleton("a"), p -> onePerm.addAll(p));
		assertEquals(1, onePerm.size());
		assertTrue(onePerm.contains("a"));

		final List<String> threePerm = new ArrayList<>();
		final List<String> list = new ArrayList<>();
		list.add("a");
		list.add("b");
		list.add("c");
		Utils.consumeAllPermutations(list, p -> {
			final StringBuilder sb = new StringBuilder();
			p.forEach(pe -> sb.append(pe));
			threePerm.add(sb.toString());
		});
		assertEquals(6, threePerm.size());
		assertTrue(threePerm.contains("abc"));
		assertTrue(threePerm.contains("acb"));
		assertTrue(threePerm.contains("bac"));
		assertTrue(threePerm.contains("bca"));
		assertTrue(threePerm.contains("cba"));
		assertTrue(threePerm.contains("cab"));
	}
}
