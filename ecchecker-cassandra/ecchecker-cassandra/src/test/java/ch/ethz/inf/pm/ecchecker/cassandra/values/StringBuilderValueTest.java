package ch.ethz.inf.pm.ecchecker.cassandra.values;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.google.common.collect.ImmutableList;

import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.Value;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.StringBuilderValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.UnknownMutableValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValueContainerValue;

public class StringBuilderValueTest {

	@Test
	public void testBasicStringFolder() {
		final Value value = StringBuilderValue.create(ImmutableList.of(
				StringValue.create("hello"),
				StringValue.create("world"),
				StringValueContainerValue.create(UnknownMutableValue.create()),
				StringValue.create("...")));

		final Value expected = StringBuilderValue.create(ImmutableList.of(
				StringValue.create("helloworld"),
				StringValueContainerValue.create(UnknownMutableValue.create()),
				StringValue.create("...")));
		assertEquals(expected, value);
	}

	@Test
	public void testNestedStringFolder() {
		final ConcreteValue inner = StringBuilderValue.create(ImmutableList.of(
				StringValue.create("hello"),
				StringValue.create("world")));
		final Value value = StringBuilderValue.create(ImmutableList.of(
				StringValueContainerValue.create(inner),
				StringValue.create("...")));

		final Value expected = StringBuilderValue.create(ImmutableList.of(StringValue.create("helloworld...")));
		assertEquals(expected, value);
	}
}
