package ch.ethz.inf.pm.ecchecker.cassandra.values.parsedstmts;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.CassandraParser;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnSelection;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnCounterUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnPartUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetValueUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnValueUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraint;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraint.EQValConstraint;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraint.INConstraint;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraint.INValsConstraint;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraint.UnknownConstraint;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraints;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ParsedCreateTableStatement;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ParsedStatement;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ParsedStatement.ParsedQueryStatement;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ParsedStatement.ParsedUpdateStatement;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.DeletePart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.QueryPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.SelectAllPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.SelectColsPart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.UpdatePart;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.UpsertPart;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractBind;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractBind.PartitionKeyTokenBind;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractBind.SystemBind;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.AbstractBind.UserBind;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.IntValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;

public class CassandraParserTest {

	private ParsedStatement parseSimpleQuery(final String query, final List<AbstractBind<ConcreteValue>> binds) {
		return new CassandraParser().parseStatementValue(StatementValue.create(query, binds));
	}

	@SuppressWarnings("unchecked")
	private <T extends ColumnUpdate> List<T> cols(final T... columns) {
		return ImmutableList.copyOf(columns);
	}

	private List<ColumnSelection> cols(final String... columns) {
		final ImmutableList.Builder<ColumnSelection> builder = ImmutableList.builder();
		for (final String column : columns) {
			builder.add(ColumnSelection.create(column));
		}
		return builder.build();
	}

	private List<Constraint> constraints(final Constraint... constraints) {
		return ImmutableList.copyOf(constraints);
	}

	@SafeVarargs
	private final List<AbstractBind<ConcreteValue>> binds(final AbstractBind<ConcreteValue>... binds) {
		return Arrays.asList(binds);
	}

	private void testSimpleStatement(final StatementPart stmtPart, final String query, final List<AbstractBind<ConcreteValue>> binds) {
		ParsedStatement stmt = parseSimpleQuery(query, binds);
		if (stmtPart instanceof UpdatePart) {
			assertEquals(ParsedUpdateStatement.create(StatementValue.create(query, binds).getQueryWithUnnamedBindMarkers(),
					(UpdatePart) stmtPart), stmt);
		} else {
			assertEquals(ParsedQueryStatement.create(StatementValue.create(query, binds).getQueryWithUnnamedBindMarkers(),
					(QueryPart) stmtPart), stmt);
		}
	}

	@Test
	public void testSelectStatements() {
		final UnknownImmutableValue widenedUnknown = UnknownImmutableValue
				.createWidened(ProgramPointId.create(CallStack.EMPTY_STACK, new Object()));
		final UnknownImmutableValue unknown = UnknownImmutableValue.create(ProgramPointId.create(CallStack.EMPTY_STACK, new Object()));

		testSimpleStatement(SelectAllPart.create("testtable",
				Constraints.create(EQValConstraint.create("key1", widenedUnknown), EQValConstraint.create("key2", IntValue.create(1)))),
				"SELECT * FROM testtable WHERE key1 = ? AND key2 = 1",
				binds(UserBind.create(IntValue.create(0), widenedUnknown)));

		testSimpleStatement(SelectAllPart.create("testtable",
				Constraints.create(EQValConstraint.create("key1", unknown), EQValConstraint.create("key2", StringValue.create("val1")))),
				"SELECT * FROM testtable WHERE key1 = :abcdef AND key2 = ?",
				binds(SystemBind.create("abcdef", unknown), UserBind.create(IntValue.create(0), StringValue.create("val1"))));

		testSimpleStatement(SelectAllPart.create("testtable", Constraints.create(UnknownConstraint.create("key"))),
				"SELECT * FROM testtable WHERE token(key) = ?",
				binds(PartitionKeyTokenBind.create(unknown)));

		testSimpleStatement(SelectAllPart.create("testtable",
				Constraints.create(EQValConstraint.create("key1", IntValue.create(1)), EQValConstraint.create("key2", IntValue.create(2)),
						EQValConstraint.create("key3", IntValue.create(3)),
						INValsConstraint.create("key4", ImmutableList.of(IntValue.create(4))))),
				"SELECT * FROM testtable WHERE key1 = :system1 AND key2 = ? AND key3 = :user2 AND key4 IN (:system2) ",
				binds(SystemBind.create("system1", IntValue.create(1)), SystemBind.create("system2", IntValue.create(4)),
						UserBind.create(IntValue.create(0), IntValue.create(2)), UserBind.create(IntValue.create(1), IntValue.create(24)),
						UserBind.create(IntValue.create(1), IntValue.create(3))));

		testSimpleStatement(SelectAllPart.create("testtable",
				Constraints.create(EQValConstraint.create("key1", IntValue.create(1)), EQValConstraint.create("key2", IntValue.create(2)),
						EQValConstraint.create("key3", IntValue.create(3)), INConstraint.create("key4"))),
				"SELECT * FROM testtable WHERE key1 = :system1 AND key2 = ? AND key3 = :user2 AND key4 IN :system2 ",
				binds(SystemBind.create("system1", IntValue.create(1)), SystemBind.create("system2", IntValue.create(4)),
						UserBind.create(IntValue.create(0), IntValue.create(2)), UserBind.create(IntValue.create(1), IntValue.create(24)),
						UserBind.create(IntValue.create(1), IntValue.create(3))));

		testSimpleStatement(SelectColsPart.create("testtable", cols("key", "val"), Constraints.create()),
				"SELECT key, val FROM testtable", binds());

		testSimpleStatement(SelectColsPart.create("testtable", cols("value1"),
				Constraints.create(EQValConstraint.create("key", IntValue.create(3)), UnknownConstraint.create("map"))),
				"SELECT value1 FROM testtable WHERE key = 3 AND map[3] = 4", binds());

		testSimpleStatement(SelectColsPart.create("testtable", cols("value1"),
				Constraints.create(EQValConstraint.create("key1", IntValue.create(3)), EQValConstraint.create("key2", IntValue.create(4)))),
				"SELECT value1 FROM testtable WHERE key1 = ? AND key2 = ?",
				binds(UserBind.create(StringValue.create("key2"), IntValue.create(4)),
						UserBind.create(StringValue.create("key1"), IntValue.create(3))));
	}

	@Test
	public void testInsertStatements() {

		testSimpleStatement(UpsertPart.createInsert("table1",
				cols(ColumnValueUpdate.create("col1", IntValue.create(1)), ColumnValueUpdate.create("col2", IntValue.create(2))), false),
				"INSERT INTO table1 (col1, col2) VALUES (1, 2) ", binds());

		testSimpleStatement(UpsertPart.createInsert("table1",
				cols(ColumnValueUpdate.create("col1", IntValue.create(1)), ColumnValueUpdate.create("col2", IntValue.create(2))), false),
				"INSERT INTO table1 (col1, col2) VALUES (?, ?) ", binds(UserBind.create(StringValue.create("col1"), IntValue.create(1)),
						UserBind.create(StringValue.create("col2"), IntValue.create(2))));
	}

	@Test
	public void testUpdateStatements() {
		testSimpleStatement(UpsertPart.createUpdate("table1",
				cols(ColumnValueUpdate.create("col1", IntValue.create(1)), ColumnValueUpdate.create("col2", StringValue.create("str"))),
				constraints(EQValConstraint.create("key", IntValue.create(2))), false, false),
				"UPDATE table1 SET col1 = 1, col2 = 'str' WHERE key = 2", binds());

		// viertes Update kann alles mögliche sein (es kann ein Set im Bind übergeben werden)
		testSimpleStatement(UpsertPart.createUpdate("table1",
				cols(ColumnCounterUpdate.create("counter1"), ColumnCounterUpdate.create("counter2"), ColumnCounterUpdate.create("counter3"),
						ColumnPartUpdate.create("counter4")),
				constraints(EQValConstraint.create("key", IntValue.create(2))), false, false),
				"UPDATE table1 SET counter1 = counter1 + 3, counter2 = counter2 - 10, counter3 = counter3 + ?,  counter4 = counter4 + ? WHERE key = 2",
				binds(UserBind.create(IntValue.create(0), IntValue.create(10))));

		testSimpleStatement(UpsertPart.createUpdate("table1",
				cols(ColumnSetValueUpdate.create("set1", ColumnSetUpdate.SetOp.ADD, IntValue.create(3)),
						ColumnSetValueUpdate.create("set2", ColumnSetUpdate.SetOp.REMOVE, IntValue.create(10)),
						ColumnSetUpdate.create("set3", ColumnSetUpdate.SetOp.REMOVE)),
				constraints(EQValConstraint.create("key", IntValue.create(2))), false, false),
				"UPDATE table1 SET set1 = set1 + {3}, set2 = set2 - {?}, set3 = set3 - {?} WHERE key = 2",
				binds(UserBind.create(IntValue.create(0), IntValue.create(10))));

		testSimpleStatement(UpsertPart.createUpdate("table1",
				cols(ColumnPartUpdate.create("list1"), ColumnPartUpdate.create("list2"),
						ColumnPartUpdate.create("list3")),
				constraints(EQValConstraint.create("key", IntValue.create(2))), false, false),
				"UPDATE table1 SET list1 = list1 + [3], list2 = list2 - [10], list3[3] = 20 WHERE key = 2", binds());

		testSimpleStatement(UpsertPart.createUpdate("table1",
				cols(ColumnValueUpdate.create("col1", IntValue.create(1))),
				constraints(EQValConstraint.create("key", IntValue.create(2))), true, false),
				"UPDATE table1 SET col1 = 1 WHERE key = 2 IF col1 = 0", binds());

		testSimpleStatement(UpsertPart.createUpdate("table1",
				cols(ColumnValueUpdate.create("col1", IntValue.create(1))),
				constraints(EQValConstraint.create("key", IntValue.create(2))), true, true),
				"UPDATE table1 SET col1 = 1 WHERE key = 2 IF EXISTS", binds());

		testSimpleStatement(UpsertPart.createUpdate("table1",
				cols(ColumnValueUpdate.create("col1", IntValue.create(1))),
				constraints(EQValConstraint.create("key", IntValue.create(2))), false, false),
				"UPDATE table1 SET col1 = TRUE WHERE key = 2", binds());
	}

	@Test
	public void testDeleteStatements() {
		testSimpleStatement(
				DeletePart.create("table1", Constraints.create(EQValConstraint.create("key", IntValue.create(2))), false, false),
				"DELETE FROM table1 WHERE key = 2", binds());

		testSimpleStatement(UpsertPart.createDelete("table1",
				cols(ColumnValueUpdate.create("col1", NullValue.create())),
				constraints(EQValConstraint.create("key", IntValue.create(2))), false, false),
				"DELETE col1 FROM table1 WHERE key = 2", binds());

		testSimpleStatement(UpsertPart.createDelete("table1",
				cols(ColumnPartUpdate.create("listcol"), ColumnPartUpdate.create("listcol")),
				constraints(EQValConstraint.create("key", IntValue.create(2))), false, false),
				"DELETE listcol[1], listcol[2] FROM table1 WHERE key = 2", binds());

		testSimpleStatement(DeletePart.create("table1", Constraints.create(EQValConstraint.create("key", IntValue.create(2))), true, false),
				"DELETE FROM table1 WHERE key = 2 IF val1 = 3", binds());
	}

	@Test
	public void testCreateTableStatements() {
		assertEquals(ParsedCreateTableStatement.create("table1", ImmutableSet.of("key"), ImmutableSet.of("val1", "val2")),
				CassandraParser.tryParseCreateTable("CREATE TABLE table1 (key text PRIMARY KEY, val1 uuid, val2 int)"));
		assertEquals(ParsedCreateTableStatement.create("table1", ImmutableSet.of("key"), ImmutableSet.of("val1", "val2")),
				CassandraParser.tryParseCreateTable("CREATE TABLE table1 (key text, val1 uuid, val2 int, PRIMARY KEY key) "));
		assertEquals(
				ParsedCreateTableStatement.create("table1", ImmutableSet.of("key1", "key2", "key3"), ImmutableSet.of("val1")),
				CassandraParser
						.tryParseCreateTable(
								"CREATE TABLE table1 (key1 text, key2 float, key3 bigint, val1 counter, PRIMARY KEY ((key1, key2), key3) "));
	}
}
