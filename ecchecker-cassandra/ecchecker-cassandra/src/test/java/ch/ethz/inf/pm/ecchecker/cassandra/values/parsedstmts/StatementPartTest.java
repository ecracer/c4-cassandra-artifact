package ch.ethz.inf.pm.ecchecker.cassandra.values.parsedstmts;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.google.common.collect.ImmutableList;

import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnPartUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetUpdate.SetOp;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetValueUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.ColumnUpdate.ColumnValueUpdate;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.Constraint.EQValConstraint;
import ch.ethz.inf.pm.ecchecker.cassandra.beans.parsedstmts.StatementPart.UpsertPart;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.IntValue;
import ch.ethz.inf.pm.ecchecker.cassandra.soot.values.concrete.immutable.NullValue;

public class StatementPartTest {

	@Test
	public void testUpsertStatements() {
		final UpsertPart delete = UpsertPart.createDelete("table1",
				ImmutableList.of(ColumnValueUpdate.create("col1", NullValue.create()),
						ColumnValueUpdate.create("col2", NullValue.create())),
				ImmutableList.of(EQValConstraint.create("key", IntValue.create(2))), false, false);
		assertEquals(false, delete.canInsert);

		final UpsertPart deletePart = UpsertPart.createDelete("table1",
				ImmutableList.of(ColumnPartUpdate.create("col1"),
						ColumnValueUpdate.create("col2", NullValue.create())),
				ImmutableList.of(EQValConstraint.create("key", IntValue.create(2))), false, false);
		assertEquals(false, deletePart.canInsert);

		final UpsertPart removeSetAndColumnElement = UpsertPart.createUpdate("table1",
				ImmutableList.of(ColumnValueUpdate.create("col1", NullValue.create()), ColumnSetUpdate.create("col2", SetOp.REMOVE),
						ColumnSetValueUpdate.create("col3", SetOp.REMOVE, IntValue.create(2))),
				ImmutableList.of(EQValConstraint.create("key", IntValue.create(2))), false, false);
		assertEquals(false, removeSetAndColumnElement.canInsert);

		final UpsertPart removeSetAddValue = UpsertPart.createUpdate("table1",
				ImmutableList.of(ColumnValueUpdate.create("col1", IntValue.create(2)), ColumnSetUpdate.create("col2", SetOp.REMOVE),
						ColumnSetValueUpdate.create("col3", SetOp.REMOVE, IntValue.create(2))),
				ImmutableList.of(EQValConstraint.create("key", IntValue.create(2))), false, false);
		assertEquals(true, removeSetAddValue.canInsert);
	}
}
