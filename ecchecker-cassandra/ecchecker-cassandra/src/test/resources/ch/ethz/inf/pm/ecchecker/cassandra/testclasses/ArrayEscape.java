import com.datastax.driver.core.Session;

public class ArrayEscape {
	
	public void test1(Session session, int k) {
		final String[] temp = new String[k];
		temp[0] = "SELECT * FROM test_1";
		session.execute(temp[0]);
	}
	
	public void test2(Session session, int k) {
		final String[] temp = new String[10];
		temp[k] = "SELECT * FROM test_2";
		session.execute(temp[k]);
	}
	
	public void test3(Session session, int k) {
		final String[] temp = new String[10];
		temp[0] = "SELECT * FROM test_3";
		final String t = temp[k];
		session.execute(temp[0]);
	}
	
	public void test4(Session session, int k) {
		final StringBuilder[] temp = new StringBuilder[10];
		temp[0] = new StringBuilder("SELECT * FROM test_4");
		final StringBuilder t = temp[k];
		session.execute(temp[0].toString());
	}
}