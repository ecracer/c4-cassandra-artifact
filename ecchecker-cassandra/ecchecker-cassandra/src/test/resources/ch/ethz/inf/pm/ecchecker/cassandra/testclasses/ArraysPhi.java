import com.datastax.driver.core.Session;

public class ArraysPhi {
	
	public void test1(Session session, int k) {
		final String query;
		if (k == 0){
			query = "SELECT * FROM test_1";
		} else {
			query = "SELECT * FROM test_1 WHERE k = " + k; 
		}
		final String[] queries = new String[]{query};
		session.execute(queries[0]);
	}
	
	public void test2(Session session, int k) {
		final String query;
		if (k == 0){
			query = "SELECT * FROM test_2";
		} else {
			query = "SELECT * FROM test_2 WHERE k = " + k; 
		}
		final Object[] queries;
		if (k == 0){
			queries = new Object[1];
			queries[0] = "SELECT * FROM test_2_1";
		} else {
			queries = new Object[2];
			queries[0] = "SELECT * FROM test_2_2";
		}
		// we do not know which ref is modified --> keep old versions
		queries[0] = query;
		session.execute((String) queries[0]);
	}
}