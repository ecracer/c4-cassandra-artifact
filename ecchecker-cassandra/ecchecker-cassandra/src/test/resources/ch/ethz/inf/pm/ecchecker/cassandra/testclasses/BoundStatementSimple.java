import com.datastax.driver.core.Session;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.ConsistencyLevel;

public class BoundStatementSimple {
	
	public void test(Session session) {
		final BoundStatement stmt = session.prepare("INSERT INTO test_table (col1, col2, col3, col4) VALUES (?, ?, ?, :val)").bind();
		stmt.bind("col1Val");
		final String bound1 = stmt.getString("name");
		stmt.setInt(1, 1024);
		stmt.setString("col3", "testVal");
		final String bound2 = stmt.getString("col3");
		stmt.setString("val", "namedParam");
		if (stmt.isSet(3) && stmt.getFetchSize() > 0){
			session.execute(stmt.enableTracing());
		}
	}
}