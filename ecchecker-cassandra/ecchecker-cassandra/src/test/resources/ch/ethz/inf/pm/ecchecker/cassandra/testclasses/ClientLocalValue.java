import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;
import com.datastax.driver.core.Session;

public class ClientLocalValue {

	private final Session session;
	
	public ClientLocalValue(final Session session){
		this.session = session;
	}
	
	@Transaction
	public void register(String username, String password){
		ClientLocalValues.set("username", username);
		final boolean userNotExists = session.execute("SELECT * FROM users WHERE username = ?", username).isExhausted();
		if (userNotExists){
			session.execute("INSERT INTO users (username, password) VALUES (?, ?)", username, password);
		}
	}
	
	@Transaction
	public void registerWithoutClientLocalValue(String username, String password){
		if (username.startsWith("a")){
			ClientLocalValues.set("username", username);
		}
		final boolean userNotExists = session.execute("SELECT * FROM users2 WHERE username = ?", username).isExhausted();
		if (userNotExists){
			session.execute("INSERT INTO users2 (username, password) VALUES (?, ?)", username, password);
		}
	}
	
	
	@Transaction
	public void setUserSetting(String setting){
		String username = ClientLocalValues.get("username"); 
		session.execute("INSERT INTO settings (username, setting) VALUES (?, ?)", username, setting);
	}
	
	@Transaction
	public void getUserSetting(){ 
		String username = ClientLocalValues.get("username");
		session.execute("SELECT settings FROM settings WHERE username = ?", username);
	}
}
