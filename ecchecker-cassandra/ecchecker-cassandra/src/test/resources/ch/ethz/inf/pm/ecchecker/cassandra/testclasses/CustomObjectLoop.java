import com.datastax.driver.core.Session;

public class CustomObjectLoop {
	
	public void test(Session session) {
		final Holder holder = new Holder("test");
		for (int i=0; i<20; i++){
			holder.set(String.valueOf(i));
		}
		session.execute("SELECT * FROM test_table WHERE i = ?", holder.constVal);
		session.execute("SELECT * FROM test_table WHERE i = ?", holder.changingVal);
	}
	
	public static class Holder {
		
		public String constVal;
		public String changingVal;
		
		public Holder(final String constVal){
			this.constVal = constVal;
		}
		
		public void set(final String changingVal){
			this.changingVal = changingVal;
		}
	}
}