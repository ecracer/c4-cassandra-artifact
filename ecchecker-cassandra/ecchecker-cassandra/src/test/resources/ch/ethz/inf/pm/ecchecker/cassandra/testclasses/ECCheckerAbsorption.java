import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import com.datastax.driver.core.Session;


public class ECCheckerAbsorption {

	private final Session session;
	
	public ECCheckerAbsorption(final Session session){
		this.session = session;
	}
	
	@Transaction
	// from KillrChat (UserResource#createUser)
	public void createUser(String username, String password){
		ClientLocalValues.set("username", username);
		session.execute("INSERT INTO users (username, password) VALUES (?, ?)", username, password);
	}
	
	@Transaction
	// from KillrChat (UserResource#findByLogin)
	public void getUser(String username){
		ClientLocalValues.set("username", username);
		session.execute("SELECT username, password FROM users WHERE username = ?", username);
	}
}
