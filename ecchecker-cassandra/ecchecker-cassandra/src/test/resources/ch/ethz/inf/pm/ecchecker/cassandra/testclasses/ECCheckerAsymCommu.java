import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import java.util.HashSet;
import java.util.UUID;

import com.datastax.driver.core.Session;


public class ECCheckerAsymCommu {

	private final Session session;
	
	public ECCheckerAsymCommu(final Session session){
		this.session = session;
	}
	
	@Transaction
	// from Playlist (LoginServlet#doPost)
	public void createUser(String username, String password){
		ClientLocalValues.set("username", username);
		session.execute("INSERT INTO users (username, password) VALUES (?, ?) IF NOT EXISTS", username, password);
	}
	
	@Transaction
	// from Playlist (PlaylistsServlet#doPost)
	public void deletePlaylist(String username, String playlistName){
		ClientLocalValues.set("username", username);
		if (!session.execute("SELECT * FROM users WHERE username = ?", username).isExhausted()){
			session.execute("DELETE FROM playlist_tracks WHERE username = ? AND playlist_name = ?", username, playlistName);
		}
	}
	
	@Transaction
	// from KillrChat (PlaylistsTrackServlet#doGet)
	public void getPlaylist(String username, String playlistName){
		ClientLocalValues.set("username", username);
        session.execute("SELECT * FROM playlist_tracks WHERE username = ? AND playlist_name = ?", username, playlistName);
	}
}
