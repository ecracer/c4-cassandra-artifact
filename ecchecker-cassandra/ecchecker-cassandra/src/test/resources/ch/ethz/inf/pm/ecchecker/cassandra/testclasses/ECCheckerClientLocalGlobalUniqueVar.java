import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import com.datastax.driver.core.Session;


public class ECCheckerClientLocalGlobalUniqueVar {

	private final Session session;
	
	public ECCheckerClientLocalGlobalUniqueVar(final Session session){
		this.session = session;
	}
	
	@Transaction
	// from Playlist (PlaylistTracksServlet#doGet)
	public void deleteTrack(String username, String playlistName, long sequenceNo){
		ClientLocalValues.set("username", username);
		session.execute("SELECT * FROM playlist_tracks WHERE username = ? AND playlist_name = ?", username, playlistName);
		session.execute("DELETE FROM playlist_tracks WHERE username = ? AND playlist_name = ? AND sequence_no = ?", username, playlistName, sequenceNo);
	}
	
}
