import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;


public class ECCheckerDisplayCode2 {
	
	private final Session session;
	
	public ECCheckerDisplayCode2(final Session session){
		this.session = session;
	}
	
	@Transaction
	// Playlist: PlaylistsServlet#doGet
	public void removePlaylist(final String username, final String playlistName){
		ClientLocalValues.set("username", username);
		session.execute("SELECT * /*!DISPLAY playlists !*/ FROM users WHERE username = ?", username);
		session.execute("UPDATE users SET playlists = playlists - {?} WHERE username = ?", playlistName, username);
		session.execute("DELETE FROM playlist_tracks WHERE username = ? AND playlist = ?", username, playlistName);
	}
	
	@Transaction
	//  Playlist: LoginServlet#doPost
	public void addUser(final String username, final String password){
		ClientLocalValues.set("username", username);
		session.execute("INSERT INTO users (username, password) VALUES (?, ?)", username, password);
	}
}
