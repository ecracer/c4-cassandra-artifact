import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;


public class ECCheckerLegalitySpec {
	
	private final Session session;
	
	public ECCheckerLegalitySpec(final Session session){
		this.session = session;
	}
	
	@Transaction
	// Playlist: PlaylistsServlet#doGet
	public void addPlaylist(final String username, final String playlist){
		if (!session.execute("SELECT * /*!DISPLAY playlist_names !*/ FROM users where username = ?", username).isExhausted()){
			session.execute("UPDATE users SET playlist_names = playlist_names + {?} WHERE username = ?", playlist, username);
		}
	}
	
	@Transaction
	// Playlist: LoginServlet#doLogin
	public void doLogin(final String username){
		session.execute("SELECT username, password FROM users where username = ?", username);
	}
}
