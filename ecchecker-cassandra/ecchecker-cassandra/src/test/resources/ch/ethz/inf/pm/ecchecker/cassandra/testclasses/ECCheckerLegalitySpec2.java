import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;


public class ECCheckerLegalitySpec2 {
	
	private final Session session;
	
	public ECCheckerLegalitySpec2(final Session session){
		this.session = session;
	}
	
	@Transaction
	// KillrChat: UserResource#createUser
	public void createUser(final String username, final String password){
		ClientLocalValues.set("username", username);
		session.execute("INSERT INTO users (login, password) VALUES (?, ?) IF NOT EXISTS", username, password);
	}
	
	@Transaction
	// KillrChat: UserResource#getRememberMeUser
	public void getUser(final String username){
		ClientLocalValues.set("username", username);
		session.execute("SELECT * /*!DISPLAY room_names !*/ FROM users WHERE login = ?", username);
	}
	
	@Transaction
	// KillrChat: KillrChat#addUserToChatRoom
	public void addUserToChatRoom(final String username, final String roomName){
		ClientLocalValues.set("username", username);
		session.execute("UPDATE users SET room_names = room_names + {?} WHERE login = ?", roomName, username);
	}
	
	@Transaction
	// KillrChat: KillrChat#removeUserFromChatRoom
	public void removeUserFromChatRoom(final String username, final String roomName){
		ClientLocalValues.set("username", username);
		session.execute("UPDATE users SET room_names = room_names - {?} WHERE login = ?", roomName, username);
	}
}
