import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import com.datastax.driver.core.Session;


public class ECCheckerPOConstraints {

	private final Session session;
	
	public ECCheckerPOConstraints(final Session session){
		this.session = session;
	}
	
	@Transaction
	// from Twitter (Twitter#register)
	public void register(String username, String password){
		ClientLocalValues.set("username", username);
        session.execute("INSERT INTO users (username, password) VALUES (?, ?)", username, password);
	}
	
	@Transaction
	// from Twitter (Twitter#follow)
	public void follow(String username, String followed){
		ClientLocalValues.set("username", username);
        if (!session.execute("SELECT username FROM users WHERE username = ?", username).isExhausted() && 
        		!session.execute("SELECT username FROM users WHERE username = ?", followed).isExhausted()){
        	session.execute("INSERT INTO followers (username, follower) VALUES (?, ?)", followed, username);
        }
	}
	
	@Transaction
	// from Twitter (Twitter#addTweet)
	public void addTweet(String username, String message){
		ClientLocalValues.set("username", username);
        session.execute("SELECT follower FROM followers WHERE username = ?", username);
	}
}
