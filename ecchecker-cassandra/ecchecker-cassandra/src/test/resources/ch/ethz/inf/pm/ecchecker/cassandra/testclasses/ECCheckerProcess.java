import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import com.datastax.driver.core.Session;


public class ECCheckerProcess {

	private final Session session;
	
	public ECCheckerProcess(final Session session){
		this.session = session;
	}
	
	@Transaction
	// from Playlist (LoginServlet#doPost)
	public void login(boolean newUser, String username, String password){
		if (newUser){
			session.execute("SELECT * FROM users WHERE username = ?", username);
			// validate login
		} else {
			session.execute("INSERT INTO users (username, password) VALUES (?, ?) IF NOT EXISTS", username, password);
		}
	}
}
