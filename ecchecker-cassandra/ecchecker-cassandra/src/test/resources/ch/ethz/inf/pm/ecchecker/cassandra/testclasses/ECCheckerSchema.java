import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;


public class ECCheckerSchema {
	
	private final Session session;
	
	public ECCheckerSchema(final Session session){
		this.session = session;
	}
	
	@Transaction
	// KillrChat: UserResource#createUser
	public void createUser(String username, String password){
		ClientLocalValues.set("username", username);
		session.execute("INSERT INTO users (username, password) VALUES (?, ?) IF NOT EXISTS", username, password);
	}
	
	@Transaction
	// KillrChat: UserResource#getUser
	public void getUser(String username){
		ClientLocalValues.set("username", username);
		session.execute("SELECT * FROM users WHERE username = ?", username);
	}
}
