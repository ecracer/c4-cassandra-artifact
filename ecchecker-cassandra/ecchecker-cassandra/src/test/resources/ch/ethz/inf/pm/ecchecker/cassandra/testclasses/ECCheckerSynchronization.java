import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import com.datastax.driver.core.Session;


public class ECCheckerSynchronization {

	private final Session session;
	
	public ECCheckerSynchronization(final Session session){
		this.session = session;
	}
	
	@Transaction
	public void register(String username, String password){
		session.execute("SELECT username FROM users WHERE username = ?", username);
			session.execute("INSERT INTO users (username, password) VALUES (?, ?) IF NOT EXISTS", username, password);
	}
}
