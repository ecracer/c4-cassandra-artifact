import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import java.util.Collections;

import com.datastax.driver.core.Session;


public class ECCheckerSynchronization2 {

	private final Session session;
	
	public ECCheckerSynchronization2(final Session session){
		this.session = session;
	}
	
	@Transaction
	// From KillrChat (ChatRoomResource#addUserToChatRoom)
	public void addUserToChatRoom(String username, String roomName){
		session.execute("UPDATE chat_rooms SET participants = participants + {?} WHERE room_name = ? IF EXISTS", username, roomName);
	}
	
	@Transaction
	// From KillrChat (ChatRoomResource#createChatRoom)
	public void createChatRoom(String username, String roomName){
		session.execute("INSERT INTO chat_rooms (room_name, participants) VALUES (?, ?) IF NOT EXISTS", roomName, Collections.singleton(username));
	}
	
	@Transaction
	// From KillrChat (ChatRoomResource#findRoomByName)
	public void findChatRoom(String roomName){
		session.execute("SELECT * FROM chat_rooms WHERE room_name = ?", roomName);
	}
}
