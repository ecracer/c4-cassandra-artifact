import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;


public class ECCheckerValueAnalysis {
	
	private final Session session;
	
	public ECCheckerValueAnalysis(final Session session){
		this.session = session;
	}
	
	@Transaction
	// Playlist: TrackServlet#doPost
	public void addTrack(String artistName, String trackName){
		session.execute("INSERT INTO track_by_id (id, artist, track) VALUES (?, ?, ?)", UUIDs.random(), artistName, trackName);
	}
	
	@Transaction
	// Playlist: PlaylistServlet#doPost
	public void getUser(String username){
		session.execute("SELECT * FROM users WHERE username = ?", username);
	}
	
	@Transaction
	// Playlist: LoginServlet#doPost
	public void addUser(String username){
		session.execute("INSERT INTO users (username) VALUES (?)", username);
	}
}
