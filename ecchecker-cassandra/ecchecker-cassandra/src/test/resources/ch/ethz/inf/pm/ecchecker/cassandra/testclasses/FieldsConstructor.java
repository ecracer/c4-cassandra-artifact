import com.datastax.driver.core.Session;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ConsistencyLevel;

public class FieldsConstructor {
	
	private Session session;
	private PreparedStatement stmt; 
	
	public FieldsConstructor(final Session session){
		this.session = session;
		prepareStatements();
	}	
	
	public void execute(){
		session.execute(stmt.bind());
	}
	
	private void prepareStatements(){
		stmt = session.prepare("SELECT * FROM test");
	}
}