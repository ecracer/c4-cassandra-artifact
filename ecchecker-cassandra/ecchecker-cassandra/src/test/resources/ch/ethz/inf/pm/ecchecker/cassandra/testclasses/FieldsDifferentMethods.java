import com.datastax.driver.core.Session;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ConsistencyLevel;

public class FieldsDifferentMethods {
	
	private final Session session;
	private final PreparedStatement query;
	private final PreparedStatement[] queries;
	private final Holder holder;
	
	public FieldsDifferentMethods(final Session session){
		this.session = session;
		query = session.prepare("SELECT * FROM test1");
		queries = new PreparedStatement[1];
		queries[0] = session.prepare("SELECT * FROM test2");
		holder = new Holder(session.prepare("SELECT * FROM test3"));
	}

	private static class Holder {
		
		public PreparedStatement statement;
		
		public Holder(PreparedStatement statement){
			this.statement = statement;
		}
	}
	
	
	public void setQuorumConsistencyLevel(){
		query.setConsistencyLevel(ConsistencyLevel.QUORUM);
		queries[0].setConsistencyLevel(ConsistencyLevel.QUORUM);
		holder.statement.setConsistencyLevel(ConsistencyLevel.QUORUM);
	}
	

	public void execute(){
		session.execute(query.bind());
		session.execute(queries[0].bind());
		session.execute(holder.statement.bind());
	}
}