import com.datastax.driver.core.Session;

public class FieldsSeveralInit {
	
	private String query;
	
	public void test1(Session session) {
		query = "SELECT * FROM test1";
		session.execute(query);
	}
	
	public void test2(Session session) {
		query = "SELECT * FROM test2";
		session.execute(query);
	}
	
	public void test3(Session session){
		query = "SELECT * FROM test3";
		session.execute(query);
	}
	
	public void test4(Session session){
		session.execute(query);
	}
}