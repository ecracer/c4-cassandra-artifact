import com.datastax.driver.core.Session;

public class FieldsSimple {
	
	private static Object obj = new Object();
	private static String field = "test_field";
	private String query;
	private String temp;
	
	public FieldsSimple(int k){
		temp = "blah";
		if (k == 0){
			query = "SELECT * FROM test_table";
		}
		else {
			query = "SELECT * FROM test_table WHERE k = " + k;
		}
	}
	
	public void test(Session session) {
		session.execute("" + query);
	}
}