import com.datastax.driver.core.Session;

public class FieldsStringBuilder {
	
	private StringBuilder query;
	private StringBuilder query2;
	
	public FieldsStringBuilder(){
		query = new StringBuilder("SELECT * FROM test1");
	}
	
	public void init() {
		query2 = query;
	}
	
	public void test1(Session session) {
		init();
		session.execute(query2.toString());
	}
	
	public void test2(Session session){
		session.execute(query2.toString() + " WHERE k=2");
	}
}