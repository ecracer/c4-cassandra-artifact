import com.datastax.driver.core.Session;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ConsistencyLevel;

public class FieldsWithArrays {
	
	private final Session session;
	private final PreparedStatement[] queries;
	
	public FieldsWithArrays(final Session session, boolean withQuorum){
		this.session = session;
		queries = new PreparedStatement[4];
		queries[0] = session.prepare("INSERT INTO test (key, val) VALUES (?, ?)");
		queries[1] = session.prepare("SELECT * FROM test WHERE key = ?");
		queries[2] = session.prepare("UPDATE test SET val = ? WHERE key = ?");
		queries[3] = session.prepare("DELETE FROM test WHERE key = ?");
		if (withQuorum){
			queries[0].setConsistencyLevel(ConsistencyLevel.QUORUM);
			queries[1].setConsistencyLevel(ConsistencyLevel.QUORUM);
			queries[2].setConsistencyLevel(ConsistencyLevel.QUORUM);
			queries[3].setConsistencyLevel(ConsistencyLevel.QUORUM);
		}
	}

	public void execute(String choice, int k, int v){
		switch (choice){
			case "create":
				create(k, v);
				break;
			case "read":
				read(k);
				break;
			case "update":
				update(k, v);
				break;
			case "delete":
				delete(k);
				break;
		}
	}
	
	private void create(int k, int v) {
		session.execute(queries[0].bind(k, v));
	}
	
	private void read(int k) {
		session.execute(queries[1].bind(k));
	}
	
	private void update(int k, int v) {
		session.execute(queries[2].bind(v, k));
	}
	
	private void delete(int k) {
		session.execute(queries[3].bind(k));
	}
}