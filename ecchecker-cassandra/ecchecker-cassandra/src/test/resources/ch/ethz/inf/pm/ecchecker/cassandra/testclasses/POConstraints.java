import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import com.datastax.driver.core.Session;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;


public class POConstraints {

	private final Session session;
	
	public POConstraints(final Session session){
		this.session = session;
	}
	
	@Transaction
	public void getUser(String username){
		ClientLocalValues.set("username", username);
		session.execute("SELECT username FROM users WHERE username = ?", username);
	}
	
	@Transaction
	public void addUser(String username, String password){
		ClientLocalValues.set("username", username);
		if (!session.execute("SELECT * FROM users WHERE username = ?", username).isExhausted()){
			session.execute("INSERT INTO users (username) VALUES (?)", username);
		}
	}
	
	@Transaction
	public void getFollowers(String username){
		ClientLocalValues.set("username", username);
		session.execute("SELECT * FROM followers WHERE username = ?", username);
	}
	
	@Transaction
	public void addFollower(String username, String follower){
		// works with SELECT * as username is the only column ever written on table users
		ClientLocalValues.set("username", username);
		if (!session.execute("SELECT * FROM users WHERE username = ?", username).isExhausted()){
			session.execute("INSERT INTO followers (username, follower) VALUES (?, ?)", username, follower);
		}
	}
}
