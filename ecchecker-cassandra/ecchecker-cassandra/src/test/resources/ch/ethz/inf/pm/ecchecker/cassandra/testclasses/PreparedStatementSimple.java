import com.datastax.driver.core.Session;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ConsistencyLevel;

public class PreparedStatementSimple {
	
	public void test(Session session, boolean quorum) {
		final PreparedStatement stmt = session.prepare("INSERT INTO test_table (col1, col2) VALUES (?,?)");
		if (quorum){
			stmt.setConsistencyLevel(ConsistencyLevel.QUORUM);
		}
		session.execute(stmt.bind("val1", "val2"));
	}
}