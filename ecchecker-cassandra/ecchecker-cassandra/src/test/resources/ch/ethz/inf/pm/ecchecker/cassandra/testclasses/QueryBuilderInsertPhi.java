import com.datastax.driver.core.Session;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Insert;

public class QueryBuilderInsertPhi {

	public void test(Session session, String username, String password){
		final Insert stmt;
		if (username != null){
			stmt = QueryBuilder.insertInto("user")
	        .value("username", username)
	        .value("password", password);
		} else {
			stmt = QueryBuilder.insertInto("blah").value("dummy", "3");
		}
		stmt.ifNotExists();
		session.execute(stmt);
	}
	
}
