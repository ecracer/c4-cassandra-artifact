import com.datastax.driver.core.Session;
import com.datastax.driver.core.querybuilder.QueryBuilder;

import java.util.Collections;

import com.datastax.driver.core.ConsistencyLevel;

public class QueryBuilderSimple {

	private final String insert1Table;
	private final Session session;
	private final String username;
	private final String password;
	
	public QueryBuilderSimple(final Session session, final String username, final String password){
		this.session = session;
		this.username = username;
		this.password = password;
		this.insert1Table = "insert1";
	}
	
	public void insert1(){
		session.execute(QueryBuilder.insertInto(insert1Table)
	            .value("username", username)
	            .value("password", password)
	            .ifNotExists());
	}
	
	public void select1(){
		session.execute(QueryBuilder.select().all()
                .from("select1")
                .where(QueryBuilder.eq("username", "alice")));
	}
	
	public void select2(){
		session.execute(QueryBuilder.
				select("col1", "col2").distinct().
				from("select2").
				where().and(QueryBuilder.in("key", 1, 2, 3)));
	}
	
	public void select3(){
		session.execute(QueryBuilder.
				select().countAll().
				from("select3").
				allowFiltering().
				setConsistencyLevel(ConsistencyLevel.ALL));
	}

	public void select4(){
		session.execute(QueryBuilder.
				select().
				column("col1").as("password").
				column("col2").
				column("col3").as("temp1").
				column("col4").as("temp2").
				from("select4").
				where(QueryBuilder.eq("col1", "alice")).and(QueryBuilder.lt("col3", 1000)).
				orderBy(QueryBuilder.desc("col4")).
				limit(10));
	}
	
	public void delete1(){
		session.execute(QueryBuilder.
				delete().
				from("delete1").
				where().and(QueryBuilder.eq("col1", "alice")).ifExists());
	}
	
	public void delete2(){
		session.execute(QueryBuilder.
				delete("col1", "col2").
				from("keyspace", "delete2").
				onlyIf(QueryBuilder.eq("col1", "bob")).and(QueryBuilder.gt("col2", 2000)).
				where(QueryBuilder.in("key", 1, 2, 3)).and(QueryBuilder.gte("col3", 100.22)));
	}
	
	public void delete3(){
		session.execute(QueryBuilder.
				delete().
				listElt("col1", 3).
				from("delete3"));
	}
	
	public void delete4(){
		session.execute(QueryBuilder.
				delete().
				all().
				from("delete4").
				where(QueryBuilder.eq("col1", 3)).
				and(QueryBuilder.in("col3", Collections.singletonList(2))));
	}
	
	public void update1(){
		session.execute(QueryBuilder.
				update("u", "update1").
				with().and(QueryBuilder.setIdx("collist", 1, "temp")).and(QueryBuilder.set("colval", "val2")).and(QueryBuilder.decr("colcounter")).
				where(QueryBuilder.eq("col1", "alice")).ifExists());
	}
	
	public void update2(){
		session.execute(QueryBuilder.
				update("update2").
				with(QueryBuilder.put("colmap", "key1", "value1")).
				onlyIf(QueryBuilder.eq("col1", "bob")).and(QueryBuilder.contains("col2", 2000)));
	}
}
