import com.datastax.driver.core.Session;

public class Recursion {
	
	public void test(Session session) {
		final Holder holder = new Holder();
		exec(session, holder);
	}
	
	public void exec(Session session, Holder holder){
		session.execute("SELECT * FROM test_table_0 WHERE i = " + holder.i);
		if (holder.i != 1){
			exec(session, holder);
		}
		holder.i = 1;
		session.execute("SELECT * FROM test_table_1 WHERE i = " + holder.i);
	}
	
	private static class Holder {
		public int i=0;
	}
}