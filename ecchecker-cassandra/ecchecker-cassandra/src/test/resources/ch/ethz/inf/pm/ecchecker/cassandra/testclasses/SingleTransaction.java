import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Cluster;

public class SingleTransaction {
	
	@Transaction
	public void method1(Session session, String key, boolean add){
		if (add){
			session.execute("INSERT INTO temp (key) VALUES (?)", key);
		} else {
			session.execute("SELECT key FROM temp WHERE key = ?", key);
		}
	}
}
