import com.datastax.driver.core.Session;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.utils.UUIDs;

import java.util.UUID;

import com.datastax.driver.core.ConsistencyLevel;

public class StatementWithUUID {
	
	public void test(Session session) {
		final UUID uuid1 = UUID.randomUUID();
		final UUID uuid2 = UUIDs.random();
		final UUID uuid3 = UUIDs.timeBased();
		
		UUID uuid4 = UUID.randomUUID();
		for (int j=0; j<10; j++){
			uuid4 = UUID.randomUUID();
			j++;
		}
		
		final PreparedStatement stmt = session.prepare("INSERT INTO test_table (key1, key2, key3, key4) VALUES (?,?,?,?)");
		session.execute(stmt.bind(uuid1, uuid2, uuid3, uuid4));
	}
}