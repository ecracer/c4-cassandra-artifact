import com.datastax.driver.core.Session;

public class StringAppendAfterUse {
	
	public void test(Session session, int i, int j) {
		String str = "SELECT * FROM test_table";
		session.execute(str);
		str = str + " WHERE j = " + j;
		session.execute(str);
	}
}