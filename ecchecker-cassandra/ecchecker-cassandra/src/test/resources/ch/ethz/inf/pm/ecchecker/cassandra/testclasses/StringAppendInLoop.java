import java.util.Random;
import java.util.HashMap;
import java.util.List;
import java.lang.Object;
import java.lang.StringBuilder;

public class StringAppendInLoop {
	
	private static final char[] ALPHANUMERIC_SYMBOLS = new char[] {'a', 'b'};
	private HashMap<Long, List<Object>> bucketMap;
	
	public String test(final int min, final int max) {
        // Fast implementation of random alphanumeric string generator.
		final Random random = new Random();
        final int stringLength = (min == max) ? max : random.nextInt(max - min) + min;
        final StringBuilder sb = new StringBuilder(stringLength);
        for (int length = 0; length < stringLength; length++) {
            sb.append(ALPHANUMERIC_SYMBOLS[random.nextInt(ALPHANUMERIC_SYMBOLS.length)]);
        }
        return sb.toString();
    }
}
