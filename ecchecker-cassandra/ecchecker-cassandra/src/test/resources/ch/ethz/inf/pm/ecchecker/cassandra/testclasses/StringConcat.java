import com.datastax.driver.core.Session;

public class StringConcat {

	private final static String TABLE_NAME = "test_table";
	
	public void test(Session session, int i, int j) {
		int k = 3;
		String str = "SELECT * FROM " + TABLE_NAME + " WHERE k = " + j + " AND i = " + i + " AND k = " + k;
		session.execute(str);
	}
}