import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Cluster;

public class TransactionsUsingStatic {
	
	@Transaction
	public void method1(Session session){
		A.call(session);
	}
	
	public void init(){
		A.init();
	}
	
	public static class A {
		private static Session session = null;
		
		public static void init() {
			Cluster cluster = Cluster.builder().addContactPoint("localhost").build();
		    session = cluster.connect("test");
		    session.execute("SELECT * FROM method1_1");
		}
		
		public static void call(Session session){
			session.execute("SELECT * FROM method1_2");
		}
	}
}
