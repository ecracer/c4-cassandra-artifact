import com.datastax.driver.core.Session;

public class TwoReturns {
	
	public void test(Session session, int i, int j) {
		final StringBuilder sb = buildStatement(i);
		sb.append(" WHERE j = " + j);
		session.execute(sb.toString());
	}
	
	public StringBuilder buildStatement(int i){
		if (i == 0){
			return new StringBuilder("SELECT * FROM test_table_1");
		} else {
			return new StringBuilder("SELECT * FROM test_table_2");
		}
	}
}