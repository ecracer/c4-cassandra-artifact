import com.datastax.driver.core.Session;

public class UnreachableExecutes {

	public void testNullInvoke(Session mayBeNonNull) {
		final Session session = null;
		String str = "SELECT * FROM test_table";
		session.execute(str);
		mayBeNonNull.execute(str);
	}
	
	public void testIfElse(Session session, int k) {
		String str = "SELECT * FROM testIfElse";
		String append = null;
		if (k == 0){
			append = " WHERE k = 0";
			str += append;
		} else {
			str += " WHERE k = " + append.substring(0, 1);
		}
		session.execute(str);
	}
}