import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import com.datastax.driver.core.Session;
import java.lang.RuntimeException;

public class WithExceptions {

	private final Session session;
	
	public WithExceptions(final Session session){
		this.session = session;
	}
	
	@Transaction
	public void method1(int i){
		session.execute("SELECT * FROM method1_1");
		try {
			if (i < 0){
				throw new MyException();
			}
			session.execute("SELECT * FROM method1_2");
		} catch (MyException e){
			
		}
		session.execute("SELECT * FROM method1_3");
	}
	
	@Transaction
	public void method2(int i){
		session.execute("SELECT * FROM method2_1");
		helper(i);
		session.execute("SELECT * FROM method2_3");
	}
	
	public void helper(int k) throws MyException{
		if (k < 0){
			throw new MyException();
		}
		session.execute("SELECT * FROM helper");
	}
	
	private static class MyException extends RuntimeException {
		private static final long serialVersionUID = 1L;
		
	}
}
