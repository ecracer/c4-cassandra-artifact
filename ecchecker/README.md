# C4 #

A static serializability checker for convergent causal consistency. C4 is designed to be independent of the data store, data store API, and programming language, and thus to serve as a basis for the analysis of any kind of system that satisfies convergence, atomic visibility and causal consistency. C4 can therefore be used as a back
end for a broad range of static analyses: in the context of distributed systems and databases it can be applied to weakly consistent mobile synchronization frameworks like TouchDevelop, to distributed databases like Antidote, Walter, CORS, and Eiger, and even to traditional relational databases with guarantees weaker than serializability but at least as strong as our model, e.g., Snapshot Isolation. Furthermore, it is useful as a bug-checker for clients of databases with weaker guarantees, for example Cassandra, Dynamo or Riak.

C4 is interfaced by static analysis front ends, which are 
 responsible for inferring a sound abstract history from application source code and providing a precise algebraic specication. We have implemented two front ends based on standard static analysis techniques, for TouchDevelop and Cassandra. The corresponding abstract histories are included
in this repository.


## License ##

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, you can obtain one at http://mozilla.org/MPL/2.0/.

## How to compile ##

Install `sbt` to compile. http://www.scala-sbt.org/

Install `z3` >=4.5 to run. https://github.com/Z3Prover/z3/releases

Type `sbt compile` to compile the code.

Type `sbt test` to test the code.

Type `sbt assembly` to produce a runnable JAR.

Type `sbt run` to run the tool.

The tool takes a JSON document from `stdin`. For an example of the format, see [this example](src/test/resources/putputgetget.json).