
// Main configuration file

name := "cfour"

version := "1.0"

scalaVersion := "2.11.8"

organization := "ch.ethz.inf.pm"

isSnapshot := true

libraryDependencies ++= Seq(
  "org.scalatest" % "scalatest_2.11" % "2.2.1", // Apache 2.0 license.
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0", // Apache 2.0 license.
  "ch.qos.logback" % "logback-classic" % "1.1.7", // EPL/LGPL dual-license
  "com.github.salat" %% "salat-core" % "1.11.0", // Apache 2.0 license.
  "com.github.salat" %% "salat-util" % "1.11.0", // Apache 2.0 license.
  "net.liftweb" %% "lift-json" % "2.6-RC2" // Apache 2.0 license.
)

assemblyJarName in assembly := "cfour.jar"
mainClass in assembly := Some("ch.ethz.inf.pm.cfour.Main")
