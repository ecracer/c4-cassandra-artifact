/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.output.Logger

object Main extends Logger {

  def main(args:Array[String]):Unit = {

    implicit var params = Parameters.default

    var unfoldBeforeAnalysis: Boolean = false

    args foreach {
      case "-println" =>
        params = params.copy(outputViaPrintln = true)
      case "-tsv" =>
        params = params.copy(outputTSV = true)
      case "-debug" =>
        params = params.copy(debug = true)
      case "-unfold" =>
        unfoldBeforeAnalysis = true
      case "-displayCode" =>
        params = params.copy(ignoreDisplayCode = true)
      case "-help" =>
        print(
          """
            | Takes a JSON document from `stdin`.
            |
            | Allowed commandline options:
            |
            |  -unfold        Unfolds the graph before analyzing it.
            |  -debug         Create extra debugging symbols in formula for better output, but slower solving
            |  -displayCode   Exclude display code from the analysis
            |  -tsv           Output data as TSV
            |  -println       Use stdout instead of the logging framework
            |
          """.stripMargin)
        sys.exit(0)
      case _ => ()
    }

    val inputGraph = AbstractHistory.fromJSON(io.Source.stdin.mkString)

    if (unfoldBeforeAnalysis) {
      val violations = Unfolder.unfoldAndCheck(inputGraph)
      for (v <- violations.set) {
        log_info(v.toString)
      }
    } else {
      log_info(Encoder.findViolations(inputGraph).toString)
    }

  }

}
