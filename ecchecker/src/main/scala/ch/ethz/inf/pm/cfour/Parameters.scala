/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour

object Parameters {
  val default = Parameters()

  var get: Parameters = default

  def set(p: Parameters): Parameters = {
    get = p
    p
  }
}

case class Parameters(
    strongCommutativityCheck: Boolean = false,
    onlyStrongCommutativityCheck: Boolean = false,
    resultGeneralizationCheck: Boolean = true,
    useUninterpretedSort: Boolean = false,
    useStaticSubsetMinimality: Boolean = true,
    encodeConstraints: Boolean = true,
    encodeAbsorption: Boolean = true,
    encodeProcesses: Boolean = true,
    encodeCommutativity: Boolean = true,
    encodeAsymmetricCommutativity: Boolean = true,
    encodeQuerySpecificAbsorption: Boolean = true,
    encodeAtomicSet: Boolean = false,
    encodeLegalitySpec: Boolean = true,
    encodeImpliedVisibility: Boolean = true,
    encodeSynchronizingOperations: Boolean = true,
    encodePrefixConsistency: Boolean = true,
    encodeNamedAtomicSets: Boolean = false,
    outputViaPrintln: Boolean = false,
    outputTSV: Boolean = false,
    debug: Boolean = false,
    unrollOnlyTwice: Boolean = true,
    noExport: Boolean = false,
    ignoreDisplayCode: Boolean = false,
    pointWiseCommutativity: Boolean = false,
    useShortNames: Boolean = true,
    useExtraCondition: Boolean = true,
    checkCommutativityAbsorptionWithSMT: Boolean = true,
    exportDirectory: Option[String] = Some("/tmp/cfour/")
)