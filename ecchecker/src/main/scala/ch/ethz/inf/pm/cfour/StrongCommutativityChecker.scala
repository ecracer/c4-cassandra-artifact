/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.ExprBuilder.{not, _}
import ch.ethz.inf.pm.cfour.Z3Prover.{Sat, Unknown, Unsat}
import ch.ethz.inf.pm.cfour.output.Logger

object StrongCommutativityChecker extends Logger {

  def strongCommutativityCheck(graph: AbstractHistory)(implicit param: Parameters): Boolean = {

    implicit val g = graph

    for (u1 <- graph.updateOps;
         u2 <- graph.updateOps if (u1 commutesWith u2) != True;
         q <- graph.queryOps if (u2 commutesWith q) != True) yield {

      val u1e = Event("u1e", "t", u1.id)
      val u2e = Event("u2e", "t", u2.id)
      val qe = Event("qe", "t", q.id)

      Z3Prover.withZ3[Unit, Expr, Var]({ z3: Z3Prover[Expr, Var] =>

        val expr = (u1e commutesWith qe) and not(u1e commutesWith u2e) and
          not(u2e commutesWith qe) and not(u1e absorbedBy u2e)

        z3.assume(expr)
        z3.check() match {
          case Sat =>
            println(s"WARNING: Found counter-example to transitive conflict property:")
            println(s"  $u1")
            println(s"  $u2")
            println(s"  $q")

            def E(e: Expr) = EventGraphZ3Converter(param).encode(e)

            println(z3.extractModel().map(x => x._1 + " = " + x._2).toList.sorted.mkString("\n"))
            println("We assumed u com q  " + E(u1e commutesWith qe))
            println("We assumed u ncom v " + E(not(u1e commutesWith u2e)))
            println("We assumed v ncom q " + E(not(u2e commutesWith qe)))
            println("We assumed u nabs v " + E(not(u1e absorbedBy u2e)))
            return false
          case Unsat => ()
          /* all good */
          case Unknown =>
            println(s"WARNING: Could not check transitive conflict property")
            return false
        }

      }, EventGraphZ3Converter(param))
    }

    true
  }

}
