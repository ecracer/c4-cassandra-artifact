/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour

case class SystemSpecification (

    operations:List[Operation],

    // Must specify commutativity between all operations.
    // You can leave out pairs where the first operation
    // is not an update. If both are updates, you may
    // omit it if the first operation has a larger (alphabetical)
    // ID than the second (due to symmetry)
    commutativitySpecs:Map[String,Expr],

    // Must specify absorption between all updates
    // For a pair (a,b) give the formula that is true if a is absorbed by b.
    absorptionSpecs: Map[String, Expr],

    // EXPERIMENTAL: May specify constraints under which for operations (a,b) we have
    // forall X,Y,  XabY legal => XbaY legal; but not forall X,Y, XbaY legal => XabY legal
    asymmetricCommutativity: Map[String, Expr] = Map.empty,

    // EXPERIMENTAL: May specify constraints under which for operations (u1,u2,q) we have
    // forall W,X,Y,Z, Wu1Xu2YqZ legal <=> WXu2YqZ legal
    querySpecificAbsorption: Map[String, Expr] = Map.empty,

    // EXPERIMENTAL: May specify constraints under which for operations (o1,o2) we have
    // o1 -ca-> o2 or o2 -ca-> o1 guaranteed by the system
    synchronizationSpec: Map[String, Expr] = Map.empty,

    // EXPERIMENTAL: May specify constraints which have to hold for any pair (o1,o2) whenever
    // o1 -ca-> o2
    legalitySpec: Map[String, Expr] = Map.empty,

    // EXPERIMENTAL: May specify constraints under which visibility is implied between two operations
    impliedVisibility: Map[String, Expr] = Map.empty,

    // EXPERIMENTAL: Here we make operations operating on row indexes commutative
    // Must specify commutativity between all operations.
    // You can leave out pairs where the first operation
    // is not an update. If both are updates, you may
    // omit it if the first operation has a larger (alphabetical)
    // ID than the second (due to symmetry)
    pointWiseCommutativitySpec: Map[String, Expr] = Map.empty,

    // EXPERIMENTAL
    atomicSets: List[AtomicSetSpec] = Nil,

    // EXPERIMENTAL
    namedAtomicSets: Map[String, AtomicSetPredicateList] = Map.empty

)

case class AtomicSetPredicateList(
    list: List[AtomicSetPredicate]
)

case class AtomicSetSpec(
    freeVars: List[FreeVar] = Nil,
    operations: Map[String, Expr] = Map.empty
) {

  def join(other: AtomicSetSpec): AtomicSetSpec = {
    AtomicSetSpec((freeVars.toSet union other.freeVars.toSet).toList, (operations.toSet union other.operations.toSet).toMap)
  }

}

object SystemSpecification {

  import ExprBuilder._

  lazy val ReadWriteRegister =
    new SystemSpecification(
      operations = List( Query("rx"), Query("ry"), Update("wx"), Update("wy")  ),
      commutativitySpecs = Map(
        "wx,rx" -> False,
        "wx,ry" -> True,
        "wx,wx" -> Equal(ArgLeft(0, IntSort), ArgRight(0, IntSort)),
        "wx,wy" -> True,
        "wy,ry" -> False,
        "wy,rx" -> True,
        "wy,wy" -> Equal(ArgLeft(0, IntSort), ArgRight(0, IntSort))
      ),
      absorptionSpecs = Map(
        "wx,wx" -> True,
        "wx,wy" -> False,
        "wy,wx" -> False,
        "wy,wy" -> True
      )
    )

  lazy val Table: SystemSpecification = {
    new SystemSpecification(
      operations = List(Update("add"), Update("del"), Query("exists"), Query("all")),
      commutativitySpecs = Map(
        "add,add" -> True,
        "add,del" -> (ArgLeft(0, StringSort) unequal ArgRight(0, StringSort)),
        "add,exists" -> (ArgLeft(0, StringSort) unequal ArgRight(0, StringSort)),
        "del,exists" -> (ArgLeft(0, StringSort) unequal ArgRight(0, StringSort)),
        "add,all" -> False,
        "del,all" -> False,
        "del,del" -> True
      ),
      absorptionSpecs = Map(
        "add,add" -> (ArgLeft(0, StringSort) equal ArgRight(0, StringSort)),
        "add,del" -> (ArgLeft(0, StringSort) equal ArgRight(0, StringSort)),
        "del,add" -> (ArgLeft(0, StringSort) equal ArgRight(0, StringSort)),
        "del,del" -> (ArgLeft(0, StringSort) equal ArgRight(0, StringSort))
      ),
      atomicSets = List(
        AtomicSetSpec(
          freeVars = List(FreeVar(0, StringSort)),
          operations = Map(
            "add" -> (ArgLeft(0, StringSort) equal FreeVar(0, StringSort)),
            "del" -> (ArgLeft(0, StringSort) equal FreeVar(0, StringSort)),
            "exists" -> (ArgLeft(0, StringSort) equal FreeVar(0, StringSort)),
            "all" -> True
          )
        )
      )
    )


  }

  lazy val PutGetIncClearMap: SystemSpecification = {
    val leftKey = ArgLeft(0, StringSort)
    val rightKey = ArgRight(0, StringSort)
    val leftVal = ArgLeft(1, IntSort)
    val rightVal = ArgRight(1, IntSort)
    val zero = IntConst(0)
    
    new SystemSpecification(
      operations = List(Query("get"), Update("put"), Update("inc"), Update("clear"), Update("put_if_absent")),
      commutativitySpecs = Map(
        "clear,clear" -> True,
        "clear,get" -> False,
        "clear,inc" -> (rightVal equal zero),
        "clear,put" -> (rightVal equal zero),
        "clear,put_if_absent" -> (rightVal equal zero),
        "inc,get" -> ((leftKey unequal rightKey) or (leftVal equal zero)),
        "inc,inc" -> True,
        "inc,put" -> ((leftKey unequal rightKey) or (leftVal equal zero)),
        "inc,put_if_absent" -> ((leftKey unequal rightKey) or (leftVal equal zero) or (rightVal equal zero)),
        "put,get" -> (leftKey unequal rightKey),
        "put,put" -> ((leftKey unequal rightKey) or (leftVal equal rightVal)),
        "put,put_if_absent" -> ((leftKey unequal rightKey) or (leftVal equal rightVal) or (rightVal equal zero)),
        "put_if_absent,get" -> ((leftKey unequal rightKey) or (leftVal equal zero)),
        "put_if_absent,put_if_absent" -> ((leftKey unequal rightKey) or (leftVal equal zero) or (rightVal equal zero) or (leftVal equal rightVal))
      ),
      absorptionSpecs = Map(
        "clear,clear" -> True,
        "clear,put" -> False,
        "clear,put_if_absent" -> False,
        "clear,inc" -> False,
        "put,clear" -> True,
        "put,inc" -> False,
        "put,put" -> (leftKey equal rightKey),
        "put,put_if_absent" -> False,
        "put_if_absent,clear" -> True,
        "put_if_absent,inc" -> (leftVal equal zero),
        "put_if_absent,put" -> (leftKey equal rightKey),
        "put_if_absent,put_if_absent" -> (leftVal equal zero),
        "inc,clear" -> True,
        "inc,inc" -> (leftVal equal zero),
        "inc,put" -> ((leftKey equal rightKey) or (leftVal equal zero)),
        "inc,put_if_absent" -> (leftVal equal zero)
      ),
      asymmetricCommutativity = Map(
        "get,put" -> (leftVal equal rightVal),
        "get,clear" -> (leftVal equal zero),
        "put,put_if_absent" -> True
      ),
      querySpecificAbsorption = Map(
        "clear,put,get" -> (ArgMiddle(0, StringSort) equal rightKey),
        "clear,put_if_absent,get" -> (ArgMiddle(0, StringSort) equal rightKey)
      )
    )
  }


}
