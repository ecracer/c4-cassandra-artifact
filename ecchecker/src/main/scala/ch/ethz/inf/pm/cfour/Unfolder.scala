/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.AbstractHistory.{EventID, TransactionID}
import ch.ethz.inf.pm.cfour.TerminationChecker.{ClientTransition, DoubleClientTransition, SingleClientTransition}
import ch.ethz.inf.pm.cfour.output.{DumpGraph, LabeledGraph, Logger, Statistics}

import scala.util.Random

object Unfolder extends Logger {

  def unfoldAndQuickCheck(inputGraph: AbstractHistory)(implicit param: Parameters): ViolationSet = {

    Cache.reset()
    Statistics.reset()

    implicit val graph = preprocess(inputGraph.transitivelyCloseTransactionOrder)

    if (param.strongCommutativityCheck) {
      log("System sanity check", { () =>
        val res = StrongCommutativityChecker.strongCommutativityCheck(graph)
        if (!res) {
          throw new IllegalArgumentException("We require the strong commutativity check  to pass")
        } else if (param.onlyStrongCommutativityCheck) {
          return ViolationSet(Set.empty)
        }
      })
    }

    var violations =
      log("Checking all unfoldings in polynomial", { () =>
        findViolationsQuick(graph)
      }, logTimeToTSV = true, terminatingTSVEntry = !param.resultGeneralizationCheck)


    if (param.resultGeneralizationCheck) {
      log("Checking if result generalized to arbitrary clients", { () =>
        val result = TerminationChecker.terminationCheck(graph, violations)
        violations = violations.copy(generalizesToArbitraryClients = result)
      }, logTimeToTSV = true, terminatingTSVEntry = true)
    }

    violations
  }

  /**
    * @param inputGraph An input graph with potentially cyclic transaction and program order. Assumes that
    *                   all transactions are connected by transaction order, and all events of a transaction
    *                   are connected by program order
    * @return
    */
  def unfoldAndCheck(inputGraph: AbstractHistory)(implicit param: Parameters): ViolationSet = {

    Cache.reset()
    Statistics.reset()

    implicit val graph = preprocess(inputGraph.transitivelyCloseTransactionOrder)

    if (param.strongCommutativityCheck) {
      log("Strong Commutativity Check", { () =>
        val res = StrongCommutativityChecker.strongCommutativityCheck(graph)
        if (!res) {
          throw new IllegalArgumentException("We require the strong commutativity check  to pass")
        } else if (param.onlyStrongCommutativityCheck) {
          return ViolationSet(Set.empty)
        }
      })
    }

    var violations =
      log("Checking all unfoldings", { () =>
        findViolations(graph)
      }, logTimeToTSV = true, terminatingTSVEntry = !param.resultGeneralizationCheck)


    if (param.resultGeneralizationCheck) {
      log("Checking if result generalized to arbitrary clients", { () =>
        val result = TerminationChecker.terminationCheck(graph, violations)
        violations = violations.copy(generalizesToArbitraryClients = result)
      }, logTimeToTSV = true, terminatingTSVEntry = true)
    }

    violations
  }

  def preprocess(g: AbstractHistory)(implicit param: Parameters): AbstractHistory = {

    // Prune display code
    val prunedGraph =
      if (param.ignoreDisplayCode && g.events.exists(_.displayCode)) {
        g.pruneEvents(_.displayCode)
      } else g

    // Unroll program order
    val unrolledG = unrollProgramOrder(prunedGraph)

    unrolledG
  }

  private def findViolations(g: AbstractHistory)(implicit param: Parameters): ViolationSet = {
    var violationSet = ViolationSet(Set.empty)

    implicit val graph = g

    // Strips client identifier from name
    def C(txn: TransactionID): TransactionID = {
      ClientNameMaker.deconstruct(txn)._1
    }

    def CE(txn: ToEdge): ToEdge = {
      txn.stripClient
    }

    // create two copies of the graph
    val clientA = g.instantiateToClient(0, Some(1)).transitivelyCloseTransactionOrder
    val clientB = g.instantiateToClient(1, Some(0)).transitivelyCloseTransactionOrder

    // two transactions
    for (

      a <- clientA.transactions;
      b <- clientB.transactions.sorted
      if a <= b
        && SummaryCycleChecker.mayHaveViolations(graph, C(a), C(b))

      ) yield {

      if (!violationSet.hasSubViolation(C(a), C(b))) {
        Statistics.possibleViolationsSize2 += 1
        val problemName = s"2_${a}_$b".replaceAll("[^\\w\\.]+", "")

        val result = {
          val restrictedA = clientA.restrictToTransaction(a)
          val restrictedB = clientB.restrictToTransaction(b)
          Encoder.findViolations(restrictedA ++ restrictedB, Some(problemName))
        }

        if (result.isDefined) {
          Statistics.verifiedViolationsSize2 += 1
          violationSet += Violation2(C(a), C(b))(result.get, problemName)
        } else None
      } else {
        Statistics.totalVerifiedViolationsSize2 += 1
      }

      Statistics.totalPossibleViolationsSize2 += 1
    }


    // three transactions
    for (

      a <- clientA.transactionOrder;
      b <- clientB.transactions
      if SummaryCycleChecker.mayHaveViolations(graph, CE(a), C(b))

    ) {

      if (!violationSet.hasSubViolation((C(a.sourceID), C(a.targetID)), C(b))) {
        Statistics.possibleViolationsSize3 += 1
        val problemName = s"3_${a.sourceID}_${a.targetID}_$b".replaceAll("[^\\w\\.]+", "")

        val result = {
          val restrictedA = clientA.restrictAndUnfoldTransactionEdge(a)
          val restrictedB = clientB.restrictToTransaction(b)
          Encoder.findViolations(restrictedA ++ restrictedB, Some(problemName))
        }

        if (result.isDefined) {
          Statistics.verifiedViolationsSize3 += 1
          violationSet += Violation3(CE(a).sourceID, CE(a).targetID, C(b))(result.get, problemName)
        } else {
          None
        }
      } else {
        Statistics.totalVerifiedViolationsSize3 += 1
      }

      Statistics.totalPossibleViolationsSize3 += 1

    }

    for (

      a <- clientA.transactionOrder;
      b <- clientB.transactionOrder;
      cA = CE(a);
      cB = CE(b)
      if (cA.sourceID < cB.sourceID || (cA.sourceID == cB.sourceID && cA.targetID <= cB.targetID))
        && SummaryCycleChecker.mayHaveViolations(graph, cA, cB)
    ) yield {

      if (!violationSet.hasSubViolation((cA.sourceID, cA.targetID), (cB.sourceID, cB.targetID))) {

        Statistics.possibleViolationsSize4 += 1
        val problemName = s"4_${a.sourceID}_${a.targetID}_${b.sourceID}_${b.targetID}".replaceAll("[^\\w\\.]+", "")

        val result = {
          val restrictedA = clientA.restrictAndUnfoldTransactionEdge(a)
          val restrictedB = clientB.restrictAndUnfoldTransactionEdge(b)
          Encoder.findViolations(restrictedA ++ restrictedB, Some(problemName))
        }

        if (result.isDefined) {
          Statistics.verifiedViolationsSize4 += 1
          Statistics.totalVerifiedViolationsSize4 += 1
          violationSet += Violation4(cA.sourceID, cA.targetID, cB.sourceID, cB.targetID)(result.get, problemName)
        } else {
          None
        }

      } else {
        Statistics.totalVerifiedViolationsSize4 += 1
      }

      Statistics.totalPossibleViolationsSize4 += 1

    }

    Statistics.dump()

    violationSet
  }

  private def findViolationsQuick(g: AbstractHistory)(implicit param: Parameters): ViolationSet = {
    var violationSet = ViolationSet(Set.empty)
    implicit val graph = g

    // Strips client identifier from name
    def C(txn: TransactionID): TransactionID = {
      ClientNameMaker.deconstruct(txn)._1
    }

    def CE(txn: ToEdge): ToEdge = {
      txn.stripClient
    }

    // create two copies of the graph
    val clientA = g.instantiateToClient(0, Some(1)).transitivelyCloseTransactionOrder
    val clientB = g.instantiateToClient(1, Some(0)).transitivelyCloseTransactionOrder

    // two transactions
    for (

      a <- clientA.transactions;
      b <- clientB.transactions.sorted
      if a <= b
        && SummaryCycleChecker.mayHaveViolations(graph, C(a), C(b))

    ) {

      if (!violationSet.hasSubViolation(C(a), C(b))) {
        Statistics.possibleViolationsSize2 += 1
        val restrictedA = clientA.restrictToTransaction(a)
        val restrictedB = clientB.restrictToTransaction(b)
        Statistics.verifiedViolationsSize2 += 1
        val problemName = s"2_${a}_$b"
        violationSet += Violation2(C(a), C(b))(SummaryCycleChecker.makeGraph(restrictedA ++ restrictedB), problemName)
      } else {
        Statistics.totalVerifiedViolationsSize2 += 1
      }

      Statistics.totalPossibleViolationsSize2 += 1
    }


    // three transactions
    for (

      a <- clientA.transactionOrder;
      b <- clientB.transactions
      if SummaryCycleChecker.mayHaveViolations(graph, CE(a), C(b))

    ) {

      if (!violationSet.hasSubViolation((C(a.sourceID), C(a.targetID)), C(b))) {

        val problemName = s"3_${a.sourceID}_${a.targetID}_$b"
        Statistics.possibleViolationsSize3 += 1
        val restrictedA = clientA.restrictAndUnfoldTransactionEdge(a)
        val restrictedB = clientB.restrictToTransaction(b)
        Statistics.verifiedViolationsSize3 += 1
        violationSet += Violation3(CE(a).sourceID, CE(a).targetID, C(b))(SummaryCycleChecker.makeGraph(restrictedA ++ restrictedB), problemName)

      } else {
        Statistics.totalVerifiedViolationsSize3 += 1
      }

      Statistics.totalPossibleViolationsSize3 += 1

    }

    for (

      a <- clientA.transactionOrder;
      b <- clientB.transactionOrder;
      cA = CE(a);
      cB = CE(b)
      if (cA.sourceID < cB.sourceID || (cA.sourceID == cB.sourceID && cA.targetID <= cB.targetID))
        && SummaryCycleChecker.mayHaveViolations(graph, cA, cB)
    ) {

      if (!violationSet.hasSubViolation((cA.sourceID, cA.targetID), (cB.sourceID, cB.targetID))) {

        val problemName = s"4_${a.sourceID}_${a.targetID}_${b.sourceID}_${b.targetID}"
        Statistics.possibleViolationsSize4 += 1
        val restrictedA = clientA.restrictAndUnfoldTransactionEdge(a)
        val restrictedB = clientB.restrictAndUnfoldTransactionEdge(b)
        Statistics.verifiedViolationsSize4 += 1
        Statistics.totalVerifiedViolationsSize4 += 1
        violationSet += Violation4(cA.sourceID, cA.targetID, cB.sourceID, cB.targetID)(SummaryCycleChecker.makeGraph(restrictedA ++ restrictedB), problemName)

      } else {
        Statistics.totalVerifiedViolationsSize4 += 1
      }

      Statistics.totalPossibleViolationsSize4 += 1

    }

    Statistics.dump()

    violationSet
  }

  def unrollOneSCCinPO2Copy(g: AbstractHistory)(implicit param: Parameters): Option[AbstractHistory] = {

    def copy(x: EventID, i: Int) = x + "_" + i

    for (txn <- g.transactions) {

      val localEdges = g.programOrderByTransaction.getOrElse(txn, Nil).toSet
      val aG = g.programOrderView(txn)
      val (sccs, discovery, finish) = aG.sccAndDFSTree

      val backEdges = localEdges.filter { x =>
        val srcDisc = discovery(x.sourceID)
        srcDisc >= discovery(x.targetID) && srcDisc <= finish(x.targetID)
      }

      if (backEdges.nonEmpty) {
        for (scc <- sccs) {

          val sourceEdges = localEdges filter {
            scc contains _.sourceID
          }
          val targetEdges = localEdges filter {
            scc contains _.targetID
          }
          val innerEdges = sourceEdges intersect targetEdges
          val innerBackEdges = innerEdges intersect backEdges
          val incomingEdges = targetEdges diff innerEdges
          val outgoingEdges = sourceEdges diff innerEdges

          if (innerBackEdges.nonEmpty) {
            val remainingEdges = innerEdges -- backEdges

            val nodesCopies =
              (for (i <- 1 to 2) yield {
                scc.map(x => g.eventMap(x).copy(id = copy(x, i)))
              }).flatten

            val insideCopies =
              (for (i <- 1 to 2) yield {
                for (edge <- remainingEdges) yield edge.copy(sourceID = copy(edge.sourceID, i), targetID = copy(edge.targetID, i))
              }).flatten

            val enteringCopies =
              for (edge <- incomingEdges) yield edge.copy(sourceID = edge.sourceID, targetID = copy(edge.targetID, 1))

            val exitingCopies =
              (for (i <- 1 to 2) yield {
                for (edge <- outgoingEdges) yield edge.copy(sourceID = copy(edge.sourceID, i), targetID = edge.targetID)
              }).flatten

            // Special case for infinite loops. Here, the last copy gets an edge to the exit of t
            val specialEdges =
              if (outgoingEdges.isEmpty) {
                for (back1 <- innerBackEdges.map(_.sourceID)) yield {
                  PoEdge(copy(back1, 2), g.sink(txn)) // Remove invariant, all combinations of source/target
                }
              } else Nil

            val enteringViaBackEdges =
              (for (backTgt <- innerBackEdges.map(_.targetID)) yield {
                for (inSrc <- incomingEdges.map(_.sourceID)) yield {
                  PoEdge(inSrc, copy(backTgt, 1)) // Remove invariant, all combinations of source/target
                }
              }).flatten

            val exitingViaBackEdges =
              (for (backSrc <- innerBackEdges.map(_.sourceID)) yield {
                for (outTgt <- outgoingEdges.map(_.targetID)) yield {
                  PoEdge(copy(backSrc, 2), outTgt) // Remove invariant, all combinations of source/target
                }
              }).flatten

            val betweenCopies =
              (for (backSrc <- innerBackEdges.map(_.sourceID)) yield {
                for (backTgt <- innerBackEdges.map(_.targetID)) yield {
                  PoEdge(copy(backSrc, 1), copy(backTgt, 2)) // Remove invariant, all combinations of source/target
                }
              }).flatten

            // filter double edges
            val poToAdd = {
              val origSet = (insideCopies ++ enteringCopies ++ specialEdges ++ exitingCopies ++
                enteringViaBackEdges ++ exitingViaBackEdges ++ betweenCopies).toSet
              val doubledEdges = origSet.groupBy { x => (x.sourceID, x.targetID) }.values.filter(_.size > 1)
              var prunedSet: Set[PoEdge] = origSet
              for (s <- doubledEdges) {
                prunedSet = (prunedSet -- s) + s.head.copy(constraint = True)
              }
              prunedSet
            }

            return Some(g.extendAndReduce(
              eventsToAdd = nodesCopies.toList,
              poToAdd = poToAdd.toList,
              eventsToRemove = scc.map(g.eventMap(_)),
              poToRemove = sourceEdges ++ targetEdges))
          }

        }

      }

    }

    None

  }

  def unrollOneSCCinPO4Copy(g: AbstractHistory)(implicit param: Parameters): Option[AbstractHistory] = {

    def copy(x: EventID, i: Int) = x + "c_" + i

    for (txn <- g.transactions) {

      val localEdges = g.programOrderByTransaction.getOrElse(txn, Nil).toSet
      val aG = g.programOrderView(txn)
      val (sccs, discovery, finish) = aG.sccAndDFSTree

      val backEdges = localEdges.filter { x =>
        val srcDisc = discovery(x.sourceID)
        srcDisc >= discovery(x.targetID) && srcDisc <= finish(x.targetID)
      }

      if (backEdges.nonEmpty) {
        for (scc <- sccs) {

          val sourceEdges = localEdges filter {
            scc contains _.sourceID
          }
          val targetEdges = localEdges filter {
            scc contains _.targetID
          }
          val innerEdges = sourceEdges intersect targetEdges
          val innerBackEdges = innerEdges intersect backEdges
          val incomingEdges = targetEdges diff innerEdges
          val outgoingEdges = sourceEdges diff innerEdges

          if (innerBackEdges.nonEmpty) {
            val remainingEdges = innerEdges -- backEdges

            val nodesCopies =
              (for (i <- 1 to 4) yield {
                scc.map(x => g.eventMap(x).copy(id = copy(x, i)))
              }).flatten

            val insideCopies =
              (for (i <- 1 to 4) yield {
                for (edge <- remainingEdges) yield edge.copy(sourceID = copy(edge.sourceID, i), targetID = copy(edge.targetID, i))
              }).flatten

            val enteringCopies =
              for (edge <- incomingEdges) yield edge.copy(sourceID = edge.sourceID, targetID = copy(edge.targetID, 1))

            val exitingCopies =
              (for (i <- 1 to 4) yield {
                for (edge <- outgoingEdges) yield edge.copy(sourceID = copy(edge.sourceID, i), targetID = edge.targetID)
              }).flatten

            // Special case for infinite loops. Here, the last copy gets an edge to the exit of t
            val specialEdges =
              if (outgoingEdges.isEmpty) {
                for (back1 <- innerBackEdges.map(_.sourceID)) yield {
                  PoEdge(copy(back1, 4), g.sink(txn)) // Remove invariant, all combinations of source/target
                }
              } else Nil

            val betweenCopies =
              (for (i <- 1 to 3) yield {
                (for (back1 <- innerBackEdges.map(_.sourceID)) yield {
                  for (back2 <- innerBackEdges.map(_.targetID)) yield {
                    PoEdge(copy(back1, i), copy(back2, i + 1)) // Remove invariant, all combinations of source/target
                  }
                }).flatten
              }).flatten

            // filter double edges
            val poToAdd = {
              val origSet = (insideCopies ++ specialEdges ++ enteringCopies ++ exitingCopies ++ betweenCopies).toSet
              val doubledEdges = origSet.groupBy { x => (x.sourceID, x.targetID) }.values.filter(_.size > 1)
              var prunedSet: Set[PoEdge] = origSet
              for (s <- doubledEdges) {
                prunedSet = (prunedSet -- s) + s.head.copy(constraint = True)
              }
              prunedSet
            }

            return Some(g.extendAndReduce(
              eventsToAdd = nodesCopies.toList,
              poToAdd = poToAdd.toList,
              eventsToRemove = scc.map(g.eventMap(_)),
              poToRemove = sourceEdges ++ targetEdges))
          }

        }

      }

    }

    None

  }

  def unrollProgramOrder(g: AbstractHistory)(implicit param: Parameters): AbstractHistory = {

    assert(!g.events.exists(_.id.endsWith("_c1")))
    assert(!g.events.exists(_.id.endsWith("_c2")))
    assert(!g.events.exists(_.id.endsWith("_c3")))
    assert(!g.events.exists(_.id.endsWith("_c4")))

    if (param.debug) {
      log_debug("Before unrolling " + DumpGraph.dumpToFile("withloops_" + Random.alphanumeric.take(8).mkString(""), g.toLabeledGraph))
    }

    // Compute fix-point over "unroll one SCC" operation
    val newG = {
      var curG = g
      var continue = false
      do {
        if (param.unrollOnlyTwice) {
          unrollOneSCCinPO2Copy(curG) match {
            case Some(x) => curG = x; continue = true
            case None => continue = false
          }
        } else {
          unrollOneSCCinPO4Copy(curG) match {
            case Some(x) => curG = x; continue = true
            case None => continue = false
          }
        }
      } while (continue)
      curG
    }

    if (param.debug) {
      log_debug("After unrolling " + DumpGraph.dumpToFile("withoutloops_" + Random.alphanumeric.take(8).mkString(""), newG.toLabeledGraph))
    }

    assert {
      newG.transactions forall {
        newG.programOrderView(_).isAcyclic
      }
    }

    newG

  }

  sealed trait Violation {
    def toSet: Set[TransactionID]
    val g: LabeledGraph[String, String]
    val name: String
  }

  final case class Violation2(c1: TransactionID, c2: TransactionID)
    (val g: LabeledGraph[String, String], val name: String) extends Violation {
    def toSet: Set[TransactionID] = Set(c1, c2)
  }

  final case class Violation3(c1a: TransactionID, c1b: TransactionID, c2: TransactionID)
    (val g: LabeledGraph[String, String], val name: String) extends Violation {
    def toSet: Set[TransactionID] = Set(c1a, c1b, c2)
  }

  final case class Violation4(c1a: TransactionID, c1b: TransactionID, c2a: TransactionID, c2b: TransactionID)
    (val g: LabeledGraph[String, String], val name: String) extends Violation {
    def toSet: Set[TransactionID] = Set(c1a, c1b, c2a, c2b)
  }

  object DummyLabeledGraph extends LabeledGraph[String, String]

  case class ViolationSet(set: Set[Violation] = Set.empty, generalizesToArbitraryClients: Boolean = false) {

    def intersect(other: ViolationSet): ViolationSet =
      this.copy(set intersect other.set)

    def --(other: ViolationSet): ViolationSet = {
      this.copy(set -- other.set)
    }

    def hasSubViolation(a: ClientTransition, b: TransactionID)(implicit param: Parameters): Boolean = {
      a match {
        case SingleClientTransition(x) => hasSubViolation(x, b)
        case DoubleClientTransition(x, y) => hasSubViolation((x, y), b)
      }
    }

    def hasSubViolation(ds: Set[TransactionID])(implicit param: Parameters): Boolean = {
      assert(param.useStaticSubsetMinimality)
      set.exists(x =>
        x.toSet.subsetOf(ds)
      )
    }

    def hasSubViolation(a: TransactionID, b: TransactionID)(implicit param: Parameters): Boolean =
      if (param.useStaticSubsetMinimality) hasSubViolation(Set(a, b))
      else {
        set.contains(Violation2(a, b)(DummyLabeledGraph, "")) ||
          set.contains(Violation2(b, a)(DummyLabeledGraph, ""))
      }

    def hasSubViolation(a: (TransactionID, TransactionID), b: TransactionID)(implicit param: Parameters): Boolean =
      if (param.useStaticSubsetMinimality) hasSubViolation(Set(a._1, a._2, b))
      else {
        set.contains(Violation3(a._1, a._2, b)(DummyLabeledGraph, "")) ||
          hasSubViolation(a._1, b) || hasSubViolation(a._2, b)
      }

    def hasSubViolation(a: (TransactionID, TransactionID), b: (TransactionID, TransactionID))(implicit param: Parameters): Boolean =
      if (param.useStaticSubsetMinimality) hasSubViolation(Set(a._1, a._2, b._1, b._2))
      else {
        set.contains(Violation4(a._1, a._2, b._1, b._2)(DummyLabeledGraph, "")) ||
          set.contains(Violation4(b._1, b._2, a._1, a._2)(DummyLabeledGraph, "")) ||
          hasSubViolation(a, b._1) || hasSubViolation(a, b._2) || hasSubViolation(b, a._1) || hasSubViolation(b, a._2)
      }

    def +(a: Violation): ViolationSet = {
      this.copy(set + a)
    }

    def ++(a: List[Violation]): ViolationSet = {
      this.copy(set ++ a)
    }

    def size: Int = set.size

    def isEmpty: Boolean = set.isEmpty

    def nonEmpty: Boolean = set.nonEmpty

    def numberOfPairs: Int = set.collect { case x: Violation2 => x }.size

    def numberOfTriples: Int = set.collect { case x: Violation3 => x }.size

    def numberOfQuadruples: Int = set.collect { case x: Violation4 => x }.size

  }

}

