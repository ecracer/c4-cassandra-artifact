/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package ch.ethz.inf.pm.cfour.output

import ch.ethz.inf.pm.cfour.Parameters

object Statistics extends Logger {

  var possibleViolationsSize2 = 0
  var possibleViolationsSize3 = 0
  var possibleViolationsSize4 = 0

  var verifiedViolationsSize2 = 0
  var verifiedViolationsSize3 = 0
  var verifiedViolationsSize4 = 0

  var totalPossibleViolationsSize2 = 0
  var totalVerifiedViolationsSize2 = 0
  var totalPossibleViolationsSize3 = 0
  var totalVerifiedViolationsSize3 = 0
  var totalPossibleViolationsSize4 = 0
  var totalVerifiedViolationsSize4 = 0

  var numberSMTQueries = 0

  def reset(): Unit = {

    numberSMTQueries = 0
    possibleViolationsSize2 = 0
    possibleViolationsSize3 = 0
    possibleViolationsSize4 = 0
    verifiedViolationsSize2 = 0
    verifiedViolationsSize3 = 0
    verifiedViolationsSize4 = 0
    totalPossibleViolationsSize4 = 0
    totalVerifiedViolationsSize4 = 0
  }

  def dump()(implicit param: Parameters): Unit = {
    //    log_info(s"Violations size 2/polynomial check: $possibleViolationsSize2", Some(possibleViolationsSize2.toString))
    //    log_info(s"Violations size 2/smt check:        $verifiedViolationsSize2", Some(verifiedViolationsSize2.toString))
    //    log_info(s"Violations size 3/polynomial check: $possibleViolationsSize3", Some(possibleViolationsSize3.toString))
    //    log_info(s"Violations size 3/smt check:        $verifiedViolationsSize3", Some(verifiedViolationsSize3.toString))
    //    log_info(s"Violations size 4/polynomial check: $possibleViolationsSize4", Some(possibleViolationsSize4.toString))
    //    log_info(s"Violations size 4/smt check:        $verifiedViolationsSize4", Some(verifiedViolationsSize4.toString))
    val all = verifiedViolationsSize2 + verifiedViolationsSize3 + verifiedViolationsSize4
    log_info(s"All violations: $all", Some(all.toString))
    //    log_info(s"Total Violations size 4/polynomial check: $totalPossibleViolationsSize4", Some(totalPossibleViolationsSize4.toString))
    //    log_info(s"Total Violations size 4/smt check:        $totalVerifiedViolationsSize4", Some(totalVerifiedViolationsSize4.toString))
    //    log_info(s"Required $numberSMTQueries queries", Some(numberSMTQueries.toString))
  }
}
