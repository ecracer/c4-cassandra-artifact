/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package ch.ethz.inf.pm.cfour.utils

import scala.collection.mutable

/**
  * Various graph algorithms
  *
  * @author Lucas Brutschy
  */
trait AbstractGraph[T] {

  def hasEdge(u: T, v: T): Boolean = successors(u).toSet.contains(v)

  def hasSingleSource: Boolean = sources.size == 1

  def hasSingleSink: Boolean = sinks.size == 1

  def isAcyclic: Boolean = scc.forall(x => x.size == 1 && !successors(x.head).exists(_ == x))

  def toSimpleGraph: SimpleGraph[T] = SimpleGraph(vertices.map(x => (x, successors(x).toSet)).toMap)

  def vertices: Iterable[T]

  def successors(n: T): Iterable[T]

  def sources: Iterable[T] = vertices.toSet -- vertices.flatMap(successors)

  def sinks: Iterable[T] = vertices.filter(successors(_).isEmpty)

  def isConnected: Boolean = (vertices.toSet -- sources.flatMap(x => dfs(x) + x)).isEmpty

  lazy val edgeList: Iterable[(T, T)] = vertices.flatMap { x => successors(x).map((x, _)) }

  def isTransitivelyClosed: Boolean = {
    val edgeSet = edgeList.toSet
    for ((u, v) <- edgeList; (w, x) <- edgeList if v == w) {
      if (!edgeSet.contains((u, x))) return false
    }
    true
  }

  def bfs(q: T, i: Option[Int]): Set[T] = {

    val queue = mutable.Queue.empty[(T, Int)]
    queue.enqueue((q, 0))
    val visited = mutable.Set.empty[T]
    visited.add(q)

    while (queue.nonEmpty) {
      val (a, b) = queue.dequeue()
      for (x <- this.successors(a)) {
        if (!visited.contains(x)) {
          visited.add(x)
          if (i.forall(b + 1 < _)) {
            queue.enqueue((x, b + 1))
          }
        }
      }
    }

    visited.toSet
  }

  def sccAndDFSTree: (List[Set[T]], Map[T, Int], Map[T, Int]) = {
    val allVertices: Iterable[T] = this.vertices

    var index = -1
    var indices: Map[T, Int] = Map.empty
    var finish: Map[T, Int] = Map.empty[T, Int]
    var lowLinks: Map[T, Int] = Map.empty
    var components: List[Set[T]] = Nil
    var s: List[T] = Nil

    def strongConnect(v: T) {
      index += 1
      indices = indices + (v -> index)
      lowLinks = lowLinks + (v -> index)
      s = v :: s

      for (w <- this.successors(v)) {
        if (!indices.isDefinedAt(w)) {
          strongConnect(w)
          lowLinks = lowLinks + (v -> (lowLinks(v) min lowLinks(w)))
        } else if (s.contains(w)) {
          lowLinks = lowLinks + (v -> (lowLinks(v) min indices(w)))
        }
      }

      index += 1
      finish += (v -> index)

      if (lowLinks(v) == indices(v)) {
        var c: Set[T] = Set.empty
        var stop = false
        do {
          val x :: xs = s
          c = c + x
          s = xs
          stop = x == v
        } while (!stop)
        components = c :: components
      }
    }

    for (v <- sources.toList ::: allVertices.toList) {
      if (!indices.isDefinedAt(v)) {
        strongConnect(v)
      }
    }

    (components, indices, finish)
  }

  def dfsTree(s: T): (Map[T, Int], Map[T, Int]) = {

    val discovery: mutable.HashMap[T, Int] = mutable.HashMap.empty[T, Int]
    val finish: mutable.HashMap[T, Int] = mutable.HashMap.empty[T, Int]
    var time: Int = -1

    rec(s)

    def rec(v: T) {
      time = time + 1
      discovery += (v -> time)

      for (w <- this.successors(v)) {
        if (!discovery.contains(w)) {
          rec(w)
        }
      }

      time = time + 1
      finish += (v -> time)
    }

    (discovery.toMap, finish.toMap)
  }

  def dfs(s: T): Set[T] = {

    val marked: mutable.HashSet[T] = mutable.HashSet.empty[T]
    dfs(s)

    def dfs(v: T) {
      for (w <- this.successors(v)) {
        if (!marked.contains(w)) {
          marked.add(w)
          dfs(w)
        }
      }
    }

    marked.toSet

  }

  /** From: https://github.com/epfl-lara/leon/blob/master/src/main/scala/leon/utils/SCC.scala#L6 */
  def scc: List[Set[T]] = {
    val allVertices: Iterable[T] = this.vertices

    var index = 0
    var indices: Map[T, Int] = Map.empty
    var lowLinks: Map[T, Int] = Map.empty
    var components: List[Set[T]] = Nil
    var s: List[T] = Nil

    def strongConnect(v: T) {
      indices = indices + (v -> index)
      lowLinks = lowLinks + (v -> index)
      index += 1
      s = v :: s

      for (w <- this.successors(v)) {
        if (!indices.isDefinedAt(w)) {
          strongConnect(w)
          lowLinks = lowLinks + (v -> (lowLinks(v) min lowLinks(w)))
        } else if (s.contains(w)) {
          lowLinks = lowLinks + (v -> (lowLinks(v) min indices(w)))
        }
      }

      if (lowLinks(v) == indices(v)) {
        var c: Set[T] = Set.empty
        var stop = false
        do {
          val x :: xs = s
          c = c + x
          s = xs
          stop = x == v
        } while (!stop)
        components = c :: components
      }
    }

    for (v <- allVertices) {
      if (!indices.isDefinedAt(v)) {
        strongConnect(v)
      }
    }

    components
  }


}

object SimpleGraph {

  def apply[T](): SimpleGraph[T] = {
    SimpleGraph[T](Map.empty[T, Set[T]])
  }
  def apply[T](edges: Map[T, Set[T]], vertices: Set[T]): SimpleGraph[T] = {
    SimpleGraph[T](edges ++ (vertices -- edges.keySet).map((_, Set.empty[T])))
  }

}

/**
  * A simple graph implementation with transitive closure
  *
  * @author Lucas Brutschy
  */
case class SimpleGraph[T](edges: Map[T, Set[T]]) extends AbstractGraph[T] {

  assert(edges.values.flatten.toSet.subsetOf(edges.keySet))

  def vertices: Iterable[T] = edges.keys

  def successors(s: T): Set[T] = edges.getOrElse(s, Set.empty)

  def predecessors(s: T): Iterable[T] = edges.filter(_._2.contains(s)).keys

  def +(a: T): SimpleGraph[T] = SimpleGraph[T](edges = edges + (a -> edges.getOrElse(a, Set.empty)))

  def +(a: (T, T)): SimpleGraph[T] = SimpleGraph[T](
    edges = edges +
      (a._1 -> (edges.getOrElse(a._1, Set.empty) + a._2)) +
      (a._2 -> edges.getOrElse(a._2, Set.empty)))

  def ++(a: (T, Set[T])): SimpleGraph[T] = SimpleGraph[T](edges = edges + a)

  def -(a: T): SimpleGraph[T] = SimpleGraph[T](edges = (edges - a).mapValues(_ - a))

  def ++(a: Iterable[(T, T)]): SimpleGraph[T] = SimpleGraph[T](edges = a.foldLeft(edges) {
    case (x, y) => x + (y._1 -> (x.getOrElse(y._1, Set.empty) + y._2))
  })

  //noinspection MapFlatten
  def edgeSet: Set[(T, T)] = edges.view.map(x => x._2.map((x._1, _))).flatten.toSet

  def verticesReachableFrom(u: T): Set[T] = this.dfs(u) + u

  def verticesReachingTo(u: T): Set[T] = this.invert.dfs(u) + u

  def invert: SimpleGraph[T] = SimpleGraph[T](edges.values.toSet.flatten.map(v => (v, edges.keySet.filter(edges(_)(v)))).toMap, vertexSet)

  def projectToReachable(u: T): SimpleGraph[T] = project(this.dfs(u) + u)

  def project(u: Set[T]): SimpleGraph[T] = this -- (this.vertexSet -- u)

  def vertexSet: Set[T] = edges.keySet

  def --(a: Set[T]): SimpleGraph[T] = SimpleGraph[T](edges = (edges -- a).mapValues(_ -- a))

  def --(other: SimpleGraph[T]): SimpleGraph[T] = SimpleGraph[T](edges.keySet.map(x => x -> (edges(x) -- other.edges.getOrElse(x, Set.empty))).toMap)

  def transitiveReduction: SimpleGraph[T] = {
    transitiveClosureAndReduction._2
  }

  def transitiveClosureAndReduction: (SimpleGraph[T], SimpleGraph[T]) = {
    val tc = transitiveClosure
    (tc, transitiveReductionFromClosure(tc))
  }

  def transitiveReductionFromClosure(tc: SimpleGraph[T]): SimpleGraph[T] = {
    var red = this
    for ((a, b) <- this.edgeSet) {
      red = red ++ (a -> (red.successors(a) -- tc.successors(b)))
    }
    red
  }

  def transitiveClosure: SimpleGraph[T] = {
    SimpleGraph[T]((for (v <- this.edges.keys) yield {
      v -> this.dfs(v)
    }).toMap, this.vertexSet)
  }

}