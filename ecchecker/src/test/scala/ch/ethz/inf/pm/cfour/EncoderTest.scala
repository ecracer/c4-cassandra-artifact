/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.ExprBuilder._
import org.scalatest.{FunSuite, Matchers}

class EncoderTest extends FunSuite with Matchers with TestSettings {

  implicit val param: Parameters = testParameters

  val dekkerRW =
    AbstractHistory(
      events = List(
        Event("u1", "u1", "wx", Equal(ArgLeft(0, IntSort), IntConst(1))),
        Event("u2", "u2", "wy", Equal(ArgLeft(0, IntSort), IntConst(1))),
        Event("q1", "q1", "ry"),
        Event("q2", "q2", "rx")
      ),
      transactionOrder = List(
        ToEdge("u1", "q1"),
        ToEdge("u2", "q2")
      ),
      programOrder = Nil,
      system = SystemSpecification.ReadWriteRegister
    )

  val dekkerMap =
    AbstractHistory(
      events = List(
        Event("u1", "u1", "put"),
        Event("u2", "u2", "put"),
        Event("q1", "q1", "get"),
        Event("q2", "q2", "get")
      ),
      transactionOrder = List(
        ToEdge("u1", "q1"),
        ToEdge("u2", "q2")
      ),
      programOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap
    )

  test("Dekker R/W") {
    assert(Encoder.findViolations(dekkerRW).isDefined)
  }

  test("Global constraint used") {
    val x = dekkerRW.copy(globalConstraint = False)
    assert(Encoder.findViolations(x).isEmpty)
  }

  test("Event constraints used") {
    val x = dekkerRW.copy(events = dekkerRW.events.head.copy(constraint = False) :: dekkerRW.events.tail)
    assert(Encoder.findViolations(x).isEmpty)
  }

  test("Single read, single write") {
    val x = AbstractHistory(
      events = List(
        Event("u", "u", "put"),
        Event("q", "q", "get")
      ),
      transactionOrder = List(
        ToEdge("u", "q")
      ),
      programOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap
    )

    assert(Encoder.findViolations(x).isEmpty)
  }

  test("Dekker Map unconstrained") {
    assert(Encoder.findViolations(dekkerMap).isDefined)
  }

  test("Dekker Map with event constraints") {
    val x = dekkerMap.copy(
      events = List(
        Event("u1", "u1", "put", And(Equal(ArgLeft(0, StringSort), StringConst("x")), Equal(ArgLeft(1, IntSort), IntConst(1)))),
        Event("u2", "u2", "put", And(Equal(ArgLeft(0, StringSort), StringConst("y")), Equal(ArgLeft(1, IntSort), IntConst(1)))),
        Event("q1", "q1", "get", Equal(ArgLeft(0, StringSort), StringConst("y"))),
        Event("q2", "q2", "get", Equal(ArgLeft(0, StringSort), StringConst("x")))
      )
    )

    assert(Encoder.findViolations(x).isDefined)
  }

  test("Dekker Map on same key with event constraints") {
    val x = dekkerMap.copy(
      events = List(
        Event("u1", "u1", "put", And(Equal(ArgLeft(0, StringSort), StringConst("x")), Equal(ArgLeft(1, IntSort), IntConst(1)))),
        Event("u2", "u2", "put", And(Equal(ArgLeft(0, StringSort), StringConst("x")), Equal(ArgLeft(1, IntSort), IntConst(1)))),
        Event("q1", "q1", "get", Equal(ArgLeft(0, StringSort), StringConst("x"))),
        Event("q2", "q2", "get", Equal(ArgLeft(0, StringSort), StringConst("x")))
      )
    )

    assert(Encoder.findViolations(x).isEmpty)
  }

  test("Dekker Map on same key with global constraints") {
    val x = dekkerMap.copy(
      events = List(
        Event("u1", "u1", "put", Equal(ArgLeft(0, StringSort), GlobalVar("Left", StringSort))),
        Event("u2", "u2", "put", Equal(ArgLeft(0, StringSort), GlobalVar("Right", StringSort))),
        Event("q1", "q1", "get", Equal(ArgLeft(0, StringSort), GlobalVar("Left", StringSort))),
        Event("q2", "q2", "get", Equal(ArgLeft(0, StringSort), GlobalVar("Right", StringSort)))
      ),
      globalConstraint = Equal(GlobalVar("Left", StringSort), GlobalVar("Right", StringSort))
    )
    assert(Encoder.findViolations(x).isEmpty)
  }

  test("Dekker Map on different keys with global constraints") {
    val x = dekkerMap.copy(
      events = List(
        Event("u1", "u1", "put", Equal(ArgLeft(0, StringSort), GlobalVar("Left", StringSort))),
        Event("u2", "u2", "put", Equal(ArgLeft(0, StringSort), GlobalVar("Right", StringSort))),
        Event("q1", "q1", "get", Equal(ArgLeft(0, StringSort), GlobalVar("Right", StringSort))),
        Event("q2", "q2", "get", Equal(ArgLeft(0, StringSort), GlobalVar("Left", StringSort)))
      ),
      globalConstraint = Unequal(GlobalVar("Left", StringSort), GlobalVar("Right", StringSort))
    )
    assert(Encoder.findViolations(x).isDefined)
  }

  test("Two transactions") {
    val x = AbstractHistory(
      events = List(
        Event("u1", "t1", "put"),
        Event("u2", "t2", "put"),
        Event("q1", "t1", "get"),
        Event("q2", "t2", "get")
      ),
      programOrder = List(
        PoEdge("u1", "q1"),
        PoEdge("u2", "q2")
      ),
      transactionOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap
    )

    assert(Encoder.findViolations(x).nonEmpty)
  }

  test("Local store single transaction") {
    val x = AbstractHistory(
      events = List(
        Event("u1", "t1", "put"),
        Event("u2", "t2", "put"),
        Event("q1", "t1", "get"),
        Event("q2", "t2", "get")
      ),
      transactionOrder = Nil,
      programOrder = List(
        PoEdge("u1", "q1", Equal(ArgLeft(0, StringSort), ArgRight(0, StringSort))),
        PoEdge("u2", "q2", Equal(ArgLeft(0, StringSort), ArgRight(0, StringSort)))
      ),
      system = SystemSpecification.PutGetIncClearMap
    )

    assert(Encoder.findViolations(x).isEmpty)
  }

  test("JSON export/import") {

    val x =
      AbstractHistory(
        events = List(
          Event("u1", "t1", "put", Equal(ArgLeft(0, StringSort), StringConst("a"))),
          Event("u2", "t2", "put", Equal(ArgLeft(0, StringSort), StringConst("b"))),
          Event("q1", "t1", "get", Equal(ArgLeft(0, StringSort), StringConst("c"))),
          Event("q2", "t2", "get", Equal(ArgLeft(0, StringSort), StringConst("d")))
        ),
        transactionOrder = Nil,
        programOrder = List(
          PoEdge("u1", "q1", Equal(ArgLeft(0, StringSort), ArgRight(0, StringSort))),
          PoEdge("u2", "q2", Equal(ArgLeft(0, StringSort), ArgRight(0, StringSort)))
        ),
        system = SystemSpecification.PutGetIncClearMap
      )

    val json = x.toJSON
    val y = AbstractHistory.fromJSON(json)
    x.toString should equal(y.toString)

  }


  test("Two Updates One Query Zero Information") {
    val x =
      AbstractHistory(
        events = List(
          Event("u1", "t1", "put"),
          Event("u2", "t2", "put"),
          Event("q1", "t2", "get")
        ),
        programOrder = List(
          PoEdge("u2", "q1")
        ),
        transactionOrder = Nil,
        system = SystemSpecification.PutGetIncClearMap
      )

    assert(Encoder.findViolations(x).isEmpty)
  }


  test("Two Updates One Query Unequal") {
    val x =
      AbstractHistory(
        events = List(
          Event("u1", "t1", "put"),
          Event("u2", "t2", "put"),
          Event("q1", "t2", "get")
        ),
        programOrder = List(
          PoEdge("u2", "q1", Unequal(ArgLeft(0, StringSort), ArgRight(0, StringSort)))
        ),
        transactionOrder = Nil,
        system = SystemSpecification.PutGetIncClearMap
      )

    assert(Encoder.findViolations(x).isEmpty)
  }

    
  test("From file") {

    val g = AbstractHistory.fromResource("putputgetget.json")
    Encoder.findViolations(g)

  }

  test("Local store name bug") {
    val x = AbstractHistory(
      events = List(
        Event("u_0", "u_0", "put"),
        Event("q_0", "q_0", "get"),
        Event("u_1", "u_1", "put"),
        Event("q_1", "q_1", "get")
      ),
      transactionOrder = List(
        ToEdge("u_0", "q_0"),
        ToEdge("q_1", "u_1")
      ),
      programOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap
    )

    assert(Encoder.findViolations(x).isEmpty)

    val y = AbstractHistory(
      events = List(
        Event("u_0", "u_0", "put"),
        Event("q_0", "q_0", "get"),
        Event("u_1", "u_1", "put"),
        Event("q_1", "q_1", "get")
      ),
      transactionOrder = List(
        ToEdge("u_1", "q_1"),
        ToEdge("q_0", "u_0")
      ),
      programOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap
    )

    assert(Encoder.findViolations(y).isEmpty)
  }

  test("Query-specific absorption") {
    val x = AbstractHistory(
      events = List(
        Event("u_0", "t0", "clear"),
        Event("u_1", "t1", "put"),
        Event("q_1", "t1", "get")
      ),
      transactionOrder = Nil,
      programOrder = List(
        PoEdge("u_1", "q_1", ArgLeft(0, StringSort) equal ArgRight(0, StringSort))
      ),
      system = SystemSpecification.PutGetIncClearMap
    )
    assert(Encoder.findViolations(x)(testParameters.copy(encodeQuerySpecificAbsorption = false)).nonEmpty)
    assert(Encoder.findViolations(x)(testParameters.copy(encodeQuerySpecificAbsorption = true)).isEmpty)
  }

  test("U/Q Asymmetric commutativity") {
    val x = AbstractHistory(
      events = List(
        Event("u_0", "t0", "put", (ArgLeft(0, StringSort) equal StringConst("a")) and (ArgLeft(1, IntSort) equal IntConst(6))),
        Event("q_1", "t1", "get", (ArgLeft(0, StringSort) equal StringConst("a")) and (ArgLeft(1, IntSort) equal IntConst(6))),
        Event("u_1", "t1", "inc", ArgLeft(0, StringSort) equal StringConst("a"))
      ),
      transactionOrder = Nil,
      programOrder = List(
        PoEdge("q_1", "u_1")
      ),
      system = SystemSpecification.PutGetIncClearMap
    )
    assert(Encoder.findViolations(x)(testParameters.copy(encodeAsymmetricCommutativity = false)).nonEmpty)
    assert(Encoder.findViolations(x)(testParameters.copy(encodeAsymmetricCommutativity = true)).isEmpty)
  }

  test("U/U Asymmetric commutativity") {
    val x = AbstractHistory(
      events = List(
        Event("u_0", "t0", "put", ArgLeft(0, StringSort) equal StringConst("a")),
        Event("u_1", "t1", "put_if_absent", ArgLeft(0, StringSort) equal StringConst("a")),
        Event("q_1", "t1", "get", ArgLeft(0, StringSort) equal StringConst("a"))
      ),
      transactionOrder = Nil,
      programOrder = List(
        PoEdge("u_1", "q_1")
      ),
      system = SystemSpecification.PutGetIncClearMap
    )
    assert(Encoder.findViolations(x)(testParameters.copy(encodeAsymmetricCommutativity = false)).nonEmpty)
    assert(Encoder.findViolations(x)(testParameters.copy(encodeAsymmetricCommutativity = true)).isEmpty)
  }

}
