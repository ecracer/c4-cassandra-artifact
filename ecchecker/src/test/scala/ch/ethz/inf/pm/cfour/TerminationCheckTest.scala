/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.output.Statistics
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}

class TerminationCheckTest extends FunSuite with Matchers with TestSettings with BeforeAndAfter {

  before {
    Statistics.reset()
  }

  test("Three roles problem") {

    assert(!check(TestPrograms.threeRolesProblem))

  }

  test("Single dekker client") {

    assert(check(TestPrograms.singleDekkerClient))

  }

  test("Path sensitive 4 violation") {

    assert(check(TestPrograms.pathSensitive4Violation))

  }

  test("Path sensitive no violation") {

    assert(check(TestPrograms.pathSensitiveNoViolation))

  }

  test("Path sensitive violation") {

    assert(check(TestPrograms.pathSensitiveViolation))

  }

  def check(g: AbstractHistory): Boolean = {
    Unfolder.unfoldAndCheck(g)(testParameters.copy(resultGeneralizationCheck = true)).generalizesToArbitraryClients
  }
}
