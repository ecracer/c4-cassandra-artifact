/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package ch.ethz.inf.pm.cfour

import java.io.File

trait TestSettings {

  lazy val testParameters: Parameters = {

    val param = Parameters.default.copy(debug = true, outputViaPrintln = true)
    param.exportDirectory.foreach { x =>
      val d = new File(x)
      if (!d.exists()) d.mkdir()
    }
    Parameters.set(param)

  }

  def getResourceGraph(dir: String, name: String): AbstractHistory = {
    AbstractHistory.fromResource(s"$dir/$name.json")
  }

}
