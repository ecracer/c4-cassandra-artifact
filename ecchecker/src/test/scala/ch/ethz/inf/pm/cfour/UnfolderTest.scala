/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package ch.ethz.inf.pm.cfour

import ch.ethz.inf.pm.cfour.ExprBuilder._
import ch.ethz.inf.pm.cfour.output.Statistics
import org.scalatest.{BeforeAndAfter, FunSuite}

class UnfolderTest extends FunSuite with TestSettings with BeforeAndAfter {

  implicit val param: Parameters = testParameters

  before {
    Statistics.reset()
  }

  test("Unfolded dekker unconstrained with Entry/Exit") {
    val x = Unfolder.unfoldAndCheck(TestPrograms.singleDekkerClientWithEntryExit)
    assert(x.size == 1)
    assert(Statistics.numberSMTQueries == 2)
  }

  test("Unfolded dekker unconstrained") {
    val x = Unfolder.unfoldAndCheck(TestPrograms.singleDekkerClient)
    assert(x.size == 1)
    assert(Statistics.numberSMTQueries == 2)
  }

  test("Unfolded local store one transaction") {
    val x = Unfolder.unfoldAndCheck(TestPrograms.singleLocalStoreOneTransaction)
    assert(x.isEmpty)
    assert(Statistics.numberSMTQueries == 1)
  }

  test("Single transaction storing and loading a value") {
    val x = Unfolder.unfoldAndCheck(TestPrograms.allSessionOnSameValue)
    assert(x.isEmpty)
    assert(Statistics.numberSMTQueries == 0)
  }

  test("Unfolded dekker with client relational constraints") {
    val x = Unfolder.unfoldAndCheck(TestPrograms.singleDekkerClientWithClientLocalConstraint)
    assert(x.isEmpty)
    assert(Statistics.numberSMTQueries == 2)

    val y = Unfolder.unfoldAndCheck(TestPrograms.singleDekkerClientWithClientLocalConstraint.copy(globalConstraint = True))
    assert(y.size == 1)
    assert(Statistics.numberSMTQueries == 2)
  }

  test("Unfolded local store two transactions") {
    val x = Unfolder.unfoldAndCheck(TestPrograms.singleLocalStoreClient)
    assert(x.isEmpty)
    assert(Statistics.numberSMTQueries == 2)
  }

  test("Loop in program order") {
    Unfolder.unrollProgramOrder(TestPrograms.loopInProgramOrder)
  }


  test("Complex loop in program order") {
    Unfolder.unrollProgramOrder(TestPrograms.complexLoopInProgramOrder)
  }

  test("Preferring 2 transaction violations over 4 transaction violations, and avoid permutations") {

    // We get 3 violations here. t1/t2, t1/t1, t2/t2
    val x = Unfolder.unfoldAndCheck(TestPrograms.dekkerSingleTransaction)
    assert(x.size == 2)
    assert(x.numberOfPairs == 2)
    assert(Statistics.numberSMTQueries == 2)

  }

  test("With 4 transactions, detect cycle between 3") {

    val x = Unfolder.unfoldAndCheck(TestPrograms.incPutGet)
    assert(x.size == 3)
    assert(x.numberOfTriples == 1)
    assert(x.numberOfQuadruples == 2)
    assert(Statistics.numberSMTQueries == 4)

  }

  test("Ignoring trivially commuting transactions") {

    val x = Unfolder.unfoldAndCheck(TestPrograms.withTrivialTransactions)
    assert(x.size == 1)
    assert(x.numberOfPairs == 1)
    assert(Statistics.numberSMTQueries == 1)

  }

  test("Select one path among two") {

    val res = Unfolder.unfoldAndCheck(TestPrograms.pathSensitiveNoViolation)
    assert(res.isEmpty)

  }

  test("Select one path among two, with violation") {

    val res = Unfolder.unfoldAndCheck(TestPrograms.pathSensitiveViolation)
    assert(res.nonEmpty)

  }

  test("Unfolding loops") {

    Unfolder.unfoldAndCheck(TestPrograms.withLoop)

  }

  test("While true loop") {

    Unfolder.unfoldAndCheck(TestPrograms.whileTrueLoop)
  }


  test("Fully connected") {

    Unfolder.unfoldAndCheck(TestPrograms.fullyConnected)

  }

  test("Playlist LoginServlet doPost") {

    val res = Unfolder.unfoldAndCheck(TestPrograms.playlistLoginServletPst)
    assert(res.isEmpty)

  }

  test("PO-Bug") {

    val graph1 =
      AbstractHistory(
        events = List(
          Event("entry", "t", "skip"),
          Event("q", "t", "get"),
          Event("u", "t", "put"),
          Event("exit", "t", "skip")
        ),
        programOrder = List(
          PoEdge("entry", "q"),
          PoEdge("q", "u"),
          PoEdge("u", "exit"),
          PoEdge("q", "exit")
        ),
        transactionOrder = List(ToEdge("t", "t")),
        system = SystemSpecification.PutGetIncClearMap
      )

    val res = Unfolder.unfoldAndCheck(graph1)
    assert(res.nonEmpty)

  }

  test("Monotone register") {

    // Assume we have no puts and no negative increments
    // Monotonicity!
    val nonMonotoneDekker =
    AbstractHistory(
      events = List(
        Event("qM1", "t1", "get", ArgLeft(0, StringSort) equal StringConst("lock")),
        Event("u", "t1", "inc", ArgLeft(0, StringSort) equal StringConst("value")),
        Event("qM2", "t2", "get", ArgLeft(0, StringSort) equal StringConst("lock")),
        Event("q", "t2", "get", ArgLeft(0, StringSort) equal StringConst("value"))
      ),
      programOrder = List(
        PoEdge("qM1", "u", ArgLeft(1, IntSort) equal IntConst(2)),
        PoEdge("qM2", "q", ArgLeft(1, IntSort) equal IntConst(1))
      ),
      transactionOrder = List(
        ToEdge("t1", "t2")
      ),
      system = SystemSpecification.PutGetIncClearMap
    )

    assert(Unfolder.unfoldAndCheck(nonMonotoneDekker).nonEmpty)

    assert(Unfolder.unfoldAndCheck(nonMonotoneDekker.copy(system = nonMonotoneDekker.system.copy(
      legalitySpec = Map(
        "get,get" -> ((ArgLeft(0, StringSort) equal ArgRight(0, StringSort)) implies (ArgLeft(1, IntSort) <= ArgRight(1, IntSort)))
      )
    ))).isEmpty)

  }

  test("Synchronized dekker") {

    assert(Unfolder.unfoldAndCheck(TestPrograms.singleDekkerClient).nonEmpty)

    val synchronized =
      TestPrograms.singleDekkerClient.copy(
        system =
          SystemSpecification.PutGetIncClearMap.copy(
            synchronizationSpec = Map(
              "put,get" -> (ArgLeft(0, StringSort) equal ArgRight(0, StringSort))
            )
          )
      )

    assert(Unfolder.unfoldAndCheck(synchronized).isEmpty)

  }

  test("Symmetric violations") {
    val x = AbstractHistory(
      events = List(
        Event("u", "t0", "inc"),
        Event("q1", "t1", "get"),
        Event("q2", "t2", "get")
      ),
      transactionOrder = List(
        ToEdge("t0", "t0"),
        ToEdge("t0", "t1"),
        ToEdge("t0", "t2"),
        ToEdge("t1", "t0"),
        ToEdge("t1", "t1"),
        ToEdge("t1", "t2"),
        ToEdge("t2", "t0"),
        ToEdge("t2", "t1"),
        ToEdge("t2", "t2")
      ),
      programOrder = Nil,
      system = SystemSpecification.PutGetIncClearMap
    )
    assert(2 == Unfolder.unfoldAndCheck(x).size)
  }

  test("Table Spec / Get all, no atomic sets") {
    assert(Unfolder.unfoldAndCheck(TestPrograms.tableAddAll).nonEmpty)
  }
  test("Table Spec / Exists, no atomic sets") {
    assert(Unfolder.unfoldAndCheck(TestPrograms.tableAddExists).nonEmpty)
  }
  test("Table Spec / Get all, atomic sets") {
    assert(Unfolder.unfoldAndCheck(TestPrograms.tableAddAll)(param.copy(encodeAtomicSet = true)).isEmpty)
  }
  test("Table Spec / Exists, atomic sets") {
    assert(Unfolder.unfoldAndCheck(TestPrograms.tableAddExists)(param.copy(encodeAtomicSet = true)).isEmpty)
  }

}
